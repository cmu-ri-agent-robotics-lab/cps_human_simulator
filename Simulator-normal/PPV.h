// test1.cpp : main project file.
// This is a test line
#pragma once
#include "stdafx.h"
#include "EKGAnalyzer.h"
#include "GEWaveformTools.h"
#include "Constants.h"

namespace PPV 
{
using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
//using namespace JANK;

public ref struct calcPack
{
	float PPV;
	int RR;
	int HR;
};

// Accepts an Array of Peaks and calculates the...stuff
calcPack^ calcPPV(ArrayList^ Peaks)
{
	calcPack^ CP=gcnew calcPack();
	CP->PPV=0;
	CP->RR=0;
	CP->HR=0;

	ArrayList^ total=gcnew ArrayList();
	ArrayList^ ret=gcnew ArrayList();
	ArrayList^ highbuff = gcnew ArrayList();
	ArrayList^ lowbuff = gcnew ArrayList();
	float X=0.0;
	int count=0;
	int PI,PIf,PIf2;
	// These two are used to tag top/bottom
	int high=0;
	int low=0;
	float min=0;
	int respiratoryrate=0;
	int heartrate=0;
	float max=0;
	int previoushigh=0;
	int previouslow=0;
	// Starter
	int highcount=-1;
	int lowcount=-1;
	int totalcount=0;
	int falling=0;
	int rising=0;
	int flagcount=0;
	int highflag=0;
	// We need to match each peak to it's low and vice-versa
	// First find a low;
	for (int i=0; i<Peaks->Count-2; i++) 
	{		
		PI = (int)Peaks->default[i];
		
		PIf =(int)Peaks->default[i+1];
		PIf2 =(int)Peaks->default[i+2];
		
		
		if (falling == 0) 
		{
			if ((PIf < PI) && (PIf2 < PI))
			{
						
				falling = 1;
				
			}
		}
		else if (rising==0)
		{
			
			
			if ((PIf > PI) && (PIf2 > PI)) 
			{
				
				if(i!=0 && highflag==1)
				{
					previoushigh= (int)highbuff->default[highcount];
					if (lowcount >= 0)
					{
						previouslow=(int)lowbuff->default[lowcount];
					}
					else 
					{
						previouslow=0;
					}
				}
				
				// We were falling, but the next two are higher, so this is the low
				low = PI;	
				
				// we need to check if the difference between this low and the previous high 
				// is greater than 15 if not greater then we should ignore this low value and replace it with previous low value and remove the peak entry
				if(previoushigh-low<=15 && highflag==1)
				{
					low=previouslow;
					totalcount=total->Count-1;
					if (totalcount >= 0)
					{
						total->RemoveAt(totalcount);
					}

					//wrong low value, cut it off else will interfere in future comparisions	
					if (lowbuff->Count > 0)
					{
						int lowtotalcount=lowbuff->Count-1;
						lowbuff->RemoveAt(lowtotalcount);
					}
					lowcount--;
				}
				rising=1; 
				flagcount=0;
		
			}
		}
		else
		{
			//if(PI		
			// We're rising here, so
			if ((PIf < PI) && (PIf2 < PI))
			{
				// The next two are lower, so this is the high
				high = PI;
			}
		}

		// Now a check for completion
		if (high > 0) 
		{
			// We've got a full set.  Whoopee!
			// Add it to the total and increment our counter
		
			int a=high-low; //Difference between high and low should be greater than 15
 			if(	a>15)	
			{
			total->Add(high-low);
			
			//store the high and low in buffer so that it could be used to compare with next low
			highbuff->Add(high);
			lowbuff->Add(low);
			//count of highs and lows currently ->starts from -1
			highcount++;
			lowcount++;
			//number of peaks in a minute
			heartrate++;
			//a high has been calculated
			highflag=1;
			}
			
			
			// reset for next cycle
			high=0;
			low=0;
			rising=0;
			falling=1;  // 1 is right; we already know we're falling now.
		}
	}
	
	//Console::WriteLine("**********->->************");
	// finding the min and max
	int count1=1;

	int falling1=0;
	int rising1=0;	
	
	// We need to match each peak to it's low and vice-versa
	// First find a low;
	for (int i=0; i<total->Count-1; i++) 
	{		
		PI = (int)total->default[i];
		 PIf =(int)total->default[i+1];		


		 if (rising1==0)
		{
			
			
			if ((PIf > PI)) 
			{
				// We were falling, this is the low
				min = float(PI);	
				rising1=1;
				
			}
		}
		else
		{
			
			// We're rising here, so
			if ((PIf < PI))
			{
				// this is the high
				max = float(PI);
			}
		}

		// Now a check for completion
		if (max > 0) 
		{
			// We've got a full set.  Whoopee!
			// The array of calculated values
		float ltemp=((max-min)*100)/((max+min)/2);
		ret->Add(ltemp);	
		//One cycle is over now
		respiratoryrate++;

			// reset for next cycle
			max=0;
			min=0;
			rising1=0;
			falling1=1;  // 1 is right; we already know we're falling now.
		}
	}
		
	//sorting the value to return and cutting off outliers -> it works, values are more finer now :-) 	
	ret->Sort();
	//	for(int i; i<ret->Count;i++)
		//Console::WriteLine(ret->default[i]);
	
	for(int i=3; i<ret->Count-3;i++)
	{
		X = X + (float)(ret->default[i]);
		count1++;
	}
	
	CP->HR = heartrate;
	CP->RR = respiratoryrate;
	if (count1 > 0) 
	{
		CP->PPV = (X/count1);
	}
	else 
	{
		CP->PPV = 0;
	}
	Console::WriteLine("Heart Rate is "+heartrate);
	Console::WriteLine("Respiratory Rate is "+respiratoryrate);

	return CP;
};

}