// (c) 2012 Sironis, Inc.
// This source code is provided as-is, without any 
// warrenty expressed or implied.

#pragma once

// Action encodings
#define ACTION_FLUID_BOLUS 1
#define ACTION_STOP_BOLUS 2
#define ACTION_PHENYLEPHRINE_BOLUS 3


#define CDSIM_ARRAY_LENGTH 40

public ref class CDSIM_Net_Pack 
{
public: array<unsigned char>^ NetPack;
private: bool DataParseOK;		// Tells us if input array was parsed successfully (not if data makes sense!)

public: CDSIM_Net_Pack(void)
		{
			DataParseOK = false;
			NetPack = gcnew array<unsigned char>(CDSIM_ARRAY_LENGTH);
			this->ZeroArray();
		}
public: CDSIM_Net_Pack(array<unsigned char>^ Message)
		{
			DataParseOK = true;
			NetPack = gcnew array<unsigned char>(CDSIM_ARRAY_LENGTH);
			this->ZeroArray();
			// We've been given a char array; so confirm it matches expected data and then copy
			// the whole thing into our internal message array.
			if (Message[0] != 1) {DataParseOK = false;}
			if (Message[1] != 254) {DataParseOK = false;}
			if (Message[2] != 2) {DataParseOK = false;}
			if (Message[3] != 253) {DataParseOK = false;}

			if (DataParseOK) 
			{
				for (int i=0; i<CDSIM_ARRAY_LENGTH; i++)
				{
					NetPack[i] = Message[i];
				}
			}
		}
public: CDSIM_Net_Pack(System::DateTime^ DT, System::String^ ID, bool turbo, int HR, int SBP, int DBP, int MAP, int SV, int SVR, int PPV, int SPO2, int BolusID, int VolGiven)
		{
			DataParseOK = false;
			NetPack = gcnew array<unsigned char>(CDSIM_ARRAY_LENGTH);
			this->ZeroArray();
			PreparePacket(DT,ID,turbo,HR,SBP,DBP,MAP,SV,SVR,PPV,SPO2,BolusID,VolGiven);
		}
public: bool MessageFrameOK(void)
		{
			return this->DataParseOK;
		}
public: void ZeroArray(void)
		{
			for (int i=0; i<CDSIM_ARRAY_LENGTH; i++)
			{
				NetPack[i] = 0;
			}
		}
public: void PreparePacket(System::DateTime^ DT, System::String^ ID, bool turbo, int HR, int SBP, int DBP, int MAP, int SV, int SVR, int PPV, int SPO2, int BolusID, int VolGiven)
		{
			// Pack all of this crapola into the unsigned char array packet.
			// Start Sequence
			NetPack[0] = 1;
			NetPack[1] = 254;
			NetPack[2] = 2;
			NetPack[3] = 253;

			NetPack[4] = turbo;
			
			// 6 chars reserved
			NetPack[5] = 0;
			NetPack[6] = 0;
			NetPack[7] = 0;
			NetPack[8] = 0;
			NetPack[9] = 0;
			
			// 3 chars for time
			NetPack[10] = DT->Hour;
			NetPack[11] = DT->Minute;
			NetPack[12] = DT->Second;

			// 20 chars for patient ID
			for (int i=0; i<16; i++)
			{
				if (i > ID->Length-1)
				{
					NetPack[13+i] = 0;
				}
				else
				{
					NetPack[13+i] = char(ID[i]);
				}
			}

			// Vitals
			NetPack[29] = char(HR);
			NetPack[30] = char(SBP);
			NetPack[31] = char(DBP);
			NetPack[32] = char(MAP);
			NetPack[33] = char(SV);
			NetPack[34] = char(PPV);
			NetPack[35] = char(SPO2);

			// Bolus Data
			NetPack[36] = char(BolusID);
			int X = (int(VolGiven/100));
			NetPack[37] = char(X);
			NetPack[38] = char(VolGiven - (X*100));
		}

public: int ExtractBolusID(void)
		{
			return int(NetPack[37]);
		}
		
public: bool IsTurboMode(void)
		{
			if (NetPack[4]) {return true;}
			else return false;
		}
public: int ExtractVolumeGivenThisBolus(void)
		{
			return int((NetPack[37]*100)+NetPack[38]);
		}
public: System::String^ ExtractTimeAsString(void)
		{
			System::String^ T = "";
			if (NetPack[10] < 10) {T += "0";}
			T += NetPack[10].ToString() + ":";
			if (NetPack[11] < 10) {T += "0";}
			T += NetPack[11].ToString() + ":";
			if (NetPack[12] < 10) {T += "0";}
			T += NetPack[12].ToString();
			return T;
		}
public: System::String^ ExtractPatientID(void)
		{
			System::String^ R = "";
			for (int i=13; i<29; i++)
			{
				if (NetPack[i] == 0) {i=1000;}
				else 
				{
					R += Convert::ToChar(NetPack[i]);
				}
			}
			return R;
		}
			
public: int ExtractHR(void)
		{
			return int(NetPack[29]);
		}
public: int ExtractSBP(void)
		{
			return int(NetPack[30]);
		}
public: int ExtractDBP(void)
		{
			return int(NetPack[31]);
		}
public: int ExtractMAP(void)
		{
			return int(NetPack[32]);
		}
public: int ExtractSV(void)
		{
			return int(NetPack[33]);
		}
public: int ExtractPPV(void)
		{
			return int(NetPack[34]);
		}
public: int ExtractSPO2(void)
		{
			return int(NetPack[35]);
		}
};


public ref class CDSIM_Net_Command 
{
public: array<unsigned char>^ Pack;
private: bool DataParseOK;		// Tells us if input array was parsed successfully (not if data makes sense!)

public: CDSIM_Net_Command(unsigned char CommandType, unsigned char arg1, unsigned char arg2)
		{
			DataParseOK = false;
			Pack = gcnew array<unsigned char>(5);
			Pack[0] = CommandType;
			Pack[1] = arg1;
			Pack[2] = arg2;
		}
public: bool MessageFrameOK(void)
		{
			return this->DataParseOK;
		}
public: bool Parse(array<unsigned char>^ A)
		{
			if (A->Length == 3)
			{
				Pack = A;
				DataParseOK = true;
				return true;
			}
			return false;
		}
};

// edit by Shehzi
public ref class CDSIM_TCP_Pack{
public: int^ HR, SBP, DBP, MAP, SV, SVR, PPV, SPO2;
		System::DateTime^ Date_Time;
public: CDSIM_TCP_Pack(void){
			this->HR = 0;
			this->Date_Time = System::DateTime::MinValue;

}
public: CDSIM_TCP_Pack(System::DateTime^ DT, int HR, int SBP, int DBP, int MAP, int SV, int SVR, int PPV, int SPO2){
			this->Date_Time = DT;
			this->HR = HR;
			this->SBP = SBP;
			this->DBP = DBP;
			this->MAP = MAP;
			this->SV = SV;
			this->SVR = SVR;
			this->PPV = PPV;
			this->SPO2 = SPO2;
}
};
// edit by Shehzi
