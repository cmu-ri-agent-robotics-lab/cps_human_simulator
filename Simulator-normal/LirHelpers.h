// LirHelpers.h
// Copyright 2011-2012 Sironis, Inc.

#pragma once

namespace LIR {

public ref struct ControlResponse
{
	System::String^ Item;
	int Value;
	System::String^ CharValue;
};


//-------------------------------------------------ALERTS
// Order determines priority of display
// must be packed in (no gaps in numerical sequence, 0-indexed)
#define DEFINED_NUMBER_OF_ALERTS 6

#define ALERT_PUMPERROR 0
#define ALERT_CALIBRATE 1
#define ALERT_DAMPENED 2
#define ALERT_TEST 3
#define ALERT_CAPTURE 4
#define ALERT_PUMPRESET 5
// UPDATE DEFINED # OF ALERTS ABOVE!

#define SURGERY_MODE_OPEN 0
#define SURGERY_MODE_LAPPRONE 1

public ref class ALERTS
{
public: array<bool>^ AlertFlags;	
	
public: ALERTS(void)
		{
			AlertFlags = gcnew array<bool>(DEFINED_NUMBER_OF_ALERTS);
			for (int i=0; i<DEFINED_NUMBER_OF_ALERTS; i++)
			{
				AlertFlags[i] = false;
			}
		}
};


};