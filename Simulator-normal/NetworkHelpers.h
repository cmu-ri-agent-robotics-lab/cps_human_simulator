// GEUnityNetwork.h
// Functions for communication with GE Healthcare Devices
// J. Rinehart 2011-2012

#pragma once

namespace NetworkHelpers
{
	using namespace System::Net::Sockets;

	// TCPDoubleToInt
	// Takes two bytes from TCP packet and converts to int value
	int TCPDoubleToInt(int V1,int V2) 
	{
		if (V1 > 128) {
			V1 = V1 - 256;
			}
		return ((V1*256) + V2);
	};

	public ref struct Device
	{
		System::String^ Name;
		System::String^ Address;
	};

	ref class MessPack
	{
	public: 
		int MessageSize;
		array<unsigned char>^ Message;

	public: 
		System::String^ MessageAsString(void)
		{
			System::String^ M = "";
			for (int i=0; i<Message->Length; i++)
			{
				M += System::Char::ConvertFromUtf32(Message[i]);
			}
			return M;
		}
	};
	
Socket^ createListener(System::String^ LocalHost, int ListenPort)
{
	Socket^ Uport = gcnew Socket(AddressFamily::InterNetwork,SocketType::Dgram,ProtocolType::Udp);
	System::Net::IPEndPoint^ localIP = gcnew System::Net::IPEndPoint(System::Net::IPAddress::Parse(LocalHost),ListenPort);
	Uport->Bind(localIP);
	return Uport;
}

MessPack^ sockListen(Socket^ udpguy) 
{
	MessPack^ MP = gcnew MessPack();
	MP->MessageSize = 0;

	// See if there's any data available on the socket; if not return empty mess pack
	// NOTE THAT MP->Message is UNITIALIZED, so checking for size is important before
	// accessing this value!
	
	if (udpguy->Available < 1) {return MP;}

	MP->MessageSize = udpguy->Available;
	MP->Message = gcnew array<unsigned char>(udpguy->Available);
	udpguy->Receive(MP->Message);	
	return MP;
}

int SendUnityRequest(System::String^ Address, array<System::Byte,1>^ sendBytes) 
{
	 int LocalPort=0;
	 System::Net::Sockets::UdpClient^ Uport;

	// Now send the message
    // Must specify the local port so we know what to return
	int tryPort = 61001;
	int attempts = 0;

	while ((LocalPort == 0)&&(attempts < 2000))
	{
		System::Net::Sockets::UdpClient^ TrySock = gcnew System::Net::Sockets::UdpClient(tryPort);
		try 
		{
			TrySock->Connect(System::Net::IPAddress::Parse(Address),2000); 
		}
		catch (System::Exception^ e)
		{
			//System::Windows::Forms::MessageBox::Show(e->Message);
			//return 0;
			e;
			tryPort++;
			attempts++;
		}
		if (TrySock)
		{
			LocalPort=tryPort;
			Uport=TrySock;
		}
	}
	
	if (LocalPort == 0) 
	{
		//MessageBox::Show("Unable to bind to any local port");
		return 0;
	}

	Uport->Send(sendBytes,sendBytes->Length);

	Uport->Close();
	delete Uport;
	return LocalPort;
}


};
