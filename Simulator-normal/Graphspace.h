// Graphspace.h
// Copyright 2012, all rights reserved
// J. Rinehart

// Note that graphs can be left to right or right to left, or upside-down
// even, if you reverse the edges in the region definition.
// you can define regions that extend off screen if you wish; the off-screen
// pieces simply won't display.


// define GRAPHSPACE_PANELMODE
// to change graphspaces from Forms to panels.

#pragma once

namespace Graph {

public ref class Graphspace{

private:
	int X1;  // Left edge
	int X2;  // Right edge
	int Y1;  // Top
	int Y2;  // Bottom

	int Width;
	int Height;

	int GraphMovementMode;

	double MinRange;
	double MaxRange;
	double RangeScale;
	double RangeZero;

	array<double>^ Values;

#ifdef GRAPHSPACE_PANELMODE
	System::Windows::Forms::Panel^ PARENT;
#else
	System::Windows::Forms::Form^ PARENT;
#endif
	System::Drawing::Color^ BGCOLOR;
	System::Drawing::Color^ FORECOLOR;
	System::Drawing::Color^ FRAMECOLOR;

	bool DrawBackgroundYN;
	bool LineMode;
	bool Looped;
	bool DrawFrameYN;
	bool AutoScaleMax;

	int CurrentValue; // this is a marker for the array, not an actual value
	
public: 
#ifdef GRAPHSPACE_PANELMODE
	Graphspace (System::Windows::Forms::Panel^ NewParent, System::Drawing::Color^ NewBGCOLOR, System::Drawing::Color^ ForeColor, int LeftEdge, int RightEdge, int Top, int Bottom, double MinimumRange, double MaximumRange, double RangeZeroValue, int ValueListLength)
#else
	Graphspace (System::Windows::Forms::Form^ NewParent, System::Drawing::Color^ NewBGCOLOR, System::Drawing::Color^ ForeColor, int LeftEdge, int RightEdge, int Top, int Bottom, double MinimumRange, double MaximumRange, double RangeZeroValue, int ValueListLength)
#endif
	{
		PARENT = NewParent;
		BGCOLOR = NewBGCOLOR;
		FORECOLOR = ForeColor;
		DrawFrameYN = false;
		Looped=false;
		DrawBackgroundYN=false;
		LineMode = false;
		Values = gcnew array<double>(ValueListLength);
		ClearValues();
		ChangeScale(MinimumRange,MaximumRange,RangeZeroValue);
		ChangeRegion(LeftEdge,RightEdge,Top,Bottom);
		AutoScaleMax=false;
		GraphMovementMode = 1;
	}

private:
	void DrawFrame(void)
	{
		System::Drawing::Graphics^ g = PARENT->CreateGraphics();
		System::Drawing::Pen^ p = gcnew System::Drawing::Pen(*FRAMECOLOR);
		g->DrawRectangle(p,X1,Y1,X2-X1,Y2-Y1);
		delete p;
		delete g;
	}

public:
	void SetAutoScaleMaxOff(void)
	{
		this->AutoScaleMax = false;
	}
	void SetAutoScaleMaxOn(void)
	{
		this->AutoScaleMax = true;
	}
	// Mode 1 - New values added at border, old values scroll away (default)
	// Mode 2 - New values added away from border
	void SetGraphMovementMode(int mode)
	{
		GraphMovementMode = mode;
	}
	void SetFrameDrawOn(System::Drawing::Color^ NewFrameColor)
	{
		this->DrawFrameYN = true;
		this->FRAMECOLOR = NewFrameColor;
	}
	void SetFrameDrawOff(void)
	{
		this->DrawFrameYN = false;
	}
	void SetBarGraphModeOn(void)
	{
		LineMode = true;
	}
	void SetBarGraphModeOff(void)
	{
		LineMode = false;
	}

	/// Places a vertical line on the graph
	/// <param name="ValueAsPercentageOfX1toX2">Where to place the reference line, expressed as a value from 0-1 with X1=0 and X2=1</param>
	/// <param name="lineStyle">0=Solid, 1=dashed, 2=coarse dotted, 3=fine dotted, 4-left-side wave, 5 right-side wave</param>
	void AddVerticalReferenceLine(System::Drawing::Color^ C, double ValueAsPercentageOfX1toX2, int lineStyle)
	{
		System::Drawing::Graphics^ g = PARENT->CreateGraphics();
		System::Drawing::Pen^ pena = gcnew System::Drawing::Pen(*C);
		int X = int((this->X2 - this->X1) * ValueAsPercentageOfX1toX2);

		try {
			if ((lineStyle==0)||(lineStyle==5)||(lineStyle==4))
			{
				g->DrawLine(pena,int(X1+X),int(Y2),int(X1+X),int(Y1));
			}
			
			if (lineStyle==1)
			{
				for (int i=Y1; i<Y2; i+=9)
				{
					g->DrawLine(pena,int(X1+X),int(i),int(X1+X),int(i+6));	
				}
			}
			else if (lineStyle==2)
			{
				for (int i=Y1; i<Y2; i+=4)
				{
					g->DrawLine(pena,int(X1+X),int(i),int(X1+X),int(i+1));	
				}
			}
			else if (lineStyle==3)
			{
				for (int i=Y1; i<Y2; i+=6)
				{
					g->DrawLine(pena,int(X1+X),int(i),int(X1+X),int(i+1));
				}
			}
			else if ((lineStyle==4)&&(X1+X-1>X1))
			{
				for (int i=Y1; i<Y2; i+=6)
				{
					g->DrawLine(pena,int(X1+X-1),int(i),int(X1+X-1),int(i+1));
				}
			}
			else if ((lineStyle==5)&&(X1+X+1<X2))
			{
				for (int i=Y1; i<Y2; i+=6)
				{
					g->DrawLine(pena,int(X1+X+1),int(i),int(X1+X+1),int(i+1));
				}
			}
		}
		catch (System::Exception^ e) {e;}
	}
	
	/// Places a vertical line on the graph
	/// <param name="ValueAsPercentageOfX1toX2">Where to place the reference line, expressed as a value from 0-1 with X1=0 and X2=1</param>
	void AddVerticalReferenceLine(System::Drawing::Color^ C, double ValueAsPercentageOfX1toX2)
	{
		AddVerticalReferenceLine(C,ValueAsPercentageOfX1toX2,0);
	}

	void ClearCanvas()
	{
		// create graphics object
		System::Drawing::Graphics^ g = PARENT->CreateGraphics();
		System::Drawing::Pen^ penblack = gcnew System::Drawing::Pen(*BGCOLOR);
		
		g->DrawRectangle(penblack,X1,Y1,X2-X1,Y2-Y1);
		g->FillRectangle(penblack->Brush,X1,Y1,X2-X1,Y2-Y1);
	}

	void Draw(void)
	{
		// create graphics object
		System::Drawing::Graphics^ g = PARENT->CreateGraphics();
		System::Drawing::Pen^ penblack = gcnew System::Drawing::Pen(*BGCOLOR);
		
		// blank screen
		if (DrawBackgroundYN)
		{
			g->DrawRectangle(penblack,X1,Y1,X2-X1,Y2-Y1);
			g->FillRectangle(penblack->Brush,X1,Y1,X2-X1,Y2-Y1);
		}

		// Draw the graph
 		System::Drawing::Pen^ penfore = gcnew System::Drawing::Pen(*FORECOLOR);
		
		// The math here is correct - read the graphing process below to understand
		// how this system works before making changes!
		int Pi = CurrentValue+1;
		if (Pi >= Values->Length) {Pi = 0;}
		double PreviousValue=Values[Pi];

		// Implement GraphMovement direction - I think this will work
		
		for (int i=X1+1; i<this->X2; i++)
		{
			// This point is represented by StartPoint + proportion of graph space moved across
			// How far across the available space are we?
			// Refpt is a proportion 0-1
			double RefPt;
			if (X2 != X1) 
			{
				// This needs adjustment! If we just divide by the available range, then the very last
				// "cell" doesn't ever get reached, apparently.  Compensate by increasing the division
				// by one additional cell length
				// maybe not
				RefPt = (double(i)-double(X1))/(double(X2)-double(X1));
				//double OneMoreCell = (double(X2)-double(X1))/this->Values->Length;
				//RefPt = (double(i)-double(X1))/(OneMoreCell+double(X2)-double(X1));
			}
			else {RefPt = 0;}

			// Okay, we know how far across the graph grid we are.  We need to calculate how 
			// far across the array (absolute units) we are now.  This is stored in int ArrayRef
			double ArrayRef = this->Values->Length * RefPt;

			// Now ArrayRef needs to be adjusted based on start point
			if (GraphMovementMode == 1)
			{
				ArrayRef = ArrayRef + this->CurrentValue+1;
			}
			if (ArrayRef >= Values->Length) {ArrayRef = ArrayRef - Values->Length;}

			// Determine how high this point is as a PERCENTAGE of TOTAL RANGE
			double rheight;
			if (MaxRange != MinRange)
			{
				rheight = (double(Values[int(ArrayRef)])-double(this->MinRange))/(double(MaxRange)-double(MinRange));
			}
			else {rheight = 0;}

			// Now scale the 'height' to the graph space height
			double gheight = double(rheight * double(this->Height));

			// checks on limits
			if (gheight < 0) {gheight = 0;}
			if (Y2-gheight < Y1) {gheight = double(Y2-Y1);}

			// Now draw from previous to current
			if (LineMode)
			{
				// Need to figure out where to start the bar height wise
				// Z becomes the percentage of the range that 0 to Y2 represents
				double Z = (double(RangeZero)-double(MinRange))/(double(MaxRange)-double(MinRange));
				Z = int(Z * double(this->Height));
				if (Z < 0) {Z = 0;}
				if (Y2-Z < Y1) {Z = Y2-Y1;}
				g->DrawLine(penfore,int(i),int(Y2-int(Z)),int(i),int(Y2-gheight));
			}
			else if ((int(ArrayRef) > this->CurrentValue)||(int(ArrayRef)<2))
			{
				// do nothing
			} 
			else
			{
				g->DrawLine(penfore,int(i-1),int(Y2-PreviousValue),int(i),int(Y2-gheight));
			}
			PreviousValue = gheight;
		}
		delete g;
		if (this->DrawFrameYN) {this->DrawFrame();}
	}

public:
	void ChangeForeColor(System::Drawing::Color^ NewColor)
	{
		this->FORECOLOR = NewColor;
	}
	bool ChangeScale (double MinimumRange, double MaximumRange, double NewZero)
	{
		MinRange = MinimumRange;
		MaxRange = MaximumRange;
		RangeScale = (MaxRange-MinRange)/100;
		RangeZero = NewZero;
		return true;
	}

	void SetDrawBackgroundOnRefreshOnOff(bool drawYesOrNo)
	{
		DrawBackgroundYN = drawYesOrNo;
	}

	void ClearValues(void)
	{
		for (int i =0; i<Values->Length; i++)
		{
			Values[i] = 0;
		}
		CurrentValue=-1;
		Looped=0;
	}

	bool AddValue(double NewValue)
	{
		CurrentValue++;
		if (CurrentValue >= Values->Length)
		{
			CurrentValue=0;
			Looped=1;
		}
		Values[CurrentValue] = NewValue;
		if (this->AutoScaleMax)
		{
			if (NewValue > this->MaxRange)
			{
				this->MaxRange = NewValue;
			}
		}
		return true;
	}

	bool ChangeRegion (int LeftEdge, int RightEdge, int Top, int Bottom)
	{
		X1 = LeftEdge;
		X2 = RightEdge;
		Y1 = Top;
		Y2 = Bottom;

		Width = X2 - X1;
		Height = Y2 - Y1;

		return true;
	}

public:
	int GetWidth (void)
	{
		return Width;
	}

	int GetHeight (void)
	{
		return Height;
	}

};



};