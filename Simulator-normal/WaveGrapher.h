// WaveGrapher.h
// Reference structures and functions for GE Waveforms
// J. Rinehart 2011

#pragma once

#include "GEWaveformTools.h"
#include "EKGAnalyzer.h"
#include "Constants.h"
#include "PPV.h"

#define DEFINED_PPERDUMP 2500

namespace WAVEGRAPHER 
{

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

public ref struct DataWrapper
{
public:	
	EKGANALYZER::EKGData^ DATA;
	PPV::calcPack^ PPV;
	String^ Timestamp;
	String^ Datestamp;
	int UsedPackets;
};

public ref class WaveSpace
	{
	private:
		int LastID;		//Tracks packet ID's for output

		int pX;
		int pY;
		int Dwidth;
		int Dheight;	// Everything based on a height of 800px
		double Hx;			// height scalar constant; = Dheight/800;

		bool TrendOn;

		double speed;		// Sets scrolling speed on screen
		int processSpeed;	// Sets speed at which the wave buffer is processed (1-fast, 60-normal)
		int WaveLocation;
		int WavePacketsPerSecond;
		int CO2WaveLocation;
		int instantCO2;

		ArrayList^ EKGOutput;

		// last values as buffers
		int EKGII_last;
		int EKGV_last;
		int ART_last;
		int PLETH_last;
		int CO2_last;

		// Minimum Range
		int ART_min;
		int PLETH_min;

		// Y values (height)
		int EKGII_Y;
		int EKGV_Y;
		int ART_Y;
		int PLETH_Y;
		int CO2_Y;

		// Z values (waveform 1000% scale)
		int EKGII_Z;
		int EKGV_Z;
		int ART_Z;
		int PLETH_Z;
		int CO2_Z;

		System::Windows::Forms::Form^ Parent;

	public:
		ArrayList^ WP;

	private:
		ArrayList^ UsedPackets;
		ArrayList^ Trend;
		//ArrayList^ EKGII;
		//ArrayList^ EKGV;
		//ArrayList^ Pleth;
		//ArrayList^ Art;
	
	public: 
		WAVEGRAPHER::WaveSpace(System::Windows::Forms::Form^ PARENT, int x1, int y1, int x2, int y2)
		{
			// Set the parent so we know who to call for graphics
			this->Parent = PARENT;

			WavePacketsPerSecond = PACKETS_PER_SECOND; // True value calculated was 57...
			WaveLocation=0;
			DefineWaveSpace(x1,y1,x2,y2);
			speed = 2;

			TrendOn=0;

			LastID = 1000000;

			// 1-FAST; 20-Normal
			processSpeed=20;

			EKGII_last = 0;
			EKGV_last = 0;
			ART_last = 80;
			PLETH_last = 120;
			CO2_last = 0;

			ART_min = 0;
			PLETH_min = 0;

			EKGII_Y = 100;
			EKGV_Y =230;
			ART_Y = 600;
			PLETH_Y = 630;
			CO2_Y = 795;

			// 1000 = 100%
			EKGII_Z = 100;
			EKGV_Z = 100;
			ART_Z = 2000;
			PLETH_Z = 150;
			CO2_Z = 3000;
			
			instantCO2=0;

			WP = gcnew ArrayList();
			UsedPackets = gcnew ArrayList();
			Trend = gcnew ArrayList();
			EKGOutput = gcnew ArrayList();
			//EKGII = gcnew ArrayList;
			//EKGV = gcnew ArrayList;
			//Pleth = gcnew ArrayList;
			//Art = gcnew ArrayList;
		
			};

		// Sets up the area for waveforms
	public: int DefineWaveSpace(int x1, int y1, int x2, int y2)
		{
			// Clear the screen BEFORE we resize.
			
			// create graphics object
			Graphics^ g = this->Parent->CreateGraphics();
			// create pen
			Pen^ penblack = gcnew Pen(Color::Black);
			// blank screen
			//g->DrawRectangle(penblack,x1,y1,x2,y2);
			//g->FillRectangle(brushblack,x1,y1,(x2-x1)+1,(y2-y2)+1);
			for (int i=pX; i<=this->Dwidth + pX; i++)
			{
				g->DrawLine(penblack,i,pY,i,this->Dheight+pY);
			}
			delete penblack;
			delete g;

			double Ratio = (double(x2)-double(x1))/double(Dwidth);
			speed = speed*Ratio;
			if (speed > 10) {speed =10;}
			if (speed < 0.5) {speed =0.5;}

			// Now set the new space.
			pX = x1;
			pY = y1;
			this->Dwidth = x2-x1;
			this->Dheight = y2-y1;
			this->Hx = double(Dheight)/800;

				return 1;
		};

	public: int SetWaveformSpeed(double newspeed)
		{
			this->speed = newspeed;
			return 1;
		}
	public: int increaseWaveformSpeed()
		{
			this->speed += 1;
			if (this->speed > 10) {this->speed = 10;}
			return int(this->speed);
		}
	public: int decreaseWaveformSpeed()
		{
			this->speed += -1;
			if (this->speed < 1) {this->speed = 1;}
			return int(speed);
		}
	public: int SetProcessSpeed(int newspeed)
		{
			this->processSpeed = newspeed;
			if (this->processSpeed < 1) {this->processSpeed = 1;};
			return 1;
		}

	public: int addWavePackets(ArrayList^ NewPackets)
		{
			for (int i=0;i<NewPackets->Count;i++) 
			{
				this->WP->Add(dynamic_cast<JANK::WavePacket ^>(NewPackets->default[i]));
			}
			return WP->Count;
		}

	public: void ShowTrendInArea(int x1, int y1, int x2, int y2)
		{
			TrendOn=1;
			// Setup Area Below

		}

	public: void TrendOff(void)
		{
			TrendOn=0;
		}

	private: EKGANALYZER::EKGData^ AnalyzeDumpedPackets(ArrayList^ DP)
		{
			ArrayList^ EKG2 = gcnew ArrayList();
			EKG2 = JANK::EKGWaveFromPackets(2,DP);
		
			// Artline removed from analyze routine
			return EKGANALYZER::Analyze(EKG2);
 			//return EKGANALYZER::Analyze(ART);
 		}

	private: PPV::calcPack^ getPPVfromPackets(ArrayList^ DP)
		{
			ArrayList^ ART = gcnew ArrayList();
			ART = JANK::ArtWaveFromPackets(DP);
			PPV::calcPack^ CP = PPV::calcPPV(ART);
			CP->PPV;
			return CP;
 		}
	public: int getProcessSpeed(void)
		{
			return this->processSpeed;
		}
	public: void showTrend(ArrayList^ TREND, System::Drawing::Graphics^ g) 
		{
			// Remove any extra up front
			if ((TREND->Count*5) +100 > this->Dwidth) 
			{
				int MAX = int((this->Dwidth-300)/5)-1;
				TREND->RemoveRange(0,TREND->Count - MAX);

				Pen^ PenBlack = gcnew System::Drawing::Pen(Color::Black);
				// Clear the area
				for (int i=50; i<this->Dwidth-49; i++) 
				{
					g->DrawLine(PenBlack,i+pX,pY,i+pX,pY+int(149*Hx));
				}
			}


			for (int i=0; i<TREND->Count; i++) 
			{

				Pen^ penGreen;
				int H = int(*dynamic_cast<int ^>(TREND->default[i])*5);
				
				// H IS 5 TIMES BIGGER THAN RDVI!!
				if (H < 45) 
				{
					penGreen = gcnew Pen(Color::Lime);
				}
				else if (H < 75) 
				{
					penGreen = gcnew Pen(Color::Yellow);
				}
				else 
				{
					penGreen = gcnew Pen(Color::Red);
				}

				g->DrawLine(penGreen,pX+(i*5)+50,pY+int(140*Hx),pX+(i*5)+50,pY+int((140-H)*Hx));
				g->DrawLine(penGreen,pX+(i*5)+51,pY+int(140*Hx),pX+(i*5)+51,pY+int((140-H)*Hx));
				g->DrawLine(penGreen,pX+(i*5)+52,pY+int(140*Hx),pX+(i*5)+52,pY+int((140-H)*Hx));
				g->DrawLine(penGreen,pX+(i*5)+53,pY+int(140*Hx),pX+(i*5)+53,pY+int((140-H)*Hx));
				g->DrawLine(penGreen,pX+(i*5)+54,pY+int(140*Hx),pX+(i*5)+54,pY+int((140-H)*Hx));
				
			}
		}
	public: void setCO2(int newCO2)
		{
			this->instantCO2 = newCO2;
		}

	public: DataWrapper^ graph(String^ SaveFileName, bool showART, bool showCO2, bool showEKG, bool showPulseOx, bool drawwaves, int ArtScale)
		{
			// This function normally gets called every 50ms by the main form, or 20 times per second
			// SaveFileName - set to empty string to disable waveform file saving
			//
			// mathonly = suppress graphics, only do calculations

			DataWrapper^ WRAP = gcnew DataWrapper();
			PPV::calcPack^ PPV = gcnew PPV::calcPack();
			EKGANALYZER::EKGData^ DATA = gcnew EKGANALYZER::EKGData();
			String^ WTIME = "";
		
			WRAP->Timestamp	= "";
			WRAP->Datestamp = "";
			WRAP->UsedPackets = this->UsedPackets->Count;

			PPV->HR = 0;
			PPV->RR = 0;
			PPV->PPV = 0;

			// Optimally, there are exactly 3 packets every time we land here
			if (this->WP->Count < 3)
			{
				// We're running too fast perhaps?  Slow it down
				this->processSpeed += 1;
			}
			if (this->WP->Count > 6)
			{
				// We're running too slow, speed it up
				if (this->processSpeed > 1)
				{
					this->processSpeed += -1;
				}
				
			}

			// Make sure there's enough packets waiting before we bother with anything else
			if (this->WP->Count < (this->WavePacketsPerSecond/processSpeed))
			{
				WRAP->DATA = DATA;
				WRAP->PPV = PPV;
				return WRAP;
			}

			// create graphics object
			Graphics^ g;
			
			if (drawwaves)
			{
				g = this->Parent->CreateGraphics();
			}
			
			// Setup our speed variables
			int goa = int(this->speed/4);
			int gob = int(this->speed/2);
			int goc = goa+gob;
			int god = int(this->speed);

			//Pen^ pengray = gcnew Pen(Color::FromArgb(50,50,50));
			//g->DrawLine(pengray,0,this->EKGII_Y,this->Dwidth,this->EKGII_Y);
			//g->DrawLine(pengray,0,this->EKGII_Y-int(80*this->EKGII_Z/1000),this->Dwidth,this->EKGII_Y-int(80 * this->EKGII_Z/1000));


			// graph based on WavePacketsPerSecond and speed
			for (int i=0;i<(this->WavePacketsPerSecond/processSpeed);i++)
			{
				// blank out an area 8 paces ahead (keeps a little buffer)
				Pen^ penblack;
				if (drawwaves)
				{
					penblack= gcnew Pen(Color::Black);
				}
				int B = this->WaveLocation + 50;
				for (int i=0;i<4*this->speed;i++)
				{
					B=B+1;
					if (B > this->Dwidth+speed+1)
					{
						B = B - Dwidth - 2 - int(speed);
					}
					if (drawwaves)
					{
						g->DrawLine(penblack,B+pX,pY,B+pX,int(pY+((this->CO2_Y-130)*this->Hx)));
					}
				}
				// Don't delete pen: we're using it again below
				//delete penblack;

				// graph the lead II EKG from this packet
				//Graphics^ gr = e->Graphics;
				Pen^ pen1;
				Pen^ pen2;
				Pen^ pen3;
				Pen^ pen4;
				
				if (drawwaves)
				{
					pen1 = gcnew Pen(Color::Lime);
					pen2 = gcnew Pen(Color::Red);
					pen3 = gcnew Pen(Color::Cyan);
					pen4 = gcnew Pen(Color::WhiteSmoke);
				}

				// Pull the next packet off the packet queue
				JANK::WavePacket^ W = (dynamic_cast<JANK::WavePacket ^>(this->WP->default[0]));

				if (W->timestamp->Length > 0)
				{
					WTIME=W->timestamp;
				}

				if (drawwaves)
				{
					if (showEKG == true)
					{
						// Draw the packet EKG contents onto the monitor
						g->DrawLine(pen1, pX+this->WaveLocation,     pY+int((this->EKGII_Y - (EKGII_last * this->EKGII_Z/1000))*Hx), pX+this->WaveLocation+goa, pY+int((this->EKGII_Y - (W->EKG2a * this->EKGII_Z/1000))*Hx));
						g->DrawLine(pen1, pX+this->WaveLocation+goa, pY+int((this->EKGII_Y - (W->EKG2a * this->EKGII_Z/1000))*Hx), pX+this->WaveLocation+gob, pY+int((this->EKGII_Y - (W->EKG2b * this->EKGII_Z/1000))*Hx));
						g->DrawLine(pen1, pX+this->WaveLocation+gob, pY+int((this->EKGII_Y - (W->EKG2b * this->EKGII_Z/1000))*Hx), pX+this->WaveLocation+goc, pY+int((this->EKGII_Y - (W->EKG2c * this->EKGII_Z/1000))*Hx));
						g->DrawLine(pen1, pX+this->WaveLocation+goc, pY+int((this->EKGII_Y - (W->EKG2c * this->EKGII_Z/1000))*Hx), pX+this->WaveLocation+god, pY+int((this->EKGII_Y - (W->EKG2d * this->EKGII_Z/1000))*Hx));		
						// set the buffer for the next loop
						this->EKGII_last = W->EKG2d;	

						// Draw the packet EKG V contents onto the monitor
						g->DrawLine(pen1, pX+this->WaveLocation,     pY+int((this->EKGV_Y - (EKGV_last * this->EKGV_Z/1000))*Hx), pX+this->WaveLocation+goa, pY+int((this->EKGV_Y - (W->EKGVa * this->EKGV_Z/1000))*Hx));
						g->DrawLine(pen1, pX+this->WaveLocation+goa, pY+int((this->EKGV_Y - (W->EKGVa * this->EKGV_Z/1000))*Hx), pX+this->WaveLocation+gob, pY+int((this->EKGV_Y - (W->EKGVb * this->EKGV_Z/1000))*Hx));
						g->DrawLine(pen1, pX+this->WaveLocation+gob, pY+int((this->EKGV_Y - (W->EKGVb * this->EKGV_Z/1000))*Hx), pX+this->WaveLocation+goc, pY+int((this->EKGV_Y - (W->EKGVc * this->EKGV_Z/1000))*Hx));
						g->DrawLine(pen1, pX+this->WaveLocation+goc, pY+int((this->EKGV_Y - (W->EKGVc * this->EKGV_Z/1000))*Hx), pX+this->WaveLocation+god, pY+int((this->EKGV_Y - (W->EKGVd * this->EKGV_Z/1000))*Hx));		
						// set the buffer for the next loop
						this->EKGV_last = W->EKGVd;	
					}

					if (showART==true)
					{
						int ARTaS = ((ArtScale * (W->ARTa - W->ARTmean))/100) + W->ARTmean;
						int ARTbS = ((ArtScale * (W->ARTb - W->ARTmean))/100) + W->ARTmean;
						// Draw the packet art contents onto the monitor
						g->DrawLine(pen2, pX+this->WaveLocation,   pY+int((this->ART_Y - (ART_last * this->ART_Z/1000))*Hx), 
							pX+this->WaveLocation+gob, pY+int((this->ART_Y - (ARTaS * this->ART_Z/1000) + this->ART_min)*Hx));
						g->DrawLine(pen2, pX+this->WaveLocation+gob, pY+int((this->ART_Y - (ARTaS * this->ART_Z/1000))*Hx), 
							pX+this->WaveLocation+god, pY+int((this->ART_Y - (ARTbS * this->ART_Z/1000) + this->ART_min)*Hx));
						// set the buffer for the next loop
						this->ART_last = ARTbS;	
					}

					if (showPulseOx==true)
					{
						// Draw the packet pleth contents onto the monitor
						g->DrawLine(pen3, pX+this->WaveLocation,   pY+int((this->PLETH_Y - (PLETH_last * this->PLETH_Z/1000))*Hx), pX+this->WaveLocation+god, pY+int((this->PLETH_Y - (W->PLETH * this->PLETH_Z/1000) + this->PLETH_min)*Hx));
						// set the buffer for the next loop
						this->PLETH_last = W->PLETH;
					}				

					// CO2 Wave
					// blank out an area 8 paces ahead (keeps a little buffer)

					B = this->CO2WaveLocation;
					for (int i=0;i<10;i++)
					{
						B=B+1;
						if (B > this->Dwidth+1)
						{
							B = B - Dwidth;
						}
						g->DrawLine(penblack,B+pX,int(pY+((this->CO2_Y-130)*this->Hx)),B+pX,this->Dheight+pY);
					}
						
					if (showCO2==true)
					{
						// Now draw the CO2 waveform
						if ((this->instantCO2 < 61)&&(this->CO2_last<61)) 
						{
							g->DrawLine(pen4, pX+this->CO2WaveLocation, pY+int((this->CO2_Y - (CO2_last * this->CO2_Z/1000))*Hx), pX+this->CO2WaveLocation+1, pY+int((this->CO2_Y - (instantCO2 * this->CO2_Z/1000))*Hx));
						}
					}
					// set the buffer for the next loop
					this->CO2_last = this->instantCO2;
					this->CO2WaveLocation += 1;
					if (this->CO2WaveLocation > this->Dwidth)
					{
						this->CO2WaveLocation = 0;
					}
				} // end mathonly

				// Now delete this pen (it was created up top)
				delete penblack;


				// Advance the pointer
				this->WaveLocation = this->WaveLocation+int(this->speed);
				if (this->WaveLocation > this->Dwidth)
				{
					this->WaveLocation = 0;
				}

				if (TrendOn==1) 
				{
					// Copy the first packet to the dump
					this->UsedPackets->Add(dynamic_cast<JANK::WavePacket ^>(WP->default[0]));
				}

				// If we're recording waves, copy to the output arrays
				if (SaveFileName->Length > 0)
				{
					int NewID = (dynamic_cast<JANK::WavePacket ^>(WP->default[0]))->timecode;

					if ((NewID != LastID+1) && (NewID > LastID))
					{
						// We seem to have missed a network packet.
						for (int i=0; i<15; i++)
						{
							JANK::WavePacket^ P = gcnew JANK::WavePacket();
							this->EKGOutput->Add(P);
						}
					}

					this->EKGOutput->Add(dynamic_cast<JANK::WavePacket ^>(WP->default[0]));
					this->LastID = (dynamic_cast<JANK::WavePacket ^>(WP->default[0]))->timecode;

					if ((EKGOutput->Count > DEFINED_PPERDUMP))
					{
						DumpToFile(SaveFileName);
					}

				}

				// Now shift off the first member of the WavePackets queue
				this->WP->RemoveAt(0);
				
				if (TrendOn==1)
				{
					// if UsedPackets is > WAVE_ANALYSIS_TIME seconds worth, analyze it
					if (this->UsedPackets->Count > PACKETS_PER_SECOND * WAVE_ANALYSIS_TIME)
					{
						DATA = this->AnalyzeDumpedPackets(UsedPackets);
						PPV = this->getPPVfromPackets(UsedPackets);
							
						// Add this value to the trend
						Trend->Add(DATA->EKVRept->EKGv);

						if (drawwaves) 
						{
							this->showTrend(Trend,g);
						}
		
						// Now cut the used packets in half
						UsedPackets->RemoveRange(0,PACKETS_PER_SECOND * (WAVE_ANALYSIS_TIME/2));

						// Halt running of the wave for now
						//CallingTimer->Stop();

						// Display debug data
						//DebugBox->Text = 
					}
				}

				// break if no more packets
				if (WP->Count == 0) 
				{
					break;
				}
			}
			// clean up graphics
			delete g;

			if (WTIME->Length > 0)
			{
				WRAP->Timestamp = WTIME->Substring(WTIME->IndexOf(' '),WTIME->Length-WTIME->IndexOf(' '));
				WRAP->Datestamp = WTIME->Substring(0,WTIME->IndexOf(' '));
			}
			WRAP->PPV = PPV;		
			WRAP->DATA = DATA;
			return WRAP;
		}

public: void DumpToFile(System::String^ SaveFileName)
		{
			// Get the time
			int j=0;
			String^ lasttime="";
			while ((j<DEFINED_PPERDUMP) && (j<EKGOutput->Count) && (lasttime->Length <= 0))
			{
				if (dynamic_cast<JANK::WavePacket ^>(EKGOutput->default[j])->timestamp->Length > 0)
				{
					lasttime=dynamic_cast<JANK::WavePacket ^>(EKGOutput->default[j])->timestamp;
				}
				j++;
			}
			lasttime = lasttime->Substring(lasttime->IndexOf(' ')+1);
			lasttime = lasttime->Replace(":","-");
			lasttime = lasttime->Replace("AM","");
			lasttime = lasttime->Replace("PM","");
			//lasttime = lasttime->Substring(0,4);
					
			System::IO::StreamWriter^ DOUT = gcnew System::IO::StreamWriter(SaveFileName + ".EKGwave." + lasttime + ".CSV",0);
				for (int i=0; (i<DEFINED_PPERDUMP)&&(i<EKGOutput->Count); i++) 
				{
					if (dynamic_cast<JANK::WavePacket ^>(EKGOutput->default[i])->timestamp->Length > 0)
					{
						DOUT->Write(dynamic_cast<JANK::WavePacket ^>(EKGOutput->default[i])->timestamp);
					}
					DOUT->Write("," + dynamic_cast<JANK::WavePacket ^>(EKGOutput->default[i])->timecode);
					DOUT->Write("," + dynamic_cast<JANK::WavePacket ^>(EKGOutput->default[i])->EKG1a);
					DOUT->Write("," + dynamic_cast<JANK::WavePacket ^>(EKGOutput->default[i])->EKG2a);
					DOUT->Write("," + dynamic_cast<JANK::WavePacket ^>(EKGOutput->default[i])->EKG3a);
					DOUT->Write("," + dynamic_cast<JANK::WavePacket ^>(EKGOutput->default[i])->EKGVa + "\n");
					DOUT->Write("," + dynamic_cast<JANK::WavePacket ^>(EKGOutput->default[i])->timecode);
					DOUT->Write("," + dynamic_cast<JANK::WavePacket ^>(EKGOutput->default[i])->EKG1b);
					DOUT->Write("," + dynamic_cast<JANK::WavePacket ^>(EKGOutput->default[i])->EKG2b);
					DOUT->Write("," + dynamic_cast<JANK::WavePacket ^>(EKGOutput->default[i])->EKG3b);
					DOUT->Write("," + dynamic_cast<JANK::WavePacket ^>(EKGOutput->default[i])->EKGVb + "\n");
					DOUT->Write("," + dynamic_cast<JANK::WavePacket ^>(EKGOutput->default[i])->timecode);
					DOUT->Write("," + dynamic_cast<JANK::WavePacket ^>(EKGOutput->default[i])->EKG1c);
					DOUT->Write("," + dynamic_cast<JANK::WavePacket ^>(EKGOutput->default[i])->EKG2c);
					DOUT->Write("," + dynamic_cast<JANK::WavePacket ^>(EKGOutput->default[i])->EKG3c);
					DOUT->Write("," + dynamic_cast<JANK::WavePacket ^>(EKGOutput->default[i])->EKGVc + "\n");
					DOUT->Write("," + dynamic_cast<JANK::WavePacket ^>(EKGOutput->default[i])->timecode);
					DOUT->Write("," + dynamic_cast<JANK::WavePacket ^>(EKGOutput->default[i])->EKG1d);
					DOUT->Write("," + dynamic_cast<JANK::WavePacket ^>(EKGOutput->default[i])->EKG2d);
					DOUT->Write("," + dynamic_cast<JANK::WavePacket ^>(EKGOutput->default[i])->EKG3d);
					DOUT->Write("," + dynamic_cast<JANK::WavePacket ^>(EKGOutput->default[i])->EKGVd + "\n");
				}
				DOUT->Close();
				delete DOUT;
						 
				DOUT = gcnew System::IO::StreamWriter(SaveFileName + ".ARTwave." + lasttime + ".CSV",0);
				for (int i=0; (i<DEFINED_PPERDUMP)&&(i<EKGOutput->Count); i++) 
				{
					if (dynamic_cast<JANK::WavePacket ^>(EKGOutput->default[i])->timestamp->Length > 0)
					{
						DOUT->Write(dynamic_cast<JANK::WavePacket ^>(EKGOutput->default[i])->timestamp);
					}
					DOUT->Write("," + dynamic_cast<JANK::WavePacket ^>(EKGOutput->default[i])->timecode);
					DOUT->Write("," + dynamic_cast<JANK::WavePacket ^>(EKGOutput->default[i])->ARTa + "\n");
					DOUT->Write("," + dynamic_cast<JANK::WavePacket ^>(EKGOutput->default[i])->timecode);
					DOUT->Write("," + dynamic_cast<JANK::WavePacket ^>(EKGOutput->default[i])->ARTb + "\n");
				}
				DOUT->Close();
				delete DOUT;
				if (EKGOutput->Count > DEFINED_PPERDUMP)
				{
					EKGOutput->RemoveRange(0,DEFINED_PPERDUMP);
				}
				else
				{
					EKGOutput->Clear();
				}
				return;
		};

		
	};

};