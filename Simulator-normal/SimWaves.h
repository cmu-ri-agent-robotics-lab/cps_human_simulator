// SimWaves.h
// Cardiodynamic simulator waveform output
// Copyright Sironis 2011, all rights reserved
// J. Rinehart

// Wave position is a simple int that points to the position on the waveform templates
// each waveform is a multiple of 40 points, so WavePosition will be 0-40
// Actually, waveforms are now variable in length but specified at the time of loading

#pragma once

#define ARTBUFFERLENGTH 40
#define PLETHBUFFERLENGTH 22

#include "GEWaveformTools.h"

namespace Simulator {

public ref class SimulatorWaves
{
public:
	double WavePosition;
	double EKGWavePosition;
	double AlineWavePosition;

	// Wave Library Variables
	array<int>^ ArtWave;
	array<int>^ EKGWave;
	array<int>^ EKGVWave;
	array<int>^ PlethWave;
	int ArtStretchPoint;
	int WaveLength;

	ArrayList^ ArtBuffer;
	ArrayList^ PlethBuffer;

	String^ newAFILE;
	String^ newEFILE;
	String^ newEVFILE;
	String^ newPFILE;
	int newWaveLength;
	int newBreakPoint;


public: SimulatorWaves()
	{
		PlethWave = gcnew array<int>(80);
		ArtWave = gcnew array<int>(160);
		EKGWave = gcnew array<int>(320);
		EKGVWave = gcnew array<int>(320);

		ArtBuffer = gcnew ArrayList();
		PlethBuffer = gcnew ArrayList();

		newAFILE="";
		newPFILE="";
		newEFILE="";
		newEVFILE="";
		newWaveLength=0;
		newBreakPoint=0;

		WavePosition = 0;
		EKGWavePosition = 0;
		AlineWavePosition = 0;
	}

public: void changeWaves(String^ AFile, String^ EFile, String^ EVFile, String^ PFile, int NWL, int ARTBreakPoint) {
		// This function changes after a cycle! this is how it should normally be done.
		newAFILE=AFile;
		newEFILE=EFile;
		newEVFILE=EVFile;
		newPFILE=PFile;
		newWaveLength = NWL;
		newBreakPoint = ARTBreakPoint;
		}

public: void LoadWaves(String^ AFile, String^ EFile, String^ EVFile, String^ PFile, int newWaveLength, int ARTBreakPoint) {
		// Only for initial waveform or instant changes
		ArtStretchPoint=ARTBreakPoint;
		WaveLength = newWaveLength;

		// Load and initialize the ART array-----------------------------
		int i=0;
		try 
		{
			IO::StreamReader^ AFILE = gcnew IO::StreamReader(AFile); 
			while ((!AFILE->EndOfStream)&&(i<WaveLength*2))
			{
				String^ line = AFILE->ReadLine();
				ArtWave[i] = Int16::Parse(line);
				i++;
			}
			AFILE->Close();
		}
		catch (Exception^ e){MessageBox::Show(e->ToString());}
		while (i<80)
		{
			ArtWave[i] = 0;
			i++;
		}

		// Load and initialize the EKG array-------------------------------
		i=0;
		try 
		{
			IO::StreamReader^ EFILE = gcnew IO::StreamReader(EFile); 
			while ((!EFILE->EndOfStream)&&(i<WaveLength*4))
			{
				String^ line = EFILE->ReadLine();
				EKGWave[i] = Int16::Parse(line);
				i++;
			}
			EFILE->Close();
		}
		catch (Exception^ e){MessageBox::Show(e->ToString());}
		while (i<160)
		{
			EKGWave[i] = 0;
			i++;
		}

		// Load and initialize the EKGV array-----------------------------
		i=0;
		try 
		{
			IO::StreamReader^ EFILE = gcnew IO::StreamReader(EVFile); 
			while ((!EFILE->EndOfStream)&&(i<WaveLength*4))
			{
				String^ line = EFILE->ReadLine();
				EKGVWave[i] = Int16::Parse(line);
				i++;
			}
			EFILE->Close();
		}
		catch (Exception^ e){MessageBox::Show(e->ToString());}
		while (i<160)
		{
			EKGVWave[i] = 0;
			i++;
		}

				// Load and initialize the ART array-----------------------------
		i=0;
		try 
		{
			IO::StreamReader^ PFILE = gcnew IO::StreamReader(PFile); 
			while ((!PFILE->EndOfStream)&&(i<WaveLength))
			{
				String^ line = PFILE->ReadLine();
				PlethWave[i] = Int16::Parse(line);
				i++;
			}
			PFILE->Close();
		}
		catch (Exception^ e){MessageBox::Show(e->ToString());}
		while (i<WaveLength)
		{
			PlethWave[i] = 0;
			i++;
		}
	}

private:
	int getHybridValue(double pos, array<int>^ A)
	{
		int j = int(pos);
		double p = pos-j;
		double q = 1-p;
		int X=0;
		try 
		{
			X = int((A[j]*q) + (A[j+1]*p));
			return X;
		}
		catch (Exception^ e) 
		{
			e;
			return 0;
		}
	}

public: 
	ArrayList^ generateWavePackets(int PacketNumber, Simulator::VitalsPack^ VP, int PPVPercentStart, int PPVPercentEnd, int VVar)
	{
		ArrayList^ Packets = gcnew ArrayList();

		// Figure the magnitude of PPV: (SBP-DBP) * PPV
		// This represents the FULL POSSIBLE depression caused by PPV on the A-line
		// it'll be modulated by the PPVPercentStart/End variables
		double PPVx = (VP->SBP - VP->DBP) * (VP->PPV/100);
		double ARTx = 0;
		double iterator = double(VP->HR)/90;

		// Separate iterator for EKG; it never increases by less than 1
		// (There are two parallel counters; A-line expands & contracts as needed for rate,
		// the EKG contracts at high rates, but at low rates it expands only to the natural
		// library waveform length and beyond that the baseline alone stretches).
		
		// Aline Counter added; it compresses uniformly, but lengthes to only 200% normal,
		// then continues to lengthen only past the halfway point
		// Also used for the pleth waveform
		
		// Set iterator lengths; these are used to calculate the hybrid ranges
		double Eiterator = iterator;
		if (iterator < 1) {Eiterator = 1;}
		double Aiterator = iterator;
		if ((Aiterator < 0.5)&&(AlineWavePosition < ArtStretchPoint)) {Aiterator=0.5;}

		for (int i=0; i<PacketNumber; i++)
		{
			JANK::WavePacket^ WP = gcnew JANK::WavePacket();
			
			// WavePosition is the true mathematic percentile position
			// the other two are the adjusted positions
			double j = this->WavePosition;
			double k = this->EKGWavePosition;
			double l = this->AlineWavePosition;
			//if (l >=double(ArtStretchPoint)-2) {l = double(ArtStretchPoint)-2;}

			// Just set them for now
			/*WP->EKG1a = 0;
			WP->EKG1b = 0;
			WP->EKG1c = 0;
			WP->EKG1d = 0;
			WP->EKG3a = 0;
			WP->EKG3b = 0;
			WP->EKG3c = 0;
			WP->EKG3d = 0;*/

			// Get ARTx, the coefficient of the current art waveform point height
			// to total wave height to factor into the PPV
			// We'll use this for art both points (close enough)
			ARTx = ((this->ArtWave[int(j*2)])*(VP->SBP-VP->DBP)/100);
			if ((VP->SBP - VP->DBP) != 0) 
			{
				ARTx = ARTx/(VP->SBP - VP->DBP);
			}
			else {ARTx = 1;}

			// These are tricky; Wave position is a marker on
			// the waveform indexed to the 40(Variable) PLETH points.  We have to 
			// multiply it out for the Art (2x) and EKG(4x) which 
			// are higher resolution waveforms.
			// Plus all the PPV modifications and the pacing modifications.

			// Probably best to not mess with this until you really understand
			// what's going on. 
			if (k < WaveLength-1) 
			{
				WP->EKG2a = getHybridValue(k*4+(0*Eiterator),this->EKGWave);
				if (WP->EKG2a > 200) {WP->EKG2a += -int(VP->PPV*WP->EKG2a*PPVPercentStart/10000);}
				WP->EKG2b = getHybridValue(k*4+(1*Eiterator),this->EKGWave);
				if (WP->EKG2b > 200) {WP->EKG2b += -int(VP->PPV*WP->EKG2b*PPVPercentStart/10000);}
				WP->EKG2c = getHybridValue(k*4+(2*Eiterator),this->EKGWave);
				if (WP->EKG2c > 200) {WP->EKG2c += -int(VP->PPV*WP->EKG2c*PPVPercentStart/10000);}
				WP->EKG2d = getHybridValue(k*4+(3*Eiterator),this->EKGWave);
				if (WP->EKG2d > 200) {WP->EKG2d += -int(VP->PPV*WP->EKG2d*PPVPercentStart/10000);}

				// The V wave responds like lead-2 to resp cycles, but we're chalking this up to
				// chest impedance (b/c we know V doesn't work for fluid resp.).  We'll cycle just
				// like lead-2, but instead of PPV we're going to use the constant VVar;
				WP->EKGVa = getHybridValue(k*4+(0*Eiterator),this->EKGVWave);
				if (WP->EKGVa > 200) {WP->EKGVa += -int(VVar*WP->EKGVa*PPVPercentStart/10000);}
				WP->EKGVb = getHybridValue(k*4+(1*Eiterator),this->EKGVWave);
				if (WP->EKGVb > 200) {WP->EKGVb += -int(VVar*WP->EKGVb*PPVPercentStart/10000);}
				WP->EKGVc = getHybridValue(k*4+(2*Eiterator),this->EKGVWave);
				if (WP->EKGVc > 200) {WP->EKGVc += -int(VVar*WP->EKGVc*PPVPercentStart/10000);}
				WP->EKGVd = getHybridValue(k*4+(3*Eiterator),this->EKGVWave);
				if (WP->EKGVd > 200) {WP->EKGVd += -int(VVar*WP->EKGVd*PPVPercentStart/10000);}
			}
			else 
			{
				WP->EKG2a = 0;
				WP->EKG2b = 0;
				WP->EKG2c = 0;
				WP->EKG2d = 0;
				WP->EKGVa = 0;
				WP->EKGVb = 0;
				WP->EKGVc = 0;
				WP->EKGVd = 0;
			}

			// Pleth: Single point
			// Buffer exists to delay wave from EKG pulse (but allow synchronicity of waves in this sub)
			int X = getHybridValue(l,this->PlethWave);
			int NP = int(X - (X * double(VP->PPV)/100 * double(PPVPercentStart)/100) + (20-double(PPVPercentStart)));
			PlethBuffer->Add(NP);
			if (PlethBuffer->Count > PLETHBUFFERLENGTH)
			{
				WP->PLETH = *(dynamic_cast<int^>(PlethBuffer->default[0]));
				PlethBuffer->RemoveAt(0);
			}

			// Art line waveforms; 4 terms in equation
			// 1st: Aline value
			// 2nd: PPV subtraction (includes Resp Pattern and Art Height Cycle so
			//      that PPV is subtracted only from peaks and not from troughs)
			// 3rd: PPV subtraction from entire waveform to vary baseline
			// 4th: Adjustment back up half of third term throughout to renormalize pressures
			
			/* BASELINE & PEAK MODULATION */ 
			int AA = int((((getHybridValue(l*2,this->ArtWave))*(VP->SBP-VP->DBP)/100)+VP->DBP)  - (ARTx * PPVx * (double(PPVPercentStart)/100)) - (PPVx * (double(PPVPercentStart)/100)/2) + (PPVx/4)) ;
			int AB = int((((getHybridValue(l*2+Aiterator,this->ArtWave))*(VP->SBP-VP->DBP)/100)+VP->DBP) - (ARTx * PPVx * (double(PPVPercentEnd)/100)) - (PPVx * (double(PPVPercentStart)/100)/2) + (PPVx/4));
						
			/* NO BASELINE VARIATION VERSION */ 
			//int AA = int((((getHybridValue(l*2,this->ArtWave))*(VP->SBP-VP->DBP)/100)+VP->DBP)  - (ARTx * PPVx * (double(PPVPercentStart)/100)));
			//int AB = int((((getHybridValue(l*2+Aiterator,this->ArtWave))*(VP->SBP-VP->DBP)/100)+VP->DBP) - (ARTx * PPVx * (double(PPVPercentEnd)/100)));
			
			// Buffer exists to delay wave from EKG pulse (but allow synchronicity of waves in this sub)
			ArtBuffer->Add(AA);
			ArtBuffer->Add(AB);
			if (ArtBuffer->Count > ARTBUFFERLENGTH)
			{
				WP->ARTa = *(dynamic_cast<int^>(ArtBuffer->default[0]));
				WP->ARTb = *(dynamic_cast<int^>(ArtBuffer->default[1]));

				WP->ARTmean = int(VP->DBP + VP->SBP)/2;

				ArtBuffer->RemoveAt(0);
				ArtBuffer->RemoveAt(0);
			}

			Packets->Add(WP);

			this->WavePosition += iterator;
			this->EKGWavePosition += 1;
			if (EKGWavePosition < this->WavePosition) 
			{
				EKGWavePosition = this->WavePosition;
			}

			if (this->AlineWavePosition < ArtStretchPoint) 
			{
				this->AlineWavePosition += 0.5;
			}
			else 
			{
				this->AlineWavePosition += double(0.1);
			}
			if (AlineWavePosition < this->WavePosition)
			{
				AlineWavePosition = this->WavePosition;
			}

			

			// So long as WavePosition is < 39 the Hybrid will work
			if (this->WavePosition > WaveLength-1) 
			{
				this->WavePosition = 0;
				this->EKGWavePosition = 0;
				this->AlineWavePosition=0;

				// Load new waveforms here when needed!  It's time.
				if (newAFILE->Length > 0)
				{
					this->LoadWaves(newAFILE,newEFILE,newEVFILE,newPFILE,newWaveLength,newBreakPoint);
					newAFILE="";
					newPFILE="";
					newEFILE="";
					newEVFILE="";
					newWaveLength=0;
					newBreakPoint=0;
				}
			}
		}
		
		return Packets;
	}


};



}