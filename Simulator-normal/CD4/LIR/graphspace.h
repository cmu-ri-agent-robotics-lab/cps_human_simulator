// Graphspace.h
// Copyright 2012, all rights reserved
// J. Rinehart

// Note that graphs can be left to right or right to left, or upside-down
// even, if you reverse the edges in the region definition.
// you can define regions that extend off screen if you wish; the off-screen
// pieces simply won't display.


#pragma once

namespace Graph {

public ref class Graphspace{

private:
	int X1;  // Left edge
	int X2;  // Right edge
	int Y1;  // Top
	int Y2;  // Bottom

	double Xscale;
	double Yscale;
	int Width;
	int Height;

	int MinRange;
	int MaxRange;
	double RangeScale;

	int values<100,1>;

public: 
	bool Graphspace (int LeftEdge, int RightEdge, int Top, int Bottom, int MinimumRange, int MaximumRange)
	{
		ChangeScale(MinimumRange,MaximumRange);
		ChangeRegion(LeftEdge,RightEdge,Top,Bottom);
		return true;
	}

public:
	bool ChangeScale (int MinimumRange, int MaximumRange)
	{
		MinRange = MinimumRange;
		MaxRange = MaximumRange;
		RangeScale = (MaxRange-MinRange)/100;
		return true;
	}

	bool ChangeRegion (int LeftEdge, int RightEdge, int Top, int Bottom)
	{
		X1 = LeftEdge;
		X2 = RightEdge;
		Y1 = Top;
		Y2 = Bottom;

		Width = X2 - X1;
		Height = Y2 - Y1;

		Xscale = float(Width)/100;
		Yscale = float(Height)/100;

		return true;
	}

public:
	int GetWidth (void)
	{
		return Width;
	}

	int GetHeight (void)
	{
		return Height;
	}

};



};