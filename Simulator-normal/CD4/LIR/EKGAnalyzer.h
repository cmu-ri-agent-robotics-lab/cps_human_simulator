// EKGAnalyzer.h
// Reference structures and functions for Digital EKG Waveform Analysis
// Specifically the EKG Lead II Variability
// J. Rinehart 2011

// These functions know nothing about wave packets or vendor data structs
// They rely only on an incoming array of int values describing the 
// digitized waveform in question, and output an analysis data structure based
// on that waveform.

// DEBUGWAVE turns on outputting of 0% segments and >40% segments
//#define DEBUGWAVE


#pragma once

#include "Constants.h"

namespace EKGANALYZER 
{

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	unsigned int cycle=0;

public ref struct EKVReport
{
	int EKGv;
	int discarded;
	int used;
};


#ifdef DEBUGWAVE

public ref struct debugOutput
	{
		int value;
		int peak;
		int trough;
		int used;
	};
#endif 

public ref struct PeakInfo
	{
		int value;
		int location;
		int trough;
		int troughLocation;
		int PTheight;
	};


public ref struct PeakAnalysis
	{
		int Atop;
		int Abottom;
		int SD;
		int mean;
		int UsedPeaks;
		int DiscardedPeaks;
		int RERUN;
		int Noise;
		ArrayList^ Rpeaks;
		String^ PathNotes;
	};

public ref class EKGData
	{
	public:
		int HR;					// Heart Rate
		int RR;
		int SecondsAnalyzed;	// Total time of data passed
		unsigned int cycle;		// cycle of analysis (for tracking, debug, etc)
		EKVReport^ EKVRept;				// R-wave deflection Variability Index
		PeakAnalysis^ PAnalysis;
		
	public:
		EKGData(void) 
		{
			HR = 0;
			RR = 0;
			SecondsAnalyzed=0;
			cycle=0;
			EKVRept = gcnew EKVReport();
			PAnalysis = gcnew PeakAnalysis();
		}
	};


//***************************************************************
// Analyze Peaks - fxn takes an ArrayList<PeakInfo^> and runs statistical
// tests on them to remove noise.

PeakAnalysis^ AnalyzePeaks(ArrayList^ Xpeaks, int lowerbound, int upperbound)
{
	PeakAnalysis^ Analysis = gcnew PeakAnalysis();
	Analysis->Abottom = 0;
	Analysis->Atop = 0;
	Analysis->Rpeaks = gcnew ArrayList();
	Analysis->RERUN=0;
	Analysis->SD=0;
	Analysis->mean=0;
	Analysis->UsedPeaks=0;
	Analysis->DiscardedPeaks=0;

	ArrayList^ peaks = gcnew ArrayList();
	Analysis->PathNotes = "";

#ifdef DEBUGWAVE
	String^ DNOTES = "";
#endif

	// We now add a boundary condition to the peak process; this requires a temp array
	if ((upperbound != 0)||(lowerbound !=0))
	{
		for (int i=0; i<Xpeaks->Count; i++)
		{
			int v = static_cast<PeakInfo^>(Xpeaks->default[i])->value;
			if ((v<upperbound)&&(v>lowerbound))
			{
				peaks->Add(static_cast<PeakInfo^>(Xpeaks->default[i]));
			}
		}
	}
	else 
	{
		peaks = Xpeaks;
	}

	int mean = 0;
	int SD = 0;
	if (peaks->Count == 0) 
	{
		return Analysis;
	}

	// The peaks array contains a list of PeakInfo Packets
	// Peak and location.  
	// Before we analyze the locations, we need to analyze
	// the peak frequencies within badwith ranges

	// Find the mean
	for (int i=0; i<peaks->Count; i++) 
	{
		mean = mean + (dynamic_cast<PeakInfo^>(peaks->default[i])->value);
	}
	mean = int(mean/peaks->Count);
	Analysis->mean = mean;

	// Find the SD
	for (int i=0; i<peaks->Count; i++) 
	{
		int x = Math::Abs(mean - (dynamic_cast<PeakInfo^>(peaks->default[i])->value));
		SD = SD + x*x;
	}
	SD = int(Math::Sqrt(SD/peaks->Count));
	Analysis->SD = SD;

	// Sometimes this is easy
	if (SD*20 <= mean) 
	{
		// SD is tiny, take everything within 4 SD
		Analysis->Abottom = mean - (SD*4);
		Analysis->Atop = mean + (SD*4);
		Analysis->PathNotes += " SDtiny";
		Analysis->Noise = 0;
	}
	else if (SD*5 <= mean)
	{
		// SD was small, but not tiny.  There are a few (1-4?) errors in there.
		// Take a narrower interpretation of these points
		Analysis->Abottom = mean - (SD*2);
		Analysis->Atop = mean + (SD*2);
		Analysis->PathNotes += " SDlow";
		Analysis->Noise = 1;
	}
	else {
		// SD was wide; there's noise or t-waves or something
		// We've got mean and SD, lets find the likely R peaks
		// We want to include four standard deviations in both directions of the mean, or 8 SD total
		// Create Baskets within this range; we want them fairly broad, let's say 1/2 SD each
		// That'll be 16 total baskets with a range of 1/2 SD each.
		int BasketSize = int(SD/4);
		int Pmax=0;
		int Pbig=0;
		array<int,1>^ Baskets = gcnew array<int,1>(32);
		
		// Base is the bottom of the bottom basket
		int Base = mean - (SD*4);

		// Now run the peaks again and sort them into the baskets
		for (int i=0; i<peaks->Count; i++)
		{
			int bnum = (dynamic_cast<PeakInfo^>(peaks->default[i])->value - Base)/(SD/4);
			// Anything above 16 is >4 sd away, so ignore.	
			if (bnum < 32) 
			{
				if (bnum > 0) 
				{
					Baskets[bnum]++;
				}
			}
		} // end for

		// Now we have our baskets, figure out where the R-wave peaks are likely to be
		// Let's do this.

		// Get the maximum basket size.
		for (int i=0; i<32; i++) 
		{
			if (Baskets[i] > Pmax) {Pmax = Baskets[i];}
		}

		// The max basket size is either R-peaks, T-peaks or the wave is complete noise
		// How peaks are there in the range of the max? This will help sort the mess
		// Lets count the number that are at least 2/3 of this?
		for (int i=0; i<32; i++)
		{
			if (Baskets[i]*2/3 >= Pmax) {Pbig++;}
		}

		// How many peaks do we have?
		if (Pbig == 1)
		{
			// This is basically a WIDE SD with only one set of peaks
			Analysis->Noise = 1;
			// Only one!  This is either R-waves, or, possibly in very strange circumstances, T-waves
			// If t-waves, it would have to be detected by timing analysis earlier, so here we 
			// assume R-waves.  Get cutoff values.
			// THESE DON'T HAVE TO BE PERFECT!
			Analysis->Abottom = 0;
			for (int i=Pmax; i>=0; i=i-1)
			{
				// I'm actually going to be conservative on the uptake, the fill-in
				// is better than having too many peaks in here
				if (Baskets[i] < (Pmax/8))
				{
					Analysis->Abottom = i;
				}
			}
			Analysis->Atop = 32;
			for (int i=Pmax; i<32; i++)
			{
				if (Baskets[i] < (Pmax/8))
				{
					Analysis->Atop = i;
				}
			}

			Analysis->PathNotes += " SDhigh ONElayer";
		}
		// ELSE we have more than one set of peaks with similar concentrations
		// Now we have to figure out wtf is going on.
		// Could just be R's & T's splitting the SD, could be a goat rodeo.
		else
		{
			// THERE ARE 2 OR MORE PEAKS
			Analysis->PathNotes += " SDhigh MANYlayers";

			// A lot of wave types are ending up in this pathway.
			// We need a way to get at the very least the perfect ones
			// with big T's that get in the zone.
			
			int zeros=0;
			int total=0;
			int peakCount=0;
			int PeakOn=0;
			int BRun=0;
			int aTop=0;
			int aBottom=0;

			// Quick stats on the baskets
			for (int i=31; i>=0; i=i-1)
			{
				if (Baskets[i] == 0)
				{
					zeros++;
				}
				total += Baskets[i];
			}

			// I don't think it's possible for total to be == 0, but
			// we're going to be dividing by total, so...
			if (total>0) 
			{
				Analysis->PathNotes += " TotalPeaks=" + total.ToString();

				for (int i=31; i>=0; i=i-1)
				{
					//Analysis->PathNotes += Baskets[i].ToString() + " * 100 / total = " + ((Baskets[i]*100)/total).ToString() + "\n";
					// Filter baskets that contain < 10% of the peaks
					if ((Baskets[i]*100)/total > 10)
					{
						if (PeakOn==0)
						{
							PeakOn=1;
							peakCount++;
							if (aTop==0)
							{
								aTop=i;
							}
						}
					}
					else 
					{
						BRun++;
					}

					if ((Baskets[i]==0)||(BRun >= 3))
					{
						if (Baskets[i]==0)
						{
							BRun=0;
						}

						if (PeakOn==1)
						{
							PeakOn=0;
							if (aBottom==0)
							{
								aBottom=i+1;
							}
						}
					}

				} // end (for i)
			}// end if (total>0)
			
			Analysis->PathNotes += " peakCount=" + peakCount.ToString();

			// See if we have only two peak areas (confirmed by Pbig)
			if (peakCount==2)
			{
				int reftop = aTop;
				// We need a pinch more info
				// Take top and try to find zero, else include all
				for (int i=aTop; i<32; i++)
				{
					if (Baskets[i] == 0) 
					{
						aTop=i-1;
						i=32;
					}
				}
				// Set Noise equal to the number of small baskets encountered below the range (minus one basket).
			    // Add to this some value for baskets above
				Analysis->Noise = 0;
				
				// Bottom was already covered by Brun

				Analysis->Abottom = (SD/4)*aBottom+Base;
				Analysis->Atop = (SD/4)*(aTop+1)+Base;
				Analysis->RERUN = 1;
			}
			else
			{
				Analysis->Noise=1;
				Analysis->PathNotes += " [Too Many Basket Peaks, ZEROED]";
				Analysis->Abottom = 0;
				Analysis->Atop = 0;
			}

			#ifdef DEBUGWAVE
			DNOTES += "Pbig=" + Pbig.ToString() + " peakCount=" + peakCount.ToString() + " aTop=" + aTop.ToString() + " aBottom=" + aBottom.ToString() + "\n";
			DNOTES += "A-Atop=" + Analysis->Atop.ToString() + " A-Abot=" + Analysis->Abottom.ToString() + "\n";
			#endif
		
		} //end else multiple peaks
	
		#ifdef DEBUGWAVE
		for (int i=0; i<32; i++) 
		{
			DNOTES += "Basket " + i.ToString() + " (" + (SD/4*i+Base).ToString() + " - " + (SD/4*(i+1)+Base).ToString() + ") : " + Baskets[i].ToString() + "\n";
		}
		#endif
	} // End else SD wide

	#ifdef DEBUGWAVE
	System::IO::StreamWriter^ DOUT = gcnew System::IO::StreamWriter("DEBUGOUT_ANALYSIS.TXT",1);
	DOUT->WriteLine("Cycle " + cycle.ToString());
	DOUT->WriteLine("At:" + Analysis->Atop.ToString() + " Ab:" + Analysis->Abottom.ToString());
	DOUT->WriteLine("SD/M:" + int(Analysis->SD*100/Analysis->mean).ToString() + "% SD:" + Analysis->SD.ToString() + " mean:" + Analysis->mean.ToString() + "\n" + Analysis->PathNotes);
	for (int i=0; i<peaks->Count; i++)
	{
		int V = dynamic_cast<PeakInfo^>(peaks->default[i])->value;
		if ((V > Analysis->Atop) || (V < Analysis->Abottom)) 
		{
			DOUT->Write("[" + V.ToString() + "] ");
		}
		else 
		{
			DOUT->Write(V.ToString() + " ");
		}
	}
	DOUT->WriteLine("\n" + DNOTES);
	DOUT->WriteLine("\n");
	DOUT->Close();
	delete DOUT;
	#endif

	// Take our new peaks and pack 'em into the Rpeaks
	for (int i=0; i<peaks->Count; i++) 
	{
		PeakInfo^ P = dynamic_cast<PeakInfo^>(peaks->default[i]);
		if ((P->value <= Analysis->Atop) && (P->value >= Analysis->Abottom))
		{
			Analysis->Rpeaks->Add(P);
			Analysis->UsedPeaks++;
		}
		else
		{
			Analysis->DiscardedPeaks++;
		}
	}

	return Analysis;
}


//***********************************************
// DetectPeaks - takes an ArrayList<int^> (a wave datastream)
// and detects all peaks above baseline noise
// THIS COULD BE IMPROVED WITH DERIVATIVE DETECTION(?)

#ifdef DEBUGWAVE
ArrayList^ DetectPeaks(ArrayList^ EKG, ArrayList^ DebugWave)
#else 
ArrayList^ DetectPeaks(ArrayList^ EKG)
#endif
{
		ArrayList^ Peaks = gcnew ArrayList();
	
		int Pb1=0;
		int Pb5=0;
		int P=0;
		int Pf1=0;
		int Pf5=0;

		// Run the list of points and find all peaks above the baseline noise
		for (int i=5;i<(EKG->Count-5);i++)
		{

			P = *dynamic_cast<int ^>(EKG->default[i]);

#ifdef DEBUGWAVE
			debugOutput^ DO = gcnew debugOutput;
			DO->value = P;
			DO->peak = 0;
			DO->used = 0;
#endif

			// Range chosen to avoid overt noise; I've yet to see a 
			// true peak above 2k
			if ((P > 80)&&(P < 2000)) {
				Pb1 = *dynamic_cast<int ^>(EKG->default[i-1]);
				Pb5 = *dynamic_cast<int ^>(EKG->default[i-4]);
				Pf1 = *dynamic_cast<int ^>(EKG->default[i+1]);
				Pf5 = *dynamic_cast<int ^>(EKG->default[i+4]);
				
				// Flat plateaus are a problem; we will allow the lead
				// point to be EQUAL to the following, but not vice-versa
				// This should allow plateaus but not double booking of them.
				if ((P>Pb1)&&(P>Pb5)&&(P>=Pf1)&&(P>Pf5))
				{
					///RBTEMP->Text = RBTEMP->Text + "\n+) " + P.ToString();
					PeakInfo^ NewPeak = gcnew PeakInfo();
					NewPeak->location = i;
					NewPeak->value = P;
					NewPeak->trough = 0;
					NewPeak->troughLocation = 0;
					NewPeak->PTheight = 0;
					// Add the peak(value,location) pairs
					Peaks->Add(NewPeak);
		
#ifdef DEBUGWAVE
					DO->peak = 1;
#endif
				}
			}
#ifdef DEBUGWAVE
			DebugWave->Add(DO);
#endif
		}
		return Peaks;
}

//****************************************************
// AnalyzeTroughs - Takes a PeakAnalysis^ and an ArrayList<int> wave datastream and finds
// the corresponding trough before the peak to get the height
#ifdef DEBUGWAVE
PeakAnalysis^ AnalyzeTroughs(PeakAnalysis^ Peaks, ArrayList^ EKG, ArrayList^ DebugWave)
#else
PeakAnalysis^ AnalyzeTroughs(PeakAnalysis^ Peaks, ArrayList^ EKG)
#endif
{
	#ifdef DEBUGWAVE
	String^ NOTES;
	#endif

	// We run the list of peaks, and for each find the previous trough
	for (int i=0; i<Peaks->Rpeaks->Count; i++)
	{
		PeakInfo^ Peak = static_cast<PeakInfo^>(Peaks->Rpeaks->default[i]);
		int Loc = Peak->location;
		int Value = Peak->value;
		int Tloc = 0;

		// Now run the wave starting at the location and moving backward
		for (int j=Loc-2; (j>0)&&(Tloc==0); j=j-1)
		{
			// Loc-X AND J+X FOR pb; X MUST BE THE SAME OR ELSE THE SLOPE
			// DETECTION THING GETS FUCKED
			int p  = static_cast<int>(EKG->default[j]);
			int pa = static_cast<int>(EKG->default[j+1]);
			int pb = static_cast<int>(EKG->default[j+2]);
			
			// REMEMBER WE'RE MOVING BACKWARDS
			// we're trying to slide down the wave...we're looking for the uptick to stop
			// if p is greater than the previous point
			// or the slope becomes less than 10%
			// int m = (p - pb)/(j-j+4), which is also:
			double m = (p - pb)/-4;

			if ((p>=pa)||(m<0.5))
			{
				// this point is higher than the previous two;
				// and the next two; we're done
				// Hopefully using the next two will help eliminate error
				// from R-R', but we made need to shift
				Tloc = j+1;
			}
		}
		// Tloc contains the location in the EKG wave of the trough value
		// Peak is a damn POINTER.  This *should* work...
		// POSSIBLE NO TROUGH WAS DETECTED
		if (Tloc > 0)
		{
			Peak->troughLocation = Tloc;
			Peak->trough = static_cast<int>(EKG->default[Tloc]);
			Peak->PTheight = Peak->value - Peak->trough;
		}
		else
		{
			Peak->troughLocation = 0;
			Peak->trough = 0;
		}
		
		#ifdef DEBUGWAVE
		static_cast<debugOutput^>(DebugWave->default[Peak->troughLocation])->trough = Peak->trough;
		NOTES += "Peak " + i.ToString() + ", " + Peak->value.ToString() + " at " + Peak->location.ToString();
		NOTES += "\nTrough " + Peak->trough.ToString() + " at " + Peak->troughLocation.ToString() + "\n";
		#endif
	}

	//#ifdef DEBUGWAVE
	//System::IO::StreamWriter^ DOUT = gcnew System::IO::StreamWriter("DEBUGOUT_T_ANALYSIS.TXT",1);
	//DOUT->WriteLine("Cycle " + cycle.ToString() + "\n" + NOTES);
	//DOUT->Close();
	//delete DOUT;
	//#endif

	return Peaks;
}


//***************************************************************
// Calculate EKV takes an ArrayList<PeakInfo^> and calculates
// the mean variability in the peaks
// The PeakInfos, at this point, are the peak HEIGHTS (from trough to peak)
// not the absolute height of the points representing peaks.

EKVReport^ CalculateEKV (ArrayList^ Peaks)
{
	EKGANALYZER::PeakInfo^ PI = gcnew EKGANALYZER::PeakInfo();
	EKGANALYZER::PeakInfo^ PIf = gcnew EKGANALYZER::PeakInfo();
	EKGANALYZER::PeakInfo^ PIf2 = gcnew EKGANALYZER::PeakInfo();

	EKVReport^ ER = gcnew EKVReport();

	// For the EKV value
	ArrayList^ EKGv = gcnew ArrayList();
	int count=0;
	int Xcount=0;
	double X = 0.0;
	double SD = 0.0;
	double Mean = 0.0;

	// Tracking Values
	ER->discarded = 0;
	ER->used = 0;

	// These two are used to tag top/bottom
	int high=0;
	int low=0;

	// Starter
	int falling=0;
	int rising=0;

	// We need to match each peak to it's low and vice-versa
	// First find a low;
	for (int i=0; i<Peaks->Count-2; i++) 
	{		
		PI = dynamic_cast<EKGANALYZER::PeakInfo ^>(Peaks->default[i]);
		PIf = dynamic_cast<EKGANALYZER::PeakInfo ^>(Peaks->default[i+1]);
		PIf2 = dynamic_cast<EKGANALYZER::PeakInfo ^>(Peaks->default[i+2]);
		
		if (falling == 0) 
		{
			if ((PIf->PTheight < PI->PTheight) && (PIf2->PTheight < PI->PTheight))
			{
				falling = 1;
			}
		}
		else if (rising==0)
		{
			if ((PIf->PTheight > PI->PTheight) && (PIf2->PTheight > PI->PTheight)) 
			{
				// We were falling, but the next two are lower, so this is the low
				low = PI->PTheight;
				rising=1;
			}
		}
		else
		{
			// We're rising here, so
			if ((PIf->PTheight < PI->PTheight) && (PIf2->PTheight < PI->PTheight))
			{
				// The next two are lower, so this is the high
				high = PI->PTheight;
			}
		}

		// Now a check for completion
		if (high > 0) 
		{
			// We've got a full set.  Whoopee!
			// Add it to the list and increment our counter
			EKGv->Add(double((high-low)*100)/((high+low)/2));
			count++;

			// reset for next cycle
			high=0;
			low=0;
			rising=0;
			falling=1;  // 1 is right; we already know we're falling now.
		}
	}
	
	if (count==0)
	{
		ER->EKGv = 0;
		return ER;
	}
	else
	{
		// Now we've got the array of double values; find the average and SD
		for (int i=0; i<EKGv->Count; i++)
		{
			Mean += + static_cast<double>(EKGv->default[i]);	
		}
		Mean = (Mean/EKGv->Count);
		for (int i=0; i<EKGv->Count; i++)
		{
			SD += Math::Abs((static_cast<double>(EKGv->default[i]) - Mean) * (static_cast<double>(EKGv->default[i]) - Mean));	
		}
		SD = double(Math::Sqrt(SD/EKGv->Count));

		#ifdef DEBUGWAVE
		String^ Debug;
		Debug += "XSD:" + SD.ToString() + " XM:" + Mean.ToString() + "\n"; 
		#endif

		// Run the array again, discard any values outside of 2 SD.
		for (int i=0; i<EKGv->Count; i++)
		{
			//#ifdef DEBUGWAVE
			//Debug += "\nABS ( " + static_cast<double>(EKGv->default[i]).ToString() + "-" + Mean.ToString() + " < " + SD.ToString() + "*2";
			//#endif
			if (Math::Abs(static_cast<double>(EKGv->default[i])-Mean) < Math::Abs(SD*2))
			{
				X += static_cast<double>(EKGv->default[i]);
				Xcount++;
					
				#ifdef DEBUGWAVE
				Debug += static_cast<double>(EKGv->default[i]) + " ";
				#endif	
				ER->used++;
			}
			else 
			{
				#ifdef DEBUGWAVE
				Debug += "[" + static_cast<double>(EKGv->default[i]) + "] ";
				#endif	
				ER->discarded++;
			}	
		}

		// Gotta have at least two for agreement
		if (Xcount > 2) 
		{
			X = X/Xcount;
		}		
		else 
		{
			X = 0;
		}
		ER->EKGv = int(X);

		//#ifdef DEBUGWAVE
		//System::IO::StreamWriter^ DOUT = gcnew System::IO::StreamWriter("DEBUGOUT_PA_" + cycle.ToString() + ".TXT",1);
		//DOUT->WriteLine(Debug);
		//DOUT->Close();
		//delete DOUT;
		//#endif

		return ER;
	}
}

//***************************************************************
// Analyze
EKGANALYZER::EKGData^ Analyze(ArrayList^ EKG)
	{
		// This routine finds the peaks, then passess off
		// to the analysis routine

		cycle=cycle+1;
		
#ifdef DEBUGWAVE
		ArrayList^ DebugWave = gcnew ArrayList();
#endif

		// Create our working variable space
		EKGData^ DATA = gcnew EKGData();
		ArrayList^ Peaks = gcnew ArrayList();
		
		// Record the cycle
		DATA->cycle = cycle;

		// We need at least 20 seconds of data to do this calculation, m'kay?
		if (EKG->Count < PACKETS_PER_SECOND * 20 * 4)
		{
			return DATA;
		}

		// Find the peaks
		#ifdef DEBUGWAVE
		Peaks = DetectPeaks(EKG,DebugWave);
		#else
		Peaks = DetectPeaks(EKG);
		#endif	

		// At this point, Array Peaks has all detected peaks in PeakInfo packets
		// Array PeakV has only the list of values

		// Run the V2 peak analyzer
		DATA->PAnalysis = AnalyzePeaks(Peaks,0,0);
		//DebugBox->Text = "SD:" + PA->SD.ToString() + " mean:" + PA->mean.ToString() + "\n" + PA->PathNotes;

		// Run the trough detector
		#ifdef DEBUGWAVE
		DATA->PAnalysis = AnalyzeTroughs(DATA->PAnalysis,EKG,DebugWave);
		#else
		DATA->PAnalysis = AnalyzeTroughs(DATA->PAnalysis,EKG);
		#endif	

		// I guess we actually have to do some analysis now, huh?
		DATA->EKVRept = CalculateEKV(DATA->PAnalysis->Rpeaks);
		DATA->SecondsAnalyzed = (EKG->Count/(PACKETS_PER_SECOND * 4));
		DATA->HR = DATA->PAnalysis->Rpeaks->Count*60/(DATA->SecondsAnalyzed); 
		DATA->RR = DATA->EKVRept->discarded + DATA->EKVRept->used;


#ifdef DEBUGWAVE
		System::IO::StreamWriter^ DOUT = gcnew System::IO::StreamWriter("DEBUGOUT_" + cycle.ToString() + "_" + DATA->EKVRept->EKGv.ToString() + ".CSV");
		for (int i=0; i<DebugWave->Count; i++)
		{
			debugOutput^ DO = gcnew debugOutput();
			DO = dynamic_cast<debugOutput^>(DebugWave->default[i]);
			DOUT->WriteLine(DO->value.ToString() + "," + DO->peak.ToString() + "," + DO->trough.ToString() + "," + DO->used.ToString());
		}
		DOUT->Close();
		delete DOUT;
#endif

		// MASIMO TEMP
		if (DATA->EKVRept->EKGv > 50)
		{
			DATA->EKVRept->EKGv = 0;
		}

		return DATA;
	};


};

