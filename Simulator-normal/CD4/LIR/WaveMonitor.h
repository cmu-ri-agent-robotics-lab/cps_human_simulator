#pragma once

// RespCyclePosition is a counter that tracks the position of the respiratory cycle
// It'll mark the # of seconds since the beginning of the current resp cycle (beginning with
// inspiration then exhalation).  Length is calculated as double (60/RR) ticks (1/10 seconds).  If the RR is 
// changed, it'll immediately convert to the new point in the new cycle.

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

// In addition to the define, you'll have to target these
// slots in the constructor!
#define VITALSLOTS 6

#include "../../WaveMonitorHelpers.h"
#include "../../WaveGrapher.h"
#include "../../SimWaves.h"
#include "../../Simulator.h"
#include "../../Graphspace.h"

namespace LIR {
	/// <summary>
	/// Summary for WaveMonitor
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class WaveMonitor : public System::Windows::Forms::Form
	{
	public:
		WaveMonitor(Windows::Forms::Form^ P, Drugs^ DrugListPass)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//

			//
			//Making sure the UI elements are thread safe!
			//
			this->addDrug = gcnew addDrugDelegate(this, &WaveMonitor::addDrugMethod);
			this->removeDrug = gcnew removeDrugDelegate(this, &WaveMonitor::removeDrugByUIDMethod);

			DrugList = DrugListPass;
			ActionLog = "";

			Vitals = gcnew Simulator::VitalsPack();
			Wavemon = gcnew WAVEGRAPHER::WaveSpace(this,333,0,this->Width-225,this->GB1->Location.Y);
			SimWaves = gcnew Simulator::SimulatorWaves();
			SimWaves->LoadWaves("Art_80.txt","EKG_160.txt","EKGV_160.txt","Pleth_40.txt",40,21);
			
			Rx = gcnew System::Random();
			
			Graph1 = gcnew Graph::Graphspace(this,System::Drawing::Color::Black,System::Drawing::Color::White,this->panel3->Width+6,this->GB5->Location.X-6,this->GB1->Location.Y+8,this->GB1->Location.Y + this->GB1->Size.Height-1,0,100,0,120);
			Graph2 = gcnew Graph::Graphspace(this,System::Drawing::Color::Black,System::Drawing::Color::White,this->panel3->Width+6,this->GB5->Location.X-6,this->GB2->Location.Y+8,this->GB4->Location.Y + this->GB2->Size.Height-1,0,100,0,120);
			
			Graph1->SetFrameDrawOn(System::Drawing::Color::White);
			Graph2->SetFrameDrawOn(System::Drawing::Color::White);
			Graph1->SetDrawBackgroundOnRefreshOnOff(true);
			Graph2->SetDrawBackgroundOnRefreshOnOff(true);
			Graph1->SetAutoScaleMaxOn();
			Graph2->SetAutoScaleMaxOn();

			/////////////////////////////////////////////////////////////////////

			this->cb_Drugs->Items->Add("Select Drug");
			for (int i=0; i<DrugList->DrugList->Count; i++)
			{
				this->cb_Drugs->Items->Add(static_cast<DrugData^>(DrugList->DrugList[i])->DrugName);
			}
			this->cb_Drugs->SelectedIndex = 0;

			////////////////////////////////////////////////////

			this->ArtScale = 100;

			this->TCo = 0;
			this->TCr = 0;
			this->TBl = 0;
			//Shehzi Giving every drug a unique ID
			this->DrugCounterUID = 0;

			CO2on=1;
			ARTon=1;
			EKGon=1;
			PulseOxon=1;
			CO2Build=0;
			count=0;
			Parent = P;
			RespRate = 0;
			AirwayPressure = 0;
			RespCyclePosition=0;
			VVar = 0;
			ticktock=0;

			this->TripleSpeed = false;
			this->Med_ephedrine = 0;
			this->Med_fenty = 0;
			this->Med_phenyl = 0;
			Albumin = 0;
			LR = 0;
	
			LastRefCO=0;
			LastRefSV=0;
			LastRefMAP=0;

			#ifdef HIDE_PICTURESAVEIMAGE
			this->PictureSaveImage->Visible=false;			
			#endif

			Slots = gcnew array<MonitorSlot^>(VITALSLOTS);
			
			// Target the slots: you'll need to also change the 
			// define for VITALSLOTS
			Slots[0] = gcnew MonitorSlot();
			Slots[1] = gcnew MonitorSlot();
			Slots[2] = gcnew MonitorSlot();
			Slots[3] = gcnew MonitorSlot();
			Slots[4] = gcnew MonitorSlot();
			Slots[5] = gcnew MonitorSlot();
			
			Slots[0]->Box = this->GB1;
			Slots[0]->Label = this->GB_lab_1;
			Slots[1]->Box = this->GB2;
			Slots[1]->Label = this->GB_lab_2;
			Slots[2]->Box = this->GB3;
			Slots[2]->Label = this->GB_lab3;
			Slots[3]->Box = this->GB4;
			Slots[3]->Label = this->GB_lab4;
			Slots[4]->Box = this->GB5;
			Slots[4]->Label = this->GB_lab5;
			Slots[5]->Box = this->GB6;
			Slots[5]->Label = this->GB_lab6;
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~WaveMonitor()
		{
			if (components)
			{
				delete components;
			}
		}
	// This delegate enables asynchronous calls for setting
	// the text property on a TextBox control.
	public: delegate System::Void addDrugDelegate(String^ bolus, String^ drip, String^ drug, String^ UID);
	public: addDrugDelegate^ addDrug;
	public: delegate System::Void removeDrugDelegate(String^ UID);
	public: removeDrugDelegate^ removeDrug;

	private: System::Windows::Forms::Panel^  panel1;
	private: System::ComponentModel::IContainer^  components;
	public: 
		double Med_ephedrine;
		double Med_phenyl;
		double Med_fenty;
		int DrugCounterUID;
	private:
		bool TripleSpeed;
		double Albumin;
		double LR;
		int ArtScale;

	unsigned short DrugID;
	double DrugDose;
	bool DrugIsBolus;
	String^ DrugUID;
	
	

	/*double TCo;
	double TCr;
	double TBl;*/

	public: Drugs^ DrugList;
			// Shehzi - need to reset count every trial - ie. when Simulator is stopped.
			double TCo;
			double TCr;
			double TBl;
			

	public: Graph::Graphspace^ Graph1;
	public: Graph::Graphspace^ Graph2;

	protected: 

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>

	array<MonitorSlot^>^ Slots;

	int count;
	int ticktock;
	public:
		Simulator::VitalsPack^ Vitals;
		System::String^ ActionLog;
	private:
	WAVEGRAPHER::WaveSpace^ Wavemon;
	System::Windows::Forms::Form^ Parent;
	int RespRate;
	int AirwayPressure;
	bool CO2on;
	bool ARTon;
	bool EKGon;
	bool PulseOxon;
	double RespCyclePosition;
	int VVar;
	double CO2Build;
	System::Random^ Rx;

	public:
		double LastRefCO;
		double LastRefSV;
		double LastRefMAP;
	
	public: System::Windows::Forms::Timer^  WaveTimer;
	private: System::Windows::Forms::Label^  label1;
	public: System::Windows::Forms::Timer^  timer_pacing;
	private: 

	private: System::Windows::Forms::Label^  label_GCPS;
	private: System::Windows::Forms::Label^  label_WTMS;
	private: System::Windows::Forms::Label^  label2;


	private: System::Windows::Forms::Label^  BIG_BP;
	private: System::Windows::Forms::Label^  BIG_HR;
	private: System::Windows::Forms::Label^  BIG_MAP;
	private: System::Windows::Forms::Label^  GB_lab5;

	private: System::Windows::Forms::Label^  BIG_SPO2;
	private: System::Windows::Forms::Label^  GB_lab3;


	private: System::Windows::Forms::Label^  lab_PAP;
	private: System::Windows::Forms::Label^  lab_RR;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Label^  label6;

	private: System::Windows::Forms::GroupBox^  GB5;

	private: System::Windows::Forms::GroupBox^  GB3;

	private: System::Windows::Forms::Label^  BIG_CO2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::GroupBox^  GB2;
	private: System::Windows::Forms::Label^  GB_lab_2;
	private: System::Windows::Forms::GroupBox^  GB4;
	private: System::Windows::Forms::Label^  GB_lab4;

	private: System::Windows::Forms::GroupBox^  GB6;
private: System::Windows::Forms::Label^  GB_lab6;

	private: System::Windows::Forms::GroupBox^  GB1;
	private: System::Windows::Forms::Label^  GB_lab_1;

private: System::Windows::Forms::Panel^  panel3;
private: System::Windows::Forms::Button^  button1;








private: System::Windows::Forms::Panel^  panel12;
private: System::Windows::Forms::PictureBox^  pictureBox3;
private: System::Windows::Forms::Label^  label_CLOCK;



private: System::Windows::Forms::Timer^  timer_MedPop;















private: System::Windows::Forms::Panel^  panel_Phase;
private: System::Windows::Forms::Label^  label14;

public: System::Windows::Forms::Label^  label19;
public: System::Windows::Forms::Label^  label18;
public: System::Windows::Forms::Label^  label17;
public: System::Windows::Forms::Label^  label16;
public: System::Windows::Forms::Label^  label15;
public: System::Windows::Forms::Label^  label20;
private: System::Windows::Forms::Button^  button2;
private: System::Windows::Forms::Panel^  panel_black;
private: System::Windows::Forms::PictureBox^  PictureSaveImage;
private: System::Windows::Forms::Label^  label_WP_PacketsInBuffer;
private: System::Windows::Forms::Label^  label22;
private: System::Windows::Forms::Label^  label_ProcSpeed;
private: System::Windows::Forms::Label^  label23;
private: System::Windows::Forms::ComboBox^  cb_Drugs;
private: System::Windows::Forms::Button^  button_Administer;

private: System::Windows::Forms::Label^  label_dripUnits;
private: System::Windows::Forms::Label^  label_BolusUnits;
private: System::Windows::Forms::Label^  label24;
private: System::Windows::Forms::Label^  label21;
private: System::Windows::Forms::ComboBox^  cb_Drip;

private: System::Windows::Forms::ComboBox^  cb_Bolus;

private: System::Windows::Forms::Label^  label27;
private: System::Windows::Forms::Label^  label26;
private: System::Windows::Forms::Label^  label_DrugConf;
private: System::Windows::Forms::PictureBox^  pictureBox_cancelDrug;
private: System::Windows::Forms::Timer^  timer_Drug;
private: System::Windows::Forms::DataGridView^  dataGridView1;
private: System::Windows::Forms::Label^  label25;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  Drug;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  Dose;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  units;
private: System::Windows::Forms::DataGridViewButtonColumn^  Cancel;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  ID;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  UID;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  Given;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  IsBolusAsInfusion;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  IsBolus;
private: System::Windows::Forms::Label^  label_TotalBlood;
private: System::Windows::Forms::Label^  label11;
private: System::Windows::Forms::Label^  label_TotalColl;
private: System::Windows::Forms::Label^  label9;
private: System::Windows::Forms::Label^  label_TotalCry;
private: System::Windows::Forms::Label^  label4;






























































public: 

private: Simulator::SimulatorWaves^ SimWaves;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(WaveMonitor::typeid));
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle1 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle2 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle3 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle4 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			this->panel1 = (gcnew System::Windows::Forms::Panel());
			this->label_ProcSpeed = (gcnew System::Windows::Forms::Label());
			this->label23 = (gcnew System::Windows::Forms::Label());
			this->label_WP_PacketsInBuffer = (gcnew System::Windows::Forms::Label());
			this->label22 = (gcnew System::Windows::Forms::Label());
			this->BIG_CO2 = (gcnew System::Windows::Forms::Label());
			this->BIG_BP = (gcnew System::Windows::Forms::Label());
			this->panel12 = (gcnew System::Windows::Forms::Panel());
			this->pictureBox3 = (gcnew System::Windows::Forms::PictureBox());
			this->label_CLOCK = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->lab_RR = (gcnew System::Windows::Forms::Label());
			this->label_GCPS = (gcnew System::Windows::Forms::Label());
			this->BIG_HR = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->BIG_MAP = (gcnew System::Windows::Forms::Label());
			this->label_WTMS = (gcnew System::Windows::Forms::Label());
			this->BIG_SPO2 = (gcnew System::Windows::Forms::Label());
			this->lab_PAP = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->GB_lab3 = (gcnew System::Windows::Forms::Label());
			this->GB_lab5 = (gcnew System::Windows::Forms::Label());
			this->WaveTimer = (gcnew System::Windows::Forms::Timer(this->components));
			this->timer_pacing = (gcnew System::Windows::Forms::Timer(this->components));
			this->panel3 = (gcnew System::Windows::Forms::Panel());
			this->label25 = (gcnew System::Windows::Forms::Label());
			this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
			this->Drug = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Dose = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->units = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Cancel = (gcnew System::Windows::Forms::DataGridViewButtonColumn());
			this->ID = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->UID = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Given = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->IsBolusAsInfusion = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->IsBolus = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->pictureBox_cancelDrug = (gcnew System::Windows::Forms::PictureBox());
			this->label27 = (gcnew System::Windows::Forms::Label());
			this->label26 = (gcnew System::Windows::Forms::Label());
			this->label_DrugConf = (gcnew System::Windows::Forms::Label());
			this->button_Administer = (gcnew System::Windows::Forms::Button());
			this->label_dripUnits = (gcnew System::Windows::Forms::Label());
			this->label_BolusUnits = (gcnew System::Windows::Forms::Label());
			this->label24 = (gcnew System::Windows::Forms::Label());
			this->label21 = (gcnew System::Windows::Forms::Label());
			this->cb_Drip = (gcnew System::Windows::Forms::ComboBox());
			this->cb_Bolus = (gcnew System::Windows::Forms::ComboBox());
			this->cb_Drugs = (gcnew System::Windows::Forms::ComboBox());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->panel_Phase = (gcnew System::Windows::Forms::Panel());
			this->label20 = (gcnew System::Windows::Forms::Label());
			this->label19 = (gcnew System::Windows::Forms::Label());
			this->label18 = (gcnew System::Windows::Forms::Label());
			this->label17 = (gcnew System::Windows::Forms::Label());
			this->label16 = (gcnew System::Windows::Forms::Label());
			this->label15 = (gcnew System::Windows::Forms::Label());
			this->label14 = (gcnew System::Windows::Forms::Label());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->GB1 = (gcnew System::Windows::Forms::GroupBox());
			this->GB_lab_1 = (gcnew System::Windows::Forms::Label());
			this->GB3 = (gcnew System::Windows::Forms::GroupBox());
			this->GB6 = (gcnew System::Windows::Forms::GroupBox());
			this->GB_lab6 = (gcnew System::Windows::Forms::Label());
			this->GB4 = (gcnew System::Windows::Forms::GroupBox());
			this->GB_lab4 = (gcnew System::Windows::Forms::Label());
			this->GB2 = (gcnew System::Windows::Forms::GroupBox());
			this->GB_lab_2 = (gcnew System::Windows::Forms::Label());
			this->GB5 = (gcnew System::Windows::Forms::GroupBox());
			this->timer_MedPop = (gcnew System::Windows::Forms::Timer(this->components));
			this->panel_black = (gcnew System::Windows::Forms::Panel());
			this->PictureSaveImage = (gcnew System::Windows::Forms::PictureBox());
			this->timer_Drug = (gcnew System::Windows::Forms::Timer(this->components));
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label_TotalCry = (gcnew System::Windows::Forms::Label());
			this->label_TotalColl = (gcnew System::Windows::Forms::Label());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->label_TotalBlood = (gcnew System::Windows::Forms::Label());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->panel1->SuspendLayout();
			this->panel12->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox3))->BeginInit();
			this->panel3->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox_cancelDrug))->BeginInit();
			this->panel_Phase->SuspendLayout();
			this->GB1->SuspendLayout();
			this->GB3->SuspendLayout();
			this->GB6->SuspendLayout();
			this->GB4->SuspendLayout();
			this->GB2->SuspendLayout();
			this->GB5->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->PictureSaveImage))->BeginInit();
			this->SuspendLayout();
			// 
			// panel1
			// 
			this->panel1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->panel1->BackColor = System::Drawing::Color::Black;
			this->panel1->Controls->Add(this->label_ProcSpeed);
			this->panel1->Controls->Add(this->label23);
			this->panel1->Controls->Add(this->label_WP_PacketsInBuffer);
			this->panel1->Controls->Add(this->label22);
			this->panel1->Controls->Add(this->BIG_CO2);
			this->panel1->Controls->Add(this->BIG_BP);
			this->panel1->Controls->Add(this->panel12);
			this->panel1->Controls->Add(this->label6);
			this->panel1->Controls->Add(this->lab_RR);
			this->panel1->Controls->Add(this->label_GCPS);
			this->panel1->Controls->Add(this->BIG_HR);
			this->panel1->Controls->Add(this->label3);
			this->panel1->Controls->Add(this->BIG_MAP);
			this->panel1->Controls->Add(this->label_WTMS);
			this->panel1->Controls->Add(this->BIG_SPO2);
			this->panel1->Controls->Add(this->lab_PAP);
			this->panel1->Controls->Add(this->label2);
			this->panel1->Controls->Add(this->label1);
			this->panel1->Controls->Add(this->label5);
			this->panel1->Location = System::Drawing::Point(1140, 0);
			this->panel1->Name = L"panel1";
			this->panel1->Size = System::Drawing::Size(200, 653);
			this->panel1->TabIndex = 0;
			// 
			// label_ProcSpeed
			// 
			this->label_ProcSpeed->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->label_ProcSpeed->AutoSize = true;
			this->label_ProcSpeed->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->label_ProcSpeed->Location = System::Drawing::Point(164, 598);
			this->label_ProcSpeed->Name = L"label_ProcSpeed";
			this->label_ProcSpeed->Size = System::Drawing::Size(13, 13);
			this->label_ProcSpeed->TabIndex = 45;
			this->label_ProcSpeed->Text = L"0";
			// 
			// label23
			// 
			this->label23->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->label23->AutoSize = true;
			this->label23->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->label23->Location = System::Drawing::Point(119, 598);
			this->label23->Name = L"label23";
			this->label23->Size = System::Drawing::Size(33, 13);
			this->label23->TabIndex = 44;
			this->label23->Text = L"   PS:";
			// 
			// label_WP_PacketsInBuffer
			// 
			this->label_WP_PacketsInBuffer->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->label_WP_PacketsInBuffer->AutoSize = true;
			this->label_WP_PacketsInBuffer->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->label_WP_PacketsInBuffer->Location = System::Drawing::Point(92, 598);
			this->label_WP_PacketsInBuffer->Name = L"label_WP_PacketsInBuffer";
			this->label_WP_PacketsInBuffer->Size = System::Drawing::Size(13, 13);
			this->label_WP_PacketsInBuffer->TabIndex = 43;
			this->label_WP_PacketsInBuffer->Text = L"0";
			// 
			// label22
			// 
			this->label22->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->label22->AutoSize = true;
			this->label22->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->label22->Location = System::Drawing::Point(47, 598);
			this->label22->Name = L"label22";
			this->label22->Size = System::Drawing::Size(38, 13);
			this->label22->TabIndex = 42;
			this->label22->Text = L"WP-P:";
			// 
			// BIG_CO2
			// 
			this->BIG_CO2->AutoSize = true;
			this->BIG_CO2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 48, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->BIG_CO2->ForeColor = System::Drawing::Color::White;
			this->BIG_CO2->Location = System::Drawing::Point(2, 411);
			this->BIG_CO2->Name = L"BIG_CO2";
			this->BIG_CO2->Size = System::Drawing::Size(76, 73);
			this->BIG_CO2->TabIndex = 32;
			this->BIG_CO2->Text = L"--";
			this->BIG_CO2->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// BIG_BP
			// 
			this->BIG_BP->AutoSize = true;
			this->BIG_BP->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 40, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->BIG_BP->ForeColor = System::Drawing::Color::Red;
			this->BIG_BP->Location = System::Drawing::Point(1, 197);
			this->BIG_BP->Name = L"BIG_BP";
			this->BIG_BP->Size = System::Drawing::Size(119, 63);
			this->BIG_BP->TabIndex = 31;
			this->BIG_BP->Text = L"--/--";
			// 
			// panel12
			// 
			this->panel12->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->panel12->BackColor = System::Drawing::SystemColors::ButtonFace;
			this->panel12->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->panel12->Controls->Add(this->pictureBox3);
			this->panel12->Controls->Add(this->label_CLOCK);
			this->panel12->Location = System::Drawing::Point(20, 555);
			this->panel12->Name = L"panel12";
			this->panel12->Size = System::Drawing::Size(177, 40);
			this->panel12->TabIndex = 41;
			this->panel12->Visible = false;
			// 
			// pictureBox3
			// 
			this->pictureBox3->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"pictureBox3.Image")));
			this->pictureBox3->Location = System::Drawing::Point(3, 1);
			this->pictureBox3->Name = L"pictureBox3";
			this->pictureBox3->Size = System::Drawing::Size(37, 36);
			this->pictureBox3->TabIndex = 55;
			this->pictureBox3->TabStop = false;
			// 
			// label_CLOCK
			// 
			this->label_CLOCK->AutoSize = true;
			this->label_CLOCK->Font = (gcnew System::Drawing::Font(L"Impact", 22, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->label_CLOCK->ForeColor = System::Drawing::Color::Navy;
			this->label_CLOCK->Location = System::Drawing::Point(43, 1);
			this->label_CLOCK->Name = L"label_CLOCK";
			this->label_CLOCK->Size = System::Drawing::Size(133, 37);
			this->label_CLOCK->TabIndex = 38;
			this->label_CLOCK->Text = L"00:00:00";
			this->label_CLOCK->Click += gcnew System::EventHandler(this, &WaveMonitor::label_CLOCK_Click);
			// 
			// label6
			// 
			this->label6->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->label6->AutoSize = true;
			this->label6->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 18, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->label6->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->label6->Location = System::Drawing::Point(10, 458);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(53, 29);
			this->label6->TabIndex = 32;
			this->label6->Text = L"RR:";
			// 
			// lab_RR
			// 
			this->lab_RR->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->lab_RR->AutoSize = true;
			this->lab_RR->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 18, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->lab_RR->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->lab_RR->Location = System::Drawing::Point(69, 458);
			this->lab_RR->Name = L"lab_RR";
			this->lab_RR->Size = System::Drawing::Size(26, 29);
			this->lab_RR->TabIndex = 34;
			this->lab_RR->Text = L"0";
			// 
			// label_GCPS
			// 
			this->label_GCPS->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->label_GCPS->AutoSize = true;
			this->label_GCPS->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->label_GCPS->Location = System::Drawing::Point(92, 611);
			this->label_GCPS->Name = L"label_GCPS";
			this->label_GCPS->Size = System::Drawing::Size(13, 13);
			this->label_GCPS->TabIndex = 3;
			this->label_GCPS->Text = L"0";
			// 
			// BIG_HR
			// 
			this->BIG_HR->AutoSize = true;
			this->BIG_HR->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 48, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->BIG_HR->ForeColor = System::Drawing::Color::LimeGreen;
			this->BIG_HR->Location = System::Drawing::Point(2, 9);
			this->BIG_HR->Name = L"BIG_HR";
			this->BIG_HR->Size = System::Drawing::Size(76, 73);
			this->BIG_HR->TabIndex = 26;
			this->BIG_HR->Text = L"--";
			// 
			// label3
			// 
			this->label3->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->label3->AutoSize = true;
			this->label3->ForeColor = System::Drawing::Color::Gold;
			this->label3->Location = System::Drawing::Point(126, 624);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(35, 13);
			this->label3->TabIndex = 38;
			this->label3->Text = L"label3";
			// 
			// BIG_MAP
			// 
			this->BIG_MAP->AutoSize = true;
			this->BIG_MAP->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 32, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->BIG_MAP->ForeColor = System::Drawing::Color::Red;
			this->BIG_MAP->Location = System::Drawing::Point(17, 260);
			this->BIG_MAP->Name = L"BIG_MAP";
			this->BIG_MAP->Size = System::Drawing::Size(82, 51);
			this->BIG_MAP->TabIndex = 27;
			this->BIG_MAP->Text = L"(--)";
			// 
			// label_WTMS
			// 
			this->label_WTMS->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->label_WTMS->AutoSize = true;
			this->label_WTMS->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->label_WTMS->Location = System::Drawing::Point(92, 624);
			this->label_WTMS->Name = L"label_WTMS";
			this->label_WTMS->Size = System::Drawing::Size(13, 13);
			this->label_WTMS->TabIndex = 2;
			this->label_WTMS->Text = L"0";
			// 
			// BIG_SPO2
			// 
			this->BIG_SPO2->AutoSize = true;
			this->BIG_SPO2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 48, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->BIG_SPO2->ForeColor = System::Drawing::Color::MediumTurquoise;
			this->BIG_SPO2->Location = System::Drawing::Point(2, 328);
			this->BIG_SPO2->Name = L"BIG_SPO2";
			this->BIG_SPO2->Size = System::Drawing::Size(76, 73);
			this->BIG_SPO2->TabIndex = 28;
			this->BIG_SPO2->Text = L"--";
			this->BIG_SPO2->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// lab_PAP
			// 
			this->lab_PAP->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->lab_PAP->AutoSize = true;
			this->lab_PAP->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->lab_PAP->Location = System::Drawing::Point(163, 611);
			this->lab_PAP->Name = L"lab_PAP";
			this->lab_PAP->Size = System::Drawing::Size(13, 13);
			this->lab_PAP->TabIndex = 35;
			this->lab_PAP->Text = L"0";
			// 
			// label2
			// 
			this->label2->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->label2->AutoSize = true;
			this->label2->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->label2->Location = System::Drawing::Point(47, 611);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(39, 13);
			this->label2->TabIndex = 1;
			this->label2->Text = L"GCPS:";
			// 
			// label1
			// 
			this->label1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->label1->AutoSize = true;
			this->label1->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->label1->Location = System::Drawing::Point(42, 624);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(44, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"WTMS:";
			// 
			// label5
			// 
			this->label5->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->label5->AutoSize = true;
			this->label5->ForeColor = System::Drawing::SystemColors::ControlLightLight;
			this->label5->Location = System::Drawing::Point(126, 611);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(31, 13);
			this->label5->TabIndex = 33;
			this->label5->Text = L"PAP:";
			// 
			// GB_lab3
			// 
			this->GB_lab3->AutoSize = true;
			this->GB_lab3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 48, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->GB_lab3->ForeColor = System::Drawing::Color::White;
			this->GB_lab3->Location = System::Drawing::Point(6, 15);
			this->GB_lab3->Name = L"GB_lab3";
			this->GB_lab3->Size = System::Drawing::Size(76, 73);
			this->GB_lab3->TabIndex = 29;
			this->GB_lab3->Text = L"--";
			// 
			// GB_lab5
			// 
			this->GB_lab5->AutoSize = true;
			this->GB_lab5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 48, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->GB_lab5->ForeColor = System::Drawing::Color::White;
			this->GB_lab5->Location = System::Drawing::Point(6, 15);
			this->GB_lab5->Name = L"GB_lab5";
			this->GB_lab5->Size = System::Drawing::Size(76, 73);
			this->GB_lab5->TabIndex = 30;
			this->GB_lab5->Text = L"--";
			// 
			// WaveTimer
			// 
			this->WaveTimer->Interval = 47;
			this->WaveTimer->Tick += gcnew System::EventHandler(this, &WaveMonitor::WaveTimer_Tick);
			// 
			// timer_pacing
			// 
			this->timer_pacing->Interval = 1000;
			this->timer_pacing->Tick += gcnew System::EventHandler(this, &WaveMonitor::timer1_Tick);
			// 
			// panel3
			// 
			this->panel3->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
				| System::Windows::Forms::AnchorStyles::Left));
			this->panel3->BackColor = System::Drawing::Color::DarkGray;
			this->panel3->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->panel3->Controls->Add(this->label_TotalBlood);
			this->panel3->Controls->Add(this->label11);
			this->panel3->Controls->Add(this->label_TotalColl);
			this->panel3->Controls->Add(this->label9);
			this->panel3->Controls->Add(this->label_TotalCry);
			this->panel3->Controls->Add(this->label4);
			this->panel3->Controls->Add(this->label25);
			this->panel3->Controls->Add(this->dataGridView1);
			this->panel3->Controls->Add(this->pictureBox_cancelDrug);
			this->panel3->Controls->Add(this->label27);
			this->panel3->Controls->Add(this->label26);
			this->panel3->Controls->Add(this->label_DrugConf);
			this->panel3->Controls->Add(this->button_Administer);
			this->panel3->Controls->Add(this->label_dripUnits);
			this->panel3->Controls->Add(this->label_BolusUnits);
			this->panel3->Controls->Add(this->label24);
			this->panel3->Controls->Add(this->label21);
			this->panel3->Controls->Add(this->cb_Drip);
			this->panel3->Controls->Add(this->cb_Bolus);
			this->panel3->Controls->Add(this->cb_Drugs);
			this->panel3->Controls->Add(this->button2);
			this->panel3->Location = System::Drawing::Point(0, 0);
			this->panel3->Name = L"panel3";
			this->panel3->Size = System::Drawing::Size(333, 653);
			this->panel3->TabIndex = 42;
			// 
			// label25
			// 
			this->label25->AutoSize = true;
			this->label25->Location = System::Drawing::Point(9, 339);
			this->label25->Name = L"label25";
			this->label25->Size = System::Drawing::Size(307, 13);
			this->label25->TabIndex = 70;
			this->label25->Text = L"__________________________________________________";
			// 
			// dataGridView1
			// 
			this->dataGridView1->AllowUserToAddRows = false;
			this->dataGridView1->AllowUserToDeleteRows = false;
			this->dataGridView1->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dataGridView1->ColumnHeadersVisible = false;
			this->dataGridView1->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(9) {this->Drug, this->Dose, 
				this->units, this->Cancel, this->ID, this->Given, this->IsBolusAsInfusion, this->IsBolus, this->UID});
			this->dataGridView1->GridColor = System::Drawing::SystemColors::AppWorkspace;
			this->dataGridView1->Location = System::Drawing::Point(3, 159);
			this->dataGridView1->Name = L"dataGridView1";
			this->dataGridView1->ReadOnly = true;
			this->dataGridView1->RowHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::None;
			this->dataGridView1->RowHeadersVisible = false;
			this->dataGridView1->Size = System::Drawing::Size(320, 177);
			this->dataGridView1->TabIndex = 69;
			this->dataGridView1->CellContentClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &WaveMonitor::dataGridView1_CellContentClick);
			// 
			// Drug
			// 
			dataGridViewCellStyle1->BackColor = System::Drawing::SystemColors::AppWorkspace;
			dataGridViewCellStyle1->SelectionBackColor = System::Drawing::SystemColors::AppWorkspace;
			dataGridViewCellStyle1->SelectionForeColor = System::Drawing::Color::Black;
			this->Drug->DefaultCellStyle = dataGridViewCellStyle1;
			this->Drug->HeaderText = L"Drug";
			this->Drug->Name = L"Drug";
			this->Drug->ReadOnly = true;
			this->Drug->Width = 90;
			// 
			// Dose
			// 
			dataGridViewCellStyle2->BackColor = System::Drawing::SystemColors::AppWorkspace;
			dataGridViewCellStyle2->SelectionBackColor = System::Drawing::SystemColors::AppWorkspace;
			dataGridViewCellStyle2->SelectionForeColor = System::Drawing::Color::Black;
			this->Dose->DefaultCellStyle = dataGridViewCellStyle2;
			this->Dose->HeaderText = L"Dose";
			this->Dose->Name = L"Dose";
			this->Dose->ReadOnly = true;
			this->Dose->Width = 50;
			// 
			// units
			// 
			dataGridViewCellStyle3->BackColor = System::Drawing::SystemColors::AppWorkspace;
			dataGridViewCellStyle3->SelectionBackColor = System::Drawing::SystemColors::AppWorkspace;
			dataGridViewCellStyle3->SelectionForeColor = System::Drawing::Color::Black;
			this->units->DefaultCellStyle = dataGridViewCellStyle3;
			this->units->HeaderText = L"Units";
			this->units->Name = L"units";
			this->units->ReadOnly = true;
			this->units->Width = 60;
			// 
			// Cancel
			// 
			this->Cancel->HeaderText = L"Cancel";
			this->Cancel->Name = L"Cancel";
			this->Cancel->ReadOnly = true;
			this->Cancel->Text = L"D/C";
			this->Cancel->ToolTipText = L"Discontinue";
			this->Cancel->Width = 50;
			// 
			// ID
			// 
			this->ID->HeaderText = L"ID";
			this->ID->Name = L"ID";
			this->ID->ReadOnly = true;
			this->ID->Visible = false;
			// 
			// UID
			// 
			this->UID->HeaderText = L"UID";
			this->UID->Name = L"UID";
			this->UID->ReadOnly = true;
			this->UID->Visible = false;
			// 
			// Given
			// 
			dataGridViewCellStyle4->BackColor = System::Drawing::SystemColors::AppWorkspace;
			dataGridViewCellStyle4->ForeColor = System::Drawing::Color::Maroon;
			dataGridViewCellStyle4->Format = L"N2";
			dataGridViewCellStyle4->NullValue = nullptr;
			dataGridViewCellStyle4->SelectionBackColor = System::Drawing::SystemColors::AppWorkspace;
			dataGridViewCellStyle4->SelectionForeColor = System::Drawing::Color::Maroon;
			this->Given->DefaultCellStyle = dataGridViewCellStyle4;
			this->Given->HeaderText = L"Given";
			this->Given->Name = L"Given";
			this->Given->ReadOnly = true;
			this->Given->Width = 60;
			// 
			// IsBolusAsInfusion
			// 
			this->IsBolusAsInfusion->HeaderText = L"IsBolusAsInfusion";
			this->IsBolusAsInfusion->Name = L"IsBolusAsInfusion";
			this->IsBolusAsInfusion->ReadOnly = true;
			this->IsBolusAsInfusion->Visible = false;
			// 
			// IsBolus
			// 
			this->IsBolus->HeaderText = L"IsBolus";
			this->IsBolus->Name = L"IsBolus";
			this->IsBolus->ReadOnly = true;
			this->IsBolus->Visible = false;
			// 
			// pictureBox_cancelDrug
			// 
			this->pictureBox_cancelDrug->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"pictureBox_cancelDrug.Image")));
			this->pictureBox_cancelDrug->Location = System::Drawing::Point(287, 118);
			this->pictureBox_cancelDrug->Name = L"pictureBox_cancelDrug";
			this->pictureBox_cancelDrug->Size = System::Drawing::Size(27, 25);
			this->pictureBox_cancelDrug->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox_cancelDrug->TabIndex = 68;
			this->pictureBox_cancelDrug->TabStop = false;
			this->pictureBox_cancelDrug->Visible = false;
			this->pictureBox_cancelDrug->Click += gcnew System::EventHandler(this, &WaveMonitor::pictureBox_cancelDrug_Click);
			// 
			// label27
			// 
			this->label27->AutoSize = true;
			this->label27->Location = System::Drawing::Point(7, 137);
			this->label27->Name = L"label27";
			this->label27->Size = System::Drawing::Size(307, 13);
			this->label27->TabIndex = 66;
			this->label27->Text = L"__________________________________________________";
			// 
			// label26
			// 
			this->label26->AutoSize = true;
			this->label26->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->label26->Location = System::Drawing::Point(10, 11);
			this->label26->Name = L"label26";
			this->label26->Size = System::Drawing::Size(168, 20);
			this->label26->TabIndex = 65;
			this->label26->Text = L"Drug Administration";
			// 
			// label_DrugConf
			// 
			this->label_DrugConf->AutoSize = true;
			this->label_DrugConf->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->label_DrugConf->ForeColor = System::Drawing::Color::DarkRed;
			this->label_DrugConf->Location = System::Drawing::Point(9, 124);
			this->label_DrugConf->Name = L"label_DrugConf";
			this->label_DrugConf->Size = System::Drawing::Size(108, 13);
			this->label_DrugConf->TabIndex = 64;
			this->label_DrugConf->Text = L"Drug Confirmation";
			this->label_DrugConf->Visible = false;
			// 
			// button_Administer
			// 
			this->button_Administer->Enabled = false;
			this->button_Administer->Location = System::Drawing::Point(245, 91);
			this->button_Administer->Name = L"button_Administer";
			this->button_Administer->Size = System::Drawing::Size(71, 23);
			this->button_Administer->TabIndex = 63;
			this->button_Administer->Text = L"Administer";
			this->button_Administer->UseVisualStyleBackColor = true;
			this->button_Administer->Click += gcnew System::EventHandler(this, &WaveMonitor::button_Administer_Click);
			// 
			// label_dripUnits
			// 
			this->label_dripUnits->AutoSize = true;
			this->label_dripUnits->Location = System::Drawing::Point(175, 94);
			this->label_dripUnits->Name = L"label_dripUnits";
			this->label_dripUnits->Size = System::Drawing::Size(31, 13);
			this->label_dripUnits->TabIndex = 62;
			this->label_dripUnits->Text = L"Units";
			// 
			// label_BolusUnits
			// 
			this->label_BolusUnits->AutoSize = true;
			this->label_BolusUnits->Location = System::Drawing::Point(175, 67);
			this->label_BolusUnits->Name = L"label_BolusUnits";
			this->label_BolusUnits->Size = System::Drawing::Size(31, 13);
			this->label_BolusUnits->TabIndex = 61;
			this->label_BolusUnits->Text = L"Units";
			// 
			// label24
			// 
			this->label24->AutoSize = true;
			this->label24->Location = System::Drawing::Point(9, 94);
			this->label24->Name = L"label24";
			this->label24->Size = System::Drawing::Size(26, 13);
			this->label24->TabIndex = 60;
			this->label24->Text = L"Drip";
			// 
			// label21
			// 
			this->label21->AutoSize = true;
			this->label21->Location = System::Drawing::Point(9, 67);
			this->label21->Name = L"label21";
			this->label21->Size = System::Drawing::Size(33, 13);
			this->label21->TabIndex = 59;
			this->label21->Text = L"Bolus";
			// 
			// cb_Drip
			// 
			this->cb_Drip->Enabled = false;
			this->cb_Drip->FormattingEnabled = true;
			this->cb_Drip->Location = System::Drawing::Point(48, 91);
			this->cb_Drip->Name = L"cb_Drip";
			this->cb_Drip->Size = System::Drawing::Size(121, 21);
			this->cb_Drip->TabIndex = 58;
			this->cb_Drip->SelectedIndexChanged += gcnew System::EventHandler(this, &WaveMonitor::cb_Drip_SelectedIndexChanged);
			// 
			// cb_Bolus
			// 
			this->cb_Bolus->Enabled = false;
			this->cb_Bolus->FormattingEnabled = true;
			this->cb_Bolus->Location = System::Drawing::Point(48, 64);
			this->cb_Bolus->Name = L"cb_Bolus";
			this->cb_Bolus->Size = System::Drawing::Size(121, 21);
			this->cb_Bolus->TabIndex = 57;
			this->cb_Bolus->SelectedIndexChanged += gcnew System::EventHandler(this, &WaveMonitor::cb_Bolus_SelectedIndexChanged);
			// 
			// cb_Drugs
			// 
			this->cb_Drugs->FormattingEnabled = true;
			this->cb_Drugs->Location = System::Drawing::Point(10, 38);
			this->cb_Drugs->Name = L"cb_Drugs";
			this->cb_Drugs->Size = System::Drawing::Size(290, 21);
			this->cb_Drugs->TabIndex = 56;
			this->cb_Drugs->SelectedIndexChanged += gcnew System::EventHandler(this, &WaveMonitor::cb_Drugs_SelectedIndexChanged);
			// 
			// button2
			// 
			this->button2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->button2->Location = System::Drawing::Point(12, 586);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(59, 43);
			this->button2->TabIndex = 8;
			this->button2->Text = L"High Speed";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &WaveMonitor::button2_Click);
			// 
			// panel_Phase
			// 
			this->panel_Phase->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->panel_Phase->BackColor = System::Drawing::Color::WhiteSmoke;
			this->panel_Phase->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->panel_Phase->Controls->Add(this->label20);
			this->panel_Phase->Controls->Add(this->label19);
			this->panel_Phase->Controls->Add(this->label18);
			this->panel_Phase->Controls->Add(this->label17);
			this->panel_Phase->Controls->Add(this->label16);
			this->panel_Phase->Controls->Add(this->label15);
			this->panel_Phase->Controls->Add(this->label14);
			this->panel_Phase->Controls->Add(this->button1);
			this->panel_Phase->Location = System::Drawing::Point(1, 461);
			this->panel_Phase->Name = L"panel_Phase";
			this->panel_Phase->Size = System::Drawing::Size(332, 187);
			this->panel_Phase->TabIndex = 43;
			this->panel_Phase->Visible = false;
			// 
			// label20
			// 
			this->label20->AutoSize = true;
			this->label20->Location = System::Drawing::Point(17, 155);
			this->label20->Name = L"label20";
			this->label20->Size = System::Drawing::Size(41, 13);
			this->label20->TabIndex = 7;
			this->label20->Text = L"label20";
			// 
			// label19
			// 
			this->label19->AutoSize = true;
			this->label19->Location = System::Drawing::Point(17, 129);
			this->label19->Name = L"label19";
			this->label19->Size = System::Drawing::Size(41, 13);
			this->label19->TabIndex = 6;
			this->label19->Text = L"label19";
			// 
			// label18
			// 
			this->label18->AutoSize = true;
			this->label18->Location = System::Drawing::Point(17, 105);
			this->label18->Name = L"label18";
			this->label18->Size = System::Drawing::Size(41, 13);
			this->label18->TabIndex = 5;
			this->label18->Text = L"label18";
			// 
			// label17
			// 
			this->label17->AutoSize = true;
			this->label17->Location = System::Drawing::Point(17, 81);
			this->label17->Name = L"label17";
			this->label17->Size = System::Drawing::Size(41, 13);
			this->label17->TabIndex = 4;
			this->label17->Text = L"label17";
			// 
			// label16
			// 
			this->label16->AutoSize = true;
			this->label16->Location = System::Drawing::Point(17, 56);
			this->label16->Name = L"label16";
			this->label16->Size = System::Drawing::Size(41, 13);
			this->label16->TabIndex = 3;
			this->label16->Text = L"label16";
			// 
			// label15
			// 
			this->label15->AutoSize = true;
			this->label15->Location = System::Drawing::Point(17, 31);
			this->label15->Name = L"label15";
			this->label15->Size = System::Drawing::Size(41, 13);
			this->label15->TabIndex = 2;
			this->label15->Text = L"label15";
			// 
			// label14
			// 
			this->label14->AutoSize = true;
			this->label14->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Underline, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->label14->ForeColor = System::Drawing::SystemColors::ActiveCaptionText;
			this->label14->Location = System::Drawing::Point(3, 1);
			this->label14->Name = L"label14";
			this->label14->Size = System::Drawing::Size(140, 20);
			this->label14->TabIndex = 1;
			this->label14->Text = L"Simulation Phases";
			// 
			// button1
			// 
			this->button1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->button1->Location = System::Drawing::Point(263, 6);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(59, 43);
			this->button1->TabIndex = 0;
			this->button1->Text = L"High Speed";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &WaveMonitor::button1_Click);
			// 
			// GB1
			// 
			this->GB1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->GB1->Controls->Add(this->GB_lab_1);
			this->GB1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->GB1->ForeColor = System::Drawing::Color::White;
			this->GB1->Location = System::Drawing::Point(339, 454);
			this->GB1->Name = L"GB1";
			this->GB1->Size = System::Drawing::Size(260, 91);
			this->GB1->TabIndex = 41;
			this->GB1->TabStop = false;
			this->GB1->Text = L"GB1";
			// 
			// GB_lab_1
			// 
			this->GB_lab_1->AutoSize = true;
			this->GB_lab_1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 48, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->GB_lab_1->ForeColor = System::Drawing::Color::White;
			this->GB_lab_1->Location = System::Drawing::Point(6, 15);
			this->GB_lab_1->Name = L"GB_lab_1";
			this->GB_lab_1->Size = System::Drawing::Size(76, 73);
			this->GB_lab_1->TabIndex = 30;
			this->GB_lab_1->Text = L"--";
			// 
			// GB3
			// 
			this->GB3->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->GB3->Controls->Add(this->GB_lab3);
			this->GB3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->GB3->ForeColor = System::Drawing::Color::White;
			this->GB3->Location = System::Drawing::Point(608, 454);
			this->GB3->Name = L"GB3";
			this->GB3->Size = System::Drawing::Size(260, 91);
			this->GB3->TabIndex = 37;
			this->GB3->TabStop = false;
			this->GB3->Text = L"GB3";
			// 
			// GB6
			// 
			this->GB6->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->GB6->Controls->Add(this->GB_lab6);
			this->GB6->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->GB6->ForeColor = System::Drawing::Color::White;
			this->GB6->Location = System::Drawing::Point(874, 554);
			this->GB6->Name = L"GB6";
			this->GB6->Size = System::Drawing::Size(260, 91);
			this->GB6->TabIndex = 37;
			this->GB6->TabStop = false;
			this->GB6->Text = L"GB6";
			// 
			// GB_lab6
			// 
			this->GB_lab6->AutoSize = true;
			this->GB_lab6->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 48, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->GB_lab6->ForeColor = System::Drawing::Color::White;
			this->GB_lab6->Location = System::Drawing::Point(6, 15);
			this->GB_lab6->Name = L"GB_lab6";
			this->GB_lab6->Size = System::Drawing::Size(199, 73);
			this->GB_lab6->TabIndex = 30;
			this->GB_lab6->Text = L"60/40";
			// 
			// GB4
			// 
			this->GB4->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->GB4->Controls->Add(this->GB_lab4);
			this->GB4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->GB4->ForeColor = System::Drawing::Color::White;
			this->GB4->Location = System::Drawing::Point(608, 554);
			this->GB4->Name = L"GB4";
			this->GB4->Size = System::Drawing::Size(260, 91);
			this->GB4->TabIndex = 39;
			this->GB4->TabStop = false;
			this->GB4->Text = L"GB4";
			// 
			// GB_lab4
			// 
			this->GB_lab4->AutoSize = true;
			this->GB_lab4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 48, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->GB_lab4->ForeColor = System::Drawing::Color::White;
			this->GB_lab4->Location = System::Drawing::Point(6, 15);
			this->GB_lab4->Name = L"GB_lab4";
			this->GB_lab4->Size = System::Drawing::Size(76, 73);
			this->GB_lab4->TabIndex = 30;
			this->GB_lab4->Text = L"--";
			// 
			// GB2
			// 
			this->GB2->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->GB2->Controls->Add(this->GB_lab_2);
			this->GB2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->GB2->ForeColor = System::Drawing::Color::White;
			this->GB2->Location = System::Drawing::Point(339, 554);
			this->GB2->Name = L"GB2";
			this->GB2->Size = System::Drawing::Size(260, 91);
			this->GB2->TabIndex = 37;
			this->GB2->TabStop = false;
			this->GB2->Text = L"GB2";
			// 
			// GB_lab_2
			// 
			this->GB_lab_2->AutoSize = true;
			this->GB_lab_2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 48, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->GB_lab_2->ForeColor = System::Drawing::Color::White;
			this->GB_lab_2->Location = System::Drawing::Point(6, 15);
			this->GB_lab_2->Name = L"GB_lab_2";
			this->GB_lab_2->Size = System::Drawing::Size(76, 73);
			this->GB_lab_2->TabIndex = 30;
			this->GB_lab_2->Text = L"--";
			// 
			// GB5
			// 
			this->GB5->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->GB5->Controls->Add(this->GB_lab5);
			this->GB5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->GB5->ForeColor = System::Drawing::Color::White;
			this->GB5->Location = System::Drawing::Point(874, 454);
			this->GB5->Name = L"GB5";
			this->GB5->Size = System::Drawing::Size(260, 91);
			this->GB5->TabIndex = 36;
			this->GB5->TabStop = false;
			this->GB5->Text = L"GB5";
			// 
			// panel_black
			// 
			this->panel_black->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->panel_black->BackColor = System::Drawing::Color::Black;
			this->panel_black->Location = System::Drawing::Point(0, 461);
			this->panel_black->Name = L"panel_black";
			this->panel_black->Size = System::Drawing::Size(339, 188);
			this->panel_black->TabIndex = 44;
			this->panel_black->Visible = false;
			// 
			// PictureSaveImage
			// 
			this->PictureSaveImage->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
				| System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->PictureSaveImage->Location = System::Drawing::Point(-163, 24);
			this->PictureSaveImage->Name = L"PictureSaveImage";
			this->PictureSaveImage->Size = System::Drawing::Size(1337, 408);
			this->PictureSaveImage->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->PictureSaveImage->TabIndex = 45;
			this->PictureSaveImage->TabStop = false;
			// 
			// timer_Drug
			// 
			this->timer_Drug->Interval = 4000;
			this->timer_Drug->Tick += gcnew System::EventHandler(this, &WaveMonitor::timer_Drug_Tick);
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(15, 367);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(81, 13);
			this->label4->TabIndex = 71;
			this->label4->Text = L"Total Crystalloid";
			// 
			// label_TotalCry
			// 
			this->label_TotalCry->Location = System::Drawing::Point(231, 367);
			this->label_TotalCry->Name = L"label_TotalCry";
			this->label_TotalCry->Size = System::Drawing::Size(83, 13);
			this->label_TotalCry->TabIndex = 72;
			this->label_TotalCry->Text = L"0";
			this->label_TotalCry->TextAlign = System::Drawing::ContentAlignment::TopRight;
			// 
			// label_TotalColl
			// 
			this->label_TotalColl->Location = System::Drawing::Point(231, 387);
			this->label_TotalColl->Name = L"label_TotalColl";
			this->label_TotalColl->Size = System::Drawing::Size(83, 13);
			this->label_TotalColl->TabIndex = 74;
			this->label_TotalColl->Text = L"0";
			this->label_TotalColl->TextAlign = System::Drawing::ContentAlignment::TopRight;
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(15, 387);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(65, 13);
			this->label9->TabIndex = 73;
			this->label9->Text = L"Total Colloid";
			// 
			// label_TotalBlood
			// 
			this->label_TotalBlood->Location = System::Drawing::Point(231, 407);
			this->label_TotalBlood->Name = L"label_TotalBlood";
			this->label_TotalBlood->Size = System::Drawing::Size(83, 13);
			this->label_TotalBlood->TabIndex = 76;
			this->label_TotalBlood->Text = L"0";
			this->label_TotalBlood->TextAlign = System::Drawing::ContentAlignment::TopRight;
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->Location = System::Drawing::Point(15, 407);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(101, 13);
			this->label11->TabIndex = 75;
			this->label11->Text = L"Total Blood Product";
			// 
			// WaveMonitor
			// 
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::None;
			this->BackColor = System::Drawing::SystemColors::ActiveCaptionText;
			this->ClientSize = System::Drawing::Size(1339, 650);
			this->Controls->Add(this->panel3);
			this->Controls->Add(this->PictureSaveImage);
			this->Controls->Add(this->GB1);
			this->Controls->Add(this->GB3);
			this->Controls->Add(this->panel1);
			this->Controls->Add(this->GB6);
			this->Controls->Add(this->GB4);
			this->Controls->Add(this->GB5);
			this->Controls->Add(this->GB2);
			this->Controls->Add(this->panel_black);
			this->Controls->Add(this->panel_Phase);
			this->Icon = (cli::safe_cast<System::Drawing::Icon^  >(resources->GetObject(L"$this.Icon")));
			this->MinimumSize = System::Drawing::Size(1143, 620);
			this->Name = L"WaveMonitor";
			this->Text = L"Monitor";
			this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &WaveMonitor::WaveMonitor_FormClosing);
			this->SizeChanged += gcnew System::EventHandler(this, &WaveMonitor::WaveMonitor_SizeChanged);
			this->panel1->ResumeLayout(false);
			this->panel1->PerformLayout();
			this->panel12->ResumeLayout(false);
			this->panel12->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox3))->EndInit();
			this->panel3->ResumeLayout(false);
			this->panel3->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox_cancelDrug))->EndInit();
			this->panel_Phase->ResumeLayout(false);
			this->panel_Phase->PerformLayout();
			this->GB1->ResumeLayout(false);
			this->GB1->PerformLayout();
			this->GB3->ResumeLayout(false);
			this->GB3->PerformLayout();
			this->GB6->ResumeLayout(false);
			this->GB6->PerformLayout();
			this->GB4->ResumeLayout(false);
			this->GB4->PerformLayout();
			this->GB2->ResumeLayout(false);
			this->GB2->PerformLayout();
			this->GB5->ResumeLayout(false);
			this->GB5->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->PictureSaveImage))->EndInit();
			this->ResumeLayout(false);
		}
#pragma endregion
	public: void MonitorOn(void)
			{
				this->WaveTimer->Start();
				this->timer_pacing->Start();
				#ifdef HIDE_PICTURESSAVEIMAGE
				this->PictureSaveImage->Visible=false;
				#endif
			}
	public: void MonitorStandby(void)
			{
				this->WaveTimer->Stop();
				this->timer_pacing->Stop();
					
				#ifdef HIDE_PICTURESSAVEIMAGE
				this->PictureSaveImage->Visible=true;
				#endif
			}
	public: System::Void SetVitalSlot(int SlotNum, String^ NewName, Drawing::Color^ NewColor) {
				 if (SlotNum > VITALSLOTS) {return;}

				 this->Slots[SlotNum-1]->active = 1;
				 this->Slots[SlotNum-1]->Label->ForeColor = *NewColor;
				 this->Slots[SlotNum-1]->Name = NewName;
				 this->Slots[SlotNum-1]->Box->Visible = true;
				 this->Slots[SlotNum-1]->Box->Text = NewName;
			 }
	 public: System::Void ClearVitalSlot(int SlotNum) {
				 if (SlotNum > VITALSLOTS) {return;}
				 this->Slots[SlotNum-1]->active = 0;
				 this->Slots[SlotNum-1]->Box->Visible = false;
			 }	
	 public: void SetCO2on(void)
			 {
				 this->CO2on=true;
				 this->BIG_CO2->Visible = true;
			 }
	 public: void SetARTon(void)
			 {
				 this->ARTon=true;
				 this->BIG_MAP->Visible = true;
				 this->BIG_BP->Visible = true;
			 }
	 public: void SetCO2off(void)
			 {
				 this->CO2on=false;
				 this->BIG_CO2->Visible=false;
			 }
	 public: void SetEKGon(void)
			 {
				 this->EKGon=true;
				 this->BIG_HR->Visible = true;
			 }
	
	 public: void SetEKGoff(void)
			 {
				 this->EKGon=false;
				 this->BIG_HR->Visible = false;
			 }
	 public: void SetPulseOxon(void)
			 {
				 this->PulseOxon=true;
				 this->BIG_SPO2->Visible = true;
			 }
	
	 public: void SetPulseOxoff(void)
			 {
				 this->PulseOxon=false;
				 this->BIG_SPO2->Visible = false;
			 }
	 public: void SetARToff(void)
			 {
				 this->ARTon=false;
				 this->BIG_MAP->Visible = false;
				 this->BIG_BP->Visible = false;
			 }
	 public: System::Void ClearGraphs(void)
			 {
				 this->Graph1->ClearValues();
				 this->Graph2->ClearValues();
			 }
	 public: System::Void UpdateVitalSlot(int SlotNum,String^ T)
			 {
				 this->Slots[SlotNum-1]->Label->Text = T;
			 }
	 public: String^ getSlotId(int slotnum)
			 {
				return this->Slots[slotnum-1]->Box->Text;
			 }

	private: System::Void WaveMonitor_SizeChanged(System::Object^  sender, System::EventArgs^  e) {
				 float mult = (float(this->Height)/720);
				 int panelX = int(200 * mult);

				 this->PictureSaveImage->Width = this->Width;
				 this->PictureSaveImage->Height = this->Height;
			
				 System::Drawing::Graphics^ G = this->CreateGraphics();
				 System::Drawing::Pen^ P = gcnew System::Drawing::Pen(System::Drawing::Color::Black);
				 //G->DrawRectangle(P,0,0,this->Width,this->Height);
				 System::Drawing::Rectangle^ R= gcnew System::Drawing::Rectangle(333,0,this->Width,this->Height);
				 G->DrawRectangle(P,*R);
				 G->FillRectangle(P->Brush,*R);

				 Wavemon->DefineWaveSpace(0,0,this->Width-panelX-25,this->GB1->Location.Y-5);
				 
				 this->panel1->Width = panelX;
				 this->panel1->Location = *(gcnew Point(this->Width - panelX - 15, 0));
				 
				 int Y = int(double(9*(this->GB1->Location.Y))/520);
				 this->BIG_HR->Location = *(gcnew Point(BIG_HR->Location.X,Y));
				 this->BIG_HR->Font = gcnew Drawing::Font("Microsoft Sans Serif",(52*mult),Drawing::FontStyle::Bold);

				 Y = int(double(197*(this->GB1->Location.Y))/520);
				 this->BIG_BP->Location = *(gcnew Point(BIG_BP->Location.X,Y));
				 this->BIG_BP->Font = gcnew Drawing::Font("Microsoft Sans Serif",(40*mult),Drawing::FontStyle::Bold);

				 Y = int(double(260*(this->GB1->Location.Y))/520);
				 this->BIG_MAP->Location = *(gcnew Point(BIG_MAP->Location.X,Y));
				 this->BIG_MAP->Font = gcnew Drawing::Font("Microsoft Sans Serif",(32*mult));

				 Y = int(double(321*(this->GB1->Location.Y))/520);
				 this->BIG_SPO2->Location = *(gcnew Point(BIG_SPO2->Location.X,Y));
				 this->BIG_SPO2->Font = gcnew Drawing::Font("Microsoft Sans Serif",(48*mult),Drawing::FontStyle::Bold);

				 Y = int(double(410*(this->GB1->Location.Y))/520);
				 this->BIG_CO2->Location = *(gcnew Point(BIG_CO2->Location.X,Y));			 
				 this->BIG_CO2->Font = gcnew Drawing::Font("Microsoft Sans Serif",(48*mult),Drawing::FontStyle::Bold);

				 //Y = int(114*double(this->Height)/440);
				 //this->BIG_PPV->Location = *(gcnew Point(BIG_PPV->Location.X,Y));

				 this->GB5->Location = *(gcnew Point(this->panel1->Location.X - 266, GB5->Location.Y));
				 this->GB6->Location = *(gcnew Point(this->panel1->Location.X - 266, GB6->Location.Y));

				 this->GB3->Location = *(gcnew Point(this->panel1->Location.X - 532, GB3->Location.Y));
				 this->GB4->Location = *(gcnew Point(this->panel1->Location.X - 532, GB4->Location.Y));

				 this->GB1->Location = *(gcnew Point(this->panel1->Location.X - 801, GB1->Location.Y));
				 this->GB2->Location = *(gcnew Point(this->panel1->Location.X - 801, GB2->Location.Y));

				 Graph1->ChangeRegion(this->panel3->Width+6,this->GB5->Location.X-6,this->GB1->Location.Y+8,this->GB1->Location.Y + this->GB1->Size.Height-1);
				 Graph2->ChangeRegion(this->panel3->Width+6,this->GB5->Location.X-6,this->GB2->Location.Y+8,this->GB2->Location.Y + this->GB2->Size.Height-1);
			
			 }
	public: System::Void ChangeWaves(String^ newAFILE,String^ newEFILE,String^ newEVFILE,String^ newPFILE,int newWaveLength,int newBreakPoint) {
				 this->SimWaves->changeWaves(newAFILE,newEFILE,newEVFILE,newPFILE,newWaveLength,newBreakPoint);
			 }
	private: System::Void WaveTimer_Tick(System::Object^  sender, System::EventArgs^  e) {
				 // Ticks once every 50 microseconds give or take
				 // Except in highspeed mode, where it runs pretty much continuously
				 int start=0;
				 int finish=0;
				 double CO2x;

				  this->label_WTMS->Text = this->WaveTimer->Interval.ToString();
				 this->label_GCPS->Text = count.ToString();
				 this->lab_PAP->Text = this->AirwayPressure.ToString();
				 this->lab_RR->Text = this->RespRate.ToString();
				 this->label_ProcSpeed->Text = this->Wavemon->getProcessSpeed().ToString();
				 this->Update();

				 if ((this->TripleSpeed == true) && (this->timer_pacing->Enabled))
				 {
					this->WaveTimer->Interval = 1;
					this->timer_pacing->Stop();
				 }
				 else if (this->TripleSpeed == false)
				 {
					 if (!this->timer_pacing->Enabled)
					 {
						 this->timer_pacing->Start();
						 this->WaveTimer->Interval = 47;
					 }
				 }
				 
				 this->label3->Text = this->SimWaves->WaveLength.ToString();

				// Random chance to update the monitor to show updated values
			    // Should occur on average every 1-2 seconds
				 if (Rx->NextDouble() < 0.04)
				 {
					this->BIG_BP->Text = int(Vitals->SBP).ToString() + "/" + int(Vitals->DBP).ToString();
				 }
				 if (Rx->NextDouble() < 0.04)
				 {
					this->BIG_HR->Text = int(Vitals->HR).ToString();
				 }
				 if (Rx->NextDouble() < 0.04)
				 {
					this->BIG_MAP->Text = int(Vitals->MAP).ToString();
				 }
				
				 // This timer is actually being maintained by the second timer such
				 // that it's consistently running at 50ms per cycle.  We're going
				 // to use this fact to maintain the respiratory waveform position

					
				 this->RespCyclePosition += 0.05;
				 if (this->Vitals->RR > 0)
				 {
					 double RespCycleLength = 60/this->Vitals->RR;
					 if (this->RespCyclePosition >= (RespCycleLength))
					 {
						 this->RespCyclePosition = 0;
					 }
					 // DP values
					 start = int(100 * (1 + Math::Sin(3.141 * 2 * (RespCyclePosition/RespCycleLength)))/2);
					 finish = int(100 * (1 + Math::Sin(3.141 * 2 * ((RespCyclePosition + 0.10)/RespCycleLength)))/2);
					 // CO2 values
					 // Shift the Resp Wave a bit so that Peak Pressure = End Inspiration
					 double RCP = RespCyclePosition + (RespCycleLength*4/7);
					 if (RCP > RespCycleLength) {RCP = RCP-RespCycleLength;}
					 CO2x = double(RCP)/double(RespCycleLength);
					 }
				 else 
				 {
					start = 0;
					finish =0;
					// 0.32 IS RIGHT!  This causes etCO2 to fall very slowly from wherever it is
					CO2x = double(0.32);
				 }

				 if (CO2on==true)
				 {
					 // Update the thing
					 if (ticktock==1)
					 {
						this->BIG_CO2->Text = int(Vitals->PaCO2*Vitals->DeadspaceR).ToString();
						ticktock=0;
					 }

					 // Set CO2 --------------------------------------------------------
					 // The monitor creates the CO2 waves
					 static double IEx = 0.42; // 0.33
					 if (CO2x > IEx) 
					 {
						 // UPSWING
						 ticktock=-1;
						 double target = Vitals->PaCO2*Vitals->DeadspaceR;
						 if (CO2Build < target)
						 {
							 // How far are we from etCO2?
							 double d = target - CO2Build;
							 // Take a factor of the deadspace and the difference
							 d = d * 8 * double(CO2x - IEx) * double(Math::Pow(Vitals->DynamicOutflow,5));
							 CO2Build += d;
					 		 if (CO2Build > target) {CO2Build = target;}
						 } 
					}
					 else 
					 {
 						 // DOWNSWING
						 // CO2x goes 0-->IEx-->1-->0
						 if (ticktock==-1) {ticktock=1;}
						 if (CO2Build > 0)
						 {
							 CO2Build += -double(CO2Build*CO2x*CO2x*100);
						 }
						 if (CO2Build < 0)
						 {
							 CO2Build=0;
						 }
					}
					 Wavemon->setCO2(int(CO2Build));
				 } 

				 ArrayList^ AP;
				 // get the next three packets
				 //if (this->TripleSpeed)
				 //{
				//	 AP = SimWaves->generateWavePackets(9,Vitals,start,finish,this->VVar);
				 //}
				 //else 
				 //{
					 AP = SimWaves->generateWavePackets(3,Vitals,start,finish,this->VVar);
				 //}
				 this->Wavemon->addWavePackets(AP);
				 this->label_WP_PacketsInBuffer->Text = Wavemon->WP->Count.ToString();
				 Wavemon->graph("",ARTon,CO2on,EKGon,PulseOxon,true,this->ArtScale);
				 count++;;

				 // Now - this is purely for reference purposes
				 // Determine what the scaled MAP, SV, CO would be based on the waveform PPV
				 double PPVx = (Vitals->PPV/100);

				 this->LastRefCO = Vitals->CO - (Vitals->CO * double(start)/100 * Vitals->PPV/100) + (0.5 * Vitals->CO * Vitals->PPV/100);
				 this->LastRefSV = Vitals->SV - (Vitals->SV * double(start)/100 * Vitals->PPV/100) + (0.5 * Vitals->SV * Vitals->PPV/100);
				 this->LastRefMAP = Vitals->MAP - (Vitals->MAP * double(start)/100 * Vitals->PPV/100) + (0.5 * Vitals->MAP * Vitals->PPV/100);


				 //this->label1->Text = Wavemon->WP->Count.ToString();
			 }
	public: void SetArtScale(int newscale)
			{
				this->ArtScale = newscale;
			}
	public: void updateVitals(Simulator::VitalsPack^ VP)
			{
				this->Vitals = VP;
				this->BIG_SPO2->Text = int(Vitals->SPO2).ToString();
				// CO2 is updated at the end of a resp cycle!
			}
	public: void updateVentilation(int RR, int PAP)
			{
				this->AirwayPressure = PAP;
				this->RespRate = RR;
			}
	public: void VerifyFluidGiven(double LR, double Alb)
			{
				this->LR += LR;
				this->Albumin += Alb;
			}
	public: void SetClock(String^ S)
			{
				this->label_CLOCK->Text = S;
			}
	public: double GetMedEphedrine(void)
			{
				double F = this->Med_ephedrine;
				this->Med_ephedrine = 0;
				return F;
			}
	public: double GetMedFentanyl(void)
			{
				double F = this->Med_fenty;
				this->Med_fenty = 0;
				return F;
			}
	public: double GetMedPhenylephrine(void)
			{
				double F = this->Med_phenyl;
				this->Med_phenyl = 0;
				return F;
			}
	public: bool IsTripleSpeed(void)
			{
				return this->TripleSpeed;
			}
	private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) {

				 // Pacing Timer Shit
				 if ((count < 17)&&(this->WaveTimer->Interval > 2)) 
				 {
					 this->WaveTimer->Interval += -2;
				 }
				 else if ((count < 19)&&(this->WaveTimer->Interval > 1)) 
				 {
					 this->WaveTimer->Interval += -1;
				 }
				 else if (count > 20) 
				 {
					 this->WaveTimer->Interval += 1;
				 }
				 else if (count > 23) 
				 {
					 this->WaveTimer->Interval += 2;
				 }
				 count=0;
			 }
private: System::Void WaveMonitor_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
			 Parent->Close();
		 }
 public: void HideInteractivePanels(void)
		 {
			 this->panel_black->Visible = true;
		 }
public: void ShowInteractivePanels(void)
		{
			this->panel_black->Visible = false;
		}
public: int decreaseSweep(System::Object^  sender, System::EventArgs^  e) {
			 return Wavemon->decreaseWaveformSpeed();
		 }
public: int increaseSweep(System::Object^  sender, System::EventArgs^  e) {
			 return Wavemon->increaseWaveformSpeed();
		 }
public: void SetVVariability(int newVVar)
		{
			this->VVar = newVVar;
		}
private: System::Void label_CLOCK_Click(System::Object^  sender, System::EventArgs^  e) {
		 }
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
			 //if (this->TripleSpeed == true)
			 //{
				//this->TripleSpeed = false;
				//this->button1->BackColor = System::Drawing::Color::Transparent;
				//this->button2->BackColor = System::Drawing::Color::Transparent;
			 //}
			 //else 
			 //{
				//this->TripleSpeed = true;
				//this->button1->BackColor = System::Drawing::Color::LightGreen;
				//this->button2->BackColor = System::Drawing::Color::LightGreen;
			 //}
			 this->TripleSpeed = false;
		 }
private: System::Void pictureBox1_Click(System::Object^  sender, System::EventArgs^  e) {
		 }
private: System::Void button8_Click(System::Object^  sender, System::EventArgs^  e) {
			 this->Med_ephedrine += 5;
		 }
private: System::Void GIVE_NEO_Click(System::Object^  sender, System::EventArgs^  e) {
			 this->Med_phenyl += 50;
		 }
private: System::Void button10_Click(System::Object^  sender, System::EventArgs^  e) {
			 this->Med_fenty += 50;
		 }

private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
			 this->button1_Click(this,System::EventArgs::Empty);
		 }
private: void SetBolusDose(int ID)
		 {
			 if (ID < 0) {return;}
			 bool OK = false;
			 while (this->cb_Bolus->Items->Count > 0)
			 {
				 this->cb_Bolus->Items->RemoveAt(0);
			 }
			 DrugData^ D = static_cast<DrugData^>(DrugList->DrugList[ID]);
			 if (D->BolusStep > 0)
			 {
				 for (double i=D->BolusMin; i<=D->BolusMax; i=i+D->BolusStep)
				 {
					 OK = true;
					 this->cb_Bolus->Items->Add(System::Math::Round(i,2));
				 }
			 }
			 this->label_BolusUnits->Text = D->BolusUnits;
			 if (!OK)
			 {
				 this->cb_Bolus->Enabled = false;
			 }
			 else
			 {
				 this->cb_Bolus->Enabled = true;
				 this->button_Administer->Enabled = true;
			 }
		 }
private: void SetDripDose(int ID)
		 {
			 if (ID < 0) {return;}
			 bool OK = false;
			 while (this->cb_Drip->Items->Count > 0)
			 {
				 this->cb_Drip->Items->RemoveAt(0);
			 }
			 DrugData^ D = static_cast<DrugData^>(DrugList->DrugList[ID]);
			 if (D->dripStep > 0)
			 {
				 for (double i=D->dripMin; i<=D->dripMax; i=i+D->dripStep)
				 {
					 OK = true;
					 this->cb_Drip->Items->Add(System::Math::Round(i,2));
				 }
				 this->label_dripUnits->Text = D->DripUnits;
			 }
			 if (!OK)
			 {
				 this->cb_Drip->Enabled = false;
			 }
			 else
			 {
				 this->cb_Drip->Enabled = true;
				 this->button_Administer->Enabled = true;
			 }
		 }
public: System::Void SetInteractiveMode(bool OnTF)
		 {

			 this->label15->Text = "";
			 this->label16->Text = "";
			 this->label17->Text = "";
			 this->label18->Text = "";
			 this->label19->Text = "";
			 this->label20->Text = "";
			 if (OnTF)
			 {
				 this->panel_Phase->Visible = false;
			 }
			 else 
			 {
				this->panel_Phase->Visible = true;
			 }
		 }
private: System::Void cb_Drugs_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
			 this->button_Administer->Enabled = false;

			 if (this->cb_Drugs->SelectedIndex > 0)
			 {
				 this->SetDripDose(this->cb_Drugs->SelectedIndex-1);
				 this->SetBolusDose(this->cb_Drugs->SelectedIndex-1);
			 }
			 else 
			 {
 				 this->SetDripDose(this->cb_Drugs->SelectedIndex-1);
				 this->SetBolusDose(this->cb_Drugs->SelectedIndex-1);
				 this->cb_Bolus->Enabled = false;
				 this->cb_Drip->Enabled = false;
			 }
		 }
private: System::Void cb_Bolus_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
			 if (this->cb_Bolus->SelectedIndex != -1)
			 {
				this->cb_Drip->SelectedIndex = -1;
			 }
		 }
private: System::Void cb_Drip_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
			 if (this->cb_Drip->SelectedIndex > -1)
			 {
				 this->cb_Bolus->SelectedIndex = -1;
			 }
		 }
public: void AddTotalColloid(double K)
		{
			this->TCo+= K;
			this->label_TotalColl->Text = int(TCo).ToString() + " ml";
		}
public: void AddTotalCrystal(double K)
		{
			this->TCr += K;
			this->label_TotalCry->Text = int(TCr).ToString() + " ml";
		}
public: void AddTotalBlood(double K)
		{
			this->TBl += K;
			this->label_TotalBlood->Text = int(TBl).ToString() + " ml";
		}
		
public: System::Void addDrugMethod(String^ bolus, String^ drip, String^ drug, String^ UID) {
			 this->button_Administer->Enabled = false;

			 // Wonky, but need to find which cb is selected
			 int MODE = 0;
			 try {
				 if (bolus->Length > 0) { MODE = 1; }
			 }
			 catch (System::Exception^ e) { e; /*silent*/ };
			 try {
				 if (drip->Length > 0) { MODE = 2; }
			 }
			 catch (System::Exception^ e) { e; /*silent*/ };


			 this->DrugID = DrugList->GetDrugByName(drug)->DrugID;
			 this->DrugUID = UID;


			 if (MODE == 1)
			 {
				 this->label_DrugConf->Text = "Giving " + drug + " ";
				 this->label_DrugConf->Text += bolus + " " + this->label_BolusUnits->Text;
				 //this->label_DrugConf->Text = "Giving Bolus";
				 this->DrugIsBolus = true;
				 this->DrugDose = double::Parse(bolus);
			 }
			 else if (MODE == 2)
			 {
				 this->label_DrugConf->Text = "Starting " + drug + " ";
				 this->label_DrugConf->Text += drip + " " + this->label_dripUnits->Text;
				 //this->label_DrugConf->Text = "Giving Drip";
				 this->DrugIsBolus = false;
				 this->DrugDose = double::Parse(drip);
			 }
			 else
			 {
				 // Somehow both text boxes are empty. Fail. 
				 return;
			 }

			 this->label_DrugConf->Visible = true;
			 this->pictureBox_cancelDrug->Visible = true;
			 this->timer_Drug->Start();
			 this->ActionLog += this->label_CLOCK->Text + ": " + this->label_DrugConf->Text + "\n";
			 this->cb_Drugs->SelectedIndex = 0;
}
		
private: System::Void button_Administer_Click(System::Object^  sender, System::EventArgs^  e) {
			 String^ bolus = "";
			 String^ drip = "";
			 String^ UID = "";
			 try {
				if (this->cb_Bolus->SelectedItem->ToString()->Length > 0) {
					 bolus = this->cb_Bolus->SelectedItem->ToString();
				}
			 } catch (System::Exception^ e) {e; /*silent*/};
			 try {
			 if (this->cb_Drip->SelectedItem->ToString()->Length > 0) {
				 drip = this->cb_Drip->SelectedItem->ToString();
			 }
			 } catch (System::Exception^ e) {e; /*silent*/};

			 UID = System::Convert::ToString(this->DrugCounterUID);
			 addDrugMethod(bolus, drip, this->cb_Drugs->SelectedItem->ToString(), UID);
			 this->DrugCounterUID++;
			 //this->button_Administer->Enabled = false;

			 //// Wonky, but need to find which cb is selected
			 //int MODE = 0;
			 //try {
				//if (this->cb_Bolus->SelectedItem->ToString()->Length > 0) {MODE = 1;}
				//} catch (System::Exception^ e) {e; /*silent*/};
			 //try {
				//if (this->cb_Drip->SelectedItem->ToString()->Length > 0) {MODE = 2;}
				//} catch (System::Exception^ e) {e; /*silent*/};
			 //
			 //
			 //this->DrugID = DrugList->GetDrugByName(this->cb_Drugs->SelectedItem->ToString())->DrugID;
			 //			 
			 //if (MODE == 1)
			 //{
				// this->label_DrugConf->Text = "Giving " + this->cb_Drugs->SelectedItem->ToString() + " ";
				// this->label_DrugConf->Text += this->cb_Bolus->SelectedItem->ToString() + " " + this->label_BolusUnits->Text;
				// this->DrugIsBolus = true;
				// this->DrugDose = double::Parse(this->cb_Bolus->SelectedItem->ToString());
			 //}
			 //else if (MODE == 2) 
			 //{
				// this->label_DrugConf->Text = "Starting " + this->cb_Drugs->SelectedItem->ToString() + " ";
				// this->label_DrugConf->Text += this->cb_Drip->SelectedItem->ToString() + " " + this->label_dripUnits->Text;
				// this->DrugIsBolus = false;
				// this->DrugDose = double::Parse(this->cb_Drip->SelectedItem->ToString());
			 //}
			 //else 
			 //{
				// // Somehow both text boxes are empty. Fail. 
				// return;
			 //}

			 //this->label_DrugConf->Visible = true;
			 //this->pictureBox_cancelDrug->Visible = true;
			 //this->timer_Drug->Start();
			 //this->ActionLog += this->label_CLOCK->Text + ": " + this->label_DrugConf->Text + "\n";
			 //this->cb_Drugs->SelectedIndex = 0;
		 }
private: System::Void pictureBox_cancelDrug_Click(System::Object^  sender, System::EventArgs^  e) {
			 
			 this->label_DrugConf->Visible = false;
			 this->pictureBox_cancelDrug->Visible = false;
			 this->button_Administer->Enabled = true;
			 /*System::String^ S = dataGridView1->Rows[k]->Cells[0]->Value->ToString() + " ";
			 S = S + dataGridView1->Rows[k]->Cells[1]->Value + " " + dataGridView1->Rows[k]->Cells[2]->Value + " discontinued; Total dose: ";
			 S = S + dataGridView1->Rows[k]->Cells[5]->Value + "\n";
			 this->ActionLog += this->label_CLOCK->Text + ": " + S;*/
			 //this->ActionLog += this->label_CLOCK->Text + ": " + "Last drug action cancelled\n";
			 this->timer_Drug->Stop();
			 //
		 }

private: System::Void timer_Drug_Tick(System::Object^  sender, System::EventArgs^  e) {
			 this->label_DrugConf->Visible = false;
			 this->pictureBox_cancelDrug->Visible = false;

			 // GIVE A DRUG HERE!  Add it to the table

			 this->dataGridView1->Rows->Add();
			 System::Windows::Forms::DataGridViewRow^ ROW = this->dataGridView1->Rows[this->dataGridView1->Rows->Count-1];

			 ROW->Cells[3]->ToolTipText = "Discontinue";
			 ROW->Cells[3]->Value = "D/C";
			 ROW->Cells[8]->Value = this->DrugUID;

			 if (this->DrugIsBolus) 
			 {
				 DrugData^ D = this->DrugList->GetDrugByID(this->DrugID);
				 ROW->Cells[0]->Value = D->DrugName;
				 ROW->Cells[1]->Value = this->DrugDose;
				 ROW->Cells[2]->Value = D->BolusUnits;
				 ROW->Cells[4]->Value = D->DrugID;
				 ROW->Cells[5]->Value = "0";
				 ROW->Cells[6]->Value = "0";
				 ROW->Cells[7]->Value = "1";
				 if (D->BolusIsInfusion) { ROW->Cells[6]->Value = "1"; }
					 else { ROW->Visible = false; }
			 }
			 else
			 {
			 	 DrugData^ D = this->DrugList->GetDrugByID(this->DrugID);
				 ROW->Cells[0]->Value = D->DrugName;
				 ROW->Cells[1]->Value = this->DrugDose;
				 ROW->Cells[2]->Value = D->DripUnits;
				 ROW->Cells[4]->Value = D->DrugID;
				 ROW->Cells[5]->Value = "0";
				 ROW->Cells[6]->Value = "0";
				 ROW->Cells[7]->Value = "0";
			 }

			 this->timer_Drug->Stop();
			 
		 }
public: int GetDrugCount(void) {
			return this->dataGridView1->RowCount;
		}
public: void RemoveDrug(int k)
		{
			dataGridView1->Rows->RemoveAt(k);
		}
public: double GetTotalDose(int k)
		{
			return double::Parse(this->dataGridView1->Rows[k]->Cells[5]->Value->ToString());
		}
public: double GetUID(int k)
		{
			return double::Parse(this->dataGridView1->Rows[k]->Cells[8]->Value->ToString());
		}
public: unsigned int GetDrugIDInSlot(int k)
		{
			return int::Parse(this->dataGridView1->Rows[k]->Cells[4]->Value->ToString());
		}
public: System::String^ GetDrugInSlot(int k)
		{
			return this->dataGridView1->Rows[k]->Cells[0]->Value->ToString();
		}
public: double GetDoseInSlot(int k)
		{
			return double::Parse(this->dataGridView1->Rows[k]->Cells[1]->Value->ToString());
		}
public: int GetIsBolusAsInfusion(int k)
		{
			return int::Parse(this->dataGridView1->Rows[k]->Cells[6]->Value->ToString());
		}
public: int GetIsBolus(int k)
		{
			return int::Parse(this->dataGridView1->Rows[k]->Cells[7]->Value->ToString());
		}
public: void SetAmountGiven(int k, double GIVEN)
		{
			this->dataGridView1->Rows[k]->Cells[5]->Value = GIVEN;
		}
public: void DiscontinueDrugInRow(int k)
		{
			System::String^ S = dataGridView1->Rows[k]->Cells[0]->Value->ToString() + " ";
			S = S + dataGridView1->Rows[k]->Cells[1]->Value + " " + dataGridView1->Rows[k]->Cells[2]->Value + " Dose ended; Total dose: ";
			S = S + dataGridView1->Rows[k]->Cells[5]->Value + "\n";
			this->ActionLog += this->label_CLOCK->Text + ": " + S;
			this->dataGridView1->Rows->RemoveAt(k);
		}
public: void removeDrugByUIDMethod(String^ UID)
		{
			int row_index = -1;
			for (int k = 0; k < this->dataGridView1->RowCount; k++){
				if (this->dataGridView1->Rows[k]->Cells[8]->Value->ToString()->Equals(UID)){
					row_index = k;
					break;
				}
			}
			if (row_index != -1){
				removeDrugMethod(row_index);
			}
		}
public: System::Void removeDrugMethod(int k){
			System::String^ S = dataGridView1->Rows[k]->Cells[0]->Value->ToString() + " ";
			S = S + dataGridView1->Rows[k]->Cells[1]->Value + " " + dataGridView1->Rows[k]->Cells[2]->Value + " D/C pressed; Total dose: ";
			S = S + dataGridView1->Rows[k]->Cells[5]->Value + "\n";
			this->ActionLog += this->label_CLOCK->Text + ": " + S;
			this->dataGridView1->Rows->RemoveAt(k);
}
private: System::Void dataGridView1_CellContentClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {
			if (e->ColumnIndex==3)
			{
				removeDrugMethod(e->RowIndex);
			}
		 }
};

// End of namespace
}
