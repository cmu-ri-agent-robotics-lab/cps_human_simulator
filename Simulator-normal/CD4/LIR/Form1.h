#pragma once

// uncomment to force specific modes only
//#define DISPLAY_MODE_ONLY
///#define MASIMO

// Making active will hide form controls uneeded for sim testing
//#define LIR_TEST_VER

#define COMPILE_ID "Compiled for Research"

#ifndef LIR_TEST_VER
	#define DEFAULT_SHOW_WMON
#endif

#define HIDE_PICTURESAVEIMAGE

#include <time.h>
#include <stdlib.h>
#include <windows.h>

#include "About.h"

#include "../../Simulator.h"
#include "../../SimWaves.h"
#include "../../Medications.h"
#include "../../LirHelpers.h"
#include "../../CDSIM_Net_Tools.h"

#include "WaveMonitor.h"
#include "../../WaveMonitorHelpers.h"


// Meta keys
// 1 - action
// 99 - Alert


namespace LIR {

	using namespace System::Data::SqlClient;
	using namespace System::Data::OleDb;
	using namespace System::Xml;
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	using namespace Newtonsoft::Json;
	using namespace System::Net;
	using namespace System::Net::Sockets;
	using namespace System::Text;
	using namespace System::Threading;

	/// <summary>
	/// Summary for Form1
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//

			//Edited by Shehzi
			// TCP server Begin
			TcpReader = nullptr;
			TcpWriter = nullptr;
			//Tcp_mutex = gcnew System::Threading::Mutex();
			try{
				//this->TCP_listen = gcnew System::Net::Sockets::TcpListener(System::Net::IPAddress::Parse(this->tb_netadd->Text), int::Parse(this->tb_netport_tcp->Text));
				// TCP is wierd in windows it does not bind to a specific IP address - therefore make sure it is always 0.0.0.0, the port is important
				// TCP servers on port - Input Port and Input Port + 5
				this->TCP_listen = gcnew System::Net::Sockets::TcpListener(System::Net::IPAddress::Parse("0.0.0.0"), 3145);
				//System::Net::Sockets::Socket(System::Net::IPAddress::Parse("0.0.0.0"), 3145);
				//this->TCP_listen2 = gcnew System::Net::Sockets::TcpListener(System::Net::IPAddress::Parse("0.0.0.0"), 3150);
				//TCP_listen2 is used only for receiving data on a port that is +5 away from the sending port
				//windows uses the same buffer for sending and receiving data - this is annoying
				//this->TCP_listen->Start();
				// The line below needs to be run in a loop
				// TCP_listen->BeginAcceptTcpClient(gcnew AsyncCallback(this, &Form1::TCP_callback), TCP_listen);
			}
			catch (Exception^ e){
				e;
				System::Windows::Forms::MessageBox::Show(" TCP Server IP address error", e->ToString());
				return;
			}

			//Edited by Shehzi

			Rx = gcnew System::Random();
			
			// OLD DRIVER 5.1
			// New Driver 5.1
			System::String ^ConnectionString= "server=localhost;user=lir;database=lir;port=3306;password=lir;";
			//System::Data::Odbc::OdbcConnection ^DATB = gcnew System::Data::Odbc::OdbcConnection(ConnectionString);			
			//DATB->Open();	

			//VitalsLog^ VITALS = gcnew LIR::VitalsLog;			
			//BolusInterfaceX^ BOFACE = gcnew LIR::BolusInterfaceX;
			//BOFACE->Show();
			//VITALS->Show();
			airwayPressure = 0;

			Vitals = gcnew Simulator::VitalsPack();

			DrugList = gcnew Drugs();
			WMon = gcnew LIR::WaveMonitor(this,DrugList);
			//WMon->Show();
			this->worker = gcnew BackgroundWorker();



			this->worker->RunWorkerAsync();
#ifdef LIR_TEST_VER
			this->groupBox4->Visible = false;
			this->groupBox8->Visible = false;
			this->groupBox11->Visible = false;
			delete this->tabControl1->TabPages[1];
			delete this->tabControl1->TabPages[1];
			this->cb_BroadcastNet->Checked = true;
			this->cb_listenNetwork->Checked = true;
			this->cb_StartLag->Checked = true;
			this->CB_speed_turbo->Checked = true;
#endif

			this->WMonAlbuminGiven = 0;
			this->WMonLRGiven = 0;
			this->WMonBloodGiven = 0;
			this->WMonBloodLoss = 0;

			SVerror = 0;

			this->WaitingForControlResponse=true;

			FluidGivenByLirThisBolus=0;
			LirBolusID=0;

			this->UD_PCon->SelectedIndex = 18;
			this->UD_Resp->SelectedIndex = 28;
			this->UD_TV->SelectedIndex = 12;

			this->LoadedCommands = "";
			this->LoadedRhythym = "";

			this->PatientID="";

			KEYS = gcnew System::Collections::ArrayList();

			this->domainUpDown1->SelectedItem="1";

			SlotOptions = gcnew ArrayList();
			SlotOptions->Add(gcnew LIR::SlotOption("Off",Drawing::Color::Black,0,0,0,false));
			SlotOptions->Add(gcnew LIR::SlotOption("CO",Drawing::Color::RoyalBlue,0,10,0,true));
			SlotOptions->Add(gcnew LIR::SlotOption("CI",Drawing::Color::RoyalBlue,0,5,0,true));
			SlotOptions->Add(gcnew LIR::SlotOption("CVP",Drawing::Color::MediumTurquoise,0,15,0,false));
			//SlotOptions->Add(gcnew LIR::SlotOption("Doppler FTc",Drawing::Color::White,3,6,0,true));
			SlotOptions->Add(gcnew LIR::SlotOption("Hgb",Drawing::Color::Red,5,16,0,true));
			SlotOptions->Add(gcnew LIR::SlotOption("NIBP",Drawing::Color::White,0,200,0,false));
			SlotOptions->Add(gcnew LIR::SlotOption("MAP",Drawing::Color::White,0,150,0,false));
			//SlotOptions->Add(gcnew LIR::SlotOption("PA",Drawing::Color::Goldenrod,0,40,0,false));
			SlotOptions->Add(gcnew LIR::SlotOption("PPV",Drawing::Color::MediumPurple,0,25,0,true));
			SlotOptions->Add(gcnew LIR::SlotOption("PVI",Drawing::Color::MediumTurquoise,0,25,0,true));
			SlotOptions->Add(gcnew LIR::SlotOption("RR",Drawing::Color::Gray,0,20,0,false));
			SlotOptions->Add(gcnew LIR::SlotOption("SCvO2",Drawing::Color::MediumTurquoise,0,100,0,true));
			SlotOptions->Add(gcnew LIR::SlotOption("SV",Drawing::Color::Yellow,0,120,0,true));
			SlotOptions->Add(gcnew LIR::SlotOption("SvO2",Drawing::Color::MediumTurquoise,0,100,0,true));
			SlotOptions->Add(gcnew LIR::SlotOption("SpHb",Drawing::Color::Red,5,16,0,true));
			SlotOptions->Add(gcnew LIR::SlotOption("SVR",Drawing::Color::OrangeRed,400,2400,0,true));
			SlotOptions->Add(gcnew LIR::SlotOption("SVV",Drawing::Color::Orange,0,25,0,true));
			SlotOptions->Add(gcnew LIR::SlotOption("Temp",Drawing::Color::WhiteSmoke,34,40,0,true));
			
			this->ThirtySecondCounter=0;

			this->domainUpDown1->Items->RemoveRange(0,this->domainUpDown1->Items->Count);
			for (int i=1000;i>=0;i=i-1)
			{
				this->domainUpDown1->Items->Add(i);
			}
			this->domainUpDown1->SelectedIndex = 1000;

			for (int i=0; i<SlotOptions->Count; i++)
			{
				this->comboBox1->Items->Add(dynamic_cast<LIR::SlotOption^>(SlotOptions->default[i])->Name);
				this->comboBox2->Items->Add(dynamic_cast<LIR::SlotOption^>(SlotOptions->default[i])->Name);
				this->comboBox3->Items->Add(dynamic_cast<LIR::SlotOption^>(SlotOptions->default[i])->Name);
				this->comboBox4->Items->Add(dynamic_cast<LIR::SlotOption^>(SlotOptions->default[i])->Name);
				this->comboBox5->Items->Add(dynamic_cast<LIR::SlotOption^>(SlotOptions->default[i])->Name);
				this->comboBox6->Items->Add(dynamic_cast<LIR::SlotOption^>(SlotOptions->default[i])->Name);
			}

			this->NetBID = 0;
			this->NetRate = 0;

			this->showCases();

			this->comboBox1->SelectedIndex = 0;
			this->comboBox2->SelectedIndex = 0;
			this->comboBox3->SelectedIndex = 0;
			this->comboBox4->SelectedIndex = 0;
			this->comboBox5->SelectedIndex = 0;
			this->comboBox6->SelectedIndex = 0;
			
			this->GetAvailableScripts();

#ifdef DEFAULT_SHOW_WMON
			this->WMon->Show();
			this->cb_MonitorOn->Checked = true;
#endif

			this->checkBoxGraph1->Checked = false;
			this->checkBoxGraph2->Checked = false;
			this->comboBox1->Enabled = true;
			this->comboBox2->Enabled = true;
			this->comboBox3->Enabled = true;
			this->comboBox4->Enabled = true;

#ifdef DISPLAY_MODE_ONLY
			
			WMon->HideInteractivePanels();
			this->checkBox5->Visible = false;
			this->cb_MonitorOn->Visible = false;
			this->groupBox4->Visible = false;
			delete this->tabControl1->TabPages[3];
			delete this->tabControl1->TabPages[0];
			this->tabControl1->TabPages[0]->Text = "Simulator";
			this->panel1->Visible = false;
			this->groupBox12->Visible = false;
			this->groupBox3->Visible = false;
			this->groupBox9->Visible = false;
			this->panel2->Visible = false;
			/*
			this->groupBox11->Visible = false;
			this->groupBox13->Visible = false;
			this->groupBox14->Visible = false;
			this->groupBox10->Visible = false;
			this->groupBox15->Visible = false;
			this->groupBox16->Visible = false;
			*/
#endif
#ifdef MASIMO
			this->checkBoxGraph1->Checked = true;
			this->checkBoxGraph2->Checked = true;

			this->comboBox5->SelectedItem = "PVI";
			this->comboBox6->SelectedItem = "SpHb";
			this->comboBox1->Enabled = false;
			this->comboBox2->Enabled = false;
			this->comboBox3->Enabled = false;
			this->comboBox4->Enabled = false;

			this->checkBoxBarGraph5->Checked = false;
			this->checkBoxBarGraph6->Checked = false;

			WMon->Graph1->SetBarGraphModeOff();
			WMon->Graph2->SetBarGraphModeOff();
			WMon->Graph1->SetGraphMovementMode(2);
			WMon->Graph2->SetGraphMovementMode(2);

			//delete this->tabControl1->TabPages[1];
			// This is the original page 3
			delete this->tabControl1->TabPages[3];;
			this->tabControl1->TabPages[0]->Text = "Simulator";
			/*this->panel1->Visible = false;
			this->groupBox12->Visible = false;
			this->groupBox11->Visible = false;
			this->groupBox13->Visible = false;
			this->groupBox14->Visible = false;
			this->groupBox10->Visible = false;
			this->groupBox3->Visible = false;
			this->groupBox15->Visible = false;
			this->groupBox16->Visible = false;
			*/
#endif
		}

		//edit by Shehzi
		ref class StateObject
		{
		public:
			literal int BufferSize = 1024; // Adjust buffer size
			//Socket^ workSocket;
			LIR::Form1^ form; 
			array<Byte>^ buffer;
			StringBuilder^ sb;
			StateObject() : form(nullptr) //workSocket(nullptr)
			{
				buffer = gcnew array<Byte>(BufferSize);
				sb = gcnew StringBuilder;
			}
		};

		ref class ReceivedData
		{
		public:
			String^ action;
			String^ UID;
			String^ drug;
			String^ bolus;
			String^ drip;
			ReceivedData()
			{
				action = "";
				UID = "";
				drug = "";
				bolus = "";
				drip = "";
			}
		};
		private: System::Void TCP_callback(System::IAsyncResult^ result){
					 try{
						 // int server_number = result->AsyncState;
						 System::Net::Sockets::TcpListener^ listner = (System::Net::Sockets::TcpListener^) result->AsyncState;
						 //System::Net::Sockets::TcpClient^ client = listner->EndAcceptTcpClient(result);
						 //Socket^ listner = (Socket^) result->AsyncState;
						 this->handler = listner->EndAcceptSocket(result);
						 System::Diagnostics::Debug::WriteLine("started accpet client");
						 
						 WaitForNewComingData(this);
						 //client->NoDelay = TRUE;
						 //TCP_Server_listen(client); // server_number = 1
						 // System::Windows::Forms::MessageBox::Show("Client Accepted - on Server 1", client->ToString());
					 }
					 catch (Exception^ e){
						 e;
						 System::Diagnostics::Debug::WriteLine("TCP callback errors");
						 this->handler = nullptr;
						 //System::Windows::Forms::MessageBox::Show("TCP Call Back error", e->ToString());
						 /*this->TcpReader = nullptr;
						 this->TcpWriter = nullptr;
						 return;*/
					 }
		}

				static void WaitForNewComingData(Form1^ form){
					 // Create the state object.
					 StateObject^ state = gcnew StateObject();
					 //state->form->handlerworkSocket = handler;
					 state->form = form;
					 state->form->handler->BeginReceive(state->buffer, 0, StateObject::BufferSize, SocketFlags::None, gcnew AsyncCallback(ReadCallback), state);

				 }
				 
		private: static void ReadCallback(IAsyncResult^ ar) {

					 System::Diagnostics::Debug::WriteLine("start read call back");

					 String^ content = String::Empty;

					 // Retrieve the state object and the handler socket
					 // from the asynchronous state object.
					 StateObject^ state = (StateObject^)ar->AsyncState;
					 Socket^ handler = state->form->handler;
					 
					 System::Diagnostics::Debug::WriteLine(state->ToString());

					 // Read data from the client socket. 
					 int bytesRead = 0;
					 if (handler == nullptr || handler->Connected == false ) return;
					 try{
						 bytesRead = handler->EndReceive(ar);
					 }
					 catch (Exception^ e){
						 //System::Windows::Forms::MessageBox::Show("TCP Broke here 3");
					 }
					 if (bytesRead > 0) {
						 // There  might be more data, so store the data received so far.
						 state->sb->Append(Encoding::ASCII->GetString(
							 state->buffer, 0, bytesRead));

						 // Check for end-of-file tag. If it is not there, read 
						 // more data.
						 content = state->sb->ToString();
						 if (content->IndexOf("\n") > -1) {
							 // All the data has been read from the 
							 // client. Display it on the console.
							 System::Diagnostics::Debug::WriteLine("Read {0} bytes from socket. \n Data : {1}",
								 content->Length, content);
							 process_received_data(content,state->form);
							  //wait for a new round of data:
							 WaitForNewComingData(state->form);


						 }
						 else {
							 // Not all data received. Get more.
							 handler->BeginReceive(state->buffer, 0, StateObject::BufferSize, SocketFlags::None,
								 gcnew AsyncCallback(ReadCallback), state);
						 }
					 }
				 }

			
	private: static void process_received_data(String^ received_message, LIR::Form1^ form){


				 // Add medications
				 /*this->label_totalDrugs->Text = this->WMon->GetDrugCount().ToString();
				 for (int k = this->WMon->GetDrugCount() - 1; k >= 0; k = k - 1)*/
				 //{
					 // Is it a simple Bolus?
				 ReceivedData^ received_drug = gcnew ReceivedData();
				 received_drug = JsonConvert::DeserializeObject<ReceivedData^>(received_message);
				 if (received_drug->action == "remove"){
					form->WMon->Invoke(form->WMon->removeDrug, received_drug->UID);
				 }
				 else if (received_drug->action == "add"){
					 form->WMon->Invoke(form->WMon->addDrug, received_drug->bolus, received_drug->drip, received_drug->drug, received_drug->UID);
				 }
				 else if (received_drug->action == "ON"){
					 System::Diagnostics::Debug::WriteLine("Received ON/OFF command");
				 }
	}

	private: static void Send(Socket^ handler, String^ data) {
				 // Convert the string data to byte data using ASCII encoding.
				 try{
					 array<unsigned char>^ byteData = Encoding::ASCII->GetBytes(data);

					 // Begin sending the data to the remote device.
					 handler->BeginSend(byteData, 0, byteData->Length, SocketFlags::None,
						 gcnew AsyncCallback(SendCallback), handler);
				 }
				 catch (Exception^ e){
					 handler->Shutdown(SocketShutdown::Both);
					 handler->Close();
				 }
				}

				 private: static void SendCallback(IAsyncResult^ ar) {
					 try {
						 // Retrieve the socket from the state object.
						 Socket^ handler = (Socket^)ar->AsyncState;

						 // Complete sending the data to the remote device.
						 int bytesSent = handler->EndSend(ar);
						 System::Diagnostics::Debug::WriteLine("Sent {0} bytes to client.", bytesSent);

						/* handler->Shutdown(SocketShutdown::Both);
						 handler->Close();*/

					 }
					 catch (Exception^ e) {
						 System::Diagnostics::Debug::WriteLine(e->ToString());
					 }
				 }
		//private: System::Void TCP_callback2(System::IAsyncResult^ result){
		//			 try{
		//				 // int server_number = result->AsyncState;
		//				 System::Net::Sockets::TcpListener^ listner = (System::Net::Sockets::TcpListener^) result->AsyncState;
		//				 System::Net::Sockets::TcpClient^ client = listner->EndAcceptTcpClient(result);
		//				 client->NoDelay = TRUE;
		//				 TCP_Server_listen(client, 2); //server_number = 2
		//				 // System::Windows::Forms::MessageBox::Show("Client Accepted - on Server 2", client->ToString());
		//			 }
		//			 catch (Exception^ e){
		//				 e;
		//				 //System::Windows::Forms::MessageBox::Show("TCP Call Back error", e->ToString());
		//				 this->TcpReader2 = nullptr;
		//				 this->TcpWriter2 = nullptr;
		//				 return;
		//			 }
		//}

		private: System::Void TCP_Server_listen(System::Net::Sockets::TcpClient^ client){
					 try{
						 //if (server_number == 1){
							 System::Net::Sockets::NetworkStream^ ns = client->GetStream();
							 this->TcpReader = gcnew System::IO::StreamReader(ns, System::Text::Encoding::UTF8);
							 this->TcpWriter = gcnew System::IO::StreamWriter(ns, System::Text::Encoding::UTF8);
						 //}
						 /*else if (server_number == 2){
							 System::Net::Sockets::NetworkStream^ ns = client->GetStream();
							 this->TcpReader2 = gcnew System::IO::StreamReader(ns, System::Text::Encoding::UTF8);
							 this->TcpWriter2 = gcnew System::IO::StreamWriter(ns, System::Text::Encoding::UTF8);
						 }*/
						 //this->TcpWriter->AutoFlush = TRUE;
						 // System::Windows::Forms::MessageBox::Show(String::Concat("Streams created for server ", System::Convert::ToString(server_number)));
						 // Keep in mind for debugging - String::Concat(this->TcpReader->ToString(),this->TcpWriter->ToString())
						 //TCP_use_message(TcpReader);
					 }
					 catch (Exception^ e){
						 e;
						 System::Windows::Forms::MessageBox::Show("TCP Client - no stream", e->ToString());

						 /*this->TcpReader->Close();
						 this->TcpWriter->Close();
						 client->Close();*/

						 this->TcpReader = nullptr;
						 this->TcpWriter = nullptr;
						 //this->TcpReader2 = nullptr;
						 //this->TcpWriter2 = nullptr;
						 return;
					 }
		}

		private: System::Void TCP_use_message(System::IO::StreamReader^ reader){
					 try{
						 //int mark = 0;
						 //Tcp_mutex->WaitOne();
						 String^ A = reader->ReadLine();
						 if (A->Length > 1){
							 System::Windows::Forms::MessageBox::Show(A);
						 }
					 }
					 catch (Exception^ e){
						 //System::Windows::Forms::MessageBox::Show("TCP Use message error", e->ToString());
						 //this->TcpReader->Close();
						 return;
					 }

					 ////array<String^>^ A = gcnew array<String^>(reader->ReadLine());

					 //// this->ListenSocket->Receive(A);
					 //while (mark + 3 < A->Length)
					 //{
					 // // Command, Bolus ID, Speed
					 // if (A[0 + mark] == ACTION_FLUID_BOLUS)
					 // {
					 //	 // Start
					 //	 this->NetBID = A[1 + mark];
					 //	 this->NetRate = A[2 + mark] * 100;
					 // }
					 // else if (A[0 + mark] == ACTION_STOP_BOLUS)
					 // {
					 //	 this->NetBID = 0;
					 //	 this->NetRate = 0;
					 // }
					 // else if (A[0 + mark] == ACTION_PHENYLEPHRINE_BOLUS)
					 // {
					 //	 // next two bytes store dose in nanograms
					 //	 // first byte is high, second byte is low (Big-endian)
					 //	 double rate = double(A[1 + mark]) * 256 + double(A[2 + mark]);
					 //	 // Convert to milligrams
					 //	 rate = rate / 1000;
					 //	 Patient->AddMedication(DRUG_NEOSYNEPHRINE, rate);
					 // }
					 // mark = mark + 3;
					 //}
					 //// Running Bolus. Is is the same one?
					 //if (NetBID != this->LirBolusID)
					 //{
					 // // Reset the fluid count
					 // this->FluidGivenByLirThisBolus = 0;
					 // this->LirBolusID = NetBID;
					 //}
					 //// Get the fluid rate and type
					 //double RATE = double(NetRate) / 3600;
					 //// Insert into simulator
					 //Patient->AddMedication(200, RATE);
					 //this->FluidGivenByLirThisBolus += RATE;
		}

				 // Use only TCP server 1 to write messages
		private: System::Void TCP_send_message(){
					 try{
						 if (this->TcpWriter != nullptr){
							 System::String^ message = JsonConvert::SerializeObject(this->TCP_data);
							 //System::Windows::Forms::MessageBox::Show(message);
							 //_tprintf(_T("%s"), message);
							 //this->TcpWriter->WriteLine(message);
						 }
						 //System::Threading::Thread::Sleep(1);
						 //this->TcpWriter->Flush();
						 //System::Windows::Forms::MessageBox::Show(message);
						 // System::Windows::Forms::MessageBox::Show("I am listening for connections on " + IPAddress::Parse(((IPEndPoint^)(this->TCP_listen->LocalEndpoint))->Address->ToString()) + ":"+ ((IPEndPoint^)(this->TCP_listen->LocalEndpoint))->Port);
					 }
					 catch (Exception^ e){

						 // Shehzi - Don't know why try-catch is required!
						 // e;
						 //System::Windows::Forms::MessageBox::Show("TCP Send Message Error", e->ToString());
						 //this->TcpWriter = nullptr;
						 return;
					 }

		}


				 //edit by Shehzi

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	public:

	Drugs^ DrugList;

	Simulator::VitalsPack^ Vitals;
	Simulator::VitalsPack^ PreviousState;
	System::Random^ Rx;
	String^ PatientID;

	System::Collections::ArrayList^ KEYS;

	bool WaitingForControlResponse;

	double SVerror;
	int ThirtySecondCounter;

	Simulator::VitalsPack^ StartVitals;
	Simulator::VitalsPack^ EndVitals;
	int CrossfadeSecondsCount;
	int CrossfadeSecondTime;
	double FluidGivenByLirThisBolus;
	int LirBolusID;
	
	double WMonAlbuminGiven;
	double WMonLRGiven;
	double WMonBloodGiven;
	double WMonBloodLoss;

	protected:
		System::IO::StreamWriter^ TcpWriter; // TCP stream writer for the TCP_listen socket - the application runs as server
		System::IO::StreamReader^ TcpReader; // TCP stream reader for the TCP_listen socket 
		//System::Threading::Mutex^ Tcp_mutex; // Mutex to ensure the client is accepted asynchronously 
		System::Net::Sockets::TcpListener^ TCP_listen; // The server scoket - only for sending data from the simulator
		Socket^ handler = nullptr;
		//System::Net::Sockets::TcpListener^ TCP_listen2; // Another server only for receiving data 
		//System::IO::StreamWriter^ TcpWriter2; // TCP stream writer for the TCP_listen2 socket - the application runs as server
		//System::IO::StreamReader^ TcpReader2; // TCP stream reader for the TCP_listen2 socket 
		// To show HgB every few seconds
		int show_hgb = 180; // Show HgB on the display every 180 seconds
		int interval_hgb = 10; // Show HgB for 10 seconds
		int Action_log_flag; // Set this variable to 1 to log actions
		//System::Threading::Thread^ send_data_thread;
		CDSIM_TCP_Pack^ TCP_data;
		//edit by Pei and Shehzi
		System::Net::Sockets::UdpClient^ BroadcastSocket;
		System::Net::Sockets::Socket^ ListenSocket;
		BackgroundWorker^ worker;

	String^ LoadedCommands;
	String^ LoadedRhythym;
	ArrayList^ SlotOptions;
	int airwayPressure;
	LIR::WaveMonitor^ WMon;
	Simulator::CDSimPatient^ Patient;

	static System::String^ LASTSIMLIR="";
	//static System::Diagnostics::Process ^ENGINE = gcnew System::Diagnostics::Process();

	//static LIR::BolusInterfaceX^ BOFACE;
	///static LIR::VitalsLog^ VITALS;

	private: int NetBID;
	private: int NetRate;
			 
	private: System::Windows::Forms::Button^  button1;

	private: System::Windows::Forms::GroupBox^  groupBox1;

	private: System::Windows::Forms::Panel^  panel2;
	private: System::Windows::Forms::Label^  vitals_SV;

	private: System::Windows::Forms::Label^  label8;
	private: System::Windows::Forms::Label^  vitals_volR;

	private: System::Windows::Forms::Label^  label11;
	private: System::Windows::Forms::Label^  vitals_BloodVol;

	private: System::Windows::Forms::Label^  label13;
	private: System::Windows::Forms::Label^  vitals_LVESV;
	private: System::Windows::Forms::Label^  label14;
private: System::Windows::Forms::Label^  vitals_SepFac;

	private: System::Windows::Forms::Label^  label16;
	private: System::Windows::Forms::Label^  label12;
	private: System::Windows::Forms::TextBox^  tb_baseHR;
	private: System::Windows::Forms::TextBox^  tb_baseSBP;
	private: System::Windows::Forms::Label^  label15;
	private: System::Windows::Forms::TextBox^  tb_baseCVP;

	private: System::Windows::Forms::Label^  label17;
	private: System::Windows::Forms::TextBox^  tb_baseDBP;
	private: System::Windows::Forms::Label^  label18;
	private: System::Windows::Forms::TextBox^  tb_baseLVEDV;

	private: System::Windows::Forms::Label^  label19;
private: System::Windows::Forms::TextBox^  tb_baseSepF;


	private: System::Windows::Forms::Label^  label20;
	private: System::Windows::Forms::TextBox^  tb_baseIn;

	private: System::Windows::Forms::Label^  label22;
	private: System::Windows::Forms::TextBox^  tb_baseKG;

	private: System::Windows::Forms::Label^  label23;
	private: System::Windows::Forms::Label^  vitals_SVR;

	private: System::Windows::Forms::Label^  label25;
	private: System::Windows::Forms::Panel^  panel3;
	private: System::Windows::Forms::StatusStrip^  statusStrip1;



private: System::Windows::Forms::Label^  label_CLOCK;
private: System::Windows::Forms::Panel^  panel12;




private: System::Windows::Forms::CheckBox^  CB_speed_turbo;
private: System::Windows::Forms::CheckBox^  CB_speed_real;
private: System::Windows::Forms::Timer^  TIMER_SIM;


private: System::Windows::Forms::Label^  label7;


private: System::Windows::Forms::Label^  label26;
private: System::Windows::Forms::Label^  label27;
private: System::Windows::Forms::TextBox^  textBox1;
private: System::Windows::Forms::CheckBox^  checkBox1;
private: System::Windows::Forms::CheckBox^  checkBox2;
private: System::Windows::Forms::Label^  label28;
private: System::Windows::Forms::Label^  label29;
private: System::Windows::Forms::Panel^  panel5;
private: System::Windows::Forms::Label^  label30;
private: System::Windows::Forms::Label^  label31;
private: System::Windows::Forms::Label^  label32;
private: System::Windows::Forms::Label^  label33;
private: System::Windows::Forms::Label^  label34;
private: System::Windows::Forms::Label^  label35;
private: System::Windows::Forms::Label^  label36;
private: System::Windows::Forms::Label^  label37;
private: System::Windows::Forms::Label^  label38;
private: System::Windows::Forms::Label^  label39;
private: System::Windows::Forms::Label^  label45;
private: System::Windows::Forms::Label^  label46;
private: System::Windows::Forms::Label^  label47;
private: System::Windows::Forms::Label^  label49;
private: System::Windows::Forms::GroupBox^  groupBox2;
private: System::Windows::Forms::Label^  label51;
private: System::Windows::Forms::Button^  button2;
private: System::Windows::Forms::Panel^  panel6;
private: System::Windows::Forms::Label^  label53;
private: System::Windows::Forms::Label^  label54;
private: System::Windows::Forms::Label^  label55;
private: System::Windows::Forms::Label^  label67;
private: System::Windows::Forms::Label^  label68;
private: System::Windows::Forms::Label^  label70;
private: System::Windows::Forms::Label^  label71;
private: System::Windows::Forms::Label^  label72;
private: System::Windows::Forms::Label^  label73;
private: System::Windows::Forms::Label^  label74;
private: System::Windows::Forms::Panel^  panel7;
private: System::Windows::Forms::Label^  label75;
private: System::Windows::Forms::TextBox^  textBox2;
private: System::Windows::Forms::TextBox^  textBox3;
private: System::Windows::Forms::Label^  label76;
private: System::Windows::Forms::Label^  label77;
private: System::Windows::Forms::TextBox^  textBox4;
private: System::Windows::Forms::TextBox^  textBox5;
private: System::Windows::Forms::Label^  label78;
private: System::Windows::Forms::Label^  label79;
private: System::Windows::Forms::TextBox^  textBox7;
private: System::Windows::Forms::TextBox^  textBox8;
private: System::Windows::Forms::Label^  label80;
private: System::Windows::Forms::Label^  label81;
private: System::Windows::Forms::TextBox^  textBox9;
private: System::Windows::Forms::TextBox^  textBox10;
private: System::Windows::Forms::Label^  label82;

private: System::Windows::Forms::Panel^  panel13;

private: System::Windows::Forms::Label^  BIG_BP;
private: System::Windows::Forms::Label^  BIG_HR;
private: System::Windows::Forms::Label^  BIG_MAP;
private: System::Windows::Forms::Label^  label89;
private: System::Windows::Forms::Label^  BIG_CardO;

private: System::Windows::Forms::Button^  GIVE_NEO;
private: System::Windows::Forms::Label^  BIG_PPV;




private: System::Windows::Forms::Button^  button10;
private: System::Windows::Forms::Button^  button8;



private: System::Windows::Forms::Label^  label_medsgiven;
private: System::Windows::Forms::GroupBox^  groupBox3;
private: System::Windows::Forms::Timer^  timer_meds;





private: System::Windows::Forms::Label^  label21;
private: System::Windows::Forms::Label^  label10;
private: System::Windows::Forms::TextBox^  tb_maxIn;

private: System::Windows::Forms::TextBox^  tb_maxHR;
private: System::Windows::Forms::TextBox^  tb_maxKg;
private: System::Windows::Forms::TextBox^  tb_maxSBP;

private: System::Windows::Forms::TextBox^  tb_maxLVEDV;
private: System::Windows::Forms::TextBox^  tb_maxDBP;
private: System::Windows::Forms::TextBox^  tb_maxSepF;

private: System::Windows::Forms::TextBox^  tb_maxCVP;
private: System::Windows::Forms::Label^  label2;
private: System::Windows::Forms::Panel^  panel1;
private: System::Windows::Forms::TextBox^  tb_PPVbias;

private: System::Windows::Forms::Label^  label9;
private: System::Windows::Forms::TextBox^  tb_stability;

private: System::Windows::Forms::Label^  label6;
private: System::Windows::Forms::TextBox^  tb_Flux;

private: System::Windows::Forms::Label^  label5;
private: System::Windows::Forms::TextBox^  tb_PPVWand;

private: System::Windows::Forms::Label^  label4;
private: System::Windows::Forms::PictureBox^  pictureBox2;






private: System::Windows::Forms::Label^  lab_stim;
private: System::Windows::Forms::Label^  label57;
private: System::Windows::Forms::Label^  lab_epi;
private: System::Windows::Forms::Label^  label43;
private: System::Windows::Forms::Label^  lab_narc;
private: System::Windows::Forms::Label^  lab_neo;
private: System::Windows::Forms::Label^  label50;
private: System::Windows::Forms::Label^  label52;




private: System::Windows::Forms::Button^  Fire;
private: System::Windows::Forms::Label^  lab_preload;
private: System::Windows::Forms::Label^  label48;
private: System::Windows::Forms::Label^  label62;
private: System::Windows::Forms::Label^  label42;

private: System::Windows::Forms::Label^  label58;
private: System::Windows::Forms::Label^  label59;
private: System::Windows::Forms::Label^  label60;
private: System::Windows::Forms::Label^  label61;
private: System::Windows::Forms::MenuStrip^  menuStrip1;
private: System::Windows::Forms::ToolStripMenuItem^  fileToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  exitToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  aboutToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  aboutCDSimToolStripMenuItem;
private: System::Windows::Forms::DomainUpDown^  UD_Resp;

private: System::Windows::Forms::Label^  label63;

private: System::Windows::Forms::DomainUpDown^  UD_PCon;


private: System::Windows::Forms::DomainUpDown^  UD_TV;
private: System::Windows::Forms::GroupBox^  groupBox4;

private: System::Windows::Forms::Label^  label69;
private: System::Windows::Forms::RadioButton^  radio_PC;

private: System::Windows::Forms::RadioButton^  radio_VC;
private: System::Windows::Forms::Label^  lab_PAP;
private: System::Windows::Forms::Label^  label65;
private: System::Windows::Forms::Button^  button3;
private: System::Windows::Forms::TextBox^  tb_VVar;

private: System::Windows::Forms::Label^  label64;
private: System::Windows::Forms::TextBox^  tb_highHgb;
private: System::Windows::Forms::TextBox^  tb_lowHgb;
private: System::Windows::Forms::Label^  label83;
private: System::Windows::Forms::Label^  vitals_CM;

private: System::Windows::Forms::Label^  label85;
private: System::Windows::Forms::Label^  vitals_RCM;

private: System::Windows::Forms::Label^  label87;
private: System::Windows::Forms::Label^  vitals_PlatM;

private: System::Windows::Forms::Label^  label90;
private: System::Windows::Forms::TabControl^  tabControl1;
private: System::Windows::Forms::TabPage^  tabPage1;
private: System::Windows::Forms::TabPage^  tabPage2;
private: System::Windows::Forms::GroupBox^  groupBox5;
private: System::Windows::Forms::Label^  label84;
private: System::Windows::Forms::Button^  buttDisplayOFF;

private: System::Windows::Forms::Button^  button4;


private: System::Windows::Forms::Label^  label99;
private: System::Windows::Forms::TextBox^  dis_PACO2;



private: System::Windows::Forms::Label^  label101;
private: System::Windows::Forms::TextBox^  dis_SV;
private: System::Windows::Forms::TextBox^  dis_Deadspace;


private: System::Windows::Forms::Label^  label102;
private: System::Windows::Forms::Label^  label103;
private: System::Windows::Forms::TextBox^  dis_Hgbx;


private: System::Windows::Forms::TextBox^  dis_Outflow;

private: System::Windows::Forms::Label^  label104;
private: System::Windows::Forms::Label^  label105;
private: System::Windows::Forms::TextBox^  dis_PVI;

private: System::Windows::Forms::TextBox^  dis_SVV;

private: System::Windows::Forms::Label^  label106;
private: System::Windows::Forms::TextBox^  dis_SP02;

private: System::Windows::Forms::Label^  label86;
private: System::Windows::Forms::Label^  label91;
private: System::Windows::Forms::TextBox^  dis_SVR;

private: System::Windows::Forms::TextBox^  dis_HR;

private: System::Windows::Forms::Label^  label92;
private: System::Windows::Forms::Label^  label93;
private: System::Windows::Forms::TextBox^  dis_PPV;

private: System::Windows::Forms::TextBox^  dis_SBP;

private: System::Windows::Forms::Label^  label94;
private: System::Windows::Forms::Label^  label95;
private: System::Windows::Forms::TextBox^  dis_CO;
private: System::Windows::Forms::TextBox^  dis_DBP;


private: System::Windows::Forms::Label^  label96;
private: System::Windows::Forms::Label^  label97;
private: System::Windows::Forms::TextBox^  dis_CVP;

private: System::Windows::Forms::TextBox^  dis_MAP;

private: System::Windows::Forms::Label^  label98;


private: System::Windows::Forms::Label^  label109;
private: System::Windows::Forms::Label^  label110;
private: System::Windows::Forms::Label^  label108;
private: System::Windows::Forms::Label^  label107;
private: System::Windows::Forms::TextBox^  dis_HgB;

private: System::Windows::Forms::Label^  hgblabel;

private: System::Windows::Forms::Label^  label88;
private: System::Windows::Forms::Label^  label100;
private: System::Windows::Forms::Label^  WARN_SVR;
private: System::Windows::Forms::TabPage^  tabPage3;
private: System::Windows::Forms::Label^  label112;
private: System::Windows::Forms::Label^  label111;
private: System::Windows::Forms::Label^  label117;
private: System::Windows::Forms::Label^  label116;
private: System::Windows::Forms::Label^  label115;
private: System::Windows::Forms::Label^  label114;
private: System::Windows::Forms::Label^  label113;
private: System::Windows::Forms::ComboBox^  comboBox2;
private: System::Windows::Forms::ComboBox^  comboBox3;



private: System::Windows::Forms::ComboBox^  comboBox4;

private: System::Windows::Forms::ComboBox^  comboBox5;
private: System::Windows::Forms::ComboBox^  comboBox6;



private: System::Windows::Forms::ComboBox^  comboBox1;

private: System::Windows::Forms::TextBox^  dis_CI;

private: System::Windows::Forms::Label^  label119;
private: System::Windows::Forms::Label^  label120;
private: System::Windows::Forms::TextBox^  dis_PASP;
private: System::Windows::Forms::Label^  labelPADP;
private: System::Windows::Forms::TextBox^  dis_PADP;




private: System::Windows::Forms::Label^  label122;
private: System::Windows::Forms::TextBox^  dis_SCvO2;

private: System::Windows::Forms::TextBox^  dis_SvO2;

private: System::Windows::Forms::Label^  label123;
private: System::Windows::Forms::TextBox^  dis_xfade;

private: System::Windows::Forms::Button^  button5;
private: System::Windows::Forms::Label^  label121;
private: System::Windows::Forms::TextBox^  dis_FTc;

private: System::Windows::Forms::Label^  labelFTc;

private: System::Windows::Forms::CheckBox^  checkBox3;


private: System::Windows::Forms::Label^  label128;
private: System::Windows::Forms::Label^  label127;


private: System::Windows::Forms::Label^  label125;
private: System::Windows::Forms::ToolStripStatusLabel^  toolStripStatusLabel1;
private: System::Windows::Forms::Label^  label_PLS;



private: System::Windows::Forms::TrackBar^  trackBar_Preload;
private: System::Windows::Forms::Label^  label129;
private: System::Windows::Forms::Label^  label130;
private: System::Windows::Forms::Label^  label131;
private: System::Windows::Forms::Label^  label132;
private: System::Windows::Forms::GroupBox^  groupBox7;
private: System::Windows::Forms::GroupBox^  groupBox6;
private: System::Windows::Forms::Timer^  timerCrossfade;
private: System::Windows::Forms::ProgressBar^  progressBar1;
private: System::Windows::Forms::Label^  label135;
private: System::Windows::Forms::Label^  label134;
private: System::Windows::Forms::Label^  label133;
private: System::Windows::Forms::TextBox^  dis_CoreTemp;
private: System::Windows::Forms::Label^  label136;
private: System::Windows::Forms::TextBox^  dis_Variability;
private: System::Windows::Forms::Timer^  timerDisplayVariability;
private: System::Windows::Forms::GroupBox^  groupBox8;
private: System::Windows::Forms::Button^  button9;
private: System::Windows::Forms::Button^  button7;
//private: System::Windows::Forms::GroupBox^  groupBox8;
private: System::Windows::Forms::CheckBox^  cb_ETCO2;
private: System::Windows::Forms::CheckBox^  cb_Aline;
private: System::Windows::Forms::Button^  button11;
private: System::Windows::Forms::Button^  button12;
private: System::Windows::Forms::ComboBox^  comboBox_State;

private: System::Windows::Forms::ComboBox^  comboBox_Case;

private: System::Windows::Forms::Button^  button13;
private: System::Windows::Forms::GroupBox^  groupBox9;
private: System::Windows::Forms::Label^  label126;
private: System::Windows::Forms::Label^  label124;
private: System::Windows::Forms::ToolStripStatusLabel^  toolStripStatusLabel2;


private: System::Windows::Forms::TextBox^  textBox6;
private: System::Windows::Forms::SaveFileDialog^  saveFileDialog1;
private: System::Windows::Forms::GroupBox^  groupBox10;
private: System::Windows::Forms::CheckBox^  checkBox4;
private: System::Windows::Forms::Button^  button14;


private: System::Windows::Forms::GroupBox^  groupBox11;
private: System::Windows::Forms::CheckBox^  cb_DBrecord;
private: System::Windows::Forms::CheckBox^  cb_DBlisten;


private: System::Windows::Forms::CheckBox^  cb_MonitorOn;


private: System::Windows::Forms::PictureBox^  pictureBox1;
private: System::Windows::Forms::GroupBox^  groupBox13;
private: System::Windows::Forms::Button^  button_chooseScript;

private: System::Windows::Forms::TextBox^  textBox_script;


private: System::Windows::Forms::TextBox^  textBox_prefix;
private: System::Windows::Forms::GroupBox^  groupBox14;
private: System::Windows::Forms::Label^  label_loops;

private: System::Windows::Forms::DomainUpDown^  domainUpDown1;
private: System::Windows::Forms::PictureBox^  pictureBox3;
private: System::Windows::Forms::Timer^  timer_ControlWatcher;
private: System::Windows::Forms::Label^  label_savefile;
private: System::Windows::Forms::Label^  lab_sweep;
private: System::Windows::Forms::TextBox^  tb_SVWand;

private: System::Windows::Forms::Label^  label3;

private: System::Windows::Forms::GroupBox^  groupBox12;
private: System::Windows::Forms::Button^  button15;
private: System::Windows::Forms::Button^  button16;
private: System::Windows::Forms::Label^  label_savefile2;
private: System::Windows::Forms::TextBox^  tb_BVmax;


private: System::Windows::Forms::TextBox^  tb_BVmin;

private: System::Windows::Forms::Label^  label41;

private: System::Windows::Forms::CheckBox^  checkBoxGraph1;
private: System::Windows::Forms::TextBox^  tb_SVDsec;
private: System::Windows::Forms::Label^  label44;
private: System::Windows::Forms::CheckBox^  checkBoxGraph2;
private: System::Windows::Forms::GroupBox^  groupBox15;
private: System::Windows::Forms::GroupBox^  groupBox16;
private: System::Windows::Forms::Label^  label118;
private: System::Windows::Forms::GroupBox^  groupBox17;
private: System::Windows::Forms::ComboBox^  comboBox_SCRIPTS;
private: System::Windows::Forms::TextBox^  textBox_scenario;
private: System::Windows::Forms::TabPage^  tabPage4;
private: System::Windows::Forms::TextBox^  textBox_phase;
private: System::Windows::Forms::CheckBox^  checkBox5;
private: System::Windows::Forms::Label^  label_SVx;
private: System::Windows::Forms::Label^  label24;
private: System::Windows::Forms::Label^  label_KgWeight;
private: System::Windows::Forms::Label^  label40;






private: System::Windows::Forms::CheckBox^  cb_NoPPV;
private: System::Windows::Forms::Panel^  panel4;
private: System::Windows::Forms::Label^  label137;
private: System::Windows::Forms::CheckBox^  cb_PPVinvert;
private: System::Windows::Forms::GroupBox^  groupBox19;
private: System::Windows::Forms::GroupBox^  groupBox18;
private: System::Windows::Forms::TextBox^  tb_CNTmax;

private: System::Windows::Forms::TextBox^  tb_CNTmin;

private: System::Windows::Forms::Label^  label138;
private: System::Windows::Forms::TextBox^  tb_PPVWandFreqMax;
private: System::Windows::Forms::TextBox^  tb_PPVWandFreq;
private: System::Windows::Forms::Label^  label1;
private: System::Windows::Forms::TextBox^  tb_SVDsecMax;
private: System::Windows::Forms::TextBox^  tb_SVWandMax;

private: System::Windows::Forms::TextBox^  tb_PPVbiasMax;


private: System::Windows::Forms::TextBox^  tb_PPVWandMax;
private: System::Windows::Forms::Label^  label56;
private: System::Windows::Forms::Label^  label66;
private: System::Windows::Forms::Label^  lab_CNT;
private: System::Windows::Forms::Label^  label140;
private: System::Windows::Forms::GroupBox^  groupBox20;
private: System::Windows::Forms::Label^  label142;
private: System::Windows::Forms::TextBox^  tb_listenPort;
private: System::Windows::Forms::CheckBox^  cb_listenNetwork;
private: System::Windows::Forms::Label^  label141;
private: System::Windows::Forms::TextBox^  tb_netport;
private: System::Windows::Forms::Label^  label139;
private: System::Windows::Forms::TextBox^  tb_netadd;
private: System::Windows::Forms::CheckBox^  cb_BroadcastNet;
private: System::Windows::Forms::CheckBox^  cb_StartLag;
private: System::Windows::Forms::Label^  label_percOpt;
private: System::Windows::Forms::Label^  label144;
private: System::Windows::Forms::CheckBox^  checkBoxBarGraph6;
private: System::Windows::Forms::CheckBox^  checkBoxBarGraph5;
private: System::IO::FileSystemWatcher^  fileSystemWatcher1;
private: System::Windows::Forms::CheckBox^  checkBox_repeatScenario;
private: System::Windows::Forms::Label^  label147;
private: System::Windows::Forms::Label^  label146;
private: System::Windows::Forms::Label^  label145;
private: System::Windows::Forms::Label^  label143;
private: System::Windows::Forms::TrackBar^  trackBar_ART;
private: System::Windows::Forms::CheckBox^  cb_PulseOx;
private: System::Windows::Forms::CheckBox^  cb_EKG;
private: System::Windows::Forms::Label^  label149;
private: System::Windows::Forms::Label^  label148;
private: System::Windows::Forms::Label^  label_totalDrugs;
private: System::Windows::Forms::Label^  label_Drug_Given;
private: System::Windows::Forms::Label^  label_DOPA;
private: System::Windows::Forms::Label^  label151;

















private: System::ComponentModel::IContainer^  components;

	protected: 

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Form1::typeid));
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->vitals_SVR = (gcnew System::Windows::Forms::Label());
			this->label25 = (gcnew System::Windows::Forms::Label());
			this->panel2 = (gcnew System::Windows::Forms::Panel());
			this->label_DOPA = (gcnew System::Windows::Forms::Label());
			this->label151 = (gcnew System::Windows::Forms::Label());
			this->label_percOpt = (gcnew System::Windows::Forms::Label());
			this->lab_CNT = (gcnew System::Windows::Forms::Label());
			this->label140 = (gcnew System::Windows::Forms::Label());
			this->label144 = (gcnew System::Windows::Forms::Label());
			this->label_KgWeight = (gcnew System::Windows::Forms::Label());
			this->label40 = (gcnew System::Windows::Forms::Label());
			this->label_SVx = (gcnew System::Windows::Forms::Label());
			this->label24 = (gcnew System::Windows::Forms::Label());
			this->vitals_CM = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label85 = (gcnew System::Windows::Forms::Label());
			this->vitals_RCM = (gcnew System::Windows::Forms::Label());
			this->label87 = (gcnew System::Windows::Forms::Label());
			this->vitals_PlatM = (gcnew System::Windows::Forms::Label());
			this->label90 = (gcnew System::Windows::Forms::Label());
			this->lab_PAP = (gcnew System::Windows::Forms::Label());
			this->label65 = (gcnew System::Windows::Forms::Label());
			this->BIG_PPV = (gcnew System::Windows::Forms::Label());
			this->BIG_BP = (gcnew System::Windows::Forms::Label());
			this->BIG_MAP = (gcnew System::Windows::Forms::Label());
			this->label89 = (gcnew System::Windows::Forms::Label());
			this->BIG_CardO = (gcnew System::Windows::Forms::Label());
			this->label62 = (gcnew System::Windows::Forms::Label());
			this->label42 = (gcnew System::Windows::Forms::Label());
			this->BIG_HR = (gcnew System::Windows::Forms::Label());
			this->label58 = (gcnew System::Windows::Forms::Label());
			this->label59 = (gcnew System::Windows::Forms::Label());
			this->label60 = (gcnew System::Windows::Forms::Label());
			this->label61 = (gcnew System::Windows::Forms::Label());
			this->lab_preload = (gcnew System::Windows::Forms::Label());
			this->label48 = (gcnew System::Windows::Forms::Label());
			this->lab_stim = (gcnew System::Windows::Forms::Label());
			this->label57 = (gcnew System::Windows::Forms::Label());
			this->lab_epi = (gcnew System::Windows::Forms::Label());
			this->label43 = (gcnew System::Windows::Forms::Label());
			this->lab_narc = (gcnew System::Windows::Forms::Label());
			this->lab_neo = (gcnew System::Windows::Forms::Label());
			this->label50 = (gcnew System::Windows::Forms::Label());
			this->label52 = (gcnew System::Windows::Forms::Label());
			this->vitals_LVESV = (gcnew System::Windows::Forms::Label());
			this->label14 = (gcnew System::Windows::Forms::Label());
			this->vitals_SepFac = (gcnew System::Windows::Forms::Label());
			this->label16 = (gcnew System::Windows::Forms::Label());
			this->vitals_SV = (gcnew System::Windows::Forms::Label());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->vitals_volR = (gcnew System::Windows::Forms::Label());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->vitals_BloodVol = (gcnew System::Windows::Forms::Label());
			this->label13 = (gcnew System::Windows::Forms::Label());
			this->label12 = (gcnew System::Windows::Forms::Label());
			this->tb_baseHR = (gcnew System::Windows::Forms::TextBox());
			this->tb_baseSBP = (gcnew System::Windows::Forms::TextBox());
			this->label15 = (gcnew System::Windows::Forms::Label());
			this->tb_baseCVP = (gcnew System::Windows::Forms::TextBox());
			this->label17 = (gcnew System::Windows::Forms::Label());
			this->tb_baseDBP = (gcnew System::Windows::Forms::TextBox());
			this->label18 = (gcnew System::Windows::Forms::Label());
			this->tb_baseLVEDV = (gcnew System::Windows::Forms::TextBox());
			this->label19 = (gcnew System::Windows::Forms::Label());
			this->tb_baseSepF = (gcnew System::Windows::Forms::TextBox());
			this->label20 = (gcnew System::Windows::Forms::Label());
			this->tb_baseIn = (gcnew System::Windows::Forms::TextBox());
			this->label22 = (gcnew System::Windows::Forms::Label());
			this->tb_baseKG = (gcnew System::Windows::Forms::TextBox());
			this->label23 = (gcnew System::Windows::Forms::Label());
			this->panel3 = (gcnew System::Windows::Forms::Panel());
			this->tb_CNTmax = (gcnew System::Windows::Forms::TextBox());
			this->tb_CNTmin = (gcnew System::Windows::Forms::TextBox());
			this->label138 = (gcnew System::Windows::Forms::Label());
			this->tb_BVmax = (gcnew System::Windows::Forms::TextBox());
			this->tb_BVmin = (gcnew System::Windows::Forms::TextBox());
			this->label41 = (gcnew System::Windows::Forms::Label());
			this->tb_highHgb = (gcnew System::Windows::Forms::TextBox());
			this->tb_lowHgb = (gcnew System::Windows::Forms::TextBox());
			this->label83 = (gcnew System::Windows::Forms::Label());
			this->label21 = (gcnew System::Windows::Forms::Label());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->tb_maxIn = (gcnew System::Windows::Forms::TextBox());
			this->tb_maxHR = (gcnew System::Windows::Forms::TextBox());
			this->tb_maxKg = (gcnew System::Windows::Forms::TextBox());
			this->tb_maxSBP = (gcnew System::Windows::Forms::TextBox());
			this->tb_maxLVEDV = (gcnew System::Windows::Forms::TextBox());
			this->tb_maxDBP = (gcnew System::Windows::Forms::TextBox());
			this->tb_maxSepF = (gcnew System::Windows::Forms::TextBox());
			this->tb_maxCVP = (gcnew System::Windows::Forms::TextBox());
			this->statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
			this->toolStripStatusLabel2 = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->CB_speed_turbo = (gcnew System::Windows::Forms::CheckBox());
			this->CB_speed_real = (gcnew System::Windows::Forms::CheckBox());
			this->panel13 = (gcnew System::Windows::Forms::Panel());
			this->textBox_phase = (gcnew System::Windows::Forms::TextBox());
			this->groupBox8 = (gcnew System::Windows::Forms::GroupBox());
			this->label149 = (gcnew System::Windows::Forms::Label());
			this->label148 = (gcnew System::Windows::Forms::Label());
			this->label147 = (gcnew System::Windows::Forms::Label());
			this->label146 = (gcnew System::Windows::Forms::Label());
			this->label145 = (gcnew System::Windows::Forms::Label());
			this->label143 = (gcnew System::Windows::Forms::Label());
			this->trackBar_ART = (gcnew System::Windows::Forms::TrackBar());
			this->checkBox5 = (gcnew System::Windows::Forms::CheckBox());
			this->label118 = (gcnew System::Windows::Forms::Label());
			this->lab_sweep = (gcnew System::Windows::Forms::Label());
			this->cb_MonitorOn = (gcnew System::Windows::Forms::CheckBox());
			this->button9 = (gcnew System::Windows::Forms::Button());
			this->button7 = (gcnew System::Windows::Forms::Button());
			this->groupBox4 = (gcnew System::Windows::Forms::GroupBox());
			this->radio_PC = (gcnew System::Windows::Forms::RadioButton());
			this->radio_VC = (gcnew System::Windows::Forms::RadioButton());
			this->label69 = (gcnew System::Windows::Forms::Label());
			this->UD_TV = (gcnew System::Windows::Forms::DomainUpDown());
			this->UD_PCon = (gcnew System::Windows::Forms::DomainUpDown());
			this->UD_Resp = (gcnew System::Windows::Forms::DomainUpDown());
			this->label63 = (gcnew System::Windows::Forms::Label());
			this->groupBox3 = (gcnew System::Windows::Forms::GroupBox());
			this->label_medsgiven = (gcnew System::Windows::Forms::Label());
			this->GIVE_NEO = (gcnew System::Windows::Forms::Button());
			this->button10 = (gcnew System::Windows::Forms::Button());
			this->button8 = (gcnew System::Windows::Forms::Button());
			this->panel12 = (gcnew System::Windows::Forms::Panel());
			this->pictureBox2 = (gcnew System::Windows::Forms::PictureBox());
			this->label_CLOCK = (gcnew System::Windows::Forms::Label());
			this->groupBox12 = (gcnew System::Windows::Forms::GroupBox());
			this->Fire = (gcnew System::Windows::Forms::Button());
			this->button15 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button16 = (gcnew System::Windows::Forms::Button());
			this->TIMER_SIM = (gcnew System::Windows::Forms::Timer(this->components));
			this->label26 = (gcnew System::Windows::Forms::Label());
			this->label27 = (gcnew System::Windows::Forms::Label());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->checkBox1 = (gcnew System::Windows::Forms::CheckBox());
			this->checkBox2 = (gcnew System::Windows::Forms::CheckBox());
			this->label28 = (gcnew System::Windows::Forms::Label());
			this->label29 = (gcnew System::Windows::Forms::Label());
			this->panel5 = (gcnew System::Windows::Forms::Panel());
			this->label30 = (gcnew System::Windows::Forms::Label());
			this->label31 = (gcnew System::Windows::Forms::Label());
			this->label32 = (gcnew System::Windows::Forms::Label());
			this->label33 = (gcnew System::Windows::Forms::Label());
			this->label34 = (gcnew System::Windows::Forms::Label());
			this->label35 = (gcnew System::Windows::Forms::Label());
			this->label36 = (gcnew System::Windows::Forms::Label());
			this->label37 = (gcnew System::Windows::Forms::Label());
			this->label38 = (gcnew System::Windows::Forms::Label());
			this->label39 = (gcnew System::Windows::Forms::Label());
			this->label45 = (gcnew System::Windows::Forms::Label());
			this->label46 = (gcnew System::Windows::Forms::Label());
			this->label47 = (gcnew System::Windows::Forms::Label());
			this->label49 = (gcnew System::Windows::Forms::Label());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->label51 = (gcnew System::Windows::Forms::Label());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->panel6 = (gcnew System::Windows::Forms::Panel());
			this->label53 = (gcnew System::Windows::Forms::Label());
			this->label54 = (gcnew System::Windows::Forms::Label());
			this->label55 = (gcnew System::Windows::Forms::Label());
			this->label67 = (gcnew System::Windows::Forms::Label());
			this->label68 = (gcnew System::Windows::Forms::Label());
			this->label70 = (gcnew System::Windows::Forms::Label());
			this->label71 = (gcnew System::Windows::Forms::Label());
			this->label72 = (gcnew System::Windows::Forms::Label());
			this->label73 = (gcnew System::Windows::Forms::Label());
			this->label74 = (gcnew System::Windows::Forms::Label());
			this->panel7 = (gcnew System::Windows::Forms::Panel());
			this->label75 = (gcnew System::Windows::Forms::Label());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->label76 = (gcnew System::Windows::Forms::Label());
			this->label77 = (gcnew System::Windows::Forms::Label());
			this->textBox4 = (gcnew System::Windows::Forms::TextBox());
			this->textBox5 = (gcnew System::Windows::Forms::TextBox());
			this->label78 = (gcnew System::Windows::Forms::Label());
			this->label79 = (gcnew System::Windows::Forms::Label());
			this->textBox7 = (gcnew System::Windows::Forms::TextBox());
			this->textBox8 = (gcnew System::Windows::Forms::TextBox());
			this->label80 = (gcnew System::Windows::Forms::Label());
			this->label81 = (gcnew System::Windows::Forms::Label());
			this->textBox9 = (gcnew System::Windows::Forms::TextBox());
			this->textBox10 = (gcnew System::Windows::Forms::TextBox());
			this->label82 = (gcnew System::Windows::Forms::Label());
			this->timer_meds = (gcnew System::Windows::Forms::Timer(this->components));
			this->panel1 = (gcnew System::Windows::Forms::Panel());
			this->label56 = (gcnew System::Windows::Forms::Label());
			this->label66 = (gcnew System::Windows::Forms::Label());
			this->tb_PPVWandFreqMax = (gcnew System::Windows::Forms::TextBox());
			this->tb_PPVWandFreq = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->tb_SVDsecMax = (gcnew System::Windows::Forms::TextBox());
			this->tb_SVWandMax = (gcnew System::Windows::Forms::TextBox());
			this->tb_PPVbiasMax = (gcnew System::Windows::Forms::TextBox());
			this->tb_PPVWandMax = (gcnew System::Windows::Forms::TextBox());
			this->tb_SVDsec = (gcnew System::Windows::Forms::TextBox());
			this->label44 = (gcnew System::Windows::Forms::Label());
			this->tb_SVWand = (gcnew System::Windows::Forms::TextBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->tb_VVar = (gcnew System::Windows::Forms::TextBox());
			this->label64 = (gcnew System::Windows::Forms::Label());
			this->tb_PPVbias = (gcnew System::Windows::Forms::TextBox());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->tb_stability = (gcnew System::Windows::Forms::TextBox());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->tb_Flux = (gcnew System::Windows::Forms::TextBox());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->tb_PPVWand = (gcnew System::Windows::Forms::TextBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->fileToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->exitToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->aboutToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->aboutCDSimToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->tabControl1 = (gcnew System::Windows::Forms::TabControl());
			this->tabPage1 = (gcnew System::Windows::Forms::TabPage());
			this->groupBox17 = (gcnew System::Windows::Forms::GroupBox());
			this->label_totalDrugs = (gcnew System::Windows::Forms::Label());
			this->label_Drug_Given = (gcnew System::Windows::Forms::Label());
			this->checkBox_repeatScenario = (gcnew System::Windows::Forms::CheckBox());
			this->textBox_scenario = (gcnew System::Windows::Forms::TextBox());
			this->comboBox_SCRIPTS = (gcnew System::Windows::Forms::ComboBox());
			this->tabPage2 = (gcnew System::Windows::Forms::TabPage());
			this->button13 = (gcnew System::Windows::Forms::Button());
			this->groupBox9 = (gcnew System::Windows::Forms::GroupBox());
			this->label126 = (gcnew System::Windows::Forms::Label());
			this->label124 = (gcnew System::Windows::Forms::Label());
			this->button12 = (gcnew System::Windows::Forms::Button());
			this->comboBox_State = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox_Case = (gcnew System::Windows::Forms::ComboBox());
			this->button11 = (gcnew System::Windows::Forms::Button());
			this->label136 = (gcnew System::Windows::Forms::Label());
			this->dis_Variability = (gcnew System::Windows::Forms::TextBox());
			this->label134 = (gcnew System::Windows::Forms::Label());
			this->label133 = (gcnew System::Windows::Forms::Label());
			this->dis_CoreTemp = (gcnew System::Windows::Forms::TextBox());
			this->label135 = (gcnew System::Windows::Forms::Label());
			this->progressBar1 = (gcnew System::Windows::Forms::ProgressBar());
			this->label132 = (gcnew System::Windows::Forms::Label());
			this->groupBox7 = (gcnew System::Windows::Forms::GroupBox());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->groupBox6 = (gcnew System::Windows::Forms::GroupBox());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->dis_xfade = (gcnew System::Windows::Forms::TextBox());
			this->label121 = (gcnew System::Windows::Forms::Label());
			this->label131 = (gcnew System::Windows::Forms::Label());
			this->label130 = (gcnew System::Windows::Forms::Label());
			this->label129 = (gcnew System::Windows::Forms::Label());
			this->checkBox3 = (gcnew System::Windows::Forms::CheckBox());
			this->label_PLS = (gcnew System::Windows::Forms::Label());
			this->trackBar_Preload = (gcnew System::Windows::Forms::TrackBar());
			this->label128 = (gcnew System::Windows::Forms::Label());
			this->label127 = (gcnew System::Windows::Forms::Label());
			this->label125 = (gcnew System::Windows::Forms::Label());
			this->dis_FTc = (gcnew System::Windows::Forms::TextBox());
			this->labelFTc = (gcnew System::Windows::Forms::Label());
			this->label122 = (gcnew System::Windows::Forms::Label());
			this->dis_SCvO2 = (gcnew System::Windows::Forms::TextBox());
			this->dis_SvO2 = (gcnew System::Windows::Forms::TextBox());
			this->label123 = (gcnew System::Windows::Forms::Label());
			this->label120 = (gcnew System::Windows::Forms::Label());
			this->dis_PASP = (gcnew System::Windows::Forms::TextBox());
			this->labelPADP = (gcnew System::Windows::Forms::Label());
			this->dis_PADP = (gcnew System::Windows::Forms::TextBox());
			this->dis_CI = (gcnew System::Windows::Forms::TextBox());
			this->label119 = (gcnew System::Windows::Forms::Label());
			this->WARN_SVR = (gcnew System::Windows::Forms::Label());
			this->label100 = (gcnew System::Windows::Forms::Label());
			this->label88 = (gcnew System::Windows::Forms::Label());
			this->dis_HgB = (gcnew System::Windows::Forms::TextBox());
			this->hgblabel = (gcnew System::Windows::Forms::Label());
			this->label109 = (gcnew System::Windows::Forms::Label());
			this->label110 = (gcnew System::Windows::Forms::Label());
			this->label108 = (gcnew System::Windows::Forms::Label());
			this->label107 = (gcnew System::Windows::Forms::Label());
			this->label99 = (gcnew System::Windows::Forms::Label());
			this->dis_PACO2 = (gcnew System::Windows::Forms::TextBox());
			this->label101 = (gcnew System::Windows::Forms::Label());
			this->dis_SV = (gcnew System::Windows::Forms::TextBox());
			this->dis_Deadspace = (gcnew System::Windows::Forms::TextBox());
			this->label102 = (gcnew System::Windows::Forms::Label());
			this->label103 = (gcnew System::Windows::Forms::Label());
			this->dis_Hgbx = (gcnew System::Windows::Forms::TextBox());
			this->dis_Outflow = (gcnew System::Windows::Forms::TextBox());
			this->label104 = (gcnew System::Windows::Forms::Label());
			this->label105 = (gcnew System::Windows::Forms::Label());
			this->dis_PVI = (gcnew System::Windows::Forms::TextBox());
			this->dis_SVV = (gcnew System::Windows::Forms::TextBox());
			this->label106 = (gcnew System::Windows::Forms::Label());
			this->dis_SP02 = (gcnew System::Windows::Forms::TextBox());
			this->label86 = (gcnew System::Windows::Forms::Label());
			this->label91 = (gcnew System::Windows::Forms::Label());
			this->dis_SVR = (gcnew System::Windows::Forms::TextBox());
			this->dis_HR = (gcnew System::Windows::Forms::TextBox());
			this->label92 = (gcnew System::Windows::Forms::Label());
			this->label93 = (gcnew System::Windows::Forms::Label());
			this->dis_PPV = (gcnew System::Windows::Forms::TextBox());
			this->dis_SBP = (gcnew System::Windows::Forms::TextBox());
			this->label94 = (gcnew System::Windows::Forms::Label());
			this->label95 = (gcnew System::Windows::Forms::Label());
			this->dis_CO = (gcnew System::Windows::Forms::TextBox());
			this->dis_DBP = (gcnew System::Windows::Forms::TextBox());
			this->label96 = (gcnew System::Windows::Forms::Label());
			this->label97 = (gcnew System::Windows::Forms::Label());
			this->dis_CVP = (gcnew System::Windows::Forms::TextBox());
			this->dis_MAP = (gcnew System::Windows::Forms::TextBox());
			this->label98 = (gcnew System::Windows::Forms::Label());
			this->groupBox5 = (gcnew System::Windows::Forms::GroupBox());
			this->label84 = (gcnew System::Windows::Forms::Label());
			this->buttDisplayOFF = (gcnew System::Windows::Forms::Button());
			this->tabPage3 = (gcnew System::Windows::Forms::TabPage());
			this->groupBox19 = (gcnew System::Windows::Forms::GroupBox());
			this->cb_PulseOx = (gcnew System::Windows::Forms::CheckBox());
			this->cb_EKG = (gcnew System::Windows::Forms::CheckBox());
			this->cb_Aline = (gcnew System::Windows::Forms::CheckBox());
			this->cb_ETCO2 = (gcnew System::Windows::Forms::CheckBox());
			this->groupBox18 = (gcnew System::Windows::Forms::GroupBox());
			this->checkBoxBarGraph6 = (gcnew System::Windows::Forms::CheckBox());
			this->checkBoxBarGraph5 = (gcnew System::Windows::Forms::CheckBox());
			this->comboBox5 = (gcnew System::Windows::Forms::ComboBox());
			this->label113 = (gcnew System::Windows::Forms::Label());
			this->checkBoxGraph2 = (gcnew System::Windows::Forms::CheckBox());
			this->label114 = (gcnew System::Windows::Forms::Label());
			this->comboBox6 = (gcnew System::Windows::Forms::ComboBox());
			this->checkBoxGraph1 = (gcnew System::Windows::Forms::CheckBox());
			this->groupBox16 = (gcnew System::Windows::Forms::GroupBox());
			this->comboBox1 = (gcnew System::Windows::Forms::ComboBox());
			this->label111 = (gcnew System::Windows::Forms::Label());
			this->label115 = (gcnew System::Windows::Forms::Label());
			this->label116 = (gcnew System::Windows::Forms::Label());
			this->label117 = (gcnew System::Windows::Forms::Label());
			this->comboBox2 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox4 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox3 = (gcnew System::Windows::Forms::ComboBox());
			this->label112 = (gcnew System::Windows::Forms::Label());
			this->tabPage4 = (gcnew System::Windows::Forms::TabPage());
			this->label_loops = (gcnew System::Windows::Forms::Label());
			this->groupBox20 = (gcnew System::Windows::Forms::GroupBox());
			this->cb_StartLag = (gcnew System::Windows::Forms::CheckBox());
			this->label142 = (gcnew System::Windows::Forms::Label());
			this->tb_listenPort = (gcnew System::Windows::Forms::TextBox());
			this->cb_listenNetwork = (gcnew System::Windows::Forms::CheckBox());
			this->label141 = (gcnew System::Windows::Forms::Label());
			this->tb_netport = (gcnew System::Windows::Forms::TextBox());
			this->label139 = (gcnew System::Windows::Forms::Label());
			this->tb_netadd = (gcnew System::Windows::Forms::TextBox());
			this->cb_BroadcastNet = (gcnew System::Windows::Forms::CheckBox());
			this->domainUpDown1 = (gcnew System::Windows::Forms::DomainUpDown());
			this->panel4 = (gcnew System::Windows::Forms::Panel());
			this->label137 = (gcnew System::Windows::Forms::Label());
			this->cb_PPVinvert = (gcnew System::Windows::Forms::CheckBox());
			this->cb_NoPPV = (gcnew System::Windows::Forms::CheckBox());
			this->groupBox11 = (gcnew System::Windows::Forms::GroupBox());
			this->cb_DBlisten = (gcnew System::Windows::Forms::CheckBox());
			this->cb_DBrecord = (gcnew System::Windows::Forms::CheckBox());
			this->groupBox15 = (gcnew System::Windows::Forms::GroupBox());
			this->textBox_prefix = (gcnew System::Windows::Forms::TextBox());
			this->pictureBox3 = (gcnew System::Windows::Forms::PictureBox());
			this->groupBox14 = (gcnew System::Windows::Forms::GroupBox());
			this->label_savefile2 = (gcnew System::Windows::Forms::Label());
			this->label_savefile = (gcnew System::Windows::Forms::Label());
			this->groupBox13 = (gcnew System::Windows::Forms::GroupBox());
			this->button_chooseScript = (gcnew System::Windows::Forms::Button());
			this->textBox_script = (gcnew System::Windows::Forms::TextBox());
			this->groupBox10 = (gcnew System::Windows::Forms::GroupBox());
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->checkBox4 = (gcnew System::Windows::Forms::CheckBox());
			this->textBox6 = (gcnew System::Windows::Forms::TextBox());
			this->button14 = (gcnew System::Windows::Forms::Button());
			this->timerCrossfade = (gcnew System::Windows::Forms::Timer(this->components));
			this->timerDisplayVariability = (gcnew System::Windows::Forms::Timer(this->components));
			this->saveFileDialog1 = (gcnew System::Windows::Forms::SaveFileDialog());
			this->timer_ControlWatcher = (gcnew System::Windows::Forms::Timer(this->components));
			this->fileSystemWatcher1 = (gcnew System::IO::FileSystemWatcher());
			this->groupBox1->SuspendLayout();
			this->panel2->SuspendLayout();
			this->panel3->SuspendLayout();
			this->statusStrip1->SuspendLayout();
			this->panel13->SuspendLayout();
			this->groupBox8->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBar_ART))->BeginInit();
			this->groupBox4->SuspendLayout();
			this->groupBox3->SuspendLayout();
			this->panel12->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->BeginInit();
			this->groupBox12->SuspendLayout();
			this->panel5->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->panel6->SuspendLayout();
			this->panel7->SuspendLayout();
			this->panel1->SuspendLayout();
			this->menuStrip1->SuspendLayout();
			this->tabControl1->SuspendLayout();
			this->tabPage1->SuspendLayout();
			this->groupBox17->SuspendLayout();
			this->tabPage2->SuspendLayout();
			this->groupBox9->SuspendLayout();
			this->groupBox7->SuspendLayout();
			this->groupBox6->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBar_Preload))->BeginInit();
			this->groupBox5->SuspendLayout();
			this->tabPage3->SuspendLayout();
			this->groupBox19->SuspendLayout();
			this->groupBox18->SuspendLayout();
			this->groupBox16->SuspendLayout();
			this->tabPage4->SuspendLayout();
			this->groupBox20->SuspendLayout();
			this->panel4->SuspendLayout();
			this->groupBox11->SuspendLayout();
			this->groupBox15->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox3))->BeginInit();
			this->groupBox14->SuspendLayout();
			this->groupBox13->SuspendLayout();
			this->groupBox10->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->fileSystemWatcher1))->BeginInit();
			this->SuspendLayout();
			// 
			// button1
			// 
			this->button1->BackColor = System::Drawing::Color::LightCoral;
			this->button1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->button1->Location = System::Drawing::Point(18, 13);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(56, 27);
			this->button1->TabIndex = 0;
			this->button1->Text = L"OFF";
			this->button1->UseVisualStyleBackColor = false;
			this->button1->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->button1);
			this->groupBox1->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->groupBox1->Location = System::Drawing::Point(3, 21);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(93, 53);
			this->groupBox1->TabIndex = 2;
			this->groupBox1->TabStop = false;
			// 
			// vitals_SVR
			// 
			this->vitals_SVR->AutoSize = true;
			this->vitals_SVR->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->vitals_SVR->ForeColor = System::Drawing::Color::Black;
			this->vitals_SVR->Location = System::Drawing::Point(49, 280);
			this->vitals_SVR->Name = L"vitals_SVR";
			this->vitals_SVR->Size = System::Drawing::Size(15, 13);
			this->vitals_SVR->TabIndex = 13;
			this->vitals_SVR->Text = L"--";
			// 
			// label25
			// 
			this->label25->AutoSize = true;
			this->label25->Location = System::Drawing::Point(13, 280);
			this->label25->Name = L"label25";
			this->label25->Size = System::Drawing::Size(32, 13);
			this->label25->TabIndex = 12;
			this->label25->Text = L"SVR:";
			this->label25->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// panel2
			// 
			this->panel2->BackColor = System::Drawing::SystemColors::Control;
			this->panel2->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->panel2->Controls->Add(this->label_DOPA);
			this->panel2->Controls->Add(this->label151);
			this->panel2->Controls->Add(this->label_percOpt);
			this->panel2->Controls->Add(this->lab_CNT);
			this->panel2->Controls->Add(this->label140);
			this->panel2->Controls->Add(this->label144);
			this->panel2->Controls->Add(this->label_KgWeight);
			this->panel2->Controls->Add(this->label40);
			this->panel2->Controls->Add(this->label_SVx);
			this->panel2->Controls->Add(this->label24);
			this->panel2->Controls->Add(this->vitals_CM);
			this->panel2->Controls->Add(this->groupBox1);
			this->panel2->Controls->Add(this->label7);
			this->panel2->Controls->Add(this->label85);
			this->panel2->Controls->Add(this->vitals_RCM);
			this->panel2->Controls->Add(this->label87);
			this->panel2->Controls->Add(this->vitals_PlatM);
			this->panel2->Controls->Add(this->label90);
			this->panel2->Controls->Add(this->lab_PAP);
			this->panel2->Controls->Add(this->label65);
			this->panel2->Controls->Add(this->BIG_PPV);
			this->panel2->Controls->Add(this->BIG_BP);
			this->panel2->Controls->Add(this->BIG_MAP);
			this->panel2->Controls->Add(this->label89);
			this->panel2->Controls->Add(this->BIG_CardO);
			this->panel2->Controls->Add(this->label62);
			this->panel2->Controls->Add(this->label42);
			this->panel2->Controls->Add(this->BIG_HR);
			this->panel2->Controls->Add(this->label58);
			this->panel2->Controls->Add(this->label59);
			this->panel2->Controls->Add(this->label60);
			this->panel2->Controls->Add(this->label61);
			this->panel2->Controls->Add(this->lab_preload);
			this->panel2->Controls->Add(this->label48);
			this->panel2->Controls->Add(this->lab_stim);
			this->panel2->Controls->Add(this->label57);
			this->panel2->Controls->Add(this->lab_epi);
			this->panel2->Controls->Add(this->label43);
			this->panel2->Controls->Add(this->lab_narc);
			this->panel2->Controls->Add(this->lab_neo);
			this->panel2->Controls->Add(this->label50);
			this->panel2->Controls->Add(this->label52);
			this->panel2->Controls->Add(this->vitals_LVESV);
			this->panel2->Controls->Add(this->label14);
			this->panel2->Controls->Add(this->vitals_SVR);
			this->panel2->Controls->Add(this->vitals_SepFac);
			this->panel2->Controls->Add(this->label25);
			this->panel2->Controls->Add(this->label16);
			this->panel2->Controls->Add(this->vitals_SV);
			this->panel2->Controls->Add(this->label8);
			this->panel2->Controls->Add(this->vitals_volR);
			this->panel2->Controls->Add(this->label11);
			this->panel2->Controls->Add(this->vitals_BloodVol);
			this->panel2->Controls->Add(this->label13);
			this->panel2->Location = System::Drawing::Point(3, 26);
			this->panel2->Name = L"panel2";
			this->panel2->Size = System::Drawing::Size(101, 487);
			this->panel2->TabIndex = 4;
			// 
			// label_DOPA
			// 
			this->label_DOPA->AutoSize = true;
			this->label_DOPA->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label_DOPA->ForeColor = System::Drawing::SystemColors::WindowText;
			this->label_DOPA->Location = System::Drawing::Point(50, 353);
			this->label_DOPA->Name = L"label_DOPA";
			this->label_DOPA->Size = System::Drawing::Size(15, 13);
			this->label_DOPA->TabIndex = 63;
			this->label_DOPA->Text = L"--";
			// 
			// label151
			// 
			this->label151->AutoSize = true;
			this->label151->Location = System::Drawing::Point(8, 353);
			this->label151->Name = L"label151";
			this->label151->Size = System::Drawing::Size(33, 13);
			this->label151->TabIndex = 62;
			this->label151->Text = L"Dopa";
			this->label151->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// label_percOpt
			// 
			this->label_percOpt->AutoSize = true;
			this->label_percOpt->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label_percOpt->ForeColor = System::Drawing::Color::Firebrick;
			this->label_percOpt->Location = System::Drawing::Point(49, 391);
			this->label_percOpt->Name = L"label_percOpt";
			this->label_percOpt->Size = System::Drawing::Size(15, 13);
			this->label_percOpt->TabIndex = 61;
			this->label_percOpt->Text = L"--";
			// 
			// lab_CNT
			// 
			this->lab_CNT->AutoSize = true;
			this->lab_CNT->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->lab_CNT->ForeColor = System::Drawing::SystemColors::ControlText;
			this->lab_CNT->Location = System::Drawing::Point(48, 466);
			this->lab_CNT->Name = L"lab_CNT";
			this->lab_CNT->Size = System::Drawing::Size(15, 13);
			this->lab_CNT->TabIndex = 58;
			this->lab_CNT->Text = L"--";
			// 
			// label140
			// 
			this->label140->AutoSize = true;
			this->label140->Location = System::Drawing::Point(12, 466);
			this->label140->Name = L"label140";
			this->label140->Size = System::Drawing::Size(29, 13);
			this->label140->TabIndex = 59;
			this->label140->Text = L"CNT";
			this->label140->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// label144
			// 
			this->label144->AutoSize = true;
			this->label144->ForeColor = System::Drawing::Color::Firebrick;
			this->label144->Location = System::Drawing::Point(7, 392);
			this->label144->Name = L"label144";
			this->label144->Size = System::Drawing::Size(38, 13);
			this->label144->TabIndex = 60;
			this->label144->Text = L"% Opt:";
			this->label144->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// label_KgWeight
			// 
			this->label_KgWeight->AutoSize = true;
			this->label_KgWeight->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label_KgWeight->ForeColor = System::Drawing::SystemColors::ControlText;
			this->label_KgWeight->Location = System::Drawing::Point(48, 453);
			this->label_KgWeight->Name = L"label_KgWeight";
			this->label_KgWeight->Size = System::Drawing::Size(15, 13);
			this->label_KgWeight->TabIndex = 56;
			this->label_KgWeight->Text = L"--";
			// 
			// label40
			// 
			this->label40->AutoSize = true;
			this->label40->Location = System::Drawing::Point(19, 453);
			this->label40->Name = L"label40";
			this->label40->Size = System::Drawing::Size(23, 13);
			this->label40->TabIndex = 57;
			this->label40->Text = L"Kg:";
			this->label40->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// label_SVx
			// 
			this->label_SVx->AutoSize = true;
			this->label_SVx->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label_SVx->ForeColor = System::Drawing::SystemColors::WindowText;
			this->label_SVx->Location = System::Drawing::Point(49, 225);
			this->label_SVx->Name = L"label_SVx";
			this->label_SVx->Size = System::Drawing::Size(15, 13);
			this->label_SVx->TabIndex = 55;
			this->label_SVx->Text = L"--";
			// 
			// label24
			// 
			this->label24->AutoSize = true;
			this->label24->Location = System::Drawing::Point(17, 225);
			this->label24->Name = L"label24";
			this->label24->Size = System::Drawing::Size(29, 13);
			this->label24->TabIndex = 54;
			this->label24->Text = L"SVx:";
			this->label24->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// vitals_CM
			// 
			this->vitals_CM->AutoSize = true;
			this->vitals_CM->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->vitals_CM->ForeColor = System::Drawing::Color::Firebrick;
			this->vitals_CM->Location = System::Drawing::Point(48, 431);
			this->vitals_CM->Name = L"vitals_CM";
			this->vitals_CM->Size = System::Drawing::Size(15, 13);
			this->vitals_CM->TabIndex = 46;
			this->vitals_CM->Text = L"--";
			// 
			// label7
			// 
			this->label7->Font = (gcnew System::Drawing::Font(L"Impact", 13, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Pixel,
				static_cast<System::Byte>(0)));
			this->label7->Location = System::Drawing::Point(7, 0);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(84, 18);
			this->label7->TabIndex = 53;
			this->label7->Text = L"Internal State";
			// 
			// label85
			// 
			this->label85->AutoSize = true;
			this->label85->ForeColor = System::Drawing::Color::Firebrick;
			this->label85->Location = System::Drawing::Point(2, 431);
			this->label85->Name = L"label85";
			this->label85->Size = System::Drawing::Size(43, 13);
			this->label85->TabIndex = 45;
			this->label85->Text = L"CoagR:";
			this->label85->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// vitals_RCM
			// 
			this->vitals_RCM->AutoSize = true;
			this->vitals_RCM->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->vitals_RCM->ForeColor = System::Drawing::Color::Firebrick;
			this->vitals_RCM->Location = System::Drawing::Point(49, 405);
			this->vitals_RCM->Name = L"vitals_RCM";
			this->vitals_RCM->Size = System::Drawing::Size(15, 13);
			this->vitals_RCM->TabIndex = 44;
			this->vitals_RCM->Text = L"--";
			// 
			// label87
			// 
			this->label87->AutoSize = true;
			this->label87->ForeColor = System::Drawing::Color::Firebrick;
			this->label87->Location = System::Drawing::Point(14, 405);
			this->label87->Name = L"label87";
			this->label87->Size = System::Drawing::Size(31, 13);
			this->label87->TabIndex = 43;
			this->label87->Text = L"HgB:";
			this->label87->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// vitals_PlatM
			// 
			this->vitals_PlatM->AutoSize = true;
			this->vitals_PlatM->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->vitals_PlatM->ForeColor = System::Drawing::Color::Firebrick;
			this->vitals_PlatM->Location = System::Drawing::Point(49, 418);
			this->vitals_PlatM->Name = L"vitals_PlatM";
			this->vitals_PlatM->Size = System::Drawing::Size(15, 13);
			this->vitals_PlatM->TabIndex = 42;
			this->vitals_PlatM->Text = L"--";
			// 
			// label90
			// 
			this->label90->AutoSize = true;
			this->label90->ForeColor = System::Drawing::Color::Firebrick;
			this->label90->Location = System::Drawing::Point(17, 418);
			this->label90->Name = L"label90";
			this->label90->Size = System::Drawing::Size(28, 13);
			this->label90->TabIndex = 41;
			this->label90->Text = L"Plat:";
			this->label90->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// lab_PAP
			// 
			this->lab_PAP->AutoSize = true;
			this->lab_PAP->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->lab_PAP->ForeColor = System::Drawing::Color::BlueViolet;
			this->lab_PAP->Location = System::Drawing::Point(49, 191);
			this->lab_PAP->Name = L"lab_PAP";
			this->lab_PAP->Size = System::Drawing::Size(15, 13);
			this->lab_PAP->TabIndex = 39;
			this->lab_PAP->Text = L"--";
			// 
			// label65
			// 
			this->label65->AutoSize = true;
			this->label65->ForeColor = System::Drawing::Color::BlueViolet;
			this->label65->Location = System::Drawing::Point(15, 191);
			this->label65->Name = L"label65";
			this->label65->Size = System::Drawing::Size(31, 13);
			this->label65->TabIndex = 40;
			this->label65->Text = L"PAP:";
			this->label65->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// BIG_PPV
			// 
			this->BIG_PPV->AutoSize = true;
			this->BIG_PPV->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->BIG_PPV->ForeColor = System::Drawing::SystemColors::ControlText;
			this->BIG_PPV->Location = System::Drawing::Point(49, 170);
			this->BIG_PPV->Name = L"BIG_PPV";
			this->BIG_PPV->Size = System::Drawing::Size(15, 13);
			this->BIG_PPV->TabIndex = 23;
			this->BIG_PPV->Text = L"--";
			// 
			// BIG_BP
			// 
			this->BIG_BP->AutoSize = true;
			this->BIG_BP->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->BIG_BP->ForeColor = System::Drawing::Color::Firebrick;
			this->BIG_BP->Location = System::Drawing::Point(49, 103);
			this->BIG_BP->Name = L"BIG_BP";
			this->BIG_BP->Size = System::Drawing::Size(15, 13);
			this->BIG_BP->TabIndex = 25;
			this->BIG_BP->Text = L"--";
			// 
			// BIG_MAP
			// 
			this->BIG_MAP->AutoSize = true;
			this->BIG_MAP->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->BIG_MAP->ForeColor = System::Drawing::Color::Firebrick;
			this->BIG_MAP->Location = System::Drawing::Point(49, 116);
			this->BIG_MAP->Name = L"BIG_MAP";
			this->BIG_MAP->Size = System::Drawing::Size(15, 13);
			this->BIG_MAP->TabIndex = 19;
			this->BIG_MAP->Text = L"--";
			// 
			// label89
			// 
			this->label89->AutoSize = true;
			this->label89->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label89->ForeColor = System::Drawing::Color::RoyalBlue;
			this->label89->Location = System::Drawing::Point(49, 129);
			this->label89->Name = L"label89";
			this->label89->Size = System::Drawing::Size(15, 13);
			this->label89->TabIndex = 20;
			this->label89->Text = L"--";
			this->label89->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// BIG_CardO
			// 
			this->BIG_CardO->AutoSize = true;
			this->BIG_CardO->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->BIG_CardO->ForeColor = System::Drawing::SystemColors::ControlText;
			this->BIG_CardO->Location = System::Drawing::Point(49, 142);
			this->BIG_CardO->Name = L"BIG_CardO";
			this->BIG_CardO->Size = System::Drawing::Size(15, 13);
			this->BIG_CardO->TabIndex = 21;
			this->BIG_CardO->Text = L"--";
			// 
			// label62
			// 
			this->label62->AutoSize = true;
			this->label62->Location = System::Drawing::Point(15, 170);
			this->label62->Name = L"label62";
			this->label62->Size = System::Drawing::Size(31, 13);
			this->label62->TabIndex = 38;
			this->label62->Text = L"PPV:";
			this->label62->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// label42
			// 
			this->label42->AutoSize = true;
			this->label42->ForeColor = System::Drawing::Color::RoyalBlue;
			this->label42->Location = System::Drawing::Point(9, 129);
			this->label42->Name = L"label42";
			this->label42->Size = System::Drawing::Size(37, 13);
			this->label42->TabIndex = 37;
			this->label42->Text = L"SpO2:";
			this->label42->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// BIG_HR
			// 
			this->BIG_HR->AutoSize = true;
			this->BIG_HR->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->BIG_HR->ForeColor = System::Drawing::Color::ForestGreen;
			this->BIG_HR->Location = System::Drawing::Point(49, 90);
			this->BIG_HR->Name = L"BIG_HR";
			this->BIG_HR->Size = System::Drawing::Size(15, 13);
			this->BIG_HR->TabIndex = 17;
			this->BIG_HR->Text = L"--";
			// 
			// label58
			// 
			this->label58->AutoSize = true;
			this->label58->Location = System::Drawing::Point(21, 142);
			this->label58->Name = L"label58";
			this->label58->Size = System::Drawing::Size(25, 13);
			this->label58->TabIndex = 35;
			this->label58->Text = L"CO:";
			this->label58->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// label59
			// 
			this->label59->AutoSize = true;
			this->label59->ForeColor = System::Drawing::Color::Firebrick;
			this->label59->Location = System::Drawing::Point(13, 116);
			this->label59->Name = L"label59";
			this->label59->Size = System::Drawing::Size(33, 13);
			this->label59->TabIndex = 34;
			this->label59->Text = L"MAP:";
			this->label59->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// label60
			// 
			this->label60->AutoSize = true;
			this->label60->ForeColor = System::Drawing::Color::Firebrick;
			this->label60->Location = System::Drawing::Point(22, 103);
			this->label60->Name = L"label60";
			this->label60->Size = System::Drawing::Size(24, 13);
			this->label60->TabIndex = 33;
			this->label60->Text = L"BP:";
			this->label60->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// label61
			// 
			this->label61->AutoSize = true;
			this->label61->ForeColor = System::Drawing::Color::ForestGreen;
			this->label61->Location = System::Drawing::Point(20, 90);
			this->label61->Name = L"label61";
			this->label61->Size = System::Drawing::Size(26, 13);
			this->label61->TabIndex = 32;
			this->label61->Text = L"HR:";
			this->label61->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// lab_preload
			// 
			this->lab_preload->AutoSize = true;
			this->lab_preload->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->lab_preload->ForeColor = System::Drawing::SystemColors::WindowText;
			this->lab_preload->Location = System::Drawing::Point(49, 241);
			this->lab_preload->Name = L"lab_preload";
			this->lab_preload->Size = System::Drawing::Size(15, 13);
			this->lab_preload->TabIndex = 31;
			this->lab_preload->Text = L"--";
			// 
			// label48
			// 
			this->label48->AutoSize = true;
			this->label48->Location = System::Drawing::Point(18, 241);
			this->label48->Name = L"label48";
			this->label48->Size = System::Drawing::Size(28, 13);
			this->label48->TabIndex = 30;
			this->label48->Text = L"CVP";
			this->label48->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// lab_stim
			// 
			this->lab_stim->AutoSize = true;
			this->lab_stim->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->lab_stim->ForeColor = System::Drawing::Color::Black;
			this->lab_stim->Location = System::Drawing::Point(49, 342);
			this->lab_stim->Name = L"lab_stim";
			this->lab_stim->Size = System::Drawing::Size(15, 13);
			this->lab_stim->TabIndex = 29;
			this->lab_stim->Text = L"--";
			// 
			// label57
			// 
			this->label57->AutoSize = true;
			this->label57->Location = System::Drawing::Point(15, 342);
			this->label57->Name = L"label57";
			this->label57->Size = System::Drawing::Size(27, 13);
			this->label57->TabIndex = 28;
			this->label57->Text = L"Stim";
			this->label57->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// lab_epi
			// 
			this->lab_epi->AutoSize = true;
			this->lab_epi->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->lab_epi->ForeColor = System::Drawing::SystemColors::WindowText;
			this->lab_epi->Location = System::Drawing::Point(49, 316);
			this->lab_epi->Name = L"lab_epi";
			this->lab_epi->Size = System::Drawing::Size(15, 13);
			this->lab_epi->TabIndex = 27;
			this->lab_epi->Text = L"--";
			// 
			// label43
			// 
			this->label43->AutoSize = true;
			this->label43->Location = System::Drawing::Point(12, 316);
			this->label43->Name = L"label43";
			this->label43->Size = System::Drawing::Size(29, 13);
			this->label43->TabIndex = 26;
			this->label43->Text = L"Beta";
			this->label43->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// lab_narc
			// 
			this->lab_narc->AutoSize = true;
			this->lab_narc->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->lab_narc->ForeColor = System::Drawing::Color::Black;
			this->lab_narc->Location = System::Drawing::Point(49, 329);
			this->lab_narc->Name = L"lab_narc";
			this->lab_narc->Size = System::Drawing::Size(15, 13);
			this->lab_narc->TabIndex = 23;
			this->lab_narc->Text = L"--";
			// 
			// lab_neo
			// 
			this->lab_neo->AutoSize = true;
			this->lab_neo->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->lab_neo->ForeColor = System::Drawing::SystemColors::WindowText;
			this->lab_neo->Location = System::Drawing::Point(49, 303);
			this->lab_neo->Name = L"lab_neo";
			this->lab_neo->Size = System::Drawing::Size(15, 13);
			this->lab_neo->TabIndex = 25;
			this->lab_neo->Text = L"--";
			// 
			// label50
			// 
			this->label50->AutoSize = true;
			this->label50->Location = System::Drawing::Point(12, 329);
			this->label50->Name = L"label50";
			this->label50->Size = System::Drawing::Size(30, 13);
			this->label50->TabIndex = 22;
			this->label50->Text = L"Narc";
			this->label50->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// label52
			// 
			this->label52->AutoSize = true;
			this->label52->Location = System::Drawing::Point(7, 303);
			this->label52->Name = L"label52";
			this->label52->Size = System::Drawing::Size(34, 13);
			this->label52->TabIndex = 24;
			this->label52->Text = L"Alpha";
			this->label52->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// vitals_LVESV
			// 
			this->vitals_LVESV->AutoSize = true;
			this->vitals_LVESV->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->vitals_LVESV->ForeColor = System::Drawing::SystemColors::WindowText;
			this->vitals_LVESV->Location = System::Drawing::Point(49, 267);
			this->vitals_LVESV->Name = L"vitals_LVESV";
			this->vitals_LVESV->Size = System::Drawing::Size(15, 13);
			this->vitals_LVESV->TabIndex = 21;
			this->vitals_LVESV->Text = L"--";
			this->vitals_LVESV->Visible = false;
			// 
			// label14
			// 
			this->label14->AutoSize = true;
			this->label14->Location = System::Drawing::Point(2, 267);
			this->label14->Name = L"label14";
			this->label14->Size = System::Drawing::Size(44, 13);
			this->label14->TabIndex = 20;
			this->label14->Text = L"LVESV:";
			this->label14->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->label14->Visible = false;
			// 
			// vitals_SepFac
			// 
			this->vitals_SepFac->AutoSize = true;
			this->vitals_SepFac->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->vitals_SepFac->ForeColor = System::Drawing::SystemColors::WindowText;
			this->vitals_SepFac->Location = System::Drawing::Point(49, 254);
			this->vitals_SepFac->Name = L"vitals_SepFac";
			this->vitals_SepFac->Size = System::Drawing::Size(15, 13);
			this->vitals_SepFac->TabIndex = 19;
			this->vitals_SepFac->Text = L"--";
			// 
			// label16
			// 
			this->label16->AutoSize = true;
			this->label16->Location = System::Drawing::Point(1, 254);
			this->label16->Name = L"label16";
			this->label16->Size = System::Drawing::Size(44, 13);
			this->label16->TabIndex = 18;
			this->label16->Text = L"SepFac";
			this->label16->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// vitals_SV
			// 
			this->vitals_SV->AutoSize = true;
			this->vitals_SV->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->vitals_SV->ForeColor = System::Drawing::SystemColors::WindowText;
			this->vitals_SV->Location = System::Drawing::Point(49, 211);
			this->vitals_SV->Name = L"vitals_SV";
			this->vitals_SV->Size = System::Drawing::Size(15, 13);
			this->vitals_SV->TabIndex = 17;
			this->vitals_SV->Text = L"--";
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(22, 211);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(24, 13);
			this->label8->TabIndex = 16;
			this->label8->Text = L"SV:";
			this->label8->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// vitals_volR
			// 
			this->vitals_volR->AutoSize = true;
			this->vitals_volR->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->vitals_volR->ForeColor = System::Drawing::Color::Firebrick;
			this->vitals_volR->Location = System::Drawing::Point(49, 379);
			this->vitals_volR->Name = L"vitals_volR";
			this->vitals_volR->Size = System::Drawing::Size(15, 13);
			this->vitals_volR->TabIndex = 15;
			this->vitals_volR->Text = L"--";
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->ForeColor = System::Drawing::Color::Firebrick;
			this->label11->Location = System::Drawing::Point(1, 380);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(44, 13);
			this->label11->TabIndex = 14;
			this->label11->Text = L"% base:";
			this->label11->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// vitals_BloodVol
			// 
			this->vitals_BloodVol->AutoSize = true;
			this->vitals_BloodVol->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->vitals_BloodVol->ForeColor = System::Drawing::Color::Firebrick;
			this->vitals_BloodVol->Location = System::Drawing::Point(49, 366);
			this->vitals_BloodVol->Name = L"vitals_BloodVol";
			this->vitals_BloodVol->Size = System::Drawing::Size(15, 13);
			this->vitals_BloodVol->TabIndex = 13;
			this->vitals_BloodVol->Text = L"--";
			// 
			// label13
			// 
			this->label13->AutoSize = true;
			this->label13->ForeColor = System::Drawing::Color::Firebrick;
			this->label13->Location = System::Drawing::Point(8, 367);
			this->label13->Name = L"label13";
			this->label13->Size = System::Drawing::Size(37, 13);
			this->label13->TabIndex = 12;
			this->label13->Text = L"Blood:";
			this->label13->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// label12
			// 
			this->label12->AutoSize = true;
			this->label12->Location = System::Drawing::Point(3, 39);
			this->label12->Name = L"label12";
			this->label12->Size = System::Drawing::Size(50, 13);
			this->label12->TabIndex = 5;
			this->label12->Text = L"Base HR";
			// 
			// tb_baseHR
			// 
			this->tb_baseHR->Location = System::Drawing::Point(84, 34);
			this->tb_baseHR->Name = L"tb_baseHR";
			this->tb_baseHR->Size = System::Drawing::Size(53, 20);
			this->tb_baseHR->TabIndex = 6;
			this->tb_baseHR->Text = L"70";
			// 
			// tb_baseSBP
			// 
			this->tb_baseSBP->Location = System::Drawing::Point(84, 54);
			this->tb_baseSBP->Name = L"tb_baseSBP";
			this->tb_baseSBP->Size = System::Drawing::Size(53, 20);
			this->tb_baseSBP->TabIndex = 8;
			this->tb_baseSBP->Text = L"115";
			// 
			// label15
			// 
			this->label15->AutoSize = true;
			this->label15->Location = System::Drawing::Point(3, 59);
			this->label15->Name = L"label15";
			this->label15->Size = System::Drawing::Size(55, 13);
			this->label15->TabIndex = 7;
			this->label15->Text = L"Base SBP";
			// 
			// tb_baseCVP
			// 
			this->tb_baseCVP->Location = System::Drawing::Point(84, 94);
			this->tb_baseCVP->Name = L"tb_baseCVP";
			this->tb_baseCVP->Size = System::Drawing::Size(53, 20);
			this->tb_baseCVP->TabIndex = 12;
			this->tb_baseCVP->Text = L"5";
			// 
			// label17
			// 
			this->label17->AutoSize = true;
			this->label17->Location = System::Drawing::Point(3, 99);
			this->label17->Name = L"label17";
			this->label17->Size = System::Drawing::Size(55, 13);
			this->label17->TabIndex = 11;
			this->label17->Text = L"Base CVP";
			// 
			// tb_baseDBP
			// 
			this->tb_baseDBP->Location = System::Drawing::Point(84, 74);
			this->tb_baseDBP->Name = L"tb_baseDBP";
			this->tb_baseDBP->Size = System::Drawing::Size(53, 20);
			this->tb_baseDBP->TabIndex = 10;
			this->tb_baseDBP->Text = L"70";
			// 
			// label18
			// 
			this->label18->AutoSize = true;
			this->label18->Location = System::Drawing::Point(3, 79);
			this->label18->Name = L"label18";
			this->label18->Size = System::Drawing::Size(56, 13);
			this->label18->TabIndex = 9;
			this->label18->Text = L"Base DBP";
			// 
			// tb_baseLVEDV
			// 
			this->tb_baseLVEDV->Location = System::Drawing::Point(84, 306);
			this->tb_baseLVEDV->Name = L"tb_baseLVEDV";
			this->tb_baseLVEDV->Size = System::Drawing::Size(53, 20);
			this->tb_baseLVEDV->TabIndex = 16;
			this->tb_baseLVEDV->Text = L"0";
			this->tb_baseLVEDV->Visible = false;
			// 
			// label19
			// 
			this->label19->AutoSize = true;
			this->label19->Location = System::Drawing::Point(3, 311);
			this->label19->Name = L"label19";
			this->label19->Size = System::Drawing::Size(69, 13);
			this->label19->TabIndex = 15;
			this->label19->Text = L"Base LVEDV";
			this->label19->Visible = false;
			// 
			// tb_baseSepF
			// 
			this->tb_baseSepF->Location = System::Drawing::Point(84, 254);
			this->tb_baseSepF->Name = L"tb_baseSepF";
			this->tb_baseSepF->Size = System::Drawing::Size(53, 20);
			this->tb_baseSepF->TabIndex = 14;
			this->tb_baseSepF->Text = L"0";
			// 
			// label20
			// 
			this->label20->AutoSize = true;
			this->label20->Location = System::Drawing::Point(3, 257);
			this->label20->Name = L"label20";
			this->label20->Size = System::Drawing::Size(71, 13);
			this->label20->TabIndex = 13;
			this->label20->Text = L"Shock Factor";
			// 
			// tb_baseIn
			// 
			this->tb_baseIn->Location = System::Drawing::Point(84, 174);
			this->tb_baseIn->Name = L"tb_baseIn";
			this->tb_baseIn->Size = System::Drawing::Size(53, 20);
			this->tb_baseIn->TabIndex = 20;
			this->tb_baseIn->Text = L"70";
			// 
			// label22
			// 
			this->label22->AutoSize = true;
			this->label22->Location = System::Drawing::Point(3, 179);
			this->label22->Name = L"label22";
			this->label22->Size = System::Drawing::Size(66, 13);
			this->label22->TabIndex = 19;
			this->label22->Text = L"Base Inches";
			// 
			// tb_baseKG
			// 
			this->tb_baseKG->Location = System::Drawing::Point(84, 154);
			this->tb_baseKG->Name = L"tb_baseKG";
			this->tb_baseKG->Size = System::Drawing::Size(53, 20);
			this->tb_baseKG->TabIndex = 18;
			this->tb_baseKG->Text = L"70";
			// 
			// label23
			// 
			this->label23->AutoSize = true;
			this->label23->Location = System::Drawing::Point(3, 159);
			this->label23->Name = L"label23";
			this->label23->Size = System::Drawing::Size(47, 13);
			this->label23->TabIndex = 17;
			this->label23->Text = L"Base Kg";
			// 
			// panel3
			// 
			this->panel3->Controls->Add(this->tb_CNTmax);
			this->panel3->Controls->Add(this->tb_CNTmin);
			this->panel3->Controls->Add(this->label138);
			this->panel3->Controls->Add(this->tb_BVmax);
			this->panel3->Controls->Add(this->tb_BVmin);
			this->panel3->Controls->Add(this->label41);
			this->panel3->Controls->Add(this->tb_highHgb);
			this->panel3->Controls->Add(this->tb_lowHgb);
			this->panel3->Controls->Add(this->label83);
			this->panel3->Controls->Add(this->label21);
			this->panel3->Controls->Add(this->label10);
			this->panel3->Controls->Add(this->tb_maxIn);
			this->panel3->Controls->Add(this->tb_maxHR);
			this->panel3->Controls->Add(this->tb_maxKg);
			this->panel3->Controls->Add(this->tb_maxSBP);
			this->panel3->Controls->Add(this->tb_maxLVEDV);
			this->panel3->Controls->Add(this->tb_maxDBP);
			this->panel3->Controls->Add(this->tb_maxSepF);
			this->panel3->Controls->Add(this->tb_maxCVP);
			this->panel3->Controls->Add(this->label12);
			this->panel3->Controls->Add(this->tb_baseIn);
			this->panel3->Controls->Add(this->tb_baseHR);
			this->panel3->Controls->Add(this->label22);
			this->panel3->Controls->Add(this->label15);
			this->panel3->Controls->Add(this->tb_baseKG);
			this->panel3->Controls->Add(this->tb_baseSBP);
			this->panel3->Controls->Add(this->label23);
			this->panel3->Controls->Add(this->label18);
			this->panel3->Controls->Add(this->tb_baseLVEDV);
			this->panel3->Controls->Add(this->tb_baseDBP);
			this->panel3->Controls->Add(this->label19);
			this->panel3->Controls->Add(this->label17);
			this->panel3->Controls->Add(this->tb_baseSepF);
			this->panel3->Controls->Add(this->tb_baseCVP);
			this->panel3->Controls->Add(this->label20);
			this->panel3->Location = System::Drawing::Point(13, 100);
			this->panel3->Name = L"panel3";
			this->panel3->Size = System::Drawing::Size(278, 341);
			this->panel3->TabIndex = 21;
			// 
			// tb_CNTmax
			// 
			this->tb_CNTmax->Location = System::Drawing::Point(143, 234);
			this->tb_CNTmax->Name = L"tb_CNTmax";
			this->tb_CNTmax->Size = System::Drawing::Size(53, 20);
			this->tb_CNTmax->TabIndex = 64;
			this->tb_CNTmax->Text = L"3000";
			// 
			// tb_CNTmin
			// 
			this->tb_CNTmin->Location = System::Drawing::Point(84, 234);
			this->tb_CNTmin->Name = L"tb_CNTmin";
			this->tb_CNTmin->Size = System::Drawing::Size(53, 20);
			this->tb_CNTmin->TabIndex = 63;
			this->tb_CNTmin->Text = L"3000";
			// 
			// label138
			// 
			this->label138->AutoSize = true;
			this->label138->Location = System::Drawing::Point(3, 237);
			this->label138->Name = L"label138";
			this->label138->Size = System::Drawing::Size(61, 13);
			this->label138->TabIndex = 62;
			this->label138->Text = L"Contractility";
			// 
			// tb_BVmax
			// 
			this->tb_BVmax->Location = System::Drawing::Point(143, 214);
			this->tb_BVmax->Name = L"tb_BVmax";
			this->tb_BVmax->Size = System::Drawing::Size(53, 20);
			this->tb_BVmax->TabIndex = 36;
			this->tb_BVmax->Text = L"100";
			this->tb_BVmax->TextChanged += gcnew System::EventHandler(this, &Form1::textBox11_TextChanged);
			// 
			// tb_BVmin
			// 
			this->tb_BVmin->Location = System::Drawing::Point(84, 214);
			this->tb_BVmin->Name = L"tb_BVmin";
			this->tb_BVmin->Size = System::Drawing::Size(53, 20);
			this->tb_BVmin->TabIndex = 35;
			this->tb_BVmin->Text = L"100";
			// 
			// label41
			// 
			this->label41->AutoSize = true;
			this->label41->Location = System::Drawing::Point(3, 219);
			this->label41->Name = L"label41";
			this->label41->Size = System::Drawing::Size(63, 13);
			this->label41->TabIndex = 34;
			this->label41->Text = L"Blood Vol %";
			// 
			// tb_highHgb
			// 
			this->tb_highHgb->Location = System::Drawing::Point(143, 194);
			this->tb_highHgb->Name = L"tb_highHgb";
			this->tb_highHgb->Size = System::Drawing::Size(53, 20);
			this->tb_highHgb->TabIndex = 33;
			this->tb_highHgb->Text = L"11";
			// 
			// tb_lowHgb
			// 
			this->tb_lowHgb->Location = System::Drawing::Point(84, 194);
			this->tb_lowHgb->Name = L"tb_lowHgb";
			this->tb_lowHgb->Size = System::Drawing::Size(53, 20);
			this->tb_lowHgb->TabIndex = 32;
			this->tb_lowHgb->Text = L"11";
			// 
			// label83
			// 
			this->label83->AutoSize = true;
			this->label83->Location = System::Drawing::Point(3, 199);
			this->label83->Name = L"label83";
			this->label83->Size = System::Drawing::Size(55, 13);
			this->label83->TabIndex = 31;
			this->label83->Text = L"Base HgB";
			// 
			// label21
			// 
			this->label21->AutoSize = true;
			this->label21->Location = System::Drawing::Point(95, 17);
			this->label21->Name = L"label21";
			this->label21->Size = System::Drawing::Size(24, 13);
			this->label21->TabIndex = 30;
			this->label21->Text = L"Min";
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Location = System::Drawing::Point(152, 17);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(27, 13);
			this->label10->TabIndex = 29;
			this->label10->Text = L"Max";
			// 
			// tb_maxIn
			// 
			this->tb_maxIn->Location = System::Drawing::Point(143, 174);
			this->tb_maxIn->Name = L"tb_maxIn";
			this->tb_maxIn->Size = System::Drawing::Size(53, 20);
			this->tb_maxIn->TabIndex = 28;
			this->tb_maxIn->Text = L"70";
			// 
			// tb_maxHR
			// 
			this->tb_maxHR->Location = System::Drawing::Point(143, 34);
			this->tb_maxHR->Name = L"tb_maxHR";
			this->tb_maxHR->Size = System::Drawing::Size(53, 20);
			this->tb_maxHR->TabIndex = 21;
			this->tb_maxHR->Text = L"70";
			// 
			// tb_maxKg
			// 
			this->tb_maxKg->Location = System::Drawing::Point(143, 154);
			this->tb_maxKg->Name = L"tb_maxKg";
			this->tb_maxKg->Size = System::Drawing::Size(53, 20);
			this->tb_maxKg->TabIndex = 27;
			this->tb_maxKg->Text = L"70";
			// 
			// tb_maxSBP
			// 
			this->tb_maxSBP->Location = System::Drawing::Point(143, 54);
			this->tb_maxSBP->Name = L"tb_maxSBP";
			this->tb_maxSBP->Size = System::Drawing::Size(53, 20);
			this->tb_maxSBP->TabIndex = 22;
			this->tb_maxSBP->Text = L"115";
			// 
			// tb_maxLVEDV
			// 
			this->tb_maxLVEDV->Location = System::Drawing::Point(143, 306);
			this->tb_maxLVEDV->Name = L"tb_maxLVEDV";
			this->tb_maxLVEDV->Size = System::Drawing::Size(53, 20);
			this->tb_maxLVEDV->TabIndex = 26;
			this->tb_maxLVEDV->Text = L"0";
			this->tb_maxLVEDV->Visible = false;
			// 
			// tb_maxDBP
			// 
			this->tb_maxDBP->Location = System::Drawing::Point(143, 74);
			this->tb_maxDBP->Name = L"tb_maxDBP";
			this->tb_maxDBP->Size = System::Drawing::Size(53, 20);
			this->tb_maxDBP->TabIndex = 23;
			this->tb_maxDBP->Text = L"70";
			// 
			// tb_maxSepF
			// 
			this->tb_maxSepF->Location = System::Drawing::Point(143, 254);
			this->tb_maxSepF->Name = L"tb_maxSepF";
			this->tb_maxSepF->Size = System::Drawing::Size(53, 20);
			this->tb_maxSepF->TabIndex = 25;
			this->tb_maxSepF->Text = L"0";
			// 
			// tb_maxCVP
			// 
			this->tb_maxCVP->Location = System::Drawing::Point(143, 94);
			this->tb_maxCVP->Name = L"tb_maxCVP";
			this->tb_maxCVP->Size = System::Drawing::Size(53, 20);
			this->tb_maxCVP->TabIndex = 24;
			this->tb_maxCVP->Text = L"5";
			// 
			// statusStrip1
			// 
			this->statusStrip1->ImageScalingSize = System::Drawing::Size(20, 20);
			this->statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->toolStripStatusLabel2 });
			this->statusStrip1->Location = System::Drawing::Point(0, 520);
			this->statusStrip1->Name = L"statusStrip1";
			this->statusStrip1->Size = System::Drawing::Size(1005, 22);
			this->statusStrip1->TabIndex = 22;
			this->statusStrip1->Text = L"statusStrip1";
			// 
			// toolStripStatusLabel2
			// 
			this->toolStripStatusLabel2->Name = L"toolStripStatusLabel2";
			this->toolStripStatusLabel2->Size = System::Drawing::Size(0, 17);
			// 
			// CB_speed_turbo
			// 
			this->CB_speed_turbo->AutoSize = true;
			this->CB_speed_turbo->Location = System::Drawing::Point(6, 34);
			this->CB_speed_turbo->Name = L"CB_speed_turbo";
			this->CB_speed_turbo->Size = System::Drawing::Size(54, 17);
			this->CB_speed_turbo->TabIndex = 50;
			this->CB_speed_turbo->Text = L"Turbo";
			this->CB_speed_turbo->UseVisualStyleBackColor = true;
			this->CB_speed_turbo->CheckedChanged += gcnew System::EventHandler(this, &Form1::CB_speed_turbo_CheckedChanged);
			// 
			// CB_speed_real
			// 
			this->CB_speed_real->AutoSize = true;
			this->CB_speed_real->Checked = true;
			this->CB_speed_real->CheckState = System::Windows::Forms::CheckState::Checked;
			this->CB_speed_real->Location = System::Drawing::Point(6, 16);
			this->CB_speed_real->Name = L"CB_speed_real";
			this->CB_speed_real->Size = System::Drawing::Size(59, 17);
			this->CB_speed_real->TabIndex = 49;
			this->CB_speed_real->Text = L"Normal";
			this->CB_speed_real->UseVisualStyleBackColor = true;
			this->CB_speed_real->CheckedChanged += gcnew System::EventHandler(this, &Form1::CB_speed_real_CheckedChanged);
			// 
			// panel13
			// 
			this->panel13->BackColor = System::Drawing::Color::DimGray;
			this->panel13->Controls->Add(this->textBox_phase);
			this->panel13->Controls->Add(this->groupBox8);
			this->panel13->Controls->Add(this->groupBox4);
			this->panel13->Controls->Add(this->groupBox3);
			this->panel13->Controls->Add(this->panel12);
			this->panel13->Controls->Add(this->groupBox12);
			this->panel13->Location = System::Drawing::Point(107, 24);
			this->panel13->Name = L"panel13";
			this->panel13->Size = System::Drawing::Size(198, 497);
			this->panel13->TabIndex = 52;
			// 
			// textBox_phase
			// 
			this->textBox_phase->BackColor = System::Drawing::Color::Silver;
			this->textBox_phase->Location = System::Drawing::Point(10, 51);
			this->textBox_phase->Multiline = true;
			this->textBox_phase->Name = L"textBox_phase";
			this->textBox_phase->Size = System::Drawing::Size(179, 51);
			this->textBox_phase->TabIndex = 79;
			// 
			// groupBox8
			// 
			this->groupBox8->Controls->Add(this->label149);
			this->groupBox8->Controls->Add(this->label148);
			this->groupBox8->Controls->Add(this->label147);
			this->groupBox8->Controls->Add(this->label146);
			this->groupBox8->Controls->Add(this->label145);
			this->groupBox8->Controls->Add(this->label143);
			this->groupBox8->Controls->Add(this->trackBar_ART);
			this->groupBox8->Controls->Add(this->checkBox5);
			this->groupBox8->Controls->Add(this->label118);
			this->groupBox8->Controls->Add(this->lab_sweep);
			this->groupBox8->Controls->Add(this->cb_MonitorOn);
			this->groupBox8->Controls->Add(this->button9);
			this->groupBox8->Controls->Add(this->button7);
			this->groupBox8->Location = System::Drawing::Point(10, 356);
			this->groupBox8->Name = L"groupBox8";
			this->groupBox8->Size = System::Drawing::Size(178, 133);
			this->groupBox8->TabIndex = 47;
			this->groupBox8->TabStop = false;
			this->groupBox8->Text = L"Monitor";
			// 
			// label149
			// 
			this->label149->AutoSize = true;
			this->label149->Location = System::Drawing::Point(154, 115);
			this->label149->Name = L"label149";
			this->label149->Size = System::Drawing::Size(18, 13);
			this->label149->TabIndex = 11;
			this->label149->Text = L"4x";
			// 
			// label148
			// 
			this->label148->AutoSize = true;
			this->label148->Location = System::Drawing::Point(133, 115);
			this->label148->Name = L"label148";
			this->label148->Size = System::Drawing::Size(18, 13);
			this->label148->TabIndex = 10;
			this->label148->Text = L"3x";
			// 
			// label147
			// 
			this->label147->AutoSize = true;
			this->label147->Location = System::Drawing::Point(107, 115);
			this->label147->Name = L"label147";
			this->label147->Size = System::Drawing::Size(18, 13);
			this->label147->TabIndex = 9;
			this->label147->Text = L"2x";
			// 
			// label146
			// 
			this->label146->AutoSize = true;
			this->label146->Location = System::Drawing::Point(59, 115);
			this->label146->Name = L"label146";
			this->label146->Size = System::Drawing::Size(18, 13);
			this->label146->TabIndex = 8;
			this->label146->Text = L"0x";
			// 
			// label145
			// 
			this->label145->AutoSize = true;
			this->label145->Location = System::Drawing::Point(84, 115);
			this->label145->Name = L"label145";
			this->label145->Size = System::Drawing::Size(18, 13);
			this->label145->TabIndex = 7;
			this->label145->Text = L"1x";
			// 
			// label143
			// 
			this->label143->AutoSize = true;
			this->label143->Location = System::Drawing::Point(8, 87);
			this->label143->Name = L"label143";
			this->label143->Size = System::Drawing::Size(57, 13);
			this->label143->TabIndex = 6;
			this->label143->Text = L"ART scale";
			// 
			// trackBar_ART
			// 
			this->trackBar_ART->LargeChange = 20;
			this->trackBar_ART->Location = System::Drawing::Point(57, 83);
			this->trackBar_ART->Maximum = 400;
			this->trackBar_ART->Name = L"trackBar_ART";
			this->trackBar_ART->Size = System::Drawing::Size(117, 45);
			this->trackBar_ART->SmallChange = 10;
			this->trackBar_ART->TabIndex = 5;
			this->trackBar_ART->TickFrequency = 20;
			this->trackBar_ART->Value = 100;
			this->trackBar_ART->Scroll += gcnew System::EventHandler(this, &Form1::trackBar_ART_Scroll);
			// 
			// checkBox5
			// 
			this->checkBox5->Checked = true;
			this->checkBox5->CheckState = System::Windows::Forms::CheckState::Checked;
			this->checkBox5->Location = System::Drawing::Point(11, 60);
			this->checkBox5->Name = L"checkBox5";
			this->checkBox5->Size = System::Drawing::Size(141, 17);
			this->checkBox5->TabIndex = 4;
			this->checkBox5->Text = L"Show Interactive Panels";
			this->checkBox5->UseVisualStyleBackColor = true;
			this->checkBox5->CheckedChanged += gcnew System::EventHandler(this, &Form1::checkBox5_CheckedChanged);
			// 
			// label118
			// 
			this->label118->AutoSize = true;
			this->label118->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 11, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Pixel,
				static_cast<System::Byte>(0)));
			this->label118->Location = System::Drawing::Point(51, 12);
			this->label118->Name = L"label118";
			this->label118->Size = System::Drawing::Size(74, 13);
			this->label118->TabIndex = 3;
			this->label118->Text = L"Sweep Speed";
			// 
			// lab_sweep
			// 
			this->lab_sweep->AutoSize = true;
			this->lab_sweep->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->lab_sweep->Location = System::Drawing::Point(74, 26);
			this->lab_sweep->Name = L"lab_sweep";
			this->lab_sweep->Size = System::Drawing::Size(24, 17);
			this->lab_sweep->TabIndex = 2;
			this->lab_sweep->Text = L"12";
			// 
			// cb_MonitorOn
			// 
			this->cb_MonitorOn->Location = System::Drawing::Point(11, 41);
			this->cb_MonitorOn->Name = L"cb_MonitorOn";
			this->cb_MonitorOn->Size = System::Drawing::Size(91, 17);
			this->cb_MonitorOn->TabIndex = 0;
			this->cb_MonitorOn->Text = L"Show Monitor";
			this->cb_MonitorOn->UseVisualStyleBackColor = true;
			this->cb_MonitorOn->CheckedChanged += gcnew System::EventHandler(this, &Form1::cb_MonitorOn_CheckedChanged);
			// 
			// button9
			// 
			this->button9->BackColor = System::Drawing::SystemColors::ActiveCaption;
			this->button9->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->button9->Location = System::Drawing::Point(131, 12);
			this->button9->Name = L"button9";
			this->button9->Size = System::Drawing::Size(35, 27);
			this->button9->TabIndex = 1;
			this->button9->Text = L">>";
			this->button9->UseVisualStyleBackColor = false;
			this->button9->Click += gcnew System::EventHandler(this, &Form1::button9_Click);
			// 
			// button7
			// 
			this->button7->BackColor = System::Drawing::SystemColors::ActiveCaption;
			this->button7->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->button7->Location = System::Drawing::Point(10, 12);
			this->button7->Name = L"button7";
			this->button7->Size = System::Drawing::Size(35, 27);
			this->button7->TabIndex = 0;
			this->button7->Text = L">";
			this->button7->UseVisualStyleBackColor = false;
			this->button7->Click += gcnew System::EventHandler(this, &Form1::button7_Click);
			// 
			// groupBox4
			// 
			this->groupBox4->Controls->Add(this->radio_PC);
			this->groupBox4->Controls->Add(this->radio_VC);
			this->groupBox4->Controls->Add(this->label69);
			this->groupBox4->Controls->Add(this->UD_TV);
			this->groupBox4->Controls->Add(this->UD_PCon);
			this->groupBox4->Controls->Add(this->UD_Resp);
			this->groupBox4->Controls->Add(this->label63);
			this->groupBox4->Location = System::Drawing::Point(11, 108);
			this->groupBox4->Name = L"groupBox4";
			this->groupBox4->Size = System::Drawing::Size(178, 104);
			this->groupBox4->TabIndex = 46;
			this->groupBox4->TabStop = false;
			this->groupBox4->Text = L"Ventilator";
			// 
			// radio_PC
			// 
			this->radio_PC->Location = System::Drawing::Point(10, 69);
			this->radio_PC->Name = L"radio_PC";
			this->radio_PC->Size = System::Drawing::Size(102, 17);
			this->radio_PC->TabIndex = 72;
			this->radio_PC->Text = L"Pressure Control";
			this->radio_PC->UseVisualStyleBackColor = true;
			this->radio_PC->CheckedChanged += gcnew System::EventHandler(this, &Form1::radio_PC_CheckedChanged);
			// 
			// radio_VC
			// 
			this->radio_VC->Checked = true;
			this->radio_VC->Location = System::Drawing::Point(10, 44);
			this->radio_VC->Name = L"radio_VC";
			this->radio_VC->Size = System::Drawing::Size(96, 17);
			this->radio_VC->TabIndex = 71;
			this->radio_VC->TabStop = true;
			this->radio_VC->Text = L"Volume Control";
			this->radio_VC->UseVisualStyleBackColor = true;
			this->radio_VC->CheckedChanged += gcnew System::EventHandler(this, &Form1::radio_VC_CheckedChanged);
			// 
			// label69
			// 
			this->label69->AutoSize = true;
			this->label69->BackColor = System::Drawing::Color::Transparent;
			this->label69->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label69->ForeColor = System::Drawing::Color::White;
			this->label69->Location = System::Drawing::Point(6, 23);
			this->label69->Name = L"label69";
			this->label69->Size = System::Drawing::Size(0, 20);
			this->label69->TabIndex = 44;
			// 
			// UD_TV
			// 
			this->UD_TV->Items->Add(L"1200");
			this->UD_TV->Items->Add(L"1150");
			this->UD_TV->Items->Add(L"1100");
			this->UD_TV->Items->Add(L"1050");
			this->UD_TV->Items->Add(L"1000");
			this->UD_TV->Items->Add(L"950");
			this->UD_TV->Items->Add(L"900");
			this->UD_TV->Items->Add(L"850");
			this->UD_TV->Items->Add(L"800");
			this->UD_TV->Items->Add(L"750");
			this->UD_TV->Items->Add(L"700");
			this->UD_TV->Items->Add(L"650");
			this->UD_TV->Items->Add(L"600");
			this->UD_TV->Items->Add(L"550");
			this->UD_TV->Items->Add(L"500");
			this->UD_TV->Items->Add(L"450");
			this->UD_TV->Items->Add(L"400");
			this->UD_TV->Location = System::Drawing::Point(118, 44);
			this->UD_TV->Name = L"UD_TV";
			this->UD_TV->ReadOnly = true;
			this->UD_TV->Size = System::Drawing::Size(48, 20);
			this->UD_TV->TabIndex = 68;
			this->UD_TV->Text = L"550";
			// 
			// UD_PCon
			// 
			this->UD_PCon->Items->Add(L"50");
			this->UD_PCon->Items->Add(L"48");
			this->UD_PCon->Items->Add(L"46");
			this->UD_PCon->Items->Add(L"44");
			this->UD_PCon->Items->Add(L"42");
			this->UD_PCon->Items->Add(L"40");
			this->UD_PCon->Items->Add(L"38");
			this->UD_PCon->Items->Add(L"36");
			this->UD_PCon->Items->Add(L"34");
			this->UD_PCon->Items->Add(L"32");
			this->UD_PCon->Items->Add(L"30");
			this->UD_PCon->Items->Add(L"28");
			this->UD_PCon->Items->Add(L"26");
			this->UD_PCon->Items->Add(L"24");
			this->UD_PCon->Items->Add(L"22");
			this->UD_PCon->Items->Add(L"20");
			this->UD_PCon->Items->Add(L"18");
			this->UD_PCon->Items->Add(L"16");
			this->UD_PCon->Items->Add(L"14");
			this->UD_PCon->Items->Add(L"12");
			this->UD_PCon->Items->Add(L"10");
			this->UD_PCon->Items->Add(L"8");
			this->UD_PCon->Items->Add(L"6");
			this->UD_PCon->Items->Add(L"5");
			this->UD_PCon->Location = System::Drawing::Point(118, 69);
			this->UD_PCon->Name = L"UD_PCon";
			this->UD_PCon->ReadOnly = true;
			this->UD_PCon->Size = System::Drawing::Size(48, 20);
			this->UD_PCon->TabIndex = 70;
			this->UD_PCon->Text = L"15";
			// 
			// UD_Resp
			// 
			this->UD_Resp->Items->Add(L"40");
			this->UD_Resp->Items->Add(L"39");
			this->UD_Resp->Items->Add(L"38");
			this->UD_Resp->Items->Add(L"37");
			this->UD_Resp->Items->Add(L"36");
			this->UD_Resp->Items->Add(L"35");
			this->UD_Resp->Items->Add(L"34");
			this->UD_Resp->Items->Add(L"33");
			this->UD_Resp->Items->Add(L"32");
			this->UD_Resp->Items->Add(L"31");
			this->UD_Resp->Items->Add(L"30");
			this->UD_Resp->Items->Add(L"29");
			this->UD_Resp->Items->Add(L"28");
			this->UD_Resp->Items->Add(L"27");
			this->UD_Resp->Items->Add(L"26");
			this->UD_Resp->Items->Add(L"25");
			this->UD_Resp->Items->Add(L"24");
			this->UD_Resp->Items->Add(L"23");
			this->UD_Resp->Items->Add(L"22");
			this->UD_Resp->Items->Add(L"21");
			this->UD_Resp->Items->Add(L"20");
			this->UD_Resp->Items->Add(L"19");
			this->UD_Resp->Items->Add(L"18");
			this->UD_Resp->Items->Add(L"17");
			this->UD_Resp->Items->Add(L"16");
			this->UD_Resp->Items->Add(L"15");
			this->UD_Resp->Items->Add(L"14");
			this->UD_Resp->Items->Add(L"13");
			this->UD_Resp->Items->Add(L"12");
			this->UD_Resp->Items->Add(L"11");
			this->UD_Resp->Items->Add(L"10");
			this->UD_Resp->Items->Add(L"9");
			this->UD_Resp->Items->Add(L"8");
			this->UD_Resp->Items->Add(L"7");
			this->UD_Resp->Items->Add(L"6");
			this->UD_Resp->Items->Add(L"5");
			this->UD_Resp->Items->Add(L"4");
			this->UD_Resp->Location = System::Drawing::Point(118, 19);
			this->UD_Resp->Name = L"UD_Resp";
			this->UD_Resp->ReadOnly = true;
			this->UD_Resp->Size = System::Drawing::Size(48, 20);
			this->UD_Resp->TabIndex = 66;
			this->UD_Resp->Text = L"12";
			this->UD_Resp->SelectedItemChanged += gcnew System::EventHandler(this, &Form1::domainUpDown1_SelectedItemChanged);
			// 
			// label63
			// 
			this->label63->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 11, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Pixel,
				static_cast<System::Byte>(0)));
			this->label63->Location = System::Drawing::Point(28, 21);
			this->label63->Name = L"label63";
			this->label63->Size = System::Drawing::Size(58, 13);
			this->label63->TabIndex = 59;
			this->label63->Text = L"Resp Rate";
			// 
			// groupBox3
			// 
			this->groupBox3->Controls->Add(this->label_medsgiven);
			this->groupBox3->Controls->Add(this->GIVE_NEO);
			this->groupBox3->Controls->Add(this->button10);
			this->groupBox3->Controls->Add(this->button8);
			this->groupBox3->Location = System::Drawing::Point(88, 217);
			this->groupBox3->Name = L"groupBox3";
			this->groupBox3->Size = System::Drawing::Size(101, 135);
			this->groupBox3->TabIndex = 45;
			this->groupBox3->TabStop = false;
			this->groupBox3->Text = L"Meds";
			// 
			// label_medsgiven
			// 
			this->label_medsgiven->AutoSize = true;
			this->label_medsgiven->BackColor = System::Drawing::Color::Transparent;
			this->label_medsgiven->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label_medsgiven->ForeColor = System::Drawing::Color::White;
			this->label_medsgiven->Location = System::Drawing::Point(6, 23);
			this->label_medsgiven->Name = L"label_medsgiven";
			this->label_medsgiven->Size = System::Drawing::Size(0, 20);
			this->label_medsgiven->TabIndex = 44;
			// 
			// GIVE_NEO
			// 
			this->GIVE_NEO->BackColor = System::Drawing::Color::Thistle;
			this->GIVE_NEO->Location = System::Drawing::Point(5, 16);
			this->GIVE_NEO->Name = L"GIVE_NEO";
			this->GIVE_NEO->Size = System::Drawing::Size(91, 24);
			this->GIVE_NEO->TabIndex = 27;
			this->GIVE_NEO->Text = L"Neo 50 mcg";
			this->GIVE_NEO->UseVisualStyleBackColor = false;
			this->GIVE_NEO->Click += gcnew System::EventHandler(this, &Form1::GIVE_NEO_Click);
			// 
			// button10
			// 
			this->button10->BackColor = System::Drawing::Color::LightBlue;
			this->button10->Location = System::Drawing::Point(5, 76);
			this->button10->Name = L"button10";
			this->button10->Size = System::Drawing::Size(91, 24);
			this->button10->TabIndex = 33;
			this->button10->Text = L"Fentanyl 50 mcg";
			this->button10->UseVisualStyleBackColor = false;
			this->button10->Click += gcnew System::EventHandler(this, &Form1::button10_Click);
			// 
			// button8
			// 
			this->button8->BackColor = System::Drawing::Color::Thistle;
			this->button8->Location = System::Drawing::Point(5, 46);
			this->button8->Name = L"button8";
			this->button8->Size = System::Drawing::Size(91, 24);
			this->button8->TabIndex = 29;
			this->button8->Text = L"Ephedrine 5 mg";
			this->button8->UseVisualStyleBackColor = false;
			this->button8->Click += gcnew System::EventHandler(this, &Form1::button8_Click);
			// 
			// panel12
			// 
			this->panel12->BackColor = System::Drawing::SystemColors::ButtonFace;
			this->panel12->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->panel12->Controls->Add(this->pictureBox2);
			this->panel12->Controls->Add(this->label_CLOCK);
			this->panel12->Location = System::Drawing::Point(11, 3);
			this->panel12->Name = L"panel12";
			this->panel12->Size = System::Drawing::Size(177, 40);
			this->panel12->TabIndex = 40;
			// 
			// pictureBox2
			// 
			this->pictureBox2->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox2.Image")));
			this->pictureBox2->Location = System::Drawing::Point(3, 1);
			this->pictureBox2->Name = L"pictureBox2";
			this->pictureBox2->Size = System::Drawing::Size(37, 36);
			this->pictureBox2->TabIndex = 55;
			this->pictureBox2->TabStop = false;
			// 
			// label_CLOCK
			// 
			this->label_CLOCK->AutoSize = true;
			this->label_CLOCK->Font = (gcnew System::Drawing::Font(L"Impact", 28, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Pixel,
				static_cast<System::Byte>(0)));
			this->label_CLOCK->ForeColor = System::Drawing::Color::Navy;
			this->label_CLOCK->Location = System::Drawing::Point(43, 1);
			this->label_CLOCK->Name = L"label_CLOCK";
			this->label_CLOCK->Size = System::Drawing::Size(125, 35);
			this->label_CLOCK->TabIndex = 38;
			this->label_CLOCK->Text = L"00:00:00";
			// 
			// groupBox12
			// 
			this->groupBox12->Controls->Add(this->Fire);
			this->groupBox12->Controls->Add(this->button15);
			this->groupBox12->Controls->Add(this->button3);
			this->groupBox12->Controls->Add(this->button16);
			this->groupBox12->Location = System::Drawing::Point(11, 217);
			this->groupBox12->Name = L"groupBox12";
			this->groupBox12->Size = System::Drawing::Size(75, 135);
			this->groupBox12->TabIndex = 78;
			this->groupBox12->TabStop = false;
			this->groupBox12->Text = L"Blood Changes";
			// 
			// Fire
			// 
			this->Fire->ForeColor = System::Drawing::Color::DarkGreen;
			this->Fire->Location = System::Drawing::Point(6, 15);
			this->Fire->Name = L"Fire";
			this->Fire->Size = System::Drawing::Size(61, 25);
			this->Fire->TabIndex = 55;
			this->Fire->Text = L"+300ml";
			this->Fire->UseVisualStyleBackColor = true;
			this->Fire->Click += gcnew System::EventHandler(this, &Form1::Fire_Click);
			// 
			// button15
			// 
			this->button15->ForeColor = System::Drawing::Color::DarkRed;
			this->button15->Location = System::Drawing::Point(6, 79);
			this->button15->Name = L"button15";
			this->button15->Size = System::Drawing::Size(61, 25);
			this->button15->TabIndex = 77;
			this->button15->Text = L"-50ml";
			this->button15->UseVisualStyleBackColor = true;
			this->button15->Click += gcnew System::EventHandler(this, &Form1::button15_Click);
			// 
			// button3
			// 
			this->button3->ForeColor = System::Drawing::Color::DarkRed;
			this->button3->Location = System::Drawing::Point(6, 104);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(61, 25);
			this->button3->TabIndex = 56;
			this->button3->Text = L"-300ml";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &Form1::button3_Click_2);
			// 
			// button16
			// 
			this->button16->ForeColor = System::Drawing::Color::DarkGreen;
			this->button16->Location = System::Drawing::Point(6, 40);
			this->button16->Name = L"button16";
			this->button16->Size = System::Drawing::Size(61, 25);
			this->button16->TabIndex = 76;
			this->button16->Text = L"+50ml";
			this->button16->UseVisualStyleBackColor = true;
			this->button16->Click += gcnew System::EventHandler(this, &Form1::button16_Click);
			// 
			// TIMER_SIM
			// 
			this->TIMER_SIM->Interval = 1000;
			this->TIMER_SIM->Tick += gcnew System::EventHandler(this, &Form1::TIMER_SIM_Tick);
			// 
			// label26
			// 
			this->label26->AutoSize = true;
			this->label26->Font = (gcnew System::Drawing::Font(L"Impact", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label26->Location = System::Drawing::Point(9, 97);
			this->label26->Name = L"label26";
			this->label26->Size = System::Drawing::Size(64, 18);
			this->label26->TabIndex = 53;
			this->label26->Text = L"Sim Vitals";
			// 
			// label27
			// 
			this->label27->AutoSize = true;
			this->label27->Font = (gcnew System::Drawing::Font(L"Impact", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label27->Location = System::Drawing::Point(9, 355);
			this->label27->Name = L"label27";
			this->label27->Size = System::Drawing::Size(99, 18);
			this->label27->TabIndex = 52;
			this->label27->Text = L"Simulator Script";
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(112, 355);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(129, 20);
			this->textBox1->TabIndex = 51;
			// 
			// checkBox1
			// 
			this->checkBox1->AutoSize = true;
			this->checkBox1->Location = System::Drawing::Point(112, 51);
			this->checkBox1->Name = L"checkBox1";
			this->checkBox1->Size = System::Drawing::Size(54, 17);
			this->checkBox1->TabIndex = 50;
			this->checkBox1->Text = L"Turbo";
			this->checkBox1->UseVisualStyleBackColor = true;
			// 
			// checkBox2
			// 
			this->checkBox2->AutoSize = true;
			this->checkBox2->Checked = true;
			this->checkBox2->CheckState = System::Windows::Forms::CheckState::Checked;
			this->checkBox2->Location = System::Drawing::Point(112, 33);
			this->checkBox2->Name = L"checkBox2";
			this->checkBox2->Size = System::Drawing::Size(59, 17);
			this->checkBox2->TabIndex = 49;
			this->checkBox2->Text = L"Normal";
			this->checkBox2->UseVisualStyleBackColor = true;
			// 
			// label28
			// 
			this->label28->AutoSize = true;
			this->label28->Font = (gcnew System::Drawing::Font(L"Impact", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label28->Location = System::Drawing::Point(109, 15);
			this->label28->Name = L"label28";
			this->label28->Size = System::Drawing::Size(43, 17);
			this->label28->TabIndex = 48;
			this->label28->Text = L"Speed";
			// 
			// label29
			// 
			this->label29->AutoSize = true;
			this->label29->Font = (gcnew System::Drawing::Font(L"Impact", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label29->Location = System::Drawing::Point(142, 97);
			this->label29->Name = L"label29";
			this->label29->Size = System::Drawing::Size(101, 18);
			this->label29->TabIndex = 47;
			this->label29->Text = L"Patient Baseline";
			// 
			// panel5
			// 
			this->panel5->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->panel5->Controls->Add(this->label30);
			this->panel5->Controls->Add(this->label31);
			this->panel5->Controls->Add(this->label32);
			this->panel5->Controls->Add(this->label33);
			this->panel5->Controls->Add(this->label34);
			this->panel5->Controls->Add(this->label35);
			this->panel5->Controls->Add(this->label36);
			this->panel5->Controls->Add(this->label37);
			this->panel5->Controls->Add(this->label38);
			this->panel5->Controls->Add(this->label39);
			this->panel5->Controls->Add(this->label45);
			this->panel5->Controls->Add(this->label46);
			this->panel5->Controls->Add(this->label47);
			this->panel5->Controls->Add(this->label49);
			this->panel5->Location = System::Drawing::Point(6, 118);
			this->panel5->Name = L"panel5";
			this->panel5->Size = System::Drawing::Size(124, 117);
			this->panel5->TabIndex = 3;
			// 
			// label30
			// 
			this->label30->AutoSize = true;
			this->label30->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label30->ForeColor = System::Drawing::Color::Firebrick;
			this->label30->Location = System::Drawing::Point(49, 22);
			this->label30->Name = L"label30";
			this->label30->Size = System::Drawing::Size(15, 13);
			this->label30->TabIndex = 15;
			this->label30->Text = L"--";
			// 
			// label31
			// 
			this->label31->AutoSize = true;
			this->label31->Location = System::Drawing::Point(25, 23);
			this->label31->Name = L"label31";
			this->label31->Size = System::Drawing::Size(24, 13);
			this->label31->TabIndex = 14;
			this->label31->Text = L"BP:";
			this->label31->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// label32
			// 
			this->label32->AutoSize = true;
			this->label32->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label32->ForeColor = System::Drawing::Color::Black;
			this->label32->Location = System::Drawing::Point(49, 92);
			this->label32->Name = L"label32";
			this->label32->Size = System::Drawing::Size(15, 13);
			this->label32->TabIndex = 13;
			this->label32->Text = L"--";
			// 
			// label33
			// 
			this->label33->AutoSize = true;
			this->label33->Location = System::Drawing::Point(15, 92);
			this->label33->Name = L"label33";
			this->label33->Size = System::Drawing::Size(32, 13);
			this->label33->TabIndex = 12;
			this->label33->Text = L"SVR:";
			this->label33->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// label34
			// 
			this->label34->AutoSize = true;
			this->label34->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label34->ForeColor = System::Drawing::Color::Black;
			this->label34->Location = System::Drawing::Point(49, 79);
			this->label34->Name = L"label34";
			this->label34->Size = System::Drawing::Size(15, 13);
			this->label34->TabIndex = 11;
			this->label34->Text = L"--";
			// 
			// label35
			// 
			this->label35->AutoSize = true;
			this->label35->Location = System::Drawing::Point(27, 79);
			this->label35->Name = L"label35";
			this->label35->Size = System::Drawing::Size(20, 13);
			this->label35->TabIndex = 10;
			this->label35->Text = L"CI:";
			this->label35->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// label36
			// 
			this->label36->AutoSize = true;
			this->label36->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label36->ForeColor = System::Drawing::Color::Black;
			this->label36->Location = System::Drawing::Point(49, 66);
			this->label36->Name = L"label36";
			this->label36->Size = System::Drawing::Size(15, 13);
			this->label36->TabIndex = 9;
			this->label36->Text = L"--";
			// 
			// label37
			// 
			this->label37->AutoSize = true;
			this->label37->Location = System::Drawing::Point(22, 66);
			this->label37->Name = L"label37";
			this->label37->Size = System::Drawing::Size(25, 13);
			this->label37->TabIndex = 8;
			this->label37->Text = L"CO:";
			this->label37->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// label38
			// 
			this->label38->AutoSize = true;
			this->label38->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label38->ForeColor = System::Drawing::Color::RoyalBlue;
			this->label38->Location = System::Drawing::Point(49, 53);
			this->label38->Name = L"label38";
			this->label38->Size = System::Drawing::Size(15, 13);
			this->label38->TabIndex = 7;
			this->label38->Text = L"--";
			// 
			// label39
			// 
			this->label39->AutoSize = true;
			this->label39->Location = System::Drawing::Point(16, 53);
			this->label39->Name = L"label39";
			this->label39->Size = System::Drawing::Size(31, 13);
			this->label39->TabIndex = 6;
			this->label39->Text = L"PPV:";
			this->label39->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// label45
			// 
			this->label45->AutoSize = true;
			this->label45->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label45->ForeColor = System::Drawing::Color::Firebrick;
			this->label45->Location = System::Drawing::Point(49, 37);
			this->label45->Name = L"label45";
			this->label45->Size = System::Drawing::Size(15, 13);
			this->label45->TabIndex = 3;
			this->label45->Text = L"--";
			// 
			// label46
			// 
			this->label46->AutoSize = true;
			this->label46->Location = System::Drawing::Point(16, 38);
			this->label46->Name = L"label46";
			this->label46->Size = System::Drawing::Size(33, 13);
			this->label46->TabIndex = 2;
			this->label46->Text = L"MAP:";
			this->label46->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// label47
			// 
			this->label47->AutoSize = true;
			this->label47->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label47->ForeColor = System::Drawing::Color::Green;
			this->label47->Location = System::Drawing::Point(49, 8);
			this->label47->Name = L"label47";
			this->label47->Size = System::Drawing::Size(15, 13);
			this->label47->TabIndex = 1;
			this->label47->Text = L"--";
			// 
			// label49
			// 
			this->label49->AutoSize = true;
			this->label49->Location = System::Drawing::Point(23, 8);
			this->label49->Name = L"label49";
			this->label49->Size = System::Drawing::Size(26, 13);
			this->label49->TabIndex = 0;
			this->label49->Text = L"HR:";
			this->label49->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->label51);
			this->groupBox2->Controls->Add(this->button2);
			this->groupBox2->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->groupBox2->Location = System::Drawing::Point(6, 4);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(97, 76);
			this->groupBox2->TabIndex = 2;
			this->groupBox2->TabStop = false;
			// 
			// label51
			// 
			this->label51->AutoSize = true;
			this->label51->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label51->Location = System::Drawing::Point(10, 16);
			this->label51->Name = L"label51";
			this->label51->Size = System::Drawing::Size(76, 17);
			this->label51->TabIndex = 1;
			this->label51->Text = L"Simulator";
			// 
			// button2
			// 
			this->button2->BackColor = System::Drawing::Color::LightCoral;
			this->button2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->button2->Location = System::Drawing::Point(20, 36);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(56, 27);
			this->button2->TabIndex = 0;
			this->button2->Text = L"OFF";
			this->button2->UseVisualStyleBackColor = false;
			// 
			// panel6
			// 
			this->panel6->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->panel6->Controls->Add(this->label53);
			this->panel6->Controls->Add(this->label54);
			this->panel6->Controls->Add(this->label55);
			this->panel6->Controls->Add(this->label67);
			this->panel6->Controls->Add(this->label68);
			this->panel6->Controls->Add(this->label70);
			this->panel6->Controls->Add(this->label71);
			this->panel6->Controls->Add(this->label72);
			this->panel6->Controls->Add(this->label73);
			this->panel6->Controls->Add(this->label74);
			this->panel6->Location = System::Drawing::Point(6, 241);
			this->panel6->Name = L"panel6";
			this->panel6->Size = System::Drawing::Size(124, 90);
			this->panel6->TabIndex = 4;
			// 
			// label53
			// 
			this->label53->AutoSize = true;
			this->label53->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label53->ForeColor = System::Drawing::SystemColors::WindowText;
			this->label53->Location = System::Drawing::Point(52, 58);
			this->label53->Name = L"label53";
			this->label53->Size = System::Drawing::Size(15, 13);
			this->label53->TabIndex = 21;
			this->label53->Text = L"--";
			// 
			// label54
			// 
			this->label54->AutoSize = true;
			this->label54->Location = System::Drawing::Point(3, 58);
			this->label54->Name = L"label54";
			this->label54->Size = System::Drawing::Size(44, 13);
			this->label54->TabIndex = 20;
			this->label54->Text = L"LVESV:";
			this->label54->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// label55
			// 
			this->label55->AutoSize = true;
			this->label55->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label55->ForeColor = System::Drawing::SystemColors::WindowText;
			this->label55->Location = System::Drawing::Point(52, 45);
			this->label55->Name = L"label55";
			this->label55->Size = System::Drawing::Size(15, 13);
			this->label55->TabIndex = 19;
			this->label55->Text = L"--";
			// 
			// label67
			// 
			this->label67->AutoSize = true;
			this->label67->Location = System::Drawing::Point(2, 45);
			this->label67->Name = L"label67";
			this->label67->Size = System::Drawing::Size(45, 13);
			this->label67->TabIndex = 18;
			this->label67->Text = L"LVEDV:";
			this->label67->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// label68
			// 
			this->label68->AutoSize = true;
			this->label68->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label68->ForeColor = System::Drawing::SystemColors::WindowText;
			this->label68->Location = System::Drawing::Point(52, 32);
			this->label68->Name = L"label68";
			this->label68->Size = System::Drawing::Size(15, 13);
			this->label68->TabIndex = 17;
			this->label68->Text = L"--";
			// 
			// label70
			// 
			this->label70->AutoSize = true;
			this->label70->Location = System::Drawing::Point(23, 32);
			this->label70->Name = L"label70";
			this->label70->Size = System::Drawing::Size(24, 13);
			this->label70->TabIndex = 16;
			this->label70->Text = L"SV:";
			this->label70->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// label71
			// 
			this->label71->AutoSize = true;
			this->label71->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label71->ForeColor = System::Drawing::SystemColors::WindowText;
			this->label71->Location = System::Drawing::Point(52, 19);
			this->label71->Name = L"label71";
			this->label71->Size = System::Drawing::Size(15, 13);
			this->label71->TabIndex = 15;
			this->label71->Text = L"--";
			// 
			// label72
			// 
			this->label72->AutoSize = true;
			this->label72->Location = System::Drawing::Point(11, 19);
			this->label72->Name = L"label72";
			this->label72->Size = System::Drawing::Size(36, 13);
			this->label72->TabIndex = 14;
			this->label72->Text = L"Vol R:";
			this->label72->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// label73
			// 
			this->label73->AutoSize = true;
			this->label73->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label73->ForeColor = System::Drawing::SystemColors::WindowText;
			this->label73->Location = System::Drawing::Point(52, 6);
			this->label73->Name = L"label73";
			this->label73->Size = System::Drawing::Size(15, 13);
			this->label73->TabIndex = 13;
			this->label73->Text = L"--";
			// 
			// label74
			// 
			this->label74->AutoSize = true;
			this->label74->Location = System::Drawing::Point(10, 6);
			this->label74->Name = L"label74";
			this->label74->Size = System::Drawing::Size(37, 13);
			this->label74->TabIndex = 12;
			this->label74->Text = L"Blood:";
			this->label74->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// panel7
			// 
			this->panel7->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->panel7->Controls->Add(this->label75);
			this->panel7->Controls->Add(this->textBox2);
			this->panel7->Controls->Add(this->textBox3);
			this->panel7->Controls->Add(this->label76);
			this->panel7->Controls->Add(this->label77);
			this->panel7->Controls->Add(this->textBox4);
			this->panel7->Controls->Add(this->textBox5);
			this->panel7->Controls->Add(this->label78);
			this->panel7->Controls->Add(this->label79);
			this->panel7->Controls->Add(this->textBox7);
			this->panel7->Controls->Add(this->textBox8);
			this->panel7->Controls->Add(this->label80);
			this->panel7->Controls->Add(this->label81);
			this->panel7->Controls->Add(this->textBox9);
			this->panel7->Controls->Add(this->textBox10);
			this->panel7->Controls->Add(this->label82);
			this->panel7->Location = System::Drawing::Point(136, 118);
			this->panel7->Name = L"panel7";
			this->panel7->Size = System::Drawing::Size(142, 168);
			this->panel7->TabIndex = 21;
			// 
			// label75
			// 
			this->label75->AutoSize = true;
			this->label75->Location = System::Drawing::Point(5, 6);
			this->label75->Name = L"label75";
			this->label75->Size = System::Drawing::Size(50, 13);
			this->label75->TabIndex = 5;
			this->label75->Text = L"Base HR";
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(83, 143);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(53, 20);
			this->textBox2->TabIndex = 20;
			this->textBox2->Text = L"50";
			// 
			// textBox3
			// 
			this->textBox3->Location = System::Drawing::Point(83, 3);
			this->textBox3->Name = L"textBox3";
			this->textBox3->Size = System::Drawing::Size(53, 20);
			this->textBox3->TabIndex = 6;
			this->textBox3->Text = L"70";
			// 
			// label76
			// 
			this->label76->AutoSize = true;
			this->label76->Location = System::Drawing::Point(5, 146);
			this->label76->Name = L"label76";
			this->label76->Size = System::Drawing::Size(66, 13);
			this->label76->TabIndex = 19;
			this->label76->Text = L"Base Inches";
			// 
			// label77
			// 
			this->label77->AutoSize = true;
			this->label77->Location = System::Drawing::Point(5, 26);
			this->label77->Name = L"label77";
			this->label77->Size = System::Drawing::Size(55, 13);
			this->label77->TabIndex = 7;
			this->label77->Text = L"Base SBP";
			// 
			// textBox4
			// 
			this->textBox4->Location = System::Drawing::Point(83, 123);
			this->textBox4->Name = L"textBox4";
			this->textBox4->Size = System::Drawing::Size(53, 20);
			this->textBox4->TabIndex = 18;
			this->textBox4->Text = L"75";
			// 
			// textBox5
			// 
			this->textBox5->Location = System::Drawing::Point(83, 23);
			this->textBox5->Name = L"textBox5";
			this->textBox5->Size = System::Drawing::Size(53, 20);
			this->textBox5->TabIndex = 8;
			this->textBox5->Text = L"120";
			// 
			// label78
			// 
			this->label78->AutoSize = true;
			this->label78->Location = System::Drawing::Point(5, 126);
			this->label78->Name = L"label78";
			this->label78->Size = System::Drawing::Size(47, 13);
			this->label78->TabIndex = 17;
			this->label78->Text = L"Base Kg";
			// 
			// label79
			// 
			this->label79->AutoSize = true;
			this->label79->Location = System::Drawing::Point(5, 46);
			this->label79->Name = L"label79";
			this->label79->Size = System::Drawing::Size(56, 13);
			this->label79->TabIndex = 9;
			this->label79->Text = L"Base DBP";
			// 
			// textBox7
			// 
			this->textBox7->Location = System::Drawing::Point(83, 103);
			this->textBox7->Name = L"textBox7";
			this->textBox7->Size = System::Drawing::Size(53, 20);
			this->textBox7->TabIndex = 16;
			this->textBox7->Text = L"135";
			// 
			// textBox8
			// 
			this->textBox8->Location = System::Drawing::Point(83, 43);
			this->textBox8->Name = L"textBox8";
			this->textBox8->Size = System::Drawing::Size(53, 20);
			this->textBox8->TabIndex = 10;
			this->textBox8->Text = L"70";
			// 
			// label80
			// 
			this->label80->AutoSize = true;
			this->label80->Location = System::Drawing::Point(5, 106);
			this->label80->Name = L"label80";
			this->label80->Size = System::Drawing::Size(69, 13);
			this->label80->TabIndex = 15;
			this->label80->Text = L"Base LVEDV";
			// 
			// label81
			// 
			this->label81->AutoSize = true;
			this->label81->Location = System::Drawing::Point(5, 66);
			this->label81->Name = L"label81";
			this->label81->Size = System::Drawing::Size(55, 13);
			this->label81->TabIndex = 11;
			this->label81->Text = L"Base CVP";
			// 
			// textBox9
			// 
			this->textBox9->Location = System::Drawing::Point(83, 83);
			this->textBox9->Name = L"textBox9";
			this->textBox9->Size = System::Drawing::Size(53, 20);
			this->textBox9->TabIndex = 14;
			this->textBox9->Text = L"55";
			// 
			// textBox10
			// 
			this->textBox10->Enabled = false;
			this->textBox10->Location = System::Drawing::Point(83, 63);
			this->textBox10->Name = L"textBox10";
			this->textBox10->Size = System::Drawing::Size(53, 20);
			this->textBox10->TabIndex = 12;
			this->textBox10->Text = L"5";
			// 
			// label82
			// 
			this->label82->AutoSize = true;
			this->label82->Location = System::Drawing::Point(5, 86);
			this->label82->Name = L"label82";
			this->label82->Size = System::Drawing::Size(68, 13);
			this->label82->TabIndex = 13;
			this->label82->Text = L"Base LVESV";
			// 
			// timer_meds
			// 
			this->timer_meds->Interval = 1000;
			this->timer_meds->Tick += gcnew System::EventHandler(this, &Form1::timer_meds_Tick);
			// 
			// panel1
			// 
			this->panel1->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->panel1->Controls->Add(this->label56);
			this->panel1->Controls->Add(this->label66);
			this->panel1->Controls->Add(this->tb_PPVWandFreqMax);
			this->panel1->Controls->Add(this->tb_PPVWandFreq);
			this->panel1->Controls->Add(this->label1);
			this->panel1->Controls->Add(this->tb_SVDsecMax);
			this->panel1->Controls->Add(this->tb_SVWandMax);
			this->panel1->Controls->Add(this->tb_PPVbiasMax);
			this->panel1->Controls->Add(this->tb_PPVWandMax);
			this->panel1->Controls->Add(this->tb_SVDsec);
			this->panel1->Controls->Add(this->label44);
			this->panel1->Controls->Add(this->tb_SVWand);
			this->panel1->Controls->Add(this->label3);
			this->panel1->Controls->Add(this->tb_VVar);
			this->panel1->Controls->Add(this->label64);
			this->panel1->Controls->Add(this->tb_PPVbias);
			this->panel1->Controls->Add(this->label9);
			this->panel1->Controls->Add(this->tb_stability);
			this->panel1->Controls->Add(this->label6);
			this->panel1->Controls->Add(this->tb_Flux);
			this->panel1->Controls->Add(this->label5);
			this->panel1->Controls->Add(this->tb_PPVWand);
			this->panel1->Controls->Add(this->label2);
			this->panel1->Controls->Add(this->label4);
			this->panel1->Location = System::Drawing::Point(186, 14);
			this->panel1->Name = L"panel1";
			this->panel1->Size = System::Drawing::Size(321, 276);
			this->panel1->TabIndex = 58;
			// 
			// label56
			// 
			this->label56->AutoSize = true;
			this->label56->Location = System::Drawing::Point(111, 33);
			this->label56->Name = L"label56";
			this->label56->Size = System::Drawing::Size(24, 13);
			this->label56->TabIndex = 69;
			this->label56->Text = L"Min";
			// 
			// label66
			// 
			this->label66->AutoSize = true;
			this->label66->Location = System::Drawing::Point(150, 33);
			this->label66->Name = L"label66";
			this->label66->Size = System::Drawing::Size(27, 13);
			this->label66->TabIndex = 68;
			this->label66->Text = L"Max";
			// 
			// tb_PPVWandFreqMax
			// 
			this->tb_PPVWandFreqMax->Location = System::Drawing::Point(148, 195);
			this->tb_PPVWandFreqMax->Name = L"tb_PPVWandFreqMax";
			this->tb_PPVWandFreqMax->Size = System::Drawing::Size(35, 20);
			this->tb_PPVWandFreqMax->TabIndex = 67;
			this->tb_PPVWandFreqMax->Text = L"30";
			// 
			// tb_PPVWandFreq
			// 
			this->tb_PPVWandFreq->Location = System::Drawing::Point(107, 195);
			this->tb_PPVWandFreq->Name = L"tb_PPVWandFreq";
			this->tb_PPVWandFreq->Size = System::Drawing::Size(35, 20);
			this->tb_PPVWandFreq->TabIndex = 66;
			this->tb_PPVWandFreq->Text = L"30";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(6, 198);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(100, 13);
			this->label1->TabIndex = 65;
			this->label1->Text = L"Frequency (1/odds)";
			// 
			// tb_SVDsecMax
			// 
			this->tb_SVDsecMax->Location = System::Drawing::Point(148, 145);
			this->tb_SVDsecMax->Name = L"tb_SVDsecMax";
			this->tb_SVDsecMax->Size = System::Drawing::Size(35, 20);
			this->tb_SVDsecMax->TabIndex = 64;
			this->tb_SVDsecMax->Text = L"30";
			// 
			// tb_SVWandMax
			// 
			this->tb_SVWandMax->Location = System::Drawing::Point(148, 125);
			this->tb_SVWandMax->Name = L"tb_SVWandMax";
			this->tb_SVWandMax->Size = System::Drawing::Size(35, 20);
			this->tb_SVWandMax->TabIndex = 63;
			this->tb_SVWandMax->Text = L"2";
			// 
			// tb_PPVbiasMax
			// 
			this->tb_PPVbiasMax->Location = System::Drawing::Point(148, 215);
			this->tb_PPVbiasMax->Name = L"tb_PPVbiasMax";
			this->tb_PPVbiasMax->Size = System::Drawing::Size(35, 20);
			this->tb_PPVbiasMax->TabIndex = 61;
			this->tb_PPVbiasMax->Text = L"0";
			// 
			// tb_PPVWandMax
			// 
			this->tb_PPVWandMax->Location = System::Drawing::Point(148, 175);
			this->tb_PPVWandMax->Name = L"tb_PPVWandMax";
			this->tb_PPVWandMax->Size = System::Drawing::Size(35, 20);
			this->tb_PPVWandMax->TabIndex = 58;
			this->tb_PPVWandMax->Text = L"2";
			// 
			// tb_SVDsec
			// 
			this->tb_SVDsec->Location = System::Drawing::Point(107, 145);
			this->tb_SVDsec->Name = L"tb_SVDsec";
			this->tb_SVDsec->Size = System::Drawing::Size(35, 20);
			this->tb_SVDsec->TabIndex = 43;
			this->tb_SVDsec->Text = L"30";
			// 
			// label44
			// 
			this->label44->AutoSize = true;
			this->label44->Location = System::Drawing::Point(6, 148);
			this->label44->Name = L"label44";
			this->label44->Size = System::Drawing::Size(100, 13);
			this->label44->TabIndex = 42;
			this->label44->Text = L"Frequency (1/odds)";
			// 
			// tb_SVWand
			// 
			this->tb_SVWand->Location = System::Drawing::Point(107, 125);
			this->tb_SVWand->Name = L"tb_SVWand";
			this->tb_SVWand->Size = System::Drawing::Size(35, 20);
			this->tb_SVWand->TabIndex = 41;
			this->tb_SVWand->Text = L"2";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(6, 128);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(80, 13);
			this->label3->TabIndex = 40;
			this->label3->Text = L"SV Error % (+/-)";
			// 
			// tb_VVar
			// 
			this->tb_VVar->Location = System::Drawing::Point(107, 96);
			this->tb_VVar->MaxLength = 2;
			this->tb_VVar->Name = L"tb_VVar";
			this->tb_VVar->Size = System::Drawing::Size(35, 20);
			this->tb_VVar->TabIndex = 39;
			this->tb_VVar->Text = L"15";
			this->tb_VVar->TextChanged += gcnew System::EventHandler(this, &Form1::tb_VVar_TextChanged);
			// 
			// label64
			// 
			this->label64->AutoSize = true;
			this->label64->Location = System::Drawing::Point(6, 99);
			this->label64->Name = L"label64";
			this->label64->Size = System::Drawing::Size(69, 13);
			this->label64->TabIndex = 38;
			this->label64->Text = L"EKG-V Boost";
			// 
			// tb_PPVbias
			// 
			this->tb_PPVbias->Location = System::Drawing::Point(107, 215);
			this->tb_PPVbias->Name = L"tb_PPVbias";
			this->tb_PPVbias->Size = System::Drawing::Size(35, 20);
			this->tb_PPVbias->TabIndex = 37;
			this->tb_PPVbias->Text = L"0";
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(6, 218);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(91, 13);
			this->label9->TabIndex = 36;
			this->label9->Text = L"PPV Bias (steady)";
			// 
			// tb_stability
			// 
			this->tb_stability->Location = System::Drawing::Point(107, 48);
			this->tb_stability->Name = L"tb_stability";
			this->tb_stability->Size = System::Drawing::Size(35, 20);
			this->tb_stability->TabIndex = 35;
			this->tb_stability->Text = L"60";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(6, 51);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(84, 13);
			this->label6->TabIndex = 34;
			this->label6->Text = L"Pt Stab (30-120)";
			// 
			// tb_Flux
			// 
			this->tb_Flux->Location = System::Drawing::Point(107, 68);
			this->tb_Flux->Name = L"tb_Flux";
			this->tb_Flux->Size = System::Drawing::Size(35, 20);
			this->tb_Flux->TabIndex = 33;
			this->tb_Flux->Text = L"0.5";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(6, 71);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(87, 13);
			this->label5->TabIndex = 32;
			this->label5->Text = L"Pt Flux (0.0 - 2.0)";
			// 
			// tb_PPVWand
			// 
			this->tb_PPVWand->Location = System::Drawing::Point(107, 175);
			this->tb_PPVWand->Name = L"tb_PPVWand";
			this->tb_PPVWand->Size = System::Drawing::Size(35, 20);
			this->tb_PPVWand->TabIndex = 31;
			this->tb_PPVWand->Text = L"2";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"Impact", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label2->ForeColor = System::Drawing::Color::MidnightBlue;
			this->label2->Location = System::Drawing::Point(4, 4);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(109, 18);
			this->label2->TabIndex = 57;
			this->label2->Text = L"Advanced Params";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(6, 178);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(87, 13);
			this->label4->TabIndex = 31;
			this->label4->Text = L"PPV Error % (+/-)";
			// 
			// menuStrip1
			// 
			this->menuStrip1->ImageScalingSize = System::Drawing::Size(20, 20);
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->fileToolStripMenuItem,
					this->aboutToolStripMenuItem
			});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(1005, 24);
			this->menuStrip1->TabIndex = 55;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this->fileToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->exitToolStripMenuItem });
			this->fileToolStripMenuItem->Name = L"fileToolStripMenuItem";
			this->fileToolStripMenuItem->Size = System::Drawing::Size(37, 20);
			this->fileToolStripMenuItem->Text = L"File";
			// 
			// exitToolStripMenuItem
			// 
			this->exitToolStripMenuItem->Name = L"exitToolStripMenuItem";
			this->exitToolStripMenuItem->Size = System::Drawing::Size(92, 22);
			this->exitToolStripMenuItem->Text = L"Exit";
			this->exitToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::exitToolStripMenuItem_Click);
			// 
			// aboutToolStripMenuItem
			// 
			this->aboutToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->aboutCDSimToolStripMenuItem });
			this->aboutToolStripMenuItem->Name = L"aboutToolStripMenuItem";
			this->aboutToolStripMenuItem->Size = System::Drawing::Size(44, 20);
			this->aboutToolStripMenuItem->Text = L"Help";
			// 
			// aboutCDSimToolStripMenuItem
			// 
			this->aboutCDSimToolStripMenuItem->Name = L"aboutCDSimToolStripMenuItem";
			this->aboutCDSimToolStripMenuItem->Size = System::Drawing::Size(151, 22);
			this->aboutCDSimToolStripMenuItem->Text = L"About CD-Sim";
			this->aboutCDSimToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::aboutCDSimToolStripMenuItem_Click);
			// 
			// tabControl1
			// 
			this->tabControl1->Controls->Add(this->tabPage1);
			this->tabControl1->Controls->Add(this->tabPage2);
			this->tabControl1->Controls->Add(this->tabPage3);
			this->tabControl1->Controls->Add(this->tabPage4);
			this->tabControl1->Location = System::Drawing::Point(311, 26);
			this->tabControl1->Name = L"tabControl1";
			this->tabControl1->SelectedIndex = 0;
			this->tabControl1->Size = System::Drawing::Size(686, 491);
			this->tabControl1->TabIndex = 56;
			this->tabControl1->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::Form1_KeyPress);
			// 
			// tabPage1
			// 
			this->tabPage1->BackColor = System::Drawing::SystemColors::ButtonFace;
			this->tabPage1->Controls->Add(this->groupBox17);
			this->tabPage1->Location = System::Drawing::Point(4, 22);
			this->tabPage1->Name = L"tabPage1";
			this->tabPage1->Padding = System::Windows::Forms::Padding(3);
			this->tabPage1->Size = System::Drawing::Size(678, 465);
			this->tabPage1->TabIndex = 0;
			this->tabPage1->Text = L"Simulator Mode";
			this->tabPage1->Click += gcnew System::EventHandler(this, &Form1::tabPage1_Click);
			// 
			// groupBox17
			// 
			this->groupBox17->Controls->Add(this->label_totalDrugs);
			this->groupBox17->Controls->Add(this->label_Drug_Given);
			this->groupBox17->Controls->Add(this->checkBox_repeatScenario);
			this->groupBox17->Controls->Add(this->textBox_scenario);
			this->groupBox17->Controls->Add(this->comboBox_SCRIPTS);
			this->groupBox17->Controls->Add(this->panel3);
			this->groupBox17->Location = System::Drawing::Point(6, 6);
			this->groupBox17->Name = L"groupBox17";
			this->groupBox17->Size = System::Drawing::Size(650, 447);
			this->groupBox17->TabIndex = 81;
			this->groupBox17->TabStop = false;
			this->groupBox17->Text = L"Scenario";
			// 
			// label_totalDrugs
			// 
			this->label_totalDrugs->AutoSize = true;
			this->label_totalDrugs->Location = System::Drawing::Point(417, 197);
			this->label_totalDrugs->Name = L"label_totalDrugs";
			this->label_totalDrugs->Size = System::Drawing::Size(47, 13);
			this->label_totalDrugs->TabIndex = 25;
			this->label_totalDrugs->Text = L"label151";
			// 
			// label_Drug_Given
			// 
			this->label_Drug_Given->AutoSize = true;
			this->label_Drug_Given->Location = System::Drawing::Point(417, 179);
			this->label_Drug_Given->Name = L"label_Drug_Given";
			this->label_Drug_Given->Size = System::Drawing::Size(47, 13);
			this->label_Drug_Given->TabIndex = 24;
			this->label_Drug_Given->Text = L"label150";
			// 
			// checkBox_repeatScenario
			// 
			this->checkBox_repeatScenario->AutoSize = true;
			this->checkBox_repeatScenario->Location = System::Drawing::Point(241, 22);
			this->checkBox_repeatScenario->Name = L"checkBox_repeatScenario";
			this->checkBox_repeatScenario->Size = System::Drawing::Size(171, 17);
			this->checkBox_repeatScenario->TabIndex = 23;
			this->checkBox_repeatScenario->Text = L"Repeat Scenario Automatically";
			this->checkBox_repeatScenario->UseVisualStyleBackColor = true;
			// 
			// textBox_scenario
			// 
			this->textBox_scenario->BackColor = System::Drawing::SystemColors::Control;
			this->textBox_scenario->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->textBox_scenario->ForeColor = System::Drawing::Color::MediumBlue;
			this->textBox_scenario->Location = System::Drawing::Point(14, 43);
			this->textBox_scenario->Multiline = true;
			this->textBox_scenario->Name = L"textBox_scenario";
			this->textBox_scenario->Size = System::Drawing::Size(206, 53);
			this->textBox_scenario->TabIndex = 22;
			// 
			// comboBox_SCRIPTS
			// 
			this->comboBox_SCRIPTS->FormattingEnabled = true;
			this->comboBox_SCRIPTS->Location = System::Drawing::Point(13, 19);
			this->comboBox_SCRIPTS->Name = L"comboBox_SCRIPTS";
			this->comboBox_SCRIPTS->Size = System::Drawing::Size(208, 21);
			this->comboBox_SCRIPTS->TabIndex = 0;
			this->comboBox_SCRIPTS->SelectedIndexChanged += gcnew System::EventHandler(this, &Form1::comboBox_SCRIPTS_SelectedIndexChanged);
			// 
			// tabPage2
			// 
			this->tabPage2->BackColor = System::Drawing::SystemColors::Control;
			this->tabPage2->Controls->Add(this->button13);
			this->tabPage2->Controls->Add(this->groupBox9);
			this->tabPage2->Controls->Add(this->button11);
			this->tabPage2->Controls->Add(this->label136);
			this->tabPage2->Controls->Add(this->dis_Variability);
			this->tabPage2->Controls->Add(this->label134);
			this->tabPage2->Controls->Add(this->label133);
			this->tabPage2->Controls->Add(this->dis_CoreTemp);
			this->tabPage2->Controls->Add(this->label135);
			this->tabPage2->Controls->Add(this->progressBar1);
			this->tabPage2->Controls->Add(this->label132);
			this->tabPage2->Controls->Add(this->groupBox7);
			this->tabPage2->Controls->Add(this->groupBox6);
			this->tabPage2->Controls->Add(this->label131);
			this->tabPage2->Controls->Add(this->label130);
			this->tabPage2->Controls->Add(this->label129);
			this->tabPage2->Controls->Add(this->checkBox3);
			this->tabPage2->Controls->Add(this->label_PLS);
			this->tabPage2->Controls->Add(this->trackBar_Preload);
			this->tabPage2->Controls->Add(this->label128);
			this->tabPage2->Controls->Add(this->label127);
			this->tabPage2->Controls->Add(this->label125);
			this->tabPage2->Controls->Add(this->dis_FTc);
			this->tabPage2->Controls->Add(this->labelFTc);
			this->tabPage2->Controls->Add(this->label122);
			this->tabPage2->Controls->Add(this->dis_SCvO2);
			this->tabPage2->Controls->Add(this->dis_SvO2);
			this->tabPage2->Controls->Add(this->label123);
			this->tabPage2->Controls->Add(this->label120);
			this->tabPage2->Controls->Add(this->dis_PASP);
			this->tabPage2->Controls->Add(this->labelPADP);
			this->tabPage2->Controls->Add(this->dis_PADP);
			this->tabPage2->Controls->Add(this->dis_CI);
			this->tabPage2->Controls->Add(this->label119);
			this->tabPage2->Controls->Add(this->WARN_SVR);
			this->tabPage2->Controls->Add(this->label100);
			this->tabPage2->Controls->Add(this->label88);
			this->tabPage2->Controls->Add(this->dis_HgB);
			this->tabPage2->Controls->Add(this->hgblabel);
			this->tabPage2->Controls->Add(this->label109);
			this->tabPage2->Controls->Add(this->label110);
			this->tabPage2->Controls->Add(this->label108);
			this->tabPage2->Controls->Add(this->label107);
			this->tabPage2->Controls->Add(this->label99);
			this->tabPage2->Controls->Add(this->dis_PACO2);
			this->tabPage2->Controls->Add(this->label101);
			this->tabPage2->Controls->Add(this->dis_SV);
			this->tabPage2->Controls->Add(this->dis_Deadspace);
			this->tabPage2->Controls->Add(this->label102);
			this->tabPage2->Controls->Add(this->label103);
			this->tabPage2->Controls->Add(this->dis_Hgbx);
			this->tabPage2->Controls->Add(this->dis_Outflow);
			this->tabPage2->Controls->Add(this->label104);
			this->tabPage2->Controls->Add(this->label105);
			this->tabPage2->Controls->Add(this->dis_PVI);
			this->tabPage2->Controls->Add(this->dis_SVV);
			this->tabPage2->Controls->Add(this->label106);
			this->tabPage2->Controls->Add(this->dis_SP02);
			this->tabPage2->Controls->Add(this->label86);
			this->tabPage2->Controls->Add(this->label91);
			this->tabPage2->Controls->Add(this->dis_SVR);
			this->tabPage2->Controls->Add(this->dis_HR);
			this->tabPage2->Controls->Add(this->label92);
			this->tabPage2->Controls->Add(this->label93);
			this->tabPage2->Controls->Add(this->dis_PPV);
			this->tabPage2->Controls->Add(this->dis_SBP);
			this->tabPage2->Controls->Add(this->label94);
			this->tabPage2->Controls->Add(this->label95);
			this->tabPage2->Controls->Add(this->dis_CO);
			this->tabPage2->Controls->Add(this->dis_DBP);
			this->tabPage2->Controls->Add(this->label96);
			this->tabPage2->Controls->Add(this->label97);
			this->tabPage2->Controls->Add(this->dis_CVP);
			this->tabPage2->Controls->Add(this->dis_MAP);
			this->tabPage2->Controls->Add(this->label98);
			this->tabPage2->Controls->Add(this->groupBox5);
			this->tabPage2->Location = System::Drawing::Point(4, 22);
			this->tabPage2->Name = L"tabPage2";
			this->tabPage2->Padding = System::Windows::Forms::Padding(3);
			this->tabPage2->Size = System::Drawing::Size(678, 465);
			this->tabPage2->TabIndex = 1;
			this->tabPage2->Text = L"Display Mode";
			// 
			// button13
			// 
			this->button13->Location = System::Drawing::Point(453, 16);
			this->button13->Name = L"button13";
			this->button13->Size = System::Drawing::Size(77, 29);
			this->button13->TabIndex = 144;
			this->button13->Text = L"Normal Sinus";
			this->button13->UseVisualStyleBackColor = true;
			this->button13->Click += gcnew System::EventHandler(this, &Form1::button13_Click);
			// 
			// groupBox9
			// 
			this->groupBox9->Controls->Add(this->label126);
			this->groupBox9->Controls->Add(this->label124);
			this->groupBox9->Controls->Add(this->button12);
			this->groupBox9->Controls->Add(this->comboBox_State);
			this->groupBox9->Controls->Add(this->comboBox_Case);
			this->groupBox9->Location = System::Drawing::Point(126, 8);
			this->groupBox9->Name = L"groupBox9";
			this->groupBox9->Size = System::Drawing::Size(313, 73);
			this->groupBox9->TabIndex = 143;
			this->groupBox9->TabStop = false;
			this->groupBox9->Text = L"Presets";
			// 
			// label126
			// 
			this->label126->AutoSize = true;
			this->label126->Location = System::Drawing::Point(6, 44);
			this->label126->Name = L"label126";
			this->label126->Size = System::Drawing::Size(32, 13);
			this->label126->TabIndex = 144;
			this->label126->Text = L"State";
			// 
			// label124
			// 
			this->label124->AutoSize = true;
			this->label124->Location = System::Drawing::Point(7, 18);
			this->label124->Name = L"label124";
			this->label124->Size = System::Drawing::Size(31, 13);
			this->label124->TabIndex = 143;
			this->label124->Text = L"Case";
			// 
			// button12
			// 
			this->button12->Location = System::Drawing::Point(251, 14);
			this->button12->Name = L"button12";
			this->button12->Size = System::Drawing::Size(53, 49);
			this->button12->TabIndex = 140;
			this->button12->Text = L"Load";
			this->button12->UseVisualStyleBackColor = true;
			this->button12->Click += gcnew System::EventHandler(this, &Form1::button12_Click);
			// 
			// comboBox_State
			// 
			this->comboBox_State->FormattingEnabled = true;
			this->comboBox_State->Location = System::Drawing::Point(44, 41);
			this->comboBox_State->Name = L"comboBox_State";
			this->comboBox_State->Size = System::Drawing::Size(201, 21);
			this->comboBox_State->TabIndex = 142;
			this->comboBox_State->SelectedIndexChanged += gcnew System::EventHandler(this, &Form1::comboBox8_SelectedIndexChanged);
			// 
			// comboBox_Case
			// 
			this->comboBox_Case->FormattingEnabled = true;
			this->comboBox_Case->Location = System::Drawing::Point(44, 15);
			this->comboBox_Case->Name = L"comboBox_Case";
			this->comboBox_Case->Size = System::Drawing::Size(201, 21);
			this->comboBox_Case->TabIndex = 141;
			this->comboBox_Case->SelectedIndexChanged += gcnew System::EventHandler(this, &Form1::comboBox_Case_SelectedIndexChanged);
			// 
			// button11
			// 
			this->button11->Location = System::Drawing::Point(453, 51);
			this->button11->Name = L"button11";
			this->button11->Size = System::Drawing::Size(77, 26);
			this->button11->TabIndex = 139;
			this->button11->Text = L"Bigeminy";
			this->button11->UseVisualStyleBackColor = true;
			this->button11->Click += gcnew System::EventHandler(this, &Form1::button11_Click);
			// 
			// label136
			// 
			this->label136->AutoSize = true;
			this->label136->ForeColor = System::Drawing::Color::Black;
			this->label136->Location = System::Drawing::Point(430, 436);
			this->label136->Name = L"label136";
			this->label136->Size = System::Drawing::Size(62, 13);
			this->label136->TabIndex = 136;
			this->label136->Text = L"Variability %";
			// 
			// dis_Variability
			// 
			this->dis_Variability->Location = System::Drawing::Point(495, 433);
			this->dis_Variability->Name = L"dis_Variability";
			this->dis_Variability->Size = System::Drawing::Size(53, 20);
			this->dis_Variability->TabIndex = 96;
			this->dis_Variability->Text = L"4";
			// 
			// label134
			// 
			this->label134->AutoSize = true;
			this->label134->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label134->Location = System::Drawing::Point(413, 366);
			this->label134->Name = L"label134";
			this->label134->Size = System::Drawing::Size(49, 17);
			this->label134->TabIndex = 135;
			this->label134->Text = L"Other";
			// 
			// label133
			// 
			this->label133->AutoSize = true;
			this->label133->ForeColor = System::Drawing::Color::Black;
			this->label133->Location = System::Drawing::Point(430, 397);
			this->label133->Name = L"label133";
			this->label133->Size = System::Drawing::Size(59, 13);
			this->label133->TabIndex = 133;
			this->label133->Text = L"Core Temp";
			// 
			// dis_CoreTemp
			// 
			this->dis_CoreTemp->Location = System::Drawing::Point(495, 394);
			this->dis_CoreTemp->Name = L"dis_CoreTemp";
			this->dis_CoreTemp->Size = System::Drawing::Size(53, 20);
			this->dis_CoreTemp->TabIndex = 95;
			this->dis_CoreTemp->Text = L"36.6";
			// 
			// label135
			// 
			this->label135->AutoSize = true;
			this->label135->Location = System::Drawing::Point(440, 157);
			this->label135->Name = L"label135";
			this->label135->Size = System::Drawing::Size(12, 13);
			this->label135->TabIndex = 132;
			this->label135->Text = L"/";
			// 
			// progressBar1
			// 
			this->progressBar1->ForeColor = System::Drawing::Color::PaleGreen;
			this->progressBar1->Location = System::Drawing::Point(6, 435);
			this->progressBar1->Name = L"progressBar1";
			this->progressBar1->Size = System::Drawing::Size(97, 23);
			this->progressBar1->Step = 1;
			this->progressBar1->Style = System::Windows::Forms::ProgressBarStyle::Continuous;
			this->progressBar1->TabIndex = 129;
			this->progressBar1->Visible = false;
			// 
			// label132
			// 
			this->label132->AutoSize = true;
			this->label132->ForeColor = System::Drawing::Color::Indigo;
			this->label132->Location = System::Drawing::Point(153, 380);
			this->label132->Name = L"label132";
			this->label132->Size = System::Drawing::Size(173, 13);
			this->label132->TabIndex = 128;
			this->label132->Text = L"* If unlinked, PPV affects wavforms";
			// 
			// groupBox7
			// 
			this->groupBox7->Controls->Add(this->button4);
			this->groupBox7->Location = System::Drawing::Point(6, 284);
			this->groupBox7->Name = L"groupBox7";
			this->groupBox7->Size = System::Drawing::Size(97, 75);
			this->groupBox7->TabIndex = 127;
			this->groupBox7->TabStop = false;
			// 
			// button4
			// 
			this->button4->BackColor = System::Drawing::Color::LightGoldenrodYellow;
			this->button4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->button4->Location = System::Drawing::Point(8, 24);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(76, 39);
			this->button4->TabIndex = 94;
			this->button4->Text = L"Instant Change";
			this->button4->UseVisualStyleBackColor = false;
			this->button4->Click += gcnew System::EventHandler(this, &Form1::button4_Click);
			// 
			// groupBox6
			// 
			this->groupBox6->Controls->Add(this->button5);
			this->groupBox6->Controls->Add(this->dis_xfade);
			this->groupBox6->Controls->Add(this->label121);
			this->groupBox6->Location = System::Drawing::Point(6, 360);
			this->groupBox6->Name = L"groupBox6";
			this->groupBox6->Size = System::Drawing::Size(97, 75);
			this->groupBox6->TabIndex = 126;
			this->groupBox6->TabStop = false;
			// 
			// button5
			// 
			this->button5->BackColor = System::Drawing::Color::PaleGreen;
			this->button5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->button5->Location = System::Drawing::Point(8, 34);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(76, 35);
			this->button5->TabIndex = 109;
			this->button5->Text = L"Crossfade";
			this->button5->UseVisualStyleBackColor = false;
			this->button5->Click += gcnew System::EventHandler(this, &Form1::button5_Click);
			// 
			// dis_xfade
			// 
			this->dis_xfade->Location = System::Drawing::Point(8, 11);
			this->dis_xfade->Name = L"dis_xfade";
			this->dis_xfade->Size = System::Drawing::Size(46, 20);
			this->dis_xfade->TabIndex = 110;
			this->dis_xfade->Text = L"10";
			this->dis_xfade->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// label121
			// 
			this->label121->AutoSize = true;
			this->label121->Location = System::Drawing::Point(61, 17);
			this->label121->Name = L"label121";
			this->label121->Size = System::Drawing::Size(24, 13);
			this->label121->TabIndex = 111;
			this->label121->Text = L"sec";
			// 
			// label131
			// 
			this->label131->AutoSize = true;
			this->label131->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label131->Location = System::Drawing::Point(413, 262);
			this->label131->Name = L"label131";
			this->label131->Size = System::Drawing::Size(197, 17);
			this->label131->TabIndex = 125;
			this->label131->Text = L"Respiratory / Oxygenation";
			// 
			// label130
			// 
			this->label130->AutoSize = true;
			this->label130->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label130->Location = System::Drawing::Point(153, 92);
			this->label130->Name = L"label130";
			this->label130->Size = System::Drawing::Size(155, 17);
			this->label130->TabIndex = 124;
			this->label130->Text = L"Core Hemodynamics";
			// 
			// label129
			// 
			this->label129->AutoSize = true;
			this->label129->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label129->Location = System::Drawing::Point(153, 262);
			this->label129->Name = L"label129";
			this->label129->Size = System::Drawing::Size(148, 17);
			this->label129->TabIndex = 123;
			this->label129->Text = L"Dynamic Predictors";
			// 
			// checkBox3
			// 
			this->checkBox3->AutoSize = true;
			this->checkBox3->Checked = true;
			this->checkBox3->CheckState = System::Windows::Forms::CheckState::Checked;
			this->checkBox3->Location = System::Drawing::Point(156, 351);
			this->checkBox3->Name = L"checkBox3";
			this->checkBox3->Size = System::Drawing::Size(96, 17);
			this->checkBox3->TabIndex = 120;
			this->checkBox3->Text = L"Link Predictors";
			this->checkBox3->UseVisualStyleBackColor = true;
			this->checkBox3->CheckedChanged += gcnew System::EventHandler(this, &Form1::checkBox3_CheckedChanged);
			// 
			// label_PLS
			// 
			this->label_PLS->AutoSize = true;
			this->label_PLS->ForeColor = System::Drawing::Color::Indigo;
			this->label_PLS->Location = System::Drawing::Point(219, 290);
			this->label_PLS->Name = L"label_PLS";
			this->label_PLS->Size = System::Drawing::Size(21, 13);
			this->label_PLS->TabIndex = 122;
			this->label_PLS->Text = L"0%";
			// 
			// trackBar_Preload
			// 
			this->trackBar_Preload->LargeChange = 10;
			this->trackBar_Preload->Location = System::Drawing::Point(156, 309);
			this->trackBar_Preload->Maximum = 40;
			this->trackBar_Preload->Minimum = 3;
			this->trackBar_Preload->Name = L"trackBar_Preload";
			this->trackBar_Preload->Size = System::Drawing::Size(110, 45);
			this->trackBar_Preload->TabIndex = 121;
			this->trackBar_Preload->TickFrequency = 5;
			this->trackBar_Preload->Value = 3;
			this->trackBar_Preload->Scroll += gcnew System::EventHandler(this, &Form1::trackBar_Preload_Scroll);
			// 
			// label128
			// 
			this->label128->AutoSize = true;
			this->label128->ForeColor = System::Drawing::Color::Indigo;
			this->label128->Location = System::Drawing::Point(153, 290);
			this->label128->Name = L"label128";
			this->label128->Size = System::Drawing::Size(70, 13);
			this->label128->TabIndex = 118;
			this->label128->Text = L"Preload Sens";
			// 
			// label127
			// 
			this->label127->AutoSize = true;
			this->label127->Location = System::Drawing::Point(272, 290);
			this->label127->Name = L"label127";
			this->label127->Size = System::Drawing::Size(12, 13);
			this->label127->TabIndex = 117;
			this->label127->Text = L"/";
			// 
			// label125
			// 
			this->label125->AutoSize = true;
			this->label125->Location = System::Drawing::Point(272, 311);
			this->label125->Name = L"label125";
			this->label125->Size = System::Drawing::Size(12, 13);
			this->label125->TabIndex = 114;
			this->label125->Text = L"/";
			// 
			// dis_FTc
			// 
			this->dis_FTc->Location = System::Drawing::Point(320, 349);
			this->dis_FTc->Name = L"dis_FTc";
			this->dis_FTc->ReadOnly = true;
			this->dis_FTc->Size = System::Drawing::Size(53, 20);
			this->dis_FTc->TabIndex = 113;
			this->dis_FTc->Text = L"4.4";
			// 
			// labelFTc
			// 
			this->labelFTc->AutoSize = true;
			this->labelFTc->ForeColor = System::Drawing::Color::Indigo;
			this->labelFTc->Location = System::Drawing::Point(284, 352);
			this->labelFTc->Name = L"labelFTc";
			this->labelFTc->Size = System::Drawing::Size(26, 13);
			this->labelFTc->TabIndex = 112;
			this->labelFTc->Text = L"FTc";
			// 
			// label122
			// 
			this->label122->AutoSize = true;
			this->label122->ForeColor = System::Drawing::Color::RoyalBlue;
			this->label122->Location = System::Drawing::Point(567, 331);
			this->label122->Name = L"label122";
			this->label122->Size = System::Drawing::Size(41, 13);
			this->label122->TabIndex = 106;
			this->label122->Text = L"SCvO2";
			// 
			// dis_SCvO2
			// 
			this->dis_SCvO2->Location = System::Drawing::Point(615, 328);
			this->dis_SCvO2->Name = L"dis_SCvO2";
			this->dis_SCvO2->Size = System::Drawing::Size(53, 20);
			this->dis_SCvO2->TabIndex = 94;
			this->dis_SCvO2->Text = L"70";
			// 
			// dis_SvO2
			// 
			this->dis_SvO2->Location = System::Drawing::Point(615, 307);
			this->dis_SvO2->Name = L"dis_SvO2";
			this->dis_SvO2->Size = System::Drawing::Size(53, 20);
			this->dis_SvO2->TabIndex = 93;
			this->dis_SvO2->Text = L"70";
			// 
			// label123
			// 
			this->label123->AutoSize = true;
			this->label123->ForeColor = System::Drawing::Color::RoyalBlue;
			this->label123->Location = System::Drawing::Point(567, 310);
			this->label123->Name = L"label123";
			this->label123->Size = System::Drawing::Size(34, 13);
			this->label123->TabIndex = 105;
			this->label123->Text = L"SvO2";
			// 
			// label120
			// 
			this->label120->AutoSize = true;
			this->label120->ForeColor = System::Drawing::Color::Sienna;
			this->label120->Location = System::Drawing::Point(569, 124);
			this->label120->Name = L"label120";
			this->label120->Size = System::Drawing::Size(38, 13);
			this->label120->TabIndex = 101;
			this->label120->Text = L"PA SP";
			// 
			// dis_PASP
			// 
			this->dis_PASP->Location = System::Drawing::Point(615, 121);
			this->dis_PASP->Name = L"dis_PASP";
			this->dis_PASP->Size = System::Drawing::Size(53, 20);
			this->dis_PASP->TabIndex = 86;
			this->dis_PASP->Text = L"41";
			// 
			// labelPADP
			// 
			this->labelPADP->AutoSize = true;
			this->labelPADP->ForeColor = System::Drawing::Color::Sienna;
			this->labelPADP->Location = System::Drawing::Point(569, 144);
			this->labelPADP->Name = L"labelPADP";
			this->labelPADP->Size = System::Drawing::Size(39, 13);
			this->labelPADP->TabIndex = 102;
			this->labelPADP->Text = L"PA DP";
			// 
			// dis_PADP
			// 
			this->dis_PADP->Location = System::Drawing::Point(615, 141);
			this->dis_PADP->Name = L"dis_PADP";
			this->dis_PADP->Size = System::Drawing::Size(53, 20);
			this->dis_PADP->TabIndex = 87;
			this->dis_PADP->Text = L"21";
			// 
			// dis_CI
			// 
			this->dis_CI->Location = System::Drawing::Point(320, 119);
			this->dis_CI->Name = L"dis_CI";
			this->dis_CI->ReadOnly = true;
			this->dis_CI->Size = System::Drawing::Size(53, 20);
			this->dis_CI->TabIndex = 100;
			this->dis_CI->Text = L"2.3";
			// 
			// label119
			// 
			this->label119->AutoSize = true;
			this->label119->ForeColor = System::Drawing::Color::Black;
			this->label119->Location = System::Drawing::Point(284, 122);
			this->label119->Name = L"label119";
			this->label119->Size = System::Drawing::Size(17, 13);
			this->label119->TabIndex = 99;
			this->label119->Text = L"CI";
			// 
			// WARN_SVR
			// 
			this->WARN_SVR->AutoSize = true;
			this->WARN_SVR->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->WARN_SVR->ForeColor = System::Drawing::Color::Firebrick;
			this->WARN_SVR->Location = System::Drawing::Point(394, 151);
			this->WARN_SVR->Name = L"WARN_SVR";
			this->WARN_SVR->Size = System::Drawing::Size(0, 13);
			this->WARN_SVR->TabIndex = 98;
			// 
			// label100
			// 
			this->label100->AutoSize = true;
			this->label100->Location = System::Drawing::Point(339, 161);
			this->label100->Name = L"label100";
			this->label100->Size = System::Drawing::Size(12, 13);
			this->label100->TabIndex = 97;
			this->label100->Text = L"\\";
			// 
			// label88
			// 
			this->label88->AutoSize = true;
			this->label88->Location = System::Drawing::Point(339, 178);
			this->label88->Name = L"label88";
			this->label88->Size = System::Drawing::Size(12, 13);
			this->label88->TabIndex = 96;
			this->label88->Text = L"/";
			// 
			// dis_HgB
			// 
			this->dis_HgB->Location = System::Drawing::Point(495, 203);
			this->dis_HgB->Name = L"dis_HgB";
			this->dis_HgB->Size = System::Drawing::Size(53, 20);
			this->dis_HgB->TabIndex = 85;
			this->dis_HgB->Text = L"14.0";
			// 
			// hgblabel
			// 
			this->hgblabel->AutoSize = true;
			this->hgblabel->ForeColor = System::Drawing::Color::Firebrick;
			this->hgblabel->Location = System::Drawing::Point(449, 206);
			this->hgblabel->Name = L"hgblabel";
			this->hgblabel->Size = System::Drawing::Size(28, 13);
			this->hgblabel->TabIndex = 76;
			this->hgblabel->Text = L"HgB";
			// 
			// label109
			// 
			this->label109->AutoSize = true;
			this->label109->Location = System::Drawing::Point(269, 206);
			this->label109->Name = L"label109";
			this->label109->Size = System::Drawing::Size(12, 13);
			this->label109->TabIndex = 73;
			this->label109->Text = L"/";
			// 
			// label110
			// 
			this->label110->AutoSize = true;
			this->label110->Location = System::Drawing::Point(269, 187);
			this->label110->Name = L"label110";
			this->label110->Size = System::Drawing::Size(12, 13);
			this->label110->TabIndex = 72;
			this->label110->Text = L"\\";
			// 
			// label108
			// 
			this->label108->AutoSize = true;
			this->label108->Location = System::Drawing::Point(269, 143);
			this->label108->Name = L"label108";
			this->label108->Size = System::Drawing::Size(12, 13);
			this->label108->TabIndex = 71;
			this->label108->Text = L"/";
			// 
			// label107
			// 
			this->label107->AutoSize = true;
			this->label107->Location = System::Drawing::Point(269, 122);
			this->label107->Name = L"label107";
			this->label107->Size = System::Drawing::Size(12, 13);
			this->label107->TabIndex = 70;
			this->label107->Text = L"\\";
			// 
			// label99
			// 
			this->label99->AutoSize = true;
			this->label99->ForeColor = System::Drawing::Color::Black;
			this->label99->Location = System::Drawing::Point(450, 330);
			this->label99->Name = L"label99";
			this->label99->Size = System::Drawing::Size(41, 13);
			this->label99->TabIndex = 52;
			this->label99->Text = L"PaCO2";
			// 
			// dis_PACO2
			// 
			this->dis_PACO2->Location = System::Drawing::Point(495, 327);
			this->dis_PACO2->Name = L"dis_PACO2";
			this->dis_PACO2->Size = System::Drawing::Size(53, 20);
			this->dis_PACO2->TabIndex = 91;
			this->dis_PACO2->Text = L"40";
			// 
			// label101
			// 
			this->label101->AutoSize = true;
			this->label101->Location = System::Drawing::Point(416, 290);
			this->label101->Name = L"label101";
			this->label101->Size = System::Drawing::Size(73, 13);
			this->label101->TabIndex = 54;
			this->label101->Text = L"Deadspace %";
			// 
			// dis_SV
			// 
			this->dis_SV->Location = System::Drawing::Point(213, 143);
			this->dis_SV->Name = L"dis_SV";
			this->dis_SV->Size = System::Drawing::Size(53, 20);
			this->dis_SV->TabIndex = 81;
			this->dis_SV->Text = L"75";
			this->dis_SV->TextChanged += gcnew System::EventHandler(this, &Form1::dis_HR_TextChanged);
			// 
			// dis_Deadspace
			// 
			this->dis_Deadspace->Location = System::Drawing::Point(495, 287);
			this->dis_Deadspace->Name = L"dis_Deadspace";
			this->dis_Deadspace->Size = System::Drawing::Size(53, 20);
			this->dis_Deadspace->TabIndex = 89;
			this->dis_Deadspace->Text = L"5";
			// 
			// label102
			// 
			this->label102->AutoSize = true;
			this->label102->ForeColor = System::Drawing::Color::Sienna;
			this->label102->Location = System::Drawing::Point(169, 146);
			this->label102->Name = L"label102";
			this->label102->Size = System::Drawing::Size(21, 13);
			this->label102->TabIndex = 64;
			this->label102->Text = L"SV";
			// 
			// label103
			// 
			this->label103->AutoSize = true;
			this->label103->Location = System::Drawing::Point(413, 310);
			this->label103->Name = L"label103";
			this->label103->Size = System::Drawing::Size(76, 13);
			this->label103->TabIndex = 56;
			this->label103->Text = L"Outflow Obs %";
			// 
			// dis_Hgbx
			// 
			this->dis_Hgbx->Location = System::Drawing::Point(615, 203);
			this->dis_Hgbx->Name = L"dis_Hgbx";
			this->dis_Hgbx->Size = System::Drawing::Size(53, 20);
			this->dis_Hgbx->TabIndex = 88;
			this->dis_Hgbx->Text = L"0";
			// 
			// dis_Outflow
			// 
			this->dis_Outflow->Location = System::Drawing::Point(495, 307);
			this->dis_Outflow->Name = L"dis_Outflow";
			this->dis_Outflow->Size = System::Drawing::Size(53, 20);
			this->dis_Outflow->TabIndex = 90;
			this->dis_Outflow->Text = L"0";
			// 
			// label104
			// 
			this->label104->AutoSize = true;
			this->label104->ForeColor = System::Drawing::Color::Firebrick;
			this->label104->Location = System::Drawing::Point(566, 206);
			this->label104->Name = L"label104";
			this->label104->Size = System::Drawing::Size(34, 13);
			this->label104->TabIndex = 62;
			this->label104->Text = L"SpHb";
			// 
			// label105
			// 
			this->label105->AutoSize = true;
			this->label105->ForeColor = System::Drawing::Color::Indigo;
			this->label105->Location = System::Drawing::Point(284, 311);
			this->label105->Name = L"label105";
			this->label105->Size = System::Drawing::Size(28, 13);
			this->label105->TabIndex = 58;
			this->label105->Text = L"SVV";
			// 
			// dis_PVI
			// 
			this->dis_PVI->Location = System::Drawing::Point(320, 328);
			this->dis_PVI->Name = L"dis_PVI";
			this->dis_PVI->ReadOnly = true;
			this->dis_PVI->Size = System::Drawing::Size(53, 20);
			this->dis_PVI->TabIndex = 92;
			this->dis_PVI->Text = L"8";
			// 
			// dis_SVV
			// 
			this->dis_SVV->Location = System::Drawing::Point(320, 308);
			this->dis_SVV->Name = L"dis_SVV";
			this->dis_SVV->ReadOnly = true;
			this->dis_SVV->Size = System::Drawing::Size(53, 20);
			this->dis_SVV->TabIndex = 91;
			this->dis_SVV->Text = L"5";
			// 
			// label106
			// 
			this->label106->AutoSize = true;
			this->label106->ForeColor = System::Drawing::Color::Indigo;
			this->label106->Location = System::Drawing::Point(284, 331);
			this->label106->Name = L"label106";
			this->label106->Size = System::Drawing::Size(24, 13);
			this->label106->TabIndex = 60;
			this->label106->Text = L"PVI";
			// 
			// dis_SP02
			// 
			this->dis_SP02->Location = System::Drawing::Point(615, 287);
			this->dis_SP02->Name = L"dis_SP02";
			this->dis_SP02->Size = System::Drawing::Size(53, 20);
			this->dis_SP02->TabIndex = 92;
			this->dis_SP02->Text = L"100";
			this->dis_SP02->TextChanged += gcnew System::EventHandler(this, &Form1::dis_SP02_TextChanged);
			// 
			// label86
			// 
			this->label86->AutoSize = true;
			this->label86->ForeColor = System::Drawing::Color::RoyalBlue;
			this->label86->Location = System::Drawing::Point(567, 290);
			this->label86->Name = L"label86";
			this->label86->Size = System::Drawing::Size(35, 13);
			this->label86->TabIndex = 50;
			this->label86->Text = L"SPO2";
			// 
			// label91
			// 
			this->label91->AutoSize = true;
			this->label91->ForeColor = System::Drawing::Color::Green;
			this->label91->Location = System::Drawing::Point(169, 122);
			this->label91->Name = L"label91";
			this->label91->Size = System::Drawing::Size(23, 13);
			this->label91->TabIndex = 33;
			this->label91->Text = L"HR";
			// 
			// dis_SVR
			// 
			this->dis_SVR->Location = System::Drawing::Point(386, 167);
			this->dis_SVR->Name = L"dis_SVR";
			this->dis_SVR->ReadOnly = true;
			this->dis_SVR->Size = System::Drawing::Size(53, 20);
			this->dis_SVR->TabIndex = 48;
			this->dis_SVR->Text = L"1276";
			// 
			// dis_HR
			// 
			this->dis_HR->Location = System::Drawing::Point(213, 119);
			this->dis_HR->Name = L"dis_HR";
			this->dis_HR->Size = System::Drawing::Size(53, 20);
			this->dis_HR->TabIndex = 80;
			this->dis_HR->Text = L"70";
			this->dis_HR->TextChanged += gcnew System::EventHandler(this, &Form1::dis_HR_TextChanged);
			// 
			// label92
			// 
			this->label92->AutoSize = true;
			this->label92->ForeColor = System::Drawing::Color::Firebrick;
			this->label92->Location = System::Drawing::Point(351, 170);
			this->label92->Name = L"label92";
			this->label92->Size = System::Drawing::Size(29, 13);
			this->label92->TabIndex = 47;
			this->label92->Text = L"SVR";
			// 
			// label93
			// 
			this->label93->AutoSize = true;
			this->label93->ForeColor = System::Drawing::Color::Firebrick;
			this->label93->Location = System::Drawing::Point(168, 186);
			this->label93->Name = L"label93";
			this->label93->Size = System::Drawing::Size(28, 13);
			this->label93->TabIndex = 35;
			this->label93->Text = L"SBP";
			// 
			// dis_PPV
			// 
			this->dis_PPV->ForeColor = System::Drawing::Color::Black;
			this->dis_PPV->Location = System::Drawing::Point(320, 287);
			this->dis_PPV->Name = L"dis_PPV";
			this->dis_PPV->ReadOnly = true;
			this->dis_PPV->Size = System::Drawing::Size(53, 20);
			this->dis_PPV->TabIndex = 90;
			this->dis_PPV->Text = L"3";
			// 
			// dis_SBP
			// 
			this->dis_SBP->Location = System::Drawing::Point(214, 183);
			this->dis_SBP->Name = L"dis_SBP";
			this->dis_SBP->Size = System::Drawing::Size(53, 20);
			this->dis_SBP->TabIndex = 82;
			this->dis_SBP->Text = L"110";
			this->dis_SBP->TextChanged += gcnew System::EventHandler(this, &Form1::dis_SBP_TextChanged);
			// 
			// label94
			// 
			this->label94->AutoSize = true;
			this->label94->ForeColor = System::Drawing::Color::Indigo;
			this->label94->Location = System::Drawing::Point(284, 290);
			this->label94->Name = L"label94";
			this->label94->Size = System::Drawing::Size(28, 13);
			this->label94->TabIndex = 45;
			this->label94->Text = L"PPV";
			// 
			// label95
			// 
			this->label95->AutoSize = true;
			this->label95->ForeColor = System::Drawing::Color::Firebrick;
			this->label95->Location = System::Drawing::Point(168, 206);
			this->label95->Name = L"label95";
			this->label95->Size = System::Drawing::Size(29, 13);
			this->label95->TabIndex = 37;
			this->label95->Text = L"DBP";
			// 
			// dis_CO
			// 
			this->dis_CO->Location = System::Drawing::Point(320, 141);
			this->dis_CO->Name = L"dis_CO";
			this->dis_CO->ReadOnly = true;
			this->dis_CO->Size = System::Drawing::Size(53, 20);
			this->dis_CO->TabIndex = 81;
			this->dis_CO->Text = L"5.2";
			this->dis_CO->TextChanged += gcnew System::EventHandler(this, &Form1::dis_CO_TextChanged);
			// 
			// dis_DBP
			// 
			this->dis_DBP->Location = System::Drawing::Point(214, 203);
			this->dis_DBP->Name = L"dis_DBP";
			this->dis_DBP->Size = System::Drawing::Size(53, 20);
			this->dis_DBP->TabIndex = 83;
			this->dis_DBP->Text = L"70";
			this->dis_DBP->TextChanged += gcnew System::EventHandler(this, &Form1::dis_SBP_TextChanged);
			// 
			// label96
			// 
			this->label96->AutoSize = true;
			this->label96->ForeColor = System::Drawing::Color::Black;
			this->label96->Location = System::Drawing::Point(284, 144);
			this->label96->Name = L"label96";
			this->label96->Size = System::Drawing::Size(22, 13);
			this->label96->TabIndex = 43;
			this->label96->Text = L"CO";
			// 
			// label97
			// 
			this->label97->AutoSize = true;
			this->label97->ForeColor = System::Drawing::Color::Firebrick;
			this->label97->Location = System::Drawing::Point(284, 196);
			this->label97->Name = L"label97";
			this->label97->Size = System::Drawing::Size(30, 13);
			this->label97->TabIndex = 39;
			this->label97->Text = L"MAP";
			// 
			// dis_CVP
			// 
			this->dis_CVP->Location = System::Drawing::Point(495, 141);
			this->dis_CVP->Name = L"dis_CVP";
			this->dis_CVP->Size = System::Drawing::Size(53, 20);
			this->dis_CVP->TabIndex = 84;
			this->dis_CVP->Text = L"7";
			this->dis_CVP->TextChanged += gcnew System::EventHandler(this, &Form1::dis_CVP_TextChanged);
			// 
			// dis_MAP
			// 
			this->dis_MAP->Location = System::Drawing::Point(320, 193);
			this->dis_MAP->Name = L"dis_MAP";
			this->dis_MAP->ReadOnly = true;
			this->dis_MAP->Size = System::Drawing::Size(53, 20);
			this->dis_MAP->TabIndex = 40;
			this->dis_MAP->Text = L"83";
			// 
			// label98
			// 
			this->label98->AutoSize = true;
			this->label98->ForeColor = System::Drawing::Color::RoyalBlue;
			this->label98->Location = System::Drawing::Point(449, 145);
			this->label98->Name = L"label98";
			this->label98->Size = System::Drawing::Size(28, 13);
			this->label98->TabIndex = 41;
			this->label98->Text = L"CVP";
			// 
			// groupBox5
			// 
			this->groupBox5->Controls->Add(this->label84);
			this->groupBox5->Controls->Add(this->buttDisplayOFF);
			this->groupBox5->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->groupBox5->Location = System::Drawing::Point(6, 6);
			this->groupBox5->Name = L"groupBox5";
			this->groupBox5->Size = System::Drawing::Size(97, 76);
			this->groupBox5->TabIndex = 3;
			this->groupBox5->TabStop = false;
			// 
			// label84
			// 
			this->label84->AutoSize = true;
			this->label84->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label84->Location = System::Drawing::Point(18, 14);
			this->label84->Name = L"label84";
			this->label84->Size = System::Drawing::Size(61, 17);
			this->label84->TabIndex = 1;
			this->label84->Text = L"Display";
			// 
			// buttDisplayOFF
			// 
			this->buttDisplayOFF->BackColor = System::Drawing::Color::LightCoral;
			this->buttDisplayOFF->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->buttDisplayOFF->Location = System::Drawing::Point(21, 36);
			this->buttDisplayOFF->Name = L"buttDisplayOFF";
			this->buttDisplayOFF->Size = System::Drawing::Size(56, 27);
			this->buttDisplayOFF->TabIndex = 0;
			this->buttDisplayOFF->Text = L"OFF";
			this->buttDisplayOFF->UseVisualStyleBackColor = false;
			this->buttDisplayOFF->Click += gcnew System::EventHandler(this, &Form1::buttDisplayOFF_Click);
			// 
			// tabPage3
			// 
			this->tabPage3->BackColor = System::Drawing::SystemColors::ButtonFace;
			this->tabPage3->Controls->Add(this->groupBox19);
			this->tabPage3->Controls->Add(this->groupBox18);
			this->tabPage3->Controls->Add(this->groupBox16);
			this->tabPage3->Controls->Add(this->label112);
			this->tabPage3->Location = System::Drawing::Point(4, 22);
			this->tabPage3->Name = L"tabPage3";
			this->tabPage3->Padding = System::Windows::Forms::Padding(3);
			this->tabPage3->Size = System::Drawing::Size(678, 465);
			this->tabPage3->TabIndex = 2;
			this->tabPage3->Text = L"Settings";
			// 
			// groupBox19
			// 
			this->groupBox19->Controls->Add(this->cb_PulseOx);
			this->groupBox19->Controls->Add(this->cb_EKG);
			this->groupBox19->Controls->Add(this->cb_Aline);
			this->groupBox19->Controls->Add(this->cb_ETCO2);
			this->groupBox19->Location = System::Drawing::Point(422, 46);
			this->groupBox19->Name = L"groupBox19";
			this->groupBox19->Size = System::Drawing::Size(190, 102);
			this->groupBox19->TabIndex = 84;
			this->groupBox19->TabStop = false;
			this->groupBox19->Text = L"Waveforms On/Off";
			// 
			// cb_PulseOx
			// 
			this->cb_PulseOx->AutoSize = true;
			this->cb_PulseOx->Checked = true;
			this->cb_PulseOx->CheckState = System::Windows::Forms::CheckState::Checked;
			this->cb_PulseOx->Location = System::Drawing::Point(6, 58);
			this->cb_PulseOx->Name = L"cb_PulseOx";
			this->cb_PulseOx->Size = System::Drawing::Size(98, 17);
			this->cb_PulseOx->TabIndex = 18;
			this->cb_PulseOx->Text = L"Show Pulse Ox";
			this->cb_PulseOx->UseVisualStyleBackColor = true;
			this->cb_PulseOx->CheckedChanged += gcnew System::EventHandler(this, &Form1::cb_PulseOx_CheckedChanged);
			// 
			// cb_EKG
			// 
			this->cb_EKG->AutoSize = true;
			this->cb_EKG->Checked = true;
			this->cb_EKG->CheckState = System::Windows::Forms::CheckState::Checked;
			this->cb_EKG->Location = System::Drawing::Point(6, 18);
			this->cb_EKG->Name = L"cb_EKG";
			this->cb_EKG->Size = System::Drawing::Size(78, 17);
			this->cb_EKG->TabIndex = 17;
			this->cb_EKG->Text = L"Show EKG";
			this->cb_EKG->UseVisualStyleBackColor = true;
			this->cb_EKG->CheckedChanged += gcnew System::EventHandler(this, &Form1::cb_EKG_CheckedChanged);
			// 
			// cb_Aline
			// 
			this->cb_Aline->AutoSize = true;
			this->cb_Aline->Checked = true;
			this->cb_Aline->CheckState = System::Windows::Forms::CheckState::Checked;
			this->cb_Aline->Location = System::Drawing::Point(6, 38);
			this->cb_Aline->Name = L"cb_Aline";
			this->cb_Aline->Size = System::Drawing::Size(86, 17);
			this->cb_Aline->TabIndex = 15;
			this->cb_Aline->Text = L"Show A-Line";
			this->cb_Aline->UseVisualStyleBackColor = true;
			this->cb_Aline->CheckedChanged += gcnew System::EventHandler(this, &Form1::cb_Aline_CheckedChanged);
			// 
			// cb_ETCO2
			// 
			this->cb_ETCO2->AutoSize = true;
			this->cb_ETCO2->Checked = true;
			this->cb_ETCO2->CheckState = System::Windows::Forms::CheckState::Checked;
			this->cb_ETCO2->Location = System::Drawing::Point(6, 79);
			this->cb_ETCO2->Name = L"cb_ETCO2";
			this->cb_ETCO2->Size = System::Drawing::Size(87, 17);
			this->cb_ETCO2->TabIndex = 16;
			this->cb_ETCO2->Text = L"Show EtCO2";
			this->cb_ETCO2->UseVisualStyleBackColor = true;
			this->cb_ETCO2->CheckedChanged += gcnew System::EventHandler(this, &Form1::cb_ETCO2_CheckedChanged);
			// 
			// groupBox18
			// 
			this->groupBox18->Controls->Add(this->checkBoxBarGraph6);
			this->groupBox18->Controls->Add(this->checkBoxBarGraph5);
			this->groupBox18->Controls->Add(this->comboBox5);
			this->groupBox18->Controls->Add(this->label113);
			this->groupBox18->Controls->Add(this->checkBoxGraph2);
			this->groupBox18->Controls->Add(this->label114);
			this->groupBox18->Controls->Add(this->comboBox6);
			this->groupBox18->Controls->Add(this->checkBoxGraph1);
			this->groupBox18->Location = System::Drawing::Point(7, 43);
			this->groupBox18->Name = L"groupBox18";
			this->groupBox18->Size = System::Drawing::Size(409, 75);
			this->groupBox18->TabIndex = 83;
			this->groupBox18->TabStop = false;
			this->groupBox18->Text = L"Primary Slot Options";
			// 
			// checkBoxBarGraph6
			// 
			this->checkBoxBarGraph6->AutoSize = true;
			this->checkBoxBarGraph6->Checked = true;
			this->checkBoxBarGraph6->CheckState = System::Windows::Forms::CheckState::Checked;
			this->checkBoxBarGraph6->Location = System::Drawing::Point(252, 48);
			this->checkBoxBarGraph6->Name = L"checkBoxBarGraph6";
			this->checkBoxBarGraph6->Size = System::Drawing::Size(74, 17);
			this->checkBoxBarGraph6->TabIndex = 20;
			this->checkBoxBarGraph6->Text = L"Bar Graph";
			this->checkBoxBarGraph6->UseVisualStyleBackColor = true;
			this->checkBoxBarGraph6->CheckedChanged += gcnew System::EventHandler(this, &Form1::checkBoxBarGraph6_CheckedChanged);
			// 
			// checkBoxBarGraph5
			// 
			this->checkBoxBarGraph5->AutoSize = true;
			this->checkBoxBarGraph5->Checked = true;
			this->checkBoxBarGraph5->CheckState = System::Windows::Forms::CheckState::Checked;
			this->checkBoxBarGraph5->Location = System::Drawing::Point(252, 19);
			this->checkBoxBarGraph5->Name = L"checkBoxBarGraph5";
			this->checkBoxBarGraph5->Size = System::Drawing::Size(74, 17);
			this->checkBoxBarGraph5->TabIndex = 19;
			this->checkBoxBarGraph5->Text = L"Bar Graph";
			this->checkBoxBarGraph5->UseVisualStyleBackColor = true;
			this->checkBoxBarGraph5->CheckedChanged += gcnew System::EventHandler(this, &Form1::checkBoxBarGraph5_CheckedChanged);
			// 
			// comboBox5
			// 
			this->comboBox5->FormattingEnabled = true;
			this->comboBox5->Location = System::Drawing::Point(46, 18);
			this->comboBox5->Name = L"comboBox5";
			this->comboBox5->Size = System::Drawing::Size(109, 21);
			this->comboBox5->TabIndex = 10;
			this->comboBox5->SelectedIndexChanged += gcnew System::EventHandler(this, &Form1::comboBox5_SelectedIndexChanged);
			// 
			// label113
			// 
			this->label113->AutoSize = true;
			this->label113->Location = System::Drawing::Point(6, 49);
			this->label113->Name = L"label113";
			this->label113->Size = System::Drawing::Size(34, 13);
			this->label113->TabIndex = 2;
			this->label113->Text = L"Slot 6";
			// 
			// checkBoxGraph2
			// 
			this->checkBoxGraph2->AutoSize = true;
			this->checkBoxGraph2->Checked = true;
			this->checkBoxGraph2->CheckState = System::Windows::Forms::CheckState::Checked;
			this->checkBoxGraph2->Location = System::Drawing::Point(161, 48);
			this->checkBoxGraph2->Name = L"checkBoxGraph2";
			this->checkBoxGraph2->Size = System::Drawing::Size(85, 17);
			this->checkBoxGraph2->TabIndex = 18;
			this->checkBoxGraph2->Text = L"Graph Slot 6";
			this->checkBoxGraph2->UseVisualStyleBackColor = true;
			this->checkBoxGraph2->CheckedChanged += gcnew System::EventHandler(this, &Form1::checkBoxGraph2_CheckedChanged_1);
			// 
			// label114
			// 
			this->label114->AutoSize = true;
			this->label114->Location = System::Drawing::Point(6, 21);
			this->label114->Name = L"label114";
			this->label114->Size = System::Drawing::Size(34, 13);
			this->label114->TabIndex = 3;
			this->label114->Text = L"Slot 5";
			// 
			// comboBox6
			// 
			this->comboBox6->FormattingEnabled = true;
			this->comboBox6->Location = System::Drawing::Point(46, 46);
			this->comboBox6->Name = L"comboBox6";
			this->comboBox6->Size = System::Drawing::Size(109, 21);
			this->comboBox6->TabIndex = 9;
			this->comboBox6->SelectedIndexChanged += gcnew System::EventHandler(this, &Form1::comboBox6_SelectedIndexChanged);
			// 
			// checkBoxGraph1
			// 
			this->checkBoxGraph1->AutoSize = true;
			this->checkBoxGraph1->Checked = true;
			this->checkBoxGraph1->CheckState = System::Windows::Forms::CheckState::Checked;
			this->checkBoxGraph1->Location = System::Drawing::Point(161, 19);
			this->checkBoxGraph1->Name = L"checkBoxGraph1";
			this->checkBoxGraph1->Size = System::Drawing::Size(85, 17);
			this->checkBoxGraph1->TabIndex = 17;
			this->checkBoxGraph1->Text = L"Graph Slot 5";
			this->checkBoxGraph1->UseVisualStyleBackColor = true;
			this->checkBoxGraph1->CheckedChanged += gcnew System::EventHandler(this, &Form1::checkBoxGraph1_CheckedChanged);
			// 
			// groupBox16
			// 
			this->groupBox16->Controls->Add(this->comboBox1);
			this->groupBox16->Controls->Add(this->label111);
			this->groupBox16->Controls->Add(this->label115);
			this->groupBox16->Controls->Add(this->label116);
			this->groupBox16->Controls->Add(this->label117);
			this->groupBox16->Controls->Add(this->comboBox2);
			this->groupBox16->Controls->Add(this->comboBox4);
			this->groupBox16->Controls->Add(this->comboBox3);
			this->groupBox16->Location = System::Drawing::Point(6, 124);
			this->groupBox16->Name = L"groupBox16";
			this->groupBox16->Size = System::Drawing::Size(332, 89);
			this->groupBox16->TabIndex = 19;
			this->groupBox16->TabStop = false;
			this->groupBox16->Text = L"Alternate Slot Options";
			// 
			// comboBox1
			// 
			this->comboBox1->FormattingEnabled = true;
			this->comboBox1->Location = System::Drawing::Point(46, 23);
			this->comboBox1->Name = L"comboBox1";
			this->comboBox1->Size = System::Drawing::Size(109, 21);
			this->comboBox1->TabIndex = 7;
			this->comboBox1->SelectedIndexChanged += gcnew System::EventHandler(this, &Form1::comboBox1_SelectedIndexChanged);
			// 
			// label111
			// 
			this->label111->AutoSize = true;
			this->label111->Location = System::Drawing::Point(6, 26);
			this->label111->Name = L"label111";
			this->label111->Size = System::Drawing::Size(34, 13);
			this->label111->TabIndex = 0;
			this->label111->Text = L"Slot 1";
			// 
			// label115
			// 
			this->label115->AutoSize = true;
			this->label115->Location = System::Drawing::Point(175, 54);
			this->label115->Name = L"label115";
			this->label115->Size = System::Drawing::Size(34, 13);
			this->label115->TabIndex = 4;
			this->label115->Text = L"Slot 4";
			// 
			// label116
			// 
			this->label116->AutoSize = true;
			this->label116->Location = System::Drawing::Point(175, 26);
			this->label116->Name = L"label116";
			this->label116->Size = System::Drawing::Size(34, 13);
			this->label116->TabIndex = 5;
			this->label116->Text = L"Slot 3";
			// 
			// label117
			// 
			this->label117->AutoSize = true;
			this->label117->Location = System::Drawing::Point(6, 54);
			this->label117->Name = L"label117";
			this->label117->Size = System::Drawing::Size(34, 13);
			this->label117->TabIndex = 6;
			this->label117->Text = L"Slot 2";
			// 
			// comboBox2
			// 
			this->comboBox2->FormattingEnabled = true;
			this->comboBox2->Location = System::Drawing::Point(46, 51);
			this->comboBox2->Name = L"comboBox2";
			this->comboBox2->Size = System::Drawing::Size(109, 21);
			this->comboBox2->TabIndex = 13;
			this->comboBox2->SelectedIndexChanged += gcnew System::EventHandler(this, &Form1::comboBox2_SelectedIndexChanged);
			// 
			// comboBox4
			// 
			this->comboBox4->FormattingEnabled = true;
			this->comboBox4->Location = System::Drawing::Point(215, 51);
			this->comboBox4->Name = L"comboBox4";
			this->comboBox4->Size = System::Drawing::Size(109, 21);
			this->comboBox4->TabIndex = 11;
			this->comboBox4->SelectedIndexChanged += gcnew System::EventHandler(this, &Form1::comboBox4_SelectedIndexChanged);
			// 
			// comboBox3
			// 
			this->comboBox3->FormattingEnabled = true;
			this->comboBox3->Location = System::Drawing::Point(215, 23);
			this->comboBox3->Name = L"comboBox3";
			this->comboBox3->Size = System::Drawing::Size(109, 21);
			this->comboBox3->TabIndex = 12;
			this->comboBox3->SelectedIndexChanged += gcnew System::EventHandler(this, &Form1::comboBox3_SelectedIndexChanged);
			// 
			// label112
			// 
			this->label112->AutoSize = true;
			this->label112->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label112->Location = System::Drawing::Point(15, 23);
			this->label112->Name = L"label112";
			this->label112->Size = System::Drawing::Size(220, 17);
			this->label112->TabIndex = 1;
			this->label112->Text = L"Monitor Slots and Waveforms";
			// 
			// tabPage4
			// 
			this->tabPage4->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(220)), static_cast<System::Int32>(static_cast<System::Byte>(222)),
				static_cast<System::Int32>(static_cast<System::Byte>(255)));
			this->tabPage4->Controls->Add(this->label_loops);
			this->tabPage4->Controls->Add(this->groupBox20);
			this->tabPage4->Controls->Add(this->domainUpDown1);
			this->tabPage4->Controls->Add(this->panel4);
			this->tabPage4->Controls->Add(this->groupBox11);
			this->tabPage4->Controls->Add(this->groupBox15);
			this->tabPage4->Controls->Add(this->groupBox14);
			this->tabPage4->Controls->Add(this->label_savefile2);
			this->tabPage4->Controls->Add(this->label_savefile);
			this->tabPage4->Controls->Add(this->groupBox13);
			this->tabPage4->Controls->Add(this->panel1);
			this->tabPage4->Controls->Add(this->groupBox10);
			this->tabPage4->Location = System::Drawing::Point(4, 22);
			this->tabPage4->Name = L"tabPage4";
			this->tabPage4->Padding = System::Windows::Forms::Padding(3);
			this->tabPage4->Size = System::Drawing::Size(678, 465);
			this->tabPage4->TabIndex = 3;
			this->tabPage4->Text = L"Advanced";
			// 
			// label_loops
			// 
			this->label_loops->AutoSize = true;
			this->label_loops->Enabled = false;
			this->label_loops->Location = System::Drawing::Point(562, 384);
			this->label_loops->Name = L"label_loops";
			this->label_loops->Size = System::Drawing::Size(32, 13);
			this->label_loops->TabIndex = 76;
			this->label_loops->Text = L"loops";
			// 
			// groupBox20
			// 
			this->groupBox20->Controls->Add(this->cb_StartLag);
			this->groupBox20->Controls->Add(this->label142);
			this->groupBox20->Controls->Add(this->tb_listenPort);
			this->groupBox20->Controls->Add(this->cb_listenNetwork);
			this->groupBox20->Controls->Add(this->label141);
			this->groupBox20->Controls->Add(this->tb_netport);
			this->groupBox20->Controls->Add(this->label139);
			this->groupBox20->Controls->Add(this->tb_netadd);
			this->groupBox20->Controls->Add(this->cb_BroadcastNet);
			this->groupBox20->Location = System::Drawing::Point(3, 121);
			this->groupBox20->Name = L"groupBox20";
			this->groupBox20->Size = System::Drawing::Size(174, 169);
			this->groupBox20->TabIndex = 82;
			this->groupBox20->TabStop = false;
			this->groupBox20->Text = L"Network Output";
			// 
			// cb_StartLag
			// 
			this->cb_StartLag->AutoSize = true;
			this->cb_StartLag->Location = System::Drawing::Point(6, 93);
			this->cb_StartLag->Name = L"cb_StartLag";
			this->cb_StartLag->Size = System::Drawing::Size(124, 17);
			this->cb_StartLag->TabIndex = 58;
			this->cb_StartLag->Text = L"30 Second Start Lag";
			this->cb_StartLag->UseVisualStyleBackColor = true;
			// 
			// label142
			// 
			this->label142->AutoSize = true;
			this->label142->Location = System::Drawing::Point(9, 142);
			this->label142->Name = L"label142";
			this->label142->Size = System::Drawing::Size(26, 13);
			this->label142->TabIndex = 57;
			this->label142->Text = L"Port";
			// 
			// tb_listenPort
			// 
			this->tb_listenPort->Enabled = false;
			this->tb_listenPort->Location = System::Drawing::Point(40, 140);
			this->tb_listenPort->Name = L"tb_listenPort";
			this->tb_listenPort->Size = System::Drawing::Size(85, 20);
			this->tb_listenPort->TabIndex = 56;
			this->tb_listenPort->Text = L"3145";
			// 
			// cb_listenNetwork
			// 
			this->cb_listenNetwork->AutoSize = true;
			this->cb_listenNetwork->Checked = true;
			this->cb_listenNetwork->CheckState = System::Windows::Forms::CheckState::Checked;
			this->cb_listenNetwork->Location = System::Drawing::Point(6, 116);
			this->cb_listenNetwork->Name = L"cb_listenNetwork";
			this->cb_listenNetwork->Size = System::Drawing::Size(101, 17);
			this->cb_listenNetwork->TabIndex = 55;
			this->cb_listenNetwork->Text = L"Listen for Inputs";
			this->cb_listenNetwork->UseVisualStyleBackColor = true;
			this->cb_listenNetwork->CheckedChanged += gcnew System::EventHandler(this, &Form1::cb_listenNetwork_CheckedChanged);
			// 
			// label141
			// 
			this->label141->AutoSize = true;
			this->label141->Location = System::Drawing::Point(4, 65);
			this->label141->Name = L"label141";
			this->label141->Size = System::Drawing::Size(52, 13);
			this->label141->TabIndex = 54;
			this->label141->Text = L"UDP Port";
			// 
			// tb_netport
			// 
			this->tb_netport->Enabled = false;
			this->tb_netport->Location = System::Drawing::Point(80, 64);
			this->tb_netport->Name = L"tb_netport";
			this->tb_netport->Size = System::Drawing::Size(85, 20);
			this->tb_netport->TabIndex = 53;
			this->tb_netport->Text = L"3144";
			// 
			// label139
			// 
			this->label139->AutoSize = true;
			this->label139->Location = System::Drawing::Point(4, 42);
			this->label139->Name = L"label139";
			this->label139->Size = System::Drawing::Size(58, 13);
			this->label139->TabIndex = 52;
			this->label139->Text = L"IP Address";
			// 
			// tb_netadd
			// 
			this->tb_netadd->Enabled = false;
			this->tb_netadd->Location = System::Drawing::Point(79, 39);
			this->tb_netadd->Name = L"tb_netadd";
			this->tb_netadd->Size = System::Drawing::Size(85, 20);
			this->tb_netadd->TabIndex = 51;
			this->tb_netadd->Text = L"127.0.0.1";
			// 
			// cb_BroadcastNet
			// 
			this->cb_BroadcastNet->AutoSize = true;
			this->cb_BroadcastNet->Checked = true;
			this->cb_BroadcastNet->CheckState = System::Windows::Forms::CheckState::Checked;
			this->cb_BroadcastNet->Location = System::Drawing::Point(6, 19);
			this->cb_BroadcastNet->Name = L"cb_BroadcastNet";
			this->cb_BroadcastNet->Size = System::Drawing::Size(132, 17);
			this->cb_BroadcastNet->TabIndex = 50;
			this->cb_BroadcastNet->Text = L"Broadcast Data (UDP)";
			this->cb_BroadcastNet->UseVisualStyleBackColor = true;
			this->cb_BroadcastNet->CheckedChanged += gcnew System::EventHandler(this, &Form1::cb_BroadcastNet_CheckedChanged);
			// 
			// domainUpDown1
			// 
			this->domainUpDown1->Items->Add(L"40");
			this->domainUpDown1->Items->Add(L"39");
			this->domainUpDown1->Items->Add(L"38");
			this->domainUpDown1->Items->Add(L"37");
			this->domainUpDown1->Items->Add(L"36");
			this->domainUpDown1->Items->Add(L"35");
			this->domainUpDown1->Items->Add(L"34");
			this->domainUpDown1->Items->Add(L"33");
			this->domainUpDown1->Items->Add(L"32");
			this->domainUpDown1->Items->Add(L"31");
			this->domainUpDown1->Items->Add(L"30");
			this->domainUpDown1->Items->Add(L"29");
			this->domainUpDown1->Items->Add(L"28");
			this->domainUpDown1->Items->Add(L"27");
			this->domainUpDown1->Items->Add(L"26");
			this->domainUpDown1->Items->Add(L"25");
			this->domainUpDown1->Items->Add(L"24");
			this->domainUpDown1->Items->Add(L"23");
			this->domainUpDown1->Items->Add(L"22");
			this->domainUpDown1->Items->Add(L"21");
			this->domainUpDown1->Items->Add(L"20");
			this->domainUpDown1->Items->Add(L"19");
			this->domainUpDown1->Items->Add(L"18");
			this->domainUpDown1->Items->Add(L"17");
			this->domainUpDown1->Items->Add(L"16");
			this->domainUpDown1->Items->Add(L"15");
			this->domainUpDown1->Items->Add(L"14");
			this->domainUpDown1->Items->Add(L"13");
			this->domainUpDown1->Items->Add(L"12");
			this->domainUpDown1->Items->Add(L"11");
			this->domainUpDown1->Items->Add(L"10");
			this->domainUpDown1->Items->Add(L"9");
			this->domainUpDown1->Items->Add(L"8");
			this->domainUpDown1->Items->Add(L"7");
			this->domainUpDown1->Items->Add(L"6");
			this->domainUpDown1->Items->Add(L"5");
			this->domainUpDown1->Items->Add(L"4");
			this->domainUpDown1->Items->Add(L"3");
			this->domainUpDown1->Items->Add(L"2");
			this->domainUpDown1->Items->Add(L"1");
			this->domainUpDown1->Location = System::Drawing::Point(599, 380);
			this->domainUpDown1->Name = L"domainUpDown1";
			this->domainUpDown1->Size = System::Drawing::Size(53, 20);
			this->domainUpDown1->TabIndex = 75;
			// 
			// panel4
			// 
			this->panel4->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->panel4->Controls->Add(this->label137);
			this->panel4->Controls->Add(this->cb_PPVinvert);
			this->panel4->Controls->Add(this->cb_NoPPV);
			this->panel4->Location = System::Drawing::Point(512, 14);
			this->panel4->Name = L"panel4";
			this->panel4->Size = System::Drawing::Size(153, 276);
			this->panel4->TabIndex = 81;
			// 
			// label137
			// 
			this->label137->AutoSize = true;
			this->label137->Font = (gcnew System::Drawing::Font(L"Impact", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label137->ForeColor = System::Drawing::Color::MidnightBlue;
			this->label137->Location = System::Drawing::Point(3, 4);
			this->label137->Name = L"label137";
			this->label137->Size = System::Drawing::Size(75, 18);
			this->label137->TabIndex = 64;
			this->label137->Text = L"Disruptions";
			// 
			// cb_PPVinvert
			// 
			this->cb_PPVinvert->AutoSize = true;
			this->cb_PPVinvert->Location = System::Drawing::Point(6, 50);
			this->cb_PPVinvert->Name = L"cb_PPVinvert";
			this->cb_PPVinvert->Size = System::Drawing::Size(77, 17);
			this->cb_PPVinvert->TabIndex = 64;
			this->cb_PPVinvert->Text = L"PPV Invert";
			this->cb_PPVinvert->UseVisualStyleBackColor = true;
			// 
			// cb_NoPPV
			// 
			this->cb_NoPPV->AutoSize = true;
			this->cb_NoPPV->Location = System::Drawing::Point(6, 70);
			this->cb_NoPPV->Name = L"cb_NoPPV";
			this->cb_NoPPV->Size = System::Drawing::Size(85, 17);
			this->cb_NoPPV->TabIndex = 82;
			this->cb_NoPPV->Text = L"PPV Silence";
			this->cb_NoPPV->UseVisualStyleBackColor = true;
			// 
			// groupBox11
			// 
			this->groupBox11->Controls->Add(this->cb_DBlisten);
			this->groupBox11->Controls->Add(this->cb_DBrecord);
			this->groupBox11->Location = System::Drawing::Point(6, 296);
			this->groupBox11->Name = L"groupBox11";
			this->groupBox11->Size = System::Drawing::Size(538, 58);
			this->groupBox11->TabIndex = 71;
			this->groupBox11->TabStop = false;
			this->groupBox11->Text = L"Database Interaction";
			// 
			// cb_DBlisten
			// 
			this->cb_DBlisten->AutoSize = true;
			this->cb_DBlisten->Location = System::Drawing::Point(8, 35);
			this->cb_DBlisten->Name = L"cb_DBlisten";
			this->cb_DBlisten->Size = System::Drawing::Size(84, 17);
			this->cb_DBlisten->TabIndex = 1;
			this->cb_DBlisten->Text = L"Listen to DB";
			this->cb_DBlisten->UseVisualStyleBackColor = true;
			// 
			// cb_DBrecord
			// 
			this->cb_DBrecord->AutoSize = true;
			this->cb_DBrecord->Location = System::Drawing::Point(8, 19);
			this->cb_DBrecord->Name = L"cb_DBrecord";
			this->cb_DBrecord->Size = System::Drawing::Size(91, 17);
			this->cb_DBrecord->TabIndex = 0;
			this->cb_DBrecord->Text = L"Record to DB";
			this->cb_DBrecord->UseVisualStyleBackColor = true;
			this->cb_DBrecord->CheckedChanged += gcnew System::EventHandler(this, &Form1::cb_DBrecord_CheckedChanged);
			// 
			// groupBox15
			// 
			this->groupBox15->Controls->Add(this->textBox_prefix);
			this->groupBox15->Controls->Add(this->pictureBox3);
			this->groupBox15->Location = System::Drawing::Point(3, 7);
			this->groupBox15->Name = L"groupBox15";
			this->groupBox15->Size = System::Drawing::Size(174, 50);
			this->groupBox15->TabIndex = 80;
			this->groupBox15->TabStop = false;
			this->groupBox15->Text = L"Patient ID";
			// 
			// textBox_prefix
			// 
			this->textBox_prefix->Location = System::Drawing::Point(6, 17);
			this->textBox_prefix->Name = L"textBox_prefix";
			this->textBox_prefix->Size = System::Drawing::Size(134, 20);
			this->textBox_prefix->TabIndex = 2;
			// 
			// pictureBox3
			// 
			this->pictureBox3->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox3.Image")));
			this->pictureBox3->Location = System::Drawing::Point(146, 17);
			this->pictureBox3->Name = L"pictureBox3";
			this->pictureBox3->Size = System::Drawing::Size(20, 20);
			this->pictureBox3->TabIndex = 74;
			this->pictureBox3->TabStop = false;
			this->pictureBox3->Click += gcnew System::EventHandler(this, &Form1::pictureBox3_Click_1);
			// 
			// groupBox14
			// 
			this->groupBox14->Controls->Add(this->CB_speed_real);
			this->groupBox14->Controls->Add(this->CB_speed_turbo);
			this->groupBox14->Location = System::Drawing::Point(3, 57);
			this->groupBox14->Name = L"groupBox14";
			this->groupBox14->Size = System::Drawing::Size(174, 58);
			this->groupBox14->TabIndex = 74;
			this->groupBox14->TabStop = false;
			this->groupBox14->Text = L"Speed";
			// 
			// label_savefile2
			// 
			this->label_savefile2->AutoSize = true;
			this->label_savefile2->Location = System::Drawing::Point(9, 428);
			this->label_savefile2->Name = L"label_savefile2";
			this->label_savefile2->Size = System::Drawing::Size(48, 13);
			this->label_savefile2->TabIndex = 79;
			this->label_savefile2->Text = L"SaveFile";
			this->label_savefile2->Visible = false;
			// 
			// label_savefile
			// 
			this->label_savefile->AutoSize = true;
			this->label_savefile->Location = System::Drawing::Point(9, 443);
			this->label_savefile->Name = L"label_savefile";
			this->label_savefile->Size = System::Drawing::Size(48, 13);
			this->label_savefile->TabIndex = 75;
			this->label_savefile->Text = L"SaveFile";
			this->label_savefile->Visible = false;
			// 
			// groupBox13
			// 
			this->groupBox13->Controls->Add(this->button_chooseScript);
			this->groupBox13->Controls->Add(this->textBox_script);
			this->groupBox13->Location = System::Drawing::Point(84, 416);
			this->groupBox13->Name = L"groupBox13";
			this->groupBox13->Size = System::Drawing::Size(527, 44);
			this->groupBox13->TabIndex = 73;
			this->groupBox13->TabStop = false;
			this->groupBox13->Text = L"THIS IS DISABLED";
			this->groupBox13->Visible = false;
			// 
			// button_chooseScript
			// 
			this->button_chooseScript->Enabled = false;
			this->button_chooseScript->Location = System::Drawing::Point(450, 14);
			this->button_chooseScript->Name = L"button_chooseScript";
			this->button_chooseScript->Size = System::Drawing::Size(51, 20);
			this->button_chooseScript->TabIndex = 74;
			this->button_chooseScript->Text = L"Select";
			this->button_chooseScript->UseVisualStyleBackColor = true;
			this->button_chooseScript->Click += gcnew System::EventHandler(this, &Form1::button_chooseScript_Click);
			// 
			// textBox_script
			// 
			this->textBox_script->Enabled = false;
			this->textBox_script->Location = System::Drawing::Point(97, 15);
			this->textBox_script->Name = L"textBox_script";
			this->textBox_script->Size = System::Drawing::Size(347, 20);
			this->textBox_script->TabIndex = 74;
			this->textBox_script->Text = L"This is autopopulated from the ComboBox!";
			// 
			// groupBox10
			// 
			this->groupBox10->Controls->Add(this->pictureBox1);
			this->groupBox10->Controls->Add(this->checkBox4);
			this->groupBox10->Controls->Add(this->textBox6);
			this->groupBox10->Controls->Add(this->button14);
			this->groupBox10->Location = System::Drawing::Point(6, 360);
			this->groupBox10->Name = L"groupBox10";
			this->groupBox10->Size = System::Drawing::Size(538, 51);
			this->groupBox10->TabIndex = 70;
			this->groupBox10->TabStop = false;
			this->groupBox10->Text = L"Logging";
			// 
			// pictureBox1
			// 
			this->pictureBox1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox1.Image")));
			this->pictureBox1->Location = System::Drawing::Point(507, 18);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(20, 20);
			this->pictureBox1->TabIndex = 73;
			this->pictureBox1->TabStop = false;
			this->pictureBox1->Click += gcnew System::EventHandler(this, &Form1::pictureBox1_Click);
			// 
			// checkBox4
			// 
			this->checkBox4->AutoSize = true;
			this->checkBox4->Checked = true;
			this->checkBox4->CheckState = System::Windows::Forms::CheckState::Checked;
			this->checkBox4->Location = System::Drawing::Point(11, 21);
			this->checkBox4->Name = L"checkBox4";
			this->checkBox4->Size = System::Drawing::Size(80, 17);
			this->checkBox4->TabIndex = 69;
			this->checkBox4->Text = L"Enable Log";
			this->checkBox4->UseVisualStyleBackColor = true;
			this->checkBox4->CheckedChanged += gcnew System::EventHandler(this, &Form1::checkBox4_CheckedChanged);
			// 
			// textBox6
			// 
			this->textBox6->Location = System::Drawing::Point(97, 19);
			this->textBox6->Name = L"textBox6";
			this->textBox6->Size = System::Drawing::Size(347, 20);
			this->textBox6->TabIndex = 66;
			this->textBox6->Text = L"subjectX";
			this->textBox6->TextChanged += gcnew System::EventHandler(this, &Form1::textBox6_TextChanged);
			// 
			// button14
			// 
			this->button14->Enabled = false;
			this->button14->Location = System::Drawing::Point(450, 18);
			this->button14->Name = L"button14";
			this->button14->Size = System::Drawing::Size(51, 20);
			this->button14->TabIndex = 68;
			this->button14->Text = L"Select";
			this->button14->UseVisualStyleBackColor = true;
			this->button14->Click += gcnew System::EventHandler(this, &Form1::button14_Click);
			// 
			// timerCrossfade
			// 
			this->timerCrossfade->Interval = 500;
			this->timerCrossfade->Tick += gcnew System::EventHandler(this, &Form1::timerCrossfade_Tick);
			// 
			// timerDisplayVariability
			// 
			this->timerDisplayVariability->Interval = 2000;
			this->timerDisplayVariability->Tick += gcnew System::EventHandler(this, &Form1::timerDisplayVariability_Tick);
			// 
			// timer_ControlWatcher
			// 
			this->timer_ControlWatcher->Interval = 1000;
			this->timer_ControlWatcher->Tick += gcnew System::EventHandler(this, &Form1::timer_ControlWatcher_Tick);
			// 
			// fileSystemWatcher1
			// 
			this->fileSystemWatcher1->EnableRaisingEvents = true;
			this->fileSystemWatcher1->SynchronizingObject = this;
			// 
			// Form1
			// 
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::None;
			this->BackColor = System::Drawing::SystemColors::ActiveCaption;
			this->ClientSize = System::Drawing::Size(1005, 542);
			this->Controls->Add(this->tabControl1);
			this->Controls->Add(this->panel13);
			this->Controls->Add(this->statusStrip1);
			this->Controls->Add(this->menuStrip1);
			this->Controls->Add(this->panel2);
			this->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 11, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Pixel,
				static_cast<System::Byte>(0)));
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"Form1";
			this->Text = L"CDSIM";
			this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
			this->groupBox1->ResumeLayout(false);
			this->panel2->ResumeLayout(false);
			this->panel2->PerformLayout();
			this->panel3->ResumeLayout(false);
			this->panel3->PerformLayout();
			this->statusStrip1->ResumeLayout(false);
			this->statusStrip1->PerformLayout();
			this->panel13->ResumeLayout(false);
			this->panel13->PerformLayout();
			this->groupBox8->ResumeLayout(false);
			this->groupBox8->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBar_ART))->EndInit();
			this->groupBox4->ResumeLayout(false);
			this->groupBox4->PerformLayout();
			this->groupBox3->ResumeLayout(false);
			this->groupBox3->PerformLayout();
			this->panel12->ResumeLayout(false);
			this->panel12->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->EndInit();
			this->groupBox12->ResumeLayout(false);
			this->panel5->ResumeLayout(false);
			this->panel5->PerformLayout();
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->panel6->ResumeLayout(false);
			this->panel6->PerformLayout();
			this->panel7->ResumeLayout(false);
			this->panel7->PerformLayout();
			this->panel1->ResumeLayout(false);
			this->panel1->PerformLayout();
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->tabControl1->ResumeLayout(false);
			this->tabPage1->ResumeLayout(false);
			this->groupBox17->ResumeLayout(false);
			this->groupBox17->PerformLayout();
			this->tabPage2->ResumeLayout(false);
			this->tabPage2->PerformLayout();
			this->groupBox9->ResumeLayout(false);
			this->groupBox9->PerformLayout();
			this->groupBox7->ResumeLayout(false);
			this->groupBox6->ResumeLayout(false);
			this->groupBox6->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBar_Preload))->EndInit();
			this->groupBox5->ResumeLayout(false);
			this->groupBox5->PerformLayout();
			this->tabPage3->ResumeLayout(false);
			this->tabPage3->PerformLayout();
			this->groupBox19->ResumeLayout(false);
			this->groupBox19->PerformLayout();
			this->groupBox18->ResumeLayout(false);
			this->groupBox18->PerformLayout();
			this->groupBox16->ResumeLayout(false);
			this->groupBox16->PerformLayout();
			this->tabPage4->ResumeLayout(false);
			this->tabPage4->PerformLayout();
			this->groupBox20->ResumeLayout(false);
			this->groupBox20->PerformLayout();
			this->panel4->ResumeLayout(false);
			this->panel4->PerformLayout();
			this->groupBox11->ResumeLayout(false);
			this->groupBox11->PerformLayout();
			this->groupBox15->ResumeLayout(false);
			this->groupBox15->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox3))->EndInit();
			this->groupBox14->ResumeLayout(false);
			this->groupBox14->PerformLayout();
			this->groupBox13->ResumeLayout(false);
			this->groupBox13->PerformLayout();
			this->groupBox10->ResumeLayout(false);
			this->groupBox10->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->fileSystemWatcher1))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

private: LIR::ControlResponse^ CheckControlTable(String^ Item)
		 {
			 LIR::ControlResponse^ C = gcnew LIR::ControlResponse;
			C->Item = Item;
			C->Value = 0;
  			C->CharValue = "";
			System::Data::Odbc::OdbcConnection^ TmeConnection = gcnew System::Data::Odbc::OdbcConnection;
			TmeConnection->ConnectionString = "DRIVER={MySQL ODBC 5.1 Driver}; Server=localhost; option=3; Connection Timeout=5; User=lir; password=lir; Database=lir;";

			try {
 		 		 Odbc::OdbcCommand ^cmd = gcnew Odbc::OdbcCommand();
				 cmd->Connection = TmeConnection;
				 TmeConnection->Open();

				 cmd->CommandType = CommandType::Text;
				 cmd->CommandText = "select * from control where Item='"+Item+"'"; 

				 System::Data::Odbc::OdbcDataReader^ reader;
				 reader = cmd->ExecuteReader();
				 if (reader->Read()) 
				 {
					 C->Item = Item;
					 C->Value = reader->GetInt16(1);
					 C->CharValue = reader->GetString(2);
				 }
				 TmeConnection->Close();
				 return C;
			}
			catch (Exception^ e) {
				
				try {TmeConnection->Close();} catch (Exception^ e){e;}
				e;
				//System::Windows::Forms::MessageBox::Show("FluidGiven Check: " + e->Message);
				return C;
			}
		 }


	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
				ToggleSimOnOff(true);
			 }
	private: System::Void StartSim(void) {
				 //
				 // ON ON ON ON 
				 //
				 // Error checking before starting simulator
				 if (this->checkBox4->Checked == true)
				 {
					 if (this->textBox6->Text->CompareTo("") == 0)
					 {
						 MessageBox::Show("You must specify a filename to enable simulator logging.");
						 return;
					 }
					 try 
					 {
						 String^ R = "";
						 if (this->comboBox_SCRIPTS->SelectedIndex!=0)
						 {
							 R = this->domainUpDown1->SelectedItem->ToString();
							 while (R->Length < 4) {R = "0" + R;}
						 }
						 this->label_savefile->Text = this->textBox6->Text + "_" + this->comboBox_SCRIPTS->Text + "_" + DateTime::Now.Hour.ToString() + "" + DateTime::Now.Minute.ToString() + "" + DateTime::Now.Second.ToString() + "_sim.txt";
						 this->label_savefile2->Text = this->textBox6->Text + "_" + this->comboBox_SCRIPTS->Text + "_" + DateTime::Now.Hour.ToString() + "" + DateTime::Now.Minute.ToString() + "" + DateTime::Now.Second.ToString() + "_actions.txt";
						 IO::StreamWriter^ TFILE = gcnew IO::StreamWriter(this->label_savefile->Text,1);
						 TFILE->Close();
						 TFILE = gcnew IO::StreamWriter(this->label_savefile2->Text,1);
						 TFILE->Close();
						 this->Action_log_flag = 1;
					 }
					 catch(Exception^ e) 
					 {
						 e;
						 MessageBox::Show("Unable to create file for simulator logging: " + e->ToString());
						return; 
					 }
				 }

				 if (this->comboBox_SCRIPTS->SelectedIndex!=0)
				 {
					 //if (this->textBox_script->Text->CompareTo("") == 0)
					 //{
					 //	 MessageBox::Show("You must specify a script file to use simulator scripting");
					 //	 return;
					 //}
					 try 
					 {
						 IO::StreamReader^ FILE = gcnew IO::StreamReader(this->textBox_script->Text);
						 FILE->Close();
					 }
					 catch (Exception^ e)
					 {
						e;
						MessageBox::Show("Unable to open script file; please check the path and filename, confirm that the file is not already in use, and try again.");
						return;
					 }
				 }

				 if (this->CB_speed_turbo->Checked)
				 {
					 this->TIMER_SIM->Interval = 100;
				 }
				 else
				 {
					 this->TIMER_SIM->Interval = 1000;
				 }

				 if (this->cb_BroadcastNet->Checked) 
				 {
					 // Create the client; don't care what local port we start from.
					 this->BroadcastSocket = gcnew System::Net::Sockets::UdpClient();
					 ////edit by Pei and Shehzi
					 // TCP client is accepted only once - and it can start at anytime - better to put the statement below in the sim time loop
					 /*try{
						 this->send_data_thread = gcnew System::Threading::Thread(gcnew System::Threading::ThreadStart(this, &Form1::TCP_send_message));
						 this->send_data_thread->Start();
					 }
					 catch (Exception^ e){
						 System::Windows::Forms::MessageBox::Show(" Multi-Thread error", e->ToString());
						 return;
					 }*/
					 //this->TCP_listen->BeginAcceptTcpClient(gcnew AsyncCallback(this, &Form1::TCP_callback), TCP_listen);
					 try{
						 //this->TCP_listen = gcnew System::Net::Sockets::TcpListener(System::Net::IPAddress::Parse(this->tb_netadd->Text), int::Parse(this->tb_netport_tcp->Text));
						 // TCP is wierd in windows it does not bind to a specific IP address - therefore make sure it is always 0.0.0.0, the port is important
						 //this->TCP_listen = gcnew System::Net::Sockets::TcpListener(System::Net::IPAddress::Parse("0.0.0.0"), int::Parse(this->tb_netport_tcp->Text));
						 this->TCP_listen->Start();
						 try{
						 	this->TCP_listen->BeginAcceptTcpClient(gcnew AsyncCallback(this, &Form1::TCP_callback), TCP_listen);
						 }
						 catch (Exception^ e){
						 	System::Windows::Forms::MessageBox::Show("TCP Broke here 1");
						 }
						 //this->TCP_listen2->Start();
						 // The line below needs to be run in a loop
						 //TCP_listen->BeginAcceptTcpClient(gcnew AsyncCallback(this, &Form1::TCP_callback), TCP_listen);
					 }
					 catch (Exception^ e){
						 System::Windows::Forms::MessageBox::Show(" TCP Server Start error", e->ToString());
						 return;
					 }


					 //this->TcpSocket = gcnew System::Net::Sockets::TcpClient();
					 ////edit by Pei and Shehzi
					 try 
					 {
						 this->BroadcastSocket->Connect(System::Net::IPAddress::Parse(this->tb_netadd->Text),int::Parse(this->tb_netport->Text));
					 }
					catch (System::Exception^ e)
					{
						e;
						System::Windows::Forms::MessageBox::Show("Unable to connect to remote address & port '"+tb_netadd->Text+":"+tb_netport->Text+"'");
						return;
					}
				 }

				 if (this->cb_listenNetwork->Checked)
				 {
					 try 
					 {
						 // Silently try to close this port if it's already open
						 try {
							 ListenSocket->Close();
						 } catch (System::Exception^ e) {e;}
						 this->ListenSocket = gcnew System::Net::Sockets::Socket(System::Net::Sockets::AddressFamily::InterNetwork,System::Net::Sockets::SocketType::Dgram,System::Net::Sockets::ProtocolType::Udp);
						 System::Net::IPEndPoint^ localIP = gcnew System::Net::IPEndPoint(System::Net::IPAddress::Parse("127.0.0.1"),int::Parse(this->tb_listenPort->Text));
						 ListenSocket->Bind(localIP);
					 }
					 catch (System::Exception^ e)
					 {
						e;
						System::Windows::Forms::MessageBox::Show("Unable to open listen port "+tb_listenPort->Text);
						return;
					 }
				 }

				 // Okay, we're committed to starting now
				 //this->checkBox_UseScript->Enabled = false;

				 this->FluidGivenByLirThisBolus = 0;
				 this->LirBolusID = 0;

				 // If the DB record mode is on, reset the clocks
				 // (all clocks work with update queries; if the row isn't there
				 // the updates will fail, so we want to be sure the row exists)
				 if (this->cb_DBrecord->Checked == true)
				 {
					 this->SingleQuery("delete from control");
					 this->SingleQuery("delete from vitals");
					 this->SingleQuery("delete from clock");
					 this->SingleQuery("insert into clock values(1,'00:00:00')");
					 this->SingleQuery("delete from fluidio");
					 this->SingleQuery("insert into fluidio values('FluidGiven',0)");
					 if (this->CB_speed_turbo->Checked)
					 {
						this->SingleQuery("insert into control values('turbo',1,'')");
					 }
				 }
				 
				 this->WMon->ClearGraphs();
				 //Reset fluid counts
				 WMon->TCr = 0;
				 WMon->TBl = 0;
				 WMon->TCo = 0;
				 WMon->AddTotalBlood(0);
				 WMon->AddTotalColloid(0);
				 WMon->AddTotalCrystal(0);
				 //Remove all drugs
				 for (int k = this->WMon->GetDrugCount() - 1; k >= 0; k = k - 1){
					 WMon->DiscontinueDrugInRow(k);
				 }

				 this->button1->Text = "ON";
				 this->button1->BackColor::set(System::Drawing::Color::Green);

				 //this->SingleQuery("delete from lir_meds");

				 this->tabPage1->Enabled = false;
				 this->tabPage4->Enabled = false;
				 this->tabPage2->Enabled = false;

				 // Create the patient
				 Patient = gcnew Simulator::CDSimPatient(
					 double::Parse(tb_baseSBP->Text),double::Parse(tb_maxSBP->Text),
					 double::Parse(tb_baseDBP->Text),double::Parse(tb_maxDBP->Text),
					 double::Parse(tb_baseHR->Text),double::Parse(tb_maxHR->Text),
					 double::Parse(tb_baseKG->Text),double::Parse(tb_maxKg->Text),
					 double::Parse(tb_baseIn->Text),double::Parse(tb_maxIn->Text),
					 double::Parse(tb_baseCVP->Text),double::Parse(tb_maxCVP->Text),
					 double::Parse(tb_lowHgb->Text),double::Parse(tb_highHgb->Text),
					 double::Parse(tb_BVmin->Text),double::Parse(tb_BVmax->Text),
					 double::Parse(tb_CNTmin->Text),double::Parse(tb_CNTmax->Text),
					 double::Parse(tb_baseSepF->Text),double::Parse(tb_maxSepF->Text)
					 );
				 // Get a new ID for this patient
				 String^ code = "";
				 if (this->textBox_prefix->Text->Length > 0)
				 {
					 if (this->comboBox_SCRIPTS->SelectedIndex!=0) 
					 {
						 code = this->domainUpDown1->SelectedItem->ToString();
						 while (code->Length < 4) {code = "0" + code;}
					 }
					 this->PatientID = this->textBox_prefix->Text + code;
				 }
				 else 
				 {
					 this->PatientID = "Unspecified";
				 }
				 if (this->cb_DBrecord->Checked == true)
				 {
					this->SingleQuery("insert into control values('PatientID',0,'"+PatientID+"')");
					this->SingleQuery("insert into control values('PatientWeight',"+Patient->Base->Kg.ToString()+",'')");
				 }			 

				 // Load script if needed
				 if (this->comboBox_SCRIPTS->SelectedIndex!=0)
				 {
					 if (!(Patient->LoadSimScript(this->textBox_script->Text)))
					 {
						 Windows::Forms::MessageBox::Show("Sim Script parsing failed; file is not in the correct format.");
					 }
				 }
				 // Set stability & flux
				 Patient->SetPatientStability(double::Parse(this->tb_stability->Text),double::Parse(this->tb_Flux->Text));
				 
				 // Set PPV settings
				 double PPVWandR = Rx->NextDouble()*(double::Parse(this->tb_PPVWandFreqMax->Text) - double::Parse(this->tb_PPVWandFreq->Text));
				 PPVWandR += double::Parse(this->tb_PPVWandFreqMax->Text);
				 double PPVWand = Rx->NextDouble()*(double::Parse(this->tb_PPVWandMax->Text) - double::Parse(this->tb_PPVWand->Text));
				 PPVWand += double::Parse(this->tb_PPVWand->Text);
				 double PPVbias = Rx->NextDouble()*(double::Parse(this->tb_PPVbiasMax->Text) - double::Parse(this->tb_PPVbias->Text));
				 PPVbias += double::Parse(this->tb_PPVbias->Text);
				 Patient->SetPPVBehavior(PPVWand,PPVWandR,PPVbias,this->cb_NoPPV->Checked,this->cb_PPVinvert->Checked);
				 
				 // Set SV settings
				 double SVerr = Rx->NextDouble()*(double::Parse(this->tb_SVWandMax->Text) - double::Parse(this->tb_SVWand->Text));
				 SVerr += double::Parse(this->tb_SVWand->Text);
				 double SVD = Rx->NextDouble()*(double::Parse(this->tb_SVDsecMax->Text) - double::Parse(this->tb_SVDsec->Text));
				 SVD += double::Parse(this->tb_SVDsec->Text);
				 Patient->SetSVBehavior(SVerr,SVD);

				 
				 // Set the wavemonitor VVar
				 this->WMon->SetVVariability(Int16::Parse(this->tb_VVar->Text));
				 // Start Logger if selected
				 if (this->checkBox4->Checked == true)
				 {
					 Patient->StartLogging(this->label_savefile->Text);
				 }	
				 // Cycle once to initiate
				 Patient->Cycle();

				 // Start sim timer - but delay start for 5 seconds - value in milliseconds
				 System::Threading::Thread::Sleep(10000);
				 this->TIMER_SIM->Start();

				 if (this->cb_MonitorOn->Checked==true)
				 {
					 // Start the wave monitor running
					 WMon->MonitorOn();
					 // Update the slots real quick
					 Simulator::VitalsPack^ V = gcnew Simulator::VitalsPack();
					 this->UpdateMonitorSlots(V,1,1);
				 }
			}

	private: System::Void ToggleSimOnOff(bool ButtonWasPushed) {
				 // See if the button says "ON" and the user pushed it
				 if ((ButtonWasPushed)&&(this->button1->Text->CompareTo("ON")==0))
				 {
					//Shut down the system
					this->StopSim(true);
					return;
				 }
				 if (this->button1->Text->CompareTo("ON") != 0)
				 {
					 this->StartSim();
				 }
				 else 
				 {
					this->StopSim(false);
				 }
			 }
	private: System::Void StopSim(bool Force) {

			 // 

			 this->button1->Text = "OFF";
			 this->button1->BackColor::set(System::Drawing::Color::LightCoral);
			 this->button1->Update();

			 // Tick once more
			 //this->TIMER_SIM_Tick(this,System::EventArgs::Empty);
			 // Turn off simulator ticker
			 this->TIMER_SIM->Stop();
			 // Turn off monitor
			 WMon->MonitorStandby();
			 WMon->timer_pacing->Stop();
			 //Reset fluid counts
			 WMon->TCr = 0;
			 WMon->TBl = 0;
			 WMon->TCo = 0;
			 WMon->AddTotalBlood(0);
			 WMon->AddTotalColloid(0);
			 WMon->AddTotalCrystal(0);
			 //Remove all drugs
			 for (int k = this->WMon->GetDrugCount() - 1; k >= 0; k = k - 1){
				 WMon->DiscontinueDrugInRow(k);
			 }
			 //Simulator::VitalsPack^ V = gcnew Simulator::VitalsPack();
			 //WMon->ClearGraphs();
			 //this->UpdateMonitorSlots(V,true,true);

			 if (this->checkBox4->Checked) 
			 {	
				 if (!System::IO::File::Exists("SimSummaries.txt"))
				 {
					 this->Patient->WriteSummaryHeaderToFile("SimSummaries.txt");
				 }
				 this->Patient->WriteSummaryToFile("SimSummaries.txt",this->PatientID);
			 }

			 // Before we reactivate all the controls...
			 // Are we set to loop?  If so, do it
			 if ((Force==false)&&(this->comboBox_SCRIPTS->SelectedIndex!=0))
			 {
				 if (int::Parse(this->domainUpDown1->SelectedItem->ToString()) > 1)
				 {
					 this->SingleQuery("delete from control");
					// Update the domain (the cycle counter)
					 this->domainUpDown1->SelectedIndex = this->domainUpDown1->SelectedIndex+1;
					 // Alert the database that we're cycling:
					 this->SingleQuery("Insert into control values('NewPatient',1,'')");	

					 // Kill this patient
					 delete this->Patient;

					 this->button1->Text = "Wait";
					 this->button1->BackColor::set(System::Drawing::Color::Yellow);
					 this->button1->Update();

					 // Start the watch timer waiting for the DB response
					 this->timer_ControlWatcher->Start();
					 
					 return;
				 }
				 // One more check here; if we got here we're using scripting
				 // See if we're recording to DB
				 if (this->cb_DBrecord->Checked)
				 {
					 this->SingleQuery("delete from control");
					 // The NewPatient tag is the first control tag looked for by LIR
					 this->SingleQuery("Insert into control values('NewPatient',1,'')");
					 // Finiding NewPatient will cause it to look for a NewID or and END
					 this->SingleQuery("insert into control values('END',1,'')");
					 // And now proceed to shut down the system
				 }
			 }

			 if (this->cb_BroadcastNet->Checked) 
 			 {
				 this->BroadcastSocket->Close();
				 //edit by Pei and Shehzi
				 /*this->TcpReader->Close();
				 this->TcpWriter->Close();*/
				 try{
					 
					 if (this->handler != nullptr && this->handler->Connected){
						 this->handler->Shutdown( SocketShutdown::Both );
						 this->handler->Close();
					 }
					 this->TCP_listen->Stop();
					 //System::Windows::Forms::MessageBox::Show("Successfuly closed TCP port");
				 }
				 catch (System::Exception^ e){
					 System::Windows::Forms::MessageBox::Show("Unable to close TCP port properly");
				 }
				 //this->TCP_listen2->Stop();
				 //this->send_data_thread->Join();
				 //this->TcpSocket->Close();
				 //edit by Pei and Shehzi
			 }
			 if (this->cb_listenNetwork->Checked)
			 {
				 this->ListenSocket->Close();
			 }

			 this->tabPage1->Enabled = true;
			 this->tabPage4->Enabled = true;
			 this->tabPage2->Enabled = true;				 
			 }

	private: System::String^ SingleQuery(System::String^ Query) {
		 System::Data::Odbc::OdbcConnection ^TmeConnection = gcnew System::Data::Odbc::OdbcConnection;
		 //SqlConnection ^TmeConnection = gcnew SqlConnection();
		 //TmeConnection->ConnectionString = "Data source=MYSQL;database=lir;password=lir;uid=lir";
		 TmeConnection->ConnectionString = "DRIVER={MySQL ODBC 5.1 Driver}; Server=localhost; option=3; Connection Timeout=5; User=lir; password=lir; Database=lir;";
		 
		 //Data Source=127.0.0.1; Initial Catalog=lir";

		try {
			Odbc::OdbcCommand ^cmd = gcnew Odbc::OdbcCommand();
			 //SqlCommand ^cmd = gcnew SqlCommand();
			 cmd->Connection = TmeConnection;
			 TmeConnection->Open();

			 cmd->CommandType = CommandType::Text;
			 cmd->CommandText = Query;
			 int affected = cmd->ExecuteNonQuery();
			 //System::Windows::Forms::MessageBox::Show(affected.ToString());
			}
		catch (Exception^ e) {
			e;
			//System::Windows::Forms::MessageBox::Show("Single Query Exception: " + e->Message + " for command '" + Query +"'");
			try {TmeConnection->Close();} catch (Exception^ e){e;}
			return e->ToString();
			}
		finally {
			TmeConnection->Close();
			delete TmeConnection;
			}
		return "";
		}		

 /*
	private: System::Void button_RESET_DB_Click(System::Object^  sender, System::EventArgs^  e) {
		 //this->SingleQuery("delete from display");
		 this->SingleQuery("delete from lir_vitalslog");
		 this->SingleQuery("delete from lir_fluidresults");
		 this->SingleQuery("delete from lir_fluids");
		 //this->SingleQuery("delete from lir_fluidsavailable");
		 this->SingleQuery("delete from log_full");
		 this->SingleQuery("delete from lir_meta");
		 this->SingleQuery("delete from log");
		 this->SingleQuery("delete from vitals");
		 //System::Windows::Forms::MessageBox::Show("All history information deleted.");
		 }

*/
private: System::Void button6_Click(System::Object^  sender, System::EventArgs^  e) {
			 LIR::About^ AB = gcnew LIR::About();
			 AB->Show();
		 }
private: System::Void ToggleTurbo(void) {
			 if (this->CB_speed_turbo->Checked==true)
			 {
				 this->TIMER_SIM->Interval = 1;
			 }
			 else
			 {
				this->TIMER_SIM->Interval = 1000;
			 }
		 }
private: System::Void CB_speed_real_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 if (this->CB_speed_real->Checked == true) {
				 this->CB_speed_turbo->Checked = false;
			 }
			 else {
				 this->CB_speed_turbo->Checked = true;
			 }
			 ToggleTurbo();
		 }
private: System::Void CB_speed_turbo_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			if (this->CB_speed_turbo->Checked == true) {
				 this->CB_speed_real->Checked = false;
			 }
			 else {
				 this->CB_speed_real->Checked = true;
			 }
			 ToggleTurbo();
		 }


private: System::Void UpdateMonitorSlots(Simulator::VitalsPack^ V,bool updateNIBP, bool updateGraphs)
		 {
			for (int j=1; j<=6; j++)
			{
				if (WMon->getSlotId(j)->CompareTo("CO") == 0)
				{
					String^ S = (double(int(10*V->CO))/10).ToString();
					if (S->Length<3) {S += ".0";}
					WMon->UpdateVitalSlot(j,S);
				}
				else if (WMon->getSlotId(j)->CompareTo("CVP") == 0)
				{
					WMon->UpdateVitalSlot(j,int(V->CVP).ToString());
				}
				else if (WMon->getSlotId(j)->CompareTo("PA") == 0)
				{
					WMon->UpdateVitalSlot(j,int(V->PA_SP).ToString() + "/" + int(V->PA_DP).ToString());
				}
				else if (WMon->getSlotId(j)->CompareTo("CI") == 0)
				{
					String^ S = (double(int(10*V->CI))/10).ToString();
					if (S->Length<3) {S += ".0";}
					WMon->UpdateVitalSlot(j,S);
				}
				else if (WMon->getSlotId(j)->CompareTo("SV") == 0)
				{
					String^ S = int(V->SV).ToString();// = int((V->CO*1000)/V->HR).ToString();
					WMon->UpdateVitalSlot(j,S);
				}
				else if (WMon->getSlotId(j)->CompareTo("SVV") == 0)
				{
					WMon->UpdateVitalSlot(j,int(V->SVV).ToString());
				}
				else if (WMon->getSlotId(j)->CompareTo("PPV") == 0)
				{
					WMon->UpdateVitalSlot(j,int(V->PPV).ToString());
				}
				else if (WMon->getSlotId(j)->CompareTo("RR") == 0)
				{
					WMon->UpdateVitalSlot(j,int(V->RR).ToString());
				}
				else if (WMon->getSlotId(j)->CompareTo("PVI") == 0)
				{
					WMon->UpdateVitalSlot(j,int(V->PVI).ToString());
				}
				else if (WMon->getSlotId(j)->CompareTo("SpHb") == 0)
				{
					WMon->UpdateVitalSlot(j,(double(int((V->HgB + V->SpHbx)*10))/10).ToString());
				}
				else if (WMon->getSlotId(j)->CompareTo("Hgb") == 0)
				{
					WMon->UpdateVitalSlot(j,(double(int((V->HgB)*10))/10).ToString());
				}
				else if (WMon->getSlotId(j)->CompareTo("Doppler FTc") == 0)
				{
					WMon->UpdateVitalSlot(j,(double(int(V->FTc*10))/10).ToString());
				}
				else if (WMon->getSlotId(j)->CompareTo("SvO2") == 0)
				{
					WMon->UpdateVitalSlot(j,int(V->SvO2).ToString());
				}
				else if (WMon->getSlotId(j)->CompareTo("SCvO2") == 0)
				{
					WMon->UpdateVitalSlot(j,int(V->SCvO2).ToString());
				}
				else if (WMon->getSlotId(j)->CompareTo("Temp") == 0)
				{
					String^ S = (double(int(V->CoreTemp*10))/10).ToString();
					if (S->Length < 3) {S += ".0";}
					WMon->UpdateVitalSlot(j,S);
				}
				else if ((WMon->getSlotId(j)->CompareTo("NIBP") == 0)&&(updateNIBP==true))
				{
					WMon->UpdateVitalSlot(j,int(V->SBP).ToString() + "/" + int(V->DBP).ToString());
				}
				else if ((WMon->getSlotId(j)->CompareTo("MAP") == 0)&&(updateNIBP==true))
				{
					WMon->UpdateVitalSlot(j,"(" + int((V->SBP+V->DBP*2)/3).ToString() + ")");
				}
				else if (WMon->getSlotId(j)->CompareTo("SVR") == 0)
				{
					WMon->UpdateVitalSlot(j,int(V->SVR).ToString());
				}
			}
			if (updateGraphs)
			{
				for (int j=5; j<=6; j++)
				{
					if  (
						(this->checkBoxGraph1->Checked)&&(j==5) 
						||
						(this->checkBoxGraph2->Checked)&&(j==6)
						)
					{
						Graph::Graphspace^ G;
						if (j==5) {G = WMon->Graph1;}
						if (j==6) {G = WMon->Graph2;}

						if (WMon->getSlotId(j)->CompareTo("CO") == 0)
						{
							G->AddValue(V->CO);
						}
						else if (WMon->getSlotId(j)->CompareTo("CVP") == 0)
						{
							G->AddValue(V->CVP);
						}
						else if (WMon->getSlotId(j)->CompareTo("PA") == 0)
						{
							G->AddValue(V->PA_SP);
						}
						else if (WMon->getSlotId(j)->CompareTo("CI") == 0)
						{
							G->AddValue(V->CI);
						}
						else if (WMon->getSlotId(j)->CompareTo("SV") == 0)
						{
							double S = V->SV;//(V->CO*1000)/V->HR;
							G->AddValue(S);
						}
						else if (WMon->getSlotId(j)->CompareTo("SVV") == 0)
						{
							G->AddValue(V->SVV);
						}
						else if (WMon->getSlotId(j)->CompareTo("PPV") == 0)
						{
							G->AddValue(V->PPV);
						}
						else if (WMon->getSlotId(j)->CompareTo("RR") == 0)
						{
							G->AddValue(V->RR);
						}
						else if (WMon->getSlotId(j)->CompareTo("PVI") == 0)
						{
							G->AddValue(V->PVI);
						}
						else if (WMon->getSlotId(j)->CompareTo("SpHb") == 0)
						{
							G->AddValue((double(int((V->HgB + V->SpHbx)*10))/10));
						}
						else if (WMon->getSlotId(j)->CompareTo("Hgb") == 0)
						{
							G->AddValue((double(int((V->HgB)*10))/10));
						}
						else if (WMon->getSlotId(j)->CompareTo("Doppler FTc") == 0)
						{
							G->AddValue(V->FTc);
						}
						else if (WMon->getSlotId(j)->CompareTo("SvO2") == 0)
						{
							G->AddValue(V->SvO2);
						}
						else if (WMon->getSlotId(j)->CompareTo("SCvO2") == 0)
						{
							G->AddValue(V->SCvO2);
						}
						else if (WMon->getSlotId(j)->CompareTo("Temp") == 0)
						{
							G->AddValue(V->CoreTemp);
						}
						else if ((WMon->getSlotId(j)->CompareTo("NIBP") == 0)&&(updateNIBP==true))
						{
							G->AddValue(V->MAP);
						}
						else if ((WMon->getSlotId(j)->CompareTo("MAP") == 0)&&(updateNIBP==true))
						{
							G->AddValue(V->MAP);
						}
						else if (WMon->getSlotId(j)->CompareTo("SVR") == 0)
						{
							G->AddValue(V->SVR);
						}

						if (j==5) {WMon->Graph1->Draw();}
						if (j==6) {WMon->Graph2->Draw();}

					}//End if checked
				}//End if updateGraph
			}	
		 }

private: System::Void VentilationUpdate(void)
		 {
			 // Calculate the airway pressure
			 if (this->radio_PC->Checked == true)
			 {
				// Pressure control; AP is basically the PC
				 this->airwayPressure = Int16::Parse(this->UD_PCon->SelectedItem->ToString());
			 }
			 else 
			 {
				// Gotta figure an AP from the TV & Kg.
				// Usually done by Kg weight, but height makes more sense
				 // If we use patient specific data it doesn't work in display mode,
				 // so just use 80 as base Kg for the estimate for now.
				 double mlkg = double(Double::Parse(this->UD_TV->SelectedItem->ToString())/80);
				 // 0-30cmH20 over 5-15mlKg
				 this->airwayPressure = int(2.5*mlkg);
			 }
		 }

private: System::Void TIMER_SIM_Tick(System::Object^  sender, System::EventArgs^  e) {
			 // This is the sim timer.  When on it will tick once a cycle
			 // in realtime mode this is once a second; in high speed it'll run faster (q 10ms)
			 
			 // Regardless, one Tick is one simulated second*************
	
			 this->VentilationUpdate();
			 int x = Patient->Cycle();

			 this->label_SVx->Text = int(Patient->Vitals->SV*1000).ToString();
			 this->lab_preload->Text = (double(int(10*Patient->Vitals->CVP))/10).ToString();
			 this->label_KgWeight->Text = int(Patient->Base->Kg).ToString();
			 this->lab_CNT->Text = int(Patient->Vitals->Contractility).ToString();

			 if (x==-8) // -8 is the END sequence
			 {
			    // shut 'er down
				this->ToggleSimOnOff(false);
				if (this->checkBox_repeatScenario->Checked)
				{
					// Restart that shiznizzle
					this->ToggleSimOnOff(false);
				}
				return;
			 }

			 // Copy the phase from the simulator
			 this->textBox_phase->Text = Patient->GetScriptPhaseText(Patient->GetScriptPhaseNumber());

			 // Update the WMon script phase list
			 WMon->label15->Font = gcnew System::Drawing::Font(WMon->label15->Font,System::Drawing::FontStyle::Regular);
			 WMon->label15->ForeColor = System::Drawing::Color::Black;
			 WMon->label16->Font = gcnew System::Drawing::Font(WMon->label16->Font,System::Drawing::FontStyle::Regular);
			 WMon->label16->ForeColor = System::Drawing::Color::Black;
			 WMon->label17->Font = gcnew System::Drawing::Font(WMon->label17->Font,System::Drawing::FontStyle::Regular);
			 WMon->label17->ForeColor = System::Drawing::Color::Black;
			 WMon->label18->Font = gcnew System::Drawing::Font(WMon->label18->Font,System::Drawing::FontStyle::Regular);
			 WMon->label18->ForeColor = System::Drawing::Color::Black;
			 WMon->label19->Font = gcnew System::Drawing::Font(WMon->label19->Font,System::Drawing::FontStyle::Regular);
			 WMon->label19->ForeColor = System::Drawing::Color::Black;
			 WMon->label20->Font = gcnew System::Drawing::Font(WMon->label20->Font,System::Drawing::FontStyle::Regular);
			 WMon->label20->ForeColor = System::Drawing::Color::Black;
			 
			 WMon->label15->Text = Patient->GetScriptPhaseText(0);
			 WMon->label16->Text = Patient->GetScriptPhaseText(1);
			 WMon->label17->Text = Patient->GetScriptPhaseText(2);
			 WMon->label18->Text = Patient->GetScriptPhaseText(3);
			 WMon->label19->Text = Patient->GetScriptPhaseText(4);
			 WMon->label20->Text = Patient->GetScriptPhaseText(5);

			 switch (Patient->GetScriptPhaseNumber())
			 {
			 case 0: 
				 WMon->label15->Font = gcnew System::Drawing::Font(WMon->label15->Font,System::Drawing::FontStyle::Bold);
				 WMon->label15->ForeColor = System::Drawing::Color::MediumBlue;
				 break;
			 case 1: 
				 WMon->label16->Font = gcnew System::Drawing::Font(WMon->label16->Font,System::Drawing::FontStyle::Bold);
				 WMon->label16->ForeColor = System::Drawing::Color::MediumBlue;
				 break;
			 case 2: 
				 WMon->label17->Font = gcnew System::Drawing::Font(WMon->label17->Font,System::Drawing::FontStyle::Bold);
				 WMon->label17->ForeColor = System::Drawing::Color::MediumBlue;
				 break;
			 case 3: 
				 WMon->label18->Font = gcnew System::Drawing::Font(WMon->label18->Font,System::Drawing::FontStyle::Bold);
				 WMon->label18->ForeColor = System::Drawing::Color::MediumBlue;
				 break;
			 case 4: 
				 WMon->label19->Font = gcnew System::Drawing::Font(WMon->label19->Font,System::Drawing::FontStyle::Bold);
				 WMon->label19->ForeColor = System::Drawing::Color::MediumBlue;
				 break;
			 case 5: 
				 WMon->label20->Font = gcnew System::Drawing::Font(WMon->label20->Font,System::Drawing::FontStyle::Bold);
				 WMon->label20->ForeColor = System::Drawing::Color::MediumBlue;
				 break;
			 }


	 		 // Copy the vitals from the simulator
			 this->Vitals = Patient->Vitals->Clone();

			 this->BIG_HR->Text = int(this->Vitals->HR).ToString();
			 this->BIG_BP->Text = int(this->Vitals->SBP).ToString() + "/" + int(this->Vitals->DBP).ToString();
			 this->BIG_PPV->Text = int(this->Vitals->PPV).ToString();

			 if (!this->cb_MonitorOn->Checked)
			 {
				 this->BIG_CardO->Text = (double(int(this->Vitals->CO*10))/10).ToString();
				 this->vitals_SV->Text = int(this->Vitals->SV*1000).ToString();
				 this->BIG_MAP->Text = "(" + int(this->Vitals->MAP).ToString() + ")";
			 }
			 else 
			 {
				 this->BIG_CardO->Text = (double(int(WMon->LastRefCO*10))/10).ToString();
				 this->vitals_SV->Text = int(WMon->LastRefSV*1000).ToString();
				 this->BIG_MAP->Text = "(" + int(WMon->LastRefMAP).ToString() + ")";
			 }

			 this->lab_PAP->Text = this->airwayPressure.ToString();
			 this->vitals_BloodVol->Text = int((this->Vitals->BloodVolumeArt+this->Vitals->BloodVolumeVen)*1000).ToString();
			 this->label_percOpt->Text = (int(1000*this->Patient->Vitals->TotalBloodVolume()*1000*10/(Patient->GetOptimalBloodVolume(Patient->Base->Kg,Patient->Vitals->Contractility)))).ToString();
			//this->vitals_BloodVol->Text = int((this->Vitals->BloodVolume)).ToString();
			 this->vitals_SepFac->Text = (float(int(10*this->Vitals->SepsisFactor))/10).ToString();
			 //this->vitals_LVESV->Text = int(this->Vitals->LVESV).ToString();
			 this->vitals_SVR->Text = int(this->Vitals->SVR*80).ToString();
			 this->vitals_volR->Text = (int(1000*this->Patient->Vitals->TotalBloodVolume()*1000*10/(Patient->Base->Kg*61+634))/100).ToString();
		 
			 this->lab_stim->Text = this->Vitals->SympatheticOutflow.ToString();
			 this->lab_narc->Text = Patient->NARC.ToString();
			 this->lab_epi->Text = Patient->Vitals->BetaStim.ToString();  //Patient->EPHEDRINE.ToString();
			 this->lab_neo->Text = Patient->Vitals->AlphaStim.ToString(); //Patient->NEO.ToString();
			 this->label_DOPA->Text = Patient->Vitals->DopaStim.ToString();

			 this->vitals_CM->Text = this->Vitals->CoagMass.ToString();
			 this->vitals_PlatM->Text = int(this->Vitals->PlateletMass/this->Vitals->TotalBloodVolume()).ToString();
			 this->vitals_RCM->Text = (double(int(10*this->Vitals->RedCellMass/this->Vitals->TotalBloodVolume()))/10).ToString();
			 this->Vitals->RR = Int16::Parse(this->UD_Resp->SelectedItem->ToString());

			 if (this->cb_MonitorOn->Checked)
			 {
				 WMon->updateVitals(this->Vitals);
				 bool NIBPu = false;
				 bool Graphsu = false;
				 if (System::Decimal(Patient->SimSeconds)/300 == int(Patient->SimSeconds/300))
				 {
					 NIBPu = true;
				 }
 				 if (System::Decimal(Patient->SimSeconds)/10 == int(Patient->SimSeconds/10))
				 {
					Graphsu = true;
				 }
				 this->UpdateMonitorSlots(this->Vitals,NIBPu,Graphsu);
				 WMon->updateVentilation(Int16::Parse(this->UD_Resp->SelectedItem->ToString()),Int16::Parse(this->lab_PAP->Text));
			 }

			 String^ clocky ="";
			 String^ datey="";
			 int ticks = Patient->SimSeconds;
			 int H=0,M=0,S=0;
			 while (ticks >= 3600) {H++;ticks=ticks-3600;}
			 while (ticks >= 60) {M++;ticks=ticks-60;}
			 S=ticks;

			 if (H < 10) {clocky += "0" + H.ToString();}
			 else {clocky += H.ToString();}

			 if (M < 10) {clocky += ":0" + M.ToString();}
			 else {clocky +=  ":"+ M.ToString();}

 			 if (S < 10) {clocky += ":0" + S.ToString();}
			 else {clocky +=  ":"+ S.ToString();}

			 this->label_CLOCK->Text = clocky;

			 datey = DateTime::Now.Year.ToString() + "-";
			 if (DateTime::Now.Month < 10) 
			 {
				datey += "0";
			 }
			 datey += DateTime::Now.Month.ToString() + "-";
			 if (DateTime::Now.Day < 10) 
			 {
				 datey += "0";
			 }
			 datey += DateTime::Now.Day.ToString();

			 // edit Shehzi
			 // Log actions from the wave monitor
			 /*if (this->checkBox4->Checked == true){
				 this->Action_log_flag = 1;
			 }*/
			 //edit Shehzi
			 // Update and interact with the wavemonitor
			 if (this->WMon->Visible)
			 {
				 this->WMon->SetClock(this->label_CLOCK->Text);
				 //this->Patient->AddMedication(DRUG_EPHEDRINE,WMon->GetMedEphedrine());
				 //this->Patient->AddMedication(DRUG_NEOSYNEPHRINE,WMon->GetMedPhenylephrine());
				 //this->Patient->AddMedication(DRUG_FENTANYL,WMon->GetMedFentanyl());
				 System::IO::StreamWriter^ TFILE = nullptr;
				 if (this->Action_log_flag == 1){
					 TFILE = gcnew IO::StreamWriter(this->label_savefile2->Text, 1);
					 //TFILE->Write(clocky + "\t");
				 }

				 // Add medications
				 this->label_totalDrugs->Text = this->WMon->GetDrugCount().ToString();

				 for (int k=this->WMon->GetDrugCount()-1; k>=0; k=k-1)
				 {
					 // Is it a simple Bolus?
					 if ((WMon->GetIsBolus(k)) && (!WMon->GetIsBolusAsInfusion(k)))
					 {
						 this->Patient->AddMedication(WMon->GetDrugIDInSlot(k),WMon->GetDoseInSlot(k));
						 // bug here - the stop actions were not logging.
						 // 
						 if (this->Action_log_flag == 1){
							 TFILE->Write(clocky + "\t UID->" + WMon->GetUID(k) + "\t Bolus->" + WMon->GetDrugIDInSlot(k) + "\t Dose->" + WMon->GetDoseInSlot(k) + "\n");
						 }
						 WMon->DiscontinueDrugInRow(k);
						 //WMon->RemoveDrug(k);
						 
					 }
					 // Is it a bolusAsInfusion
					 else if (WMon->GetIsBolusAsInfusion(k))
					 {
						 DrugData^ D = DrugList->GetDrugByID(WMon->GetDrugIDInSlot(k));
						 double DOSE = 1/D->BolusSecondsPerUnitBolusDose;
						 this->Patient->AddMedication(D->DrugID,DOSE);
						 this->WMon->SetAmountGiven(k,WMon->GetTotalDose(k) + DOSE);
						 this->Pointless(D->DrugID,DOSE);
						 if (this->Action_log_flag == 1){
							 TFILE->Write(clocky + "\t UID->" + WMon->GetUID(k) + "\t DripAsBolus->" + WMon->GetDrugIDInSlot(k) + "\t Dose->" + WMon->GetTotalDose(k) + "/" + WMon->GetDoseInSlot(k) + "\n");
						 }
						 if (WMon->GetTotalDose(k) > WMon->GetDoseInSlot(k))
						 {
							 // bug here - the stop actions were not logging.
							 //WMon->RemoveDrug(k);
							 WMon->DiscontinueDrugInRow(k);
						 }
					 }
					 // It should be an infusion
					 else if (WMon->GetIsBolus(k) == 0)
					 {
						 DrugData^ D = DrugList->GetDrugByID(WMon->GetDrugIDInSlot(k));
						 // mcg/min
						 // mcg/kg/min
						 // mcg/kg/hr
						 // mg/min
						 // ml/hr
						 // units/min
						 double MULTIPLIER = 1;
						 double IF_WEIGHT_MULTIPLIER = 1;
						 if (D->DripUnits->Contains("/min"))
						 {
							 MULTIPLIER = MULTIPLIER/60;
						 }
						 else if (D->DripUnits->Contains("/hr"))
						 {
							 MULTIPLIER = MULTIPLIER/3600;
						 }
						 if (D->DripUnits->Contains("mcg"))
						 {
							 //MULTIPLIER = MULTIPLIER /1000;
						 }
						 if (D->DripUnits->Contains("/kg"))
						 {
							 MULTIPLIER = MULTIPLIER * this->Patient->Base->Kg;
							 IF_WEIGHT_MULTIPLIER = this->Patient->Base->Kg;
						 }
						 
						 // Some of the infusions need adjustments compared to
						 // boluses to reach an appropriate steady state
						 double DoseMod=1;
						 if (D->DrugID == DRUG_EPINEPHRINE) {DoseMod = 10;}
						 if (D->DrugID == DRUG_ESMOLOL) {DoseMod = 0.5;}
						 if (D->DrugID == DRUG_VASOPRESSIN) {DoseMod = 10;}

						 double DOSE = WMon->GetDoseInSlot(k) * MULTIPLIER;
						 this->Patient->AddMedication(D->DrugID,DOSE*DoseMod);
						 this->Pointless(D->DrugID,DOSE);
						 this->WMon->SetAmountGiven(k,WMon->GetTotalDose(k) + DOSE);
						 if (this->Action_log_flag == 1){
							 TFILE->Write(clocky + "\t UID->" + WMon->GetUID(k) + "\t Drip->" + WMon->GetDrugIDInSlot(k) + "\t Dose->" + WMon->GetTotalDose(k) + "/" + WMon->GetDoseInSlot(k)*IF_WEIGHT_MULTIPLIER + "\n");
						 }
					 }
					 else
					 {
						 // This shouldn't happen
						 System::Windows::Forms::MessageBox::Show("Drug admin did not match any known pattern");
					 }
				 }

				 // edit Shehzi
				 if (this->Action_log_flag == 1){
					 //TFILE->Write("\n");
					 TFILE->Close();
				 }
				 //edit Shehzi

				 // Record whatever Action Items
				 if (WMon->ActionLog->Length > 0) {
					 /*System::IO::StreamWriter^ FOUT = gcnew System::IO::StreamWriter(this->label_savefile2->Text,true);
					 FOUT->Write(WMon->ActionLog);*/
					 WMon->ActionLog = "";
					 //FOUT->Close();
				 }
				 
				 if (WMon->IsTripleSpeed())
				 {
					 this->TIMER_SIM->Interval = 300;
				 }
				 else 
				 {
					 // Set speed here! 
					 // Normal speed - 1000

					 this->TIMER_SIM->Interval = 1000;
				 }

			 }

			 // edit Shehzi - This if for toggling HgB display for human trails
			 if (this->Patient->SimSeconds%this->show_hgb == 0){
				 this->SlotSet(3, "Hgb");
			 }
			 if (this->Patient->SimSeconds%this->show_hgb == this->interval_hgb){
				 this->SlotSet(3, "Off");
			 }
			 // edit Shehzi

			 //Logging and vitals recording

			 // IF YOU CHANGE THIS DIVISOR YOU MUST CHANGE THE DATABASE TIMERS BELOW

			if (System::Decimal(Patient->SimSeconds)/2 == int(Patient->SimSeconds/2))
			{
				if (this->checkBox4->Checked==true)
				{
				 	 Patient->LogNow(this->label_savefile->Text);
				}
			    if (this->cb_BroadcastNet->Checked)
				{
					//try{
					//	this->TCP_listen->BeginAcceptTcpClient(gcnew AsyncCallback(this, &Form1::TCP_callback), TCP_listen);
					//}
					//catch (Exception^ e){
					//	System::Windows::Forms::MessageBox::Show("TCP Broke here 1");
					//}//this->TCP_listen2->BeginAcceptTcpClient(gcnew AsyncCallback(this, &Form1::TCP_callback2), TCP_listen2);

					 if ((this->cb_StartLag->Checked)&&(this->Patient->SimSeconds < 30)) 
					 {
						 // Do Nothing
					 }
					 else 
					 {
						// Assemble a network packet: a char array
						 CDSIM_Net_Pack^ Package = gcnew CDSIM_Net_Pack(System::DateTime::Parse(datey+" "+clocky),PatientID,this->CB_speed_turbo->Checked,Patient->Vitals->HR,Patient->Vitals->SBP,Patient->Vitals->DBP,Patient->Vitals->MAP,(1000*Patient->Vitals->SV),Patient->Vitals->SVR,Patient->Vitals->PPV,Patient->Vitals->SPO2,this->NetBID,this->FluidGivenByLirThisBolus);
						// Now send the data over the network

						//edit by Shehzi
						this->TCP_data = gcnew CDSIM_TCP_Pack(System::DateTime::Parse(datey + " " + clocky), Patient->Vitals->HR, Patient->Vitals->SBP, Patient->Vitals->DBP, Patient->Vitals->MAP, (1000 * Patient->Vitals->SV), Patient->Vitals->SVR, Patient->Vitals->PPV, Patient->Vitals->SPO2);
						/*if (this->TcpWriter != nullptr){
							TCP_send_message(TCP_data);
						}*/
						try{
							if (this->handler != nullptr && this->handler->Connected){
								System::String^ message = JsonConvert::SerializeObject(this->TCP_data);
								// Need a line feed for all messages 
								message = message + "\n";
								Send(this->handler, message);
							}
						}
						catch (Exception^ e){
							System::Windows::Forms::MessageBox::Show("TCP Broke here 2");
						}
						//TCP_use_message(this->TcpReader2);
						//edit by Shehzi
						this->BroadcastSocket->Send(Package->NetPack,Package->NetPack->Length);
					 }
				 }
				 if (this->cb_listenNetwork->Checked)
				 {
					 if (this->ListenSocket->Available > 0)
					 {
						 int mark = 0;
						 array<unsigned char>^ A = gcnew array<unsigned char>(ListenSocket->Available);
						 this->ListenSocket->Receive(A);
						 while (mark+3 < A->Length)
						 {
							// Command, Bolus ID, Speed
							 if (A[0+mark] == ACTION_FLUID_BOLUS)
							 {
								// Start
								this->NetBID = A[1+mark];
								this->NetRate = A[2+mark]*100;
							 }
							 else if (A[0+mark] == ACTION_STOP_BOLUS)
							 {
								this->NetBID = 0;
								this->NetRate = 0;
							 }
							 else if (A[0+mark] == ACTION_PHENYLEPHRINE_BOLUS)
							 {
								 // next two bytes store dose in nanograms
								 // first byte is high, second byte is low (Big-endian)
								 double rate = double(A[1+mark])*256 + double(A[2+mark]);
								 // Convert to milligrams
								 rate = rate / 1000;
								 Patient->AddMedication(DRUG_NEOSYNEPHRINE,rate);
							 }
							 mark = mark + 3;
						 }
					 }
					 
					// Running Bolus. Is is the same one?
					if (NetBID != this->LirBolusID)
					{
						// Reset the fluid count
						this->FluidGivenByLirThisBolus = 0;
						this->LirBolusID = NetBID;
					}
					// Get the fluid rate and type
					double RATE = double(NetRate)/3600;
					// Insert into simulator
					Patient->AddMedication(200,RATE);
					this->FluidGivenByLirThisBolus += RATE;
				 }
			}

		 }
// Not really pointless, but I'm bored
private: void Pointless(int DrugID, double Dose)
		 {
			 if ((DrugID > 99) && (DrugID < 200))
			 {
				 WMon->AddTotalCrystal(Dose);
			 }
			 if ((DrugID > 199) && (DrugID < 300))
			 {
				 WMon->AddTotalColloid(Dose);
			 }
			 if ((DrugID >299) && (DrugID < 400))
			 {
				 WMon->AddTotalBlood(Dose);
			 }
		 }

private: int GetLirBolusID(void)
		 {
			System::Data::Odbc::OdbcConnection^ TmeConnection = gcnew System::Data::Odbc::OdbcConnection;
			TmeConnection->ConnectionString = "DRIVER={MySQL ODBC 5.1 Driver}; Server=localhost; option=3; Connection Timeout=5; User=lir; password=lir; Database=lir;";
			int BID = 0;

			try {
 		 		 Odbc::OdbcCommand ^cmd = gcnew Odbc::OdbcCommand();
				 cmd->Connection = TmeConnection;
				 TmeConnection->Open();

				 cmd->CommandType = CommandType::Text;
				 cmd->CommandText = "select * from fluidio where Item='LirFluidBolusID'"; 

				 System::Data::Odbc::OdbcDataReader^ reader;
				 reader = cmd->ExecuteReader();
				 if (reader->Read()) 
				 {
					BID = reader->GetInt16(1);
				 }
				 TmeConnection->Close();
				 delete TmeConnection;
			}
			catch (Exception^ e) {
				e;
				try {TmeConnection->Close();} catch (Exception^ e){e;}
				//System::Windows::Forms::MessageBox::Show("FluidID Check: " + e->Message);
				return 0;
				}
			return BID;
		 }

private: int GetLirBolusType(void)
		 {
			System::Data::Odbc::OdbcConnection^ TmeConnection = gcnew System::Data::Odbc::OdbcConnection;
			TmeConnection->ConnectionString = "DRIVER={MySQL ODBC 5.1 Driver}; Server=localhost; option=3; Connection Timeout=5; User=lir; password=lir; Database=lir;";
			int FID = 0;

			try {
 		 		 Odbc::OdbcCommand ^cmd = gcnew Odbc::OdbcCommand();
				 cmd->Connection = TmeConnection;
				 TmeConnection->Open();

				 cmd->CommandType = CommandType::Text;
				 cmd->CommandText = "select * from fluidio where Item='LirFluidBolusType'"; 

				 System::Data::Odbc::OdbcDataReader^ reader;
				 reader = cmd->ExecuteReader();
				 if (reader->Read()) 
				 {
					 FID = reader->GetInt16(1);
				 }
				 TmeConnection->Close();
			}
			catch (Exception^ e) {
				e;
				try {TmeConnection->Close();} catch (Exception^ e){e;}
				//System::Windows::Forms::MessageBox::Show("FluidID Check: " + e->Message);
				return 0;
				}
			return FID;
		 }

private: double GetLirBolusRate(void)
		 {
			System::Data::Odbc::OdbcConnection^ TmeConnection = gcnew System::Data::Odbc::OdbcConnection;
			TmeConnection->ConnectionString = "DRIVER={MySQL ODBC 5.1 Driver}; Server=localhost; option=3; Connection Timeout=5; User=lir; password=lir; Database=lir;";
			double RATE = 0;

			try {
 		 		 Odbc::OdbcCommand ^cmd = gcnew Odbc::OdbcCommand();
				 cmd->Connection = TmeConnection;
				 TmeConnection->Open();

				 cmd->CommandType = CommandType::Text;
				 cmd->CommandText = "select * from fluidio where Item='LirFluidBolusRate'"; 

				 System::Data::Odbc::OdbcDataReader^ reader;
				 reader = cmd->ExecuteReader();
				 if (reader->Read()) 
				 {
					 RATE = reader->GetFloat(1);
				 }
				 TmeConnection->Close();
			}
			catch (Exception^ e) 
			{
				e;
				try {TmeConnection->Close();} catch (Exception^ e){e;}
				//System::Windows::Forms::MessageBox::Show("FluidID Check: " + e->Message);
				return 0;
			}
			return RATE;
		 }


private: System::Void GIVE_NEO_Click(System::Object^  sender, System::EventArgs^  e) {
			Patient->AddMedication(DRUG_NEOSYNEPHRINE,50);
			this->label_medsgiven->Text = "50mcg Neo";
			this->timer_meds->Start();
		 }
private: System::Void button8_Click(System::Object^  sender, System::EventArgs^  e) {
			 Patient->AddMedication(DRUG_EPHEDRINE,5);
			this->label_medsgiven->Text = "5mg Ephedrine";
			 this->timer_meds->Start();
		 }
private: System::Void button10_Click(System::Object^  sender, System::EventArgs^  e) {
			 Patient->AddMedication(DRUG_FENTANYL,50);
			this->label_medsgiven->Text = "50mcg Fentanyl";
			 this->timer_meds->Start();
		 }
private: System::Void timer_meds_Tick(System::Object^  sender, System::EventArgs^  e) {
			 this->label_medsgiven->Text = "";
			 this->timer_meds->Stop();
		 }

private: System::Void pictureBox3_Click(System::Object^  sender, System::EventArgs^  e) {
		 }
private: System::Void Fire_Click(System::Object^  sender, System::EventArgs^  e) {
			 Patient->AddMedication(DRUG_LR, 170);
			 Patient->AddMedication(DRUG_PRBC,130);
		 }
private: System::Void exitToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 this->Close();
		 }
private: System::Void aboutCDSimToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 LIR::About^ AB = gcnew LIR::About();
			 AB->Show();
		 }
private: System::Void domainUpDown1_SelectedItemChanged(System::Object^  sender, System::EventArgs^  e) {
		 }
private: System::Void radioButton1_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
		 }
private: System::Void radio_PC_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 if (this->radio_PC->Checked == true)
			 {
				 this->radio_VC->Checked = false;
			 }
			 else
			 {
				 this->radio_VC->Checked = true;
			 }
		}
private: System::Void radio_VC_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 if (this->radio_VC->Checked == true)
			 {
				 this->radio_PC->Checked = false;
			 }
			 else 
			 {
				this->radio_PC->Checked = true;
			 }
		}
private: System::Void button3_Click_2(System::Object^  sender, System::EventArgs^  e) {
			 Patient->Bleed(300,false);
		 }
private: System::Void tb_VVar_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			 if (this->tb_VVar->Text->Length > 0)
			 {
				this->WMon->SetVVariability(Int16::Parse(this->tb_VVar->Text));
			 } 
		}
private: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) {
		 }
private: System::Void buttDisplayOFF_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (this->button1->Text->Length == 2)
			 {
				//Simulator is on, turn it off
				 this->button1_Click(this,e);
			 }
			 // Verify Sim Timer is off
			 this->TIMER_SIM->Stop();

			 if (this->buttDisplayOFF->Text == "OFF") 
			 {
				 this->button1->Enabled = false;
				 this->buttDisplayOFF->Text = "ON";
				 this->buttDisplayOFF->BackColor::set(System::Drawing::Color::Green);
				// Hit the update - go to set state
				 this->button4_Click(this,e);
				 WMon->MonitorOn();
				 WMon->timer_pacing->Start();
				 this->timerDisplayVariability->Start();
			 }
			 else
			 {
				 this->button1->Enabled = true;
				 this->timerDisplayVariability->Stop();
				 WMon->MonitorStandby();
				 WMon->timer_pacing->Stop();
				 WMon->ClearGraphs();
				 this->UpdateMonitorSlots(gcnew Simulator::VitalsPack(),true,true);
				 this->buttDisplayOFF->Text = "OFF";
				 this->buttDisplayOFF->BackColor::set(System::Drawing::Color::LightCoral);
			 }
		 }

private: Simulator::VitalsPack^ FillVitalsFromUserFields(void)
		 {
			 Simulator::VitalsPack^ TVitals = gcnew Simulator::VitalsPack();
			 try 
			 {
			 // Update the vitals object
				 TVitals->CI = double(Double::Parse(this->dis_CO->Text)/2.2);
				 TVitals->CO = double(Double::Parse(this->dis_CO->Text));
				 TVitals->CVP = double(Double::Parse(this->dis_CVP->Text));
				 TVitals->DBP = double(Double::Parse(this->dis_DBP->Text));
				 TVitals->SBP = double(Double::Parse(this->dis_SBP->Text));
				 TVitals->MAP = double(Double::Parse(this->dis_MAP->Text));
				 TVitals->HR = double(Double::Parse(this->dis_HR->Text));
				 TVitals->PPV = double(Double::Parse(this->dis_PPV->Text));
				 TVitals->SV = double(Double::Parse(this->dis_SV->Text));
				 TVitals->SVR = double(Double::Parse(this->dis_SVR->Text));
				 TVitals->SPO2 = double(Double::Parse(this->dis_SP02->Text));
				 TVitals->PaCO2 = double(Double::Parse(this->dis_PACO2->Text));
				 TVitals->DeadspaceR = double(100-Double::Parse(this->dis_Deadspace->Text))/100;
				 TVitals->DynamicOutflow = 1 - double(Double::Parse(this->dis_Outflow->Text))/100;
				 TVitals->RR = Int16::Parse(this->UD_Resp->SelectedItem->ToString());
				 TVitals->HgB = double(Double::Parse(this->dis_HgB->Text));
				 TVitals->PA_DP = double(Double::Parse(this->dis_PADP->Text));
				 TVitals->PA_SP = double(Double::Parse(this->dis_PASP->Text));
				 TVitals->SVV = double(Double::Parse(this->dis_SVV->Text));
				 TVitals->PVI = double(Double::Parse(this->dis_PVI->Text));
				 TVitals->SpHbx = double(Double::Parse(this->dis_Hgbx->Text));
				 TVitals->SvO2 = double(Double::Parse(this->dis_SvO2->Text));
				 TVitals->SCvO2 = double(Double::Parse(this->dis_SCvO2->Text));
				 TVitals->CoreTemp = double(Double::Parse(this->dis_CoreTemp->Text));
				 TVitals->FTc = double(Double::Parse(this->dis_FTc->Text));
			 }
			 catch (Exception^ e)
			 {
				 MessageBox::Show(e->ToString());
				e;
			 }
		 return TVitals;
		}

private: void ClearStateIndicators()
{
			 this->BIG_HR->Text = this->dis_HR->Text;
			 this->BIG_CardO->Text = this->dis_CO->Text;
			 this->BIG_BP->Text = this->dis_SBP->Text + "/" + this->dis_DBP->Text;
			 this->BIG_MAP->Text = this->dis_MAP->Text;
			 this->BIG_PPV->Text = this->dis_PPV->Text;

			 this->lab_PAP->Text = this->airwayPressure.ToString();
			 this->vitals_BloodVol->Text = "-";
			 this->lab_preload->Text = "-";
			 this->vitals_SepFac->Text = "-";
			 this->vitals_LVESV->Text = "-";
			 this->vitals_SV->Text = this->dis_SV->Text;
			 this->vitals_SVR->Text = this->dis_SVR->Text;
			 this->vitals_volR->Text = "-";
		 
			 this->lab_stim->Text = "-";
			 this->lab_narc->Text = "-";
			 this->lab_epi->Text = "-";
			 this->lab_neo->Text = "-";

			 this->vitals_CM->Text = "-";
			 this->vitals_PlatM->Text = "-";
			 this->vitals_RCM->Text = "-";
		}

private: System::Void ActOnRhythymChange(String^ R)
		 {
			if (R->CompareTo("BIGEMINY")==0)
			{
				this->button11_Click(this,System::EventArgs::Empty);
			}
			else if (R->CompareTo("NSR")==0)
			{
				this->button13_Click(this,System::EventArgs::Empty);
			}
		 }

private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) {
			 this->progressBar1->Visible = 0;
			 this->timerCrossfade->Stop();

			 this->ClearStateIndicators();
			 this->VentilationUpdate();
			 this->Vitals = this->FillVitalsFromUserFields();
			 this->statusStrip1->Text = this->ActOnStringCommands(this->LoadedCommands);
			 
			 WMon->updateVitals(this->Vitals);
			 WMon->updateVentilation(Int16::Parse(this->UD_Resp->SelectedItem->ToString()),Int16::Parse(this->lab_PAP->Text));
			 UpdateMonitorSlots(this->Vitals,1,1);
		 }

private: System::Void dis_HR_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			 try 
			 {
				 if (double::Parse(this->dis_HR->Text) > 0) 
				 {
					 this->dis_CO->Text = (double(int(double::Parse(this->dis_SV->Text) * double::Parse(this->dis_HR->Text)/100))/10).ToString();
				 }
				 else 
				 {
					 this->dis_CO->Text = "0";
				 }
			 }
			 catch(Exception^ e)
			 {
				 this->dis_CO->Text = "0";
				 e;
			 }
			 // Call the SVR changer
			 this->dis_SV_TextChanged(this,e);
		 }
private: System::Void dis_SBP_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			 try
			 {
				 this->dis_MAP->Text = int((double::Parse(this->dis_SBP->Text) + 2*double::Parse(this->dis_DBP->Text))/3).ToString();
			 }
			 catch(Exception^ e)
			 {
				 e;
				 this->dis_MAP->Text = "0";
			 }
			 // Call the SVR changer
			 this->dis_SV_TextChanged(this,e);
		}
private: System::Void dis_SV_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			this->WARN_SVR->Text = "";
			 try
			 {
				 if ((double::Parse(this->dis_CO->Text) == 0) || (double::Parse(this->dis_MAP->Text)==0)
					 || (double::Parse(this->dis_CVP->Text) == 0))
				 {
					 this->dis_SVR->Text = "0";
				 }
				 else 
				 {
					 this->dis_SVR->Text = int(80*(double::Parse(this->dis_MAP->Text) - double::Parse(this->dis_CVP->Text))/double::Parse(this->dis_CO->Text)).ToString();
					if (double::Parse(this->dis_SVR->Text) > 2800)
					{
						this->WARN_SVR->Text = "!!!!";
					}
				 }
			 }
			 catch (Exception^ e)
			 {
				 e;
				this->dis_SVR->Text = "0";
				//MessageBox::Show(e->ToString());
			 }
		 }
private: System::Void SlotSet(int S, String^ NewName)
		 {
			 //MessageBox::Show("Got " + NewName + " which evaluates to " + (NewName->CompareTo("Off")).ToString() + " compare to 'Off'");
			 if (NewName->CompareTo("Off") == 0)
			 {
				 WMon->ClearVitalSlot(S);
			 }
			 else {
				 for (int i=0; i<SlotOptions->Count; i++)
				 {
					 LIR::SlotOption^ Slot = dynamic_cast<LIR::SlotOption^>(SlotOptions->default[i]);
					 if (Slot->Name->CompareTo(NewName) == 0)
					 {
						 WMon->SetVitalSlot(S,Slot->Name,Slot->Color);
						 // Set the graph colors
						 if (S==5)
						 {
							WMon->Graph1->ChangeForeColor(Slot->Color);
							WMon->Graph1->ClearValues();
							WMon->Graph1->ChangeScale(Slot->Min,Slot->Max,Slot->Zero);
							if (Slot->BarGraph) {WMon->Graph1->SetBarGraphModeOn();}
							else {WMon->Graph1->SetBarGraphModeOff();}
						 }
						 if (S==6)
						 {
							WMon->Graph2->ChangeForeColor(Slot->Color);
							WMon->Graph2->ClearValues();
							WMon->Graph2->ChangeScale(Slot->Min,Slot->Max,Slot->Zero);
							if (Slot->BarGraph) {WMon->Graph2->SetBarGraphModeOn();}
							else {WMon->Graph2->SetBarGraphModeOff();}
						 }
					 }
				 }
			 }
			 
			 if (NewName->CompareTo("NIBP") == 0) {
				 this->UpdateMonitorSlots(WMon->Vitals,0,1);
			 } else {
				 this->UpdateMonitorSlots(WMon->Vitals,1,1);
			 }
		}

private: System::Void comboBox1_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
			 SlotSet(1,this->comboBox1->SelectedItem->ToString());
		 }
private: System::Void comboBox2_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
			 SlotSet(2,this->comboBox2->SelectedItem->ToString());
		 }
private: System::Void comboBox3_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
			 SlotSet(3,this->comboBox3->SelectedItem->ToString());
		 }
private: System::Void comboBox4_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
			 SlotSet(4,this->comboBox4->SelectedItem->ToString());
		 }
private: System::Void comboBox5_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
			 SlotSet(5,this->comboBox5->SelectedItem->ToString());
		 }
private: System::Void comboBox6_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
			 SlotSet(6,this->comboBox6->SelectedItem->ToString());
		 }
private: System::Void dis_SP02_TextChanged(System::Object^  sender, System::EventArgs^  e) {
		 }
private: System::Void checkBox3_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 if (this->checkBox3->Checked == false) 
			 {
				 this->dis_PPV->ReadOnly = false;
				 this->dis_PVI->ReadOnly = false;
				 this->dis_SVV->ReadOnly = false;
				 this->dis_FTc->ReadOnly = false;
			 }
			 else 
			 {
				 this->dis_PPV->ReadOnly = true;
				 this->dis_PVI->ReadOnly = true;
				 this->dis_SVV->ReadOnly = true;
				 this->dis_FTc->ReadOnly = true;
				 trackBar_Preload_Scroll(this,e);
			}
		 }
private: System::Void trackBar_Preload_Scroll(System::Object^  sender, System::EventArgs^  e) {
			 if (this->checkBox3->Checked == false) {return;}

			 int P = this->trackBar_Preload->Value;

			 this->label_PLS->Text = (int(double(P)*2.5)).ToString() + "%";

			 this->dis_PPV->Text = P.ToString();
			 double Q = (P+int(Rx->NextDouble()*3+5));
			 this->dis_PVI->Text = Q.ToString();
			 Q = (P+2);
			 this->dis_SVV->Text = Q.ToString();
			 Q = (4.5 - double(int(100*double(P)/150))/10);
			 if (Q > 2.1) 
			 {
				 this->dis_FTc->Text = Q.ToString();
			 }
			 else
			 {
				this->dis_FTc->Text = "2.1";
			 }
		 }
private: System::Void button5_Click(System::Object^  sender, System::EventArgs^  e) {
			 // Set the start end end points
			 // Start wherever we currently are
			 this->StartVitals = this->Vitals->Clone();

			 // End where the crossfader tells us to
			 this->EndVitals = this->FillVitalsFromUserFields();			 
				
			 this->CrossfadeSecondsCount = 0;
			 try 
			 {
				 this->CrossfadeSecondTime = Int16::Parse(this->dis_xfade->Text)*10;
			 }
			 catch(Exception^ e)
			 {
				 this->CrossfadeSecondTime = 200;
				 this->dis_xfade->Text = "20";
				 e;
			 }
			 this->progressBar1->Value=0;
			 this->progressBar1->Visible = true;
			 this->timerCrossfade->Start();

			 // Do this NOW
			 this->statusStrip1->Text = this->ActOnStringCommands(this->LoadedCommands);
		}
private: System::Void timerCrossfade_Tick(System::Object^  sender, System::EventArgs^  e) {
			 // This is gonna suck
			double R = double(CrossfadeSecondsCount)/double(CrossfadeSecondTime);

			Vitals->CO =  StartVitals->CO + (EndVitals->CO - StartVitals->CO)*R;
			Vitals->CI =  StartVitals->CI + (EndVitals->CI - StartVitals->CI)*R;
			Vitals->MAP =  StartVitals->MAP + (EndVitals->MAP - StartVitals->MAP)*R;
			Vitals->DBP =  StartVitals->DBP + (EndVitals->DBP - StartVitals->DBP)*R;
			Vitals->SBP =  StartVitals->SBP + (EndVitals->SBP - StartVitals->SBP)*R;
			Vitals->HR =  StartVitals->HR + (EndVitals->HR - StartVitals->HR)*R;
			Vitals->SV =  StartVitals->SV + (EndVitals->SV - StartVitals->SV)*R;
			Vitals->SVR =  StartVitals->SVR + (EndVitals->SVR - StartVitals->SVR)*R;
			Vitals->CVP =  StartVitals->CVP + (EndVitals->CVP - StartVitals->CVP)*R;
			Vitals->PA_DP =  StartVitals->PA_DP + (EndVitals->PA_DP - StartVitals->PA_DP)*R;
			Vitals->PA_SP =  StartVitals->PA_SP + (EndVitals->PA_SP - StartVitals->PA_DP)*R;
			Vitals->PPV =  StartVitals->PPV + (EndVitals->PPV - StartVitals->PPV)*R;
			Vitals->SVV =  StartVitals->SVV + (EndVitals->SVV - StartVitals->SVV)*R;
			Vitals->PVI =  StartVitals->PVI + (EndVitals->PVI - StartVitals->PVI)*R;
			Vitals->SPO2 =  StartVitals->SPO2 + (EndVitals->SPO2 - StartVitals->SPO2)*R;
			Vitals->RR =  StartVitals->RR + (EndVitals->RR - StartVitals->RR)*R;
			Vitals->PaCO2 =  StartVitals->PaCO2 + (EndVitals->PaCO2 - StartVitals->PaCO2)*R;
			Vitals->SpHbx =  StartVitals->SpHbx + (EndVitals->SpHbx - StartVitals->SpHbx)*R;
			Vitals->SvO2 =  StartVitals->SvO2 + (EndVitals->SvO2 - StartVitals->SvO2)*R;
			Vitals->SCvO2 =  StartVitals->SCvO2 + (EndVitals->SCvO2 - StartVitals->SCvO2)*R;
			Vitals->HgB =  StartVitals->HgB + (EndVitals->HgB - StartVitals->HgB)*R;			Vitals->CO =  StartVitals->CO + (EndVitals->CO - StartVitals->CO)*R;
			Vitals->DeadspaceR =  StartVitals->DeadspaceR + (EndVitals->DeadspaceR - StartVitals->DeadspaceR)*R;
			Vitals->CoreTemp = StartVitals->CoreTemp + (EndVitals->CoreTemp - StartVitals->CoreTemp)*R;

			//Update shiat
			WMon->updateVitals(Vitals);
			try {
				WMon->updateVentilation(Int16::Parse(this->UD_Resp->SelectedItem->ToString()),Int16::Parse(this->lab_PAP->Text));
			}
			catch (Exception^ e) {e;}
			this->progressBar1->Value = int(CrossfadeSecondsCount*100/CrossfadeSecondTime);


			if (CrossfadeSecondsCount >= CrossfadeSecondTime)
			{
				UpdateMonitorSlots(WMon->Vitals,1,1);		
				CrossfadeSecondsCount=0;
				CrossfadeSecondTime=0;
				timerCrossfade->Stop();
				this->progressBar1->Visible = false;
			}
			else
			{
				if (double(CrossfadeSecondsCount)/5 == int(CrossfadeSecondsCount/5))
				{
					UpdateMonitorSlots(WMon->Vitals,0,1);
				}
				else
				{
					UpdateMonitorSlots(WMon->Vitals,0,0);
				}
				CrossfadeSecondsCount+=5;
			}
		 }
private: System::Void dis_CO_TextChanged(System::Object^  sender, System::EventArgs^  e) {
		 }
private: System::Void dis_CVP_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			 try 
			 {
				 if (double::Parse(this->dis_CVP->Text) > 0) 
				 {
					this->dis_SV_TextChanged(this,e);
					}
			 }
			 catch (Exception^ e)
			 {
				e;
				this->dis_SVR->Text = "0";
			 }
		 }
private: System::Void timerDisplayVariability_Tick(System::Object^  sender, System::EventArgs^  e) {
			 // Ticks every 0.5 seconds to add variability
			 // Add some variability to those goddamn values in display mode
			 // Have to create a new vitals object & pass it to the monitor...damn
			 Simulator::VitalsPack^ TVP = gcnew Simulator::VitalsPack();
			 TVP = Vitals->Clone();

			 this->ThirtySecondCounter++;

			 //if (Rx->NextDouble() > 0.9)
			 {
				 double X = double::Parse(this->dis_Variability->Text)/100;

				 if (Rx->NextDouble() > 0.9)
				 {
					 TVP->HR += double(TVP->HR*(Rx->NextDouble()-0.5)*X/2);
				 }
				 if (Rx->NextDouble() > 0.8)
				 {
					 TVP->SBP += double(TVP->SBP*(Rx->NextDouble()-0.5)*X);
					 TVP->DBP += double(TVP->DBP*(Rx->NextDouble()-0.5)*X);
					 TVP->MAP = double((TVP->DBP+TVP->DBP+TVP->SBP)/3);
				 }
				 if (Rx->NextDouble() > 0.9)
				 {
					 TVP->CO += double(TVP->CO*(Rx->NextDouble()-0.5)*X);
					 TVP->CI = double(TVP->CO/2.2);
				 }
				 if (Rx->NextDouble() > 0.9)
				 {
					 TVP->SCvO2 += double(TVP->SCvO2*(Rx->NextDouble()-0.5)*X);
				 }
				 if (Rx->NextDouble() > 0.9)
				 {
					 TVP->SvO2 += double(TVP->SvO2*(Rx->NextDouble()-0.5)*X);
				 }
				 if (Rx->NextDouble() > 0.9)
				 {
					 TVP->CVP += double(TVP->CVP*(Rx->NextDouble()-0.5)*X);
				 }
				 if (Rx->NextDouble() > 0.9)
				 {
					 TVP->PA_DP += double(TVP->PA_DP*(Rx->NextDouble()-0.5)*X);
					 TVP->PA_SP += double(TVP->PA_SP*(Rx->NextDouble()-0.5)*X);
				 }
				 if (Rx->NextDouble() > 0.85)
				 {
					 TVP->PPV += double(TVP->PPV*(Rx->NextDouble()-0.5)*X*2);
					 TVP->PVI += double(TVP->PVI*(Rx->NextDouble()-0.5)*X*2);
					 TVP->SVV += double(TVP->SVV*(Rx->NextDouble()-0.5)*X*2);
				 }
				 if (Rx->NextDouble() > 0.9)
				 {
					 TVP->SpHbx += double(TVP->SpHbx*(Rx->NextDouble()-0.5)*X*2);
				 }
				 if (Rx->NextDouble() > 0.9)
				 {
					 TVP->SV += double(TVP->SV*(Rx->NextDouble()-0.5)*X);
				 }
				 if (Rx->NextDouble() > 0.9)
				 {
					 TVP->SVR += double(TVP->SVR*(Rx->NextDouble()-0.5)*X*3);
				 }
				 // Update Graphs or no?
				 if (this->ThirtySecondCounter >=15)
				 {
					 this->ThirtySecondCounter = 0;
					 this->UpdateMonitorSlots(TVP,0,1);
				 }
				 else
				 {
					this->UpdateMonitorSlots(TVP,0,0);
				 }
				 this->WMon->updateVitals(TVP);
				 }
		 }
private: System::Void button7_Click(System::Object^ sender, System::EventArgs^ e)
		 {
			int speed = WMon->decreaseSweep(this,e);
			this->lab_sweep->Text = (speed*6).ToString();
		 }

private: System::Void button11_Click(System::Object^  sender, System::EventArgs^  e) {
			 WMon->ChangeWaves("Art_160_BIGEM.txt","EKG_320_BIGEM.txt","EKGV_320_BIGEM.txt","Pleth_80_BIGEM.txt",80,41);
		 }
private: System::Void button9_Click(System::Object^  sender, System::EventArgs^  e) {
			 int speed = WMon->increaseSweep(this,e);
			 this->lab_sweep->Text = (speed*6).ToString();
		 }

private: System::Void showCases(void) {
			try {
				IO::StreamReader^ FILE = gcnew IO::StreamReader("cases.txt"); 
				ArrayList^ A = gcnew ArrayList();

				// Skip first line
				FILE->ReadLine();
				this->comboBox_Case->Items->Clear();
				this->comboBox_State->Items->Clear();
				this->comboBox_Case->Items->Add("Select Case");
				while (!FILE->EndOfStream)
				{
					// Scan 'em all
					String^ line = FILE->ReadLine();
					if (line->Length > 1)
					{
						// Need the third field
						int x = line->IndexOf('\t');
						x = line->IndexOf('\t',x+1);
						int y = line->IndexOf('\t',x+1);
						if (y-x > 1) {
							String^ C = line->Substring(x+1,y-x-1);
							this->comboBox_Case->Items->Add(C);
						}
					}
				}
				this->comboBox_Case->SelectedIndex=0;
				this->comboBox_State->Enabled = false;
				FILE->Close();
			}
			catch (System::Exception^ e)
			{
				e;
				this->comboBox_Case->Items->Clear();
				this->comboBox_State->Items->Clear();
				this->comboBox_Case->Enabled = false;
				this->comboBox_State->Enabled=false;
			}
		 }
private: System::Void showStatesForCase(int caseID) {
			IO::StreamReader^ FILE = gcnew IO::StreamReader("cases.txt"); 
			ArrayList^ A = gcnew ArrayList();

			// Skip first line
			FILE->ReadLine();
			this->comboBox_State->Items->Clear();
			this->comboBox_State->Items->Add("Select State");
			this->comboBox_State->Enabled = true;

			while (!FILE->EndOfStream)
			{
				// Scan for case
				String^ line = FILE->ReadLine();
				if ((line->Length > 1)&&(line->Substring(0,line->IndexOf('\t'))->CompareTo(caseID.ToString()) == 0))
				{
					// Need the fourth field
					int x = line->IndexOf('\t');
					x = line->IndexOf('\t',x+1);
					x = line->IndexOf('\t',x+1);
					int y = line->IndexOf('\t',x+1);
					if (y-x > 1) {
						String^ C = line->Substring(x+1,y-x-1);
						this->comboBox_State->Items->Add(C);
					}
				}
			}
			this->comboBox_State->SelectedIndex=0;
			FILE->Close();
		 }

private: String^ LoadDisplayPresets(int caseID,int stateID){
			IO::StreamReader^ FILE = gcnew IO::StreamReader("cases.txt"); 
			ArrayList^ A = gcnew ArrayList();

			while (!FILE->EndOfStream)
			{
				// Find the correct line
				String^ line = FILE->ReadLine();
				if (line->Length > 1)
				{
					//MessageBox::Show(line);
					// Only search the first 8 chars, we shouldn't have anything past 1000,1000!
					if (line->IndexOf(caseID.ToString() + "\t" + stateID.ToString(),0,10) > -1)
					{
						//MessageBox::Show(line);
						//parse the line apart into our array
						while (line->IndexOf('\t') > -1)
						{
							String^ V = line->Substring(0,line->IndexOf('\t'));
							//MessageBox::Show(V);
							A->Add(V);
							line = line->Substring(line->IndexOf('\t')+1,line->Length - line->IndexOf('\t')-1);
						}
						// Add what's left of the line to the collection
						A->Add(line);
						//MessageBox::Show(line);
					
						FILE->Close();

						// Now do something productive with this array	
						if (A->Count < 10) 
						{
							// We missed it
							return "Abnormal Line Configuration";
						}

						// Be sure systems are linked
						this->checkBox3->Checked = true;

						// Push this into a vitals object or just into the fields?  
						// Let's just push to fields
						if (dynamic_cast<String^>(A->default[4])->Length > 0) {this->dis_HR->Text = dynamic_cast<String^>(A->default[4]);}
						if (dynamic_cast<String^>(A->default[5])->Length > 0) {this->dis_SV->Text = dynamic_cast<String^>(A->default[5]);}
						if (dynamic_cast<String^>(A->default[6])->Length > 0) {this->dis_SBP->Text = dynamic_cast<String^>(A->default[6]);}
						if (dynamic_cast<String^>(A->default[7])->Length > 0) {this->dis_DBP->Text = dynamic_cast<String^>(A->default[7]);}
						if (dynamic_cast<String^>(A->default[8])->Length > 0) {this->dis_CVP->Text = dynamic_cast<String^>(A->default[8]);}
						if (dynamic_cast<String^>(A->default[9])->Length > 0) {this->dis_PASP->Text = dynamic_cast<String^>(A->default[9]);}
						if (dynamic_cast<String^>(A->default[10])->Length > 0) {this->dis_PADP->Text = dynamic_cast<String^>(A->default[10]);}
						if (dynamic_cast<String^>(A->default[11])->Length > 0) {this->dis_HgB->Text = dynamic_cast<String^>(A->default[11]);}
						if (dynamic_cast<String^>(A->default[12])->Length > 0) {this->dis_Hgbx->Text = dynamic_cast<String^>(A->default[12]);}
						if (dynamic_cast<String^>(A->default[13])->Length > 0) {this->trackBar_Preload->Value = Int16::Parse(dynamic_cast<String^>(A->default[13]));this->trackBar_Preload_Scroll(this,System::EventArgs::Empty);}
						if (dynamic_cast<String^>(A->default[14])->Length > 0) {this->dis_SP02->Text = dynamic_cast<String^>(A->default[14]);}
						if (dynamic_cast<String^>(A->default[15])->Length > 0) {this->dis_SvO2->Text = dynamic_cast<String^>(A->default[15]);}
						if (dynamic_cast<String^>(A->default[16])->Length > 0) {this->dis_SCvO2->Text = dynamic_cast<String^>(A->default[16]);}
						if (dynamic_cast<String^>(A->default[17])->Length > 0) {this->dis_PACO2->Text = dynamic_cast<String^>(A->default[17]);}
						if (dynamic_cast<String^>(A->default[18])->Length > 0) {this->UD_Resp->SelectedIndex = 40 - int::Parse(dynamic_cast<String^>(A->default[18]));}
						if (dynamic_cast<String^>(A->default[19])->Length > 0) {this->dis_CoreTemp->Text = dynamic_cast<String^>(A->default[19]);}
						if (dynamic_cast<String^>(A->default[20])->Length > 0) {this->dis_Deadspace->Text = dynamic_cast<String^>(A->default[20]);}
						if (dynamic_cast<String^>(A->default[21])->Length > 0) {this->dis_Outflow->Text = dynamic_cast<String^>(A->default[21]);}
						
						this->LoadedCommands = dynamic_cast<String^>(A->default[22]);

						return "";
					}	
				}
			}
			FILE->Close();
			return "Case and State not found";
		 }
private: String^ ActOnStringCommands(String^ M)
		 {
				String^ ABORT="";

				// Now I need to parse the monitors field and figure out what to do with it.
				M->Replace("'","");
				//MessageBox::Show(M);
				while ( (M->IndexOf('+') > -1) || (M->IndexOf('-') > -1))
				{
					int plus = M->IndexOf('+',1);
					int minus = M->IndexOf('-',1);
					int x;
					int mode;
					if ((plus>-1)&&(minus>-1))
					{
						if (plus<minus) {x=plus;}
						else {x=minus;}
					}
					else if (minus>-1) {x=minus;}
					else if (plus>-1) {x=plus;}
					else {x=M->Length;}

					String^ B = M->Substring(0,1);
					String^ C = M->Substring(1,x-1);
					M = M->Substring(x,M->Length-x);
					if (B->CompareTo("-") == 0) {mode=-1;} else {mode=1;}

					//MessageBox::Show(mode.ToString() + " " + C);

					// Now we have a string and a setting; figure out what to do with it.
					if (mode == -1)
					{
						if (C->CompareTo("ALINE") == 0)
						{
							this->cb_Aline->Checked = false;
							this->cb_Aline_CheckedChanged(this,EventArgs::Empty);
						}
						else if (C->Substring(0,1)->CompareTo("1") == 0)
						{
							this->SlotSet(1,"Off");
						}
						else if (C->Substring(0,1)->CompareTo("2") == 0)
						{
							this->SlotSet(2,"Off");
						}
						else if (C->Substring(0,1)->CompareTo("3") == 0)
						{
							this->SlotSet(3,"Off");
						}
						else if (C->Substring(0,1)->CompareTo("4") == 0)
						{
							this->SlotSet(4,"Off");
						}
						else if (C->Substring(0,1)->CompareTo("5") == 0)
						{
							this->SlotSet(5,"Off");
						}
						else if (C->Substring(0,1)->CompareTo("6") == 0)
						{
							this->SlotSet(6,"Off");
						}
						else if (C->CompareTo("CO2") == 0)
						{
							this->cb_ETCO2->Checked = false;
							this->cb_ETCO2_CheckedChanged(this,EventArgs::Empty);
						}
						else 
						{
							ABORT += " x(-1):" + C;
						}
					}
					if (mode == 1)
					{
						// We're adding something here
						if (C->Substring(0,1)->CompareTo("1") == 0)
						{
							this->SlotSet(1,C->Substring(1,C->Length-1));
						}
						else if (C->Substring(0,1)->CompareTo("2") == 0)
						{
							this->SlotSet(2,C->Substring(1,C->Length-1));
						}
						else if (C->Substring(0,1)->CompareTo("3") == 0)
						{
							this->SlotSet(3,C->Substring(1,C->Length-1));
						}
						else if (C->Substring(0,1)->CompareTo("4") == 0)
						{
							this->SlotSet(4,C->Substring(1,C->Length-1));
						}
						else if (C->Substring(0,1)->CompareTo("5") == 0)
						{
							this->SlotSet(5,C->Substring(1,C->Length-1));
						}
						else if (C->Substring(0,1)->CompareTo("6") == 0)
						{
							this->SlotSet(6,C->Substring(1,C->Length-1));
						}
						else if (C->CompareTo("ALINE") == 0)
						{
							this->cb_Aline->Checked = true;
							this->cb_Aline_CheckedChanged(this,EventArgs::Empty);
						}
						else if (C->CompareTo("CO2") == 0)
						{
							this->cb_ETCO2->Checked = true;
							this->cb_ETCO2_CheckedChanged(this,EventArgs::Empty);
						}
						else if (C->CompareTo("BIGEMINY") == 0)
						{
							this->LoadedRhythym = "BIGEMINY";
						}
						else 
						{
							ABORT += " x1:" + C;
						}

					}

				}
			this->LoadedCommands = "";
			return ABORT;
		 }
private: System::Void button12_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (this->comboBox_Case->SelectedIndex == 0) {return;}
			 if (this->comboBox_State->SelectedIndex == 0) {return;} 
			 this->LoadDisplayPresets(this->comboBox_Case->SelectedIndex,this->comboBox_State->SelectedIndex);
		 }
private: System::Void button13_Click(System::Object^  sender, System::EventArgs^  e) {
			 WMon->ChangeWaves("Art_80.txt","EKG_160.txt","EKGV_160.txt","Pleth_40.txt",40,21);
		 }
private: System::Void comboBox8_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
		}
private: System::Void comboBox_Case_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
			 if (this->comboBox_Case->SelectedIndex == 0) {return;}
			 this->showStatesForCase(this->comboBox_Case->SelectedIndex);
		 }
private: System::Void button14_Click(System::Object^  sender, System::EventArgs^  e) {
			 this->saveFileDialog1->ShowDialog();
			 this->textBox6->Text = saveFileDialog1->FileName;
		 }
private: System::Void checkBox4_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 if (this->checkBox4->Checked == true)
			 {
				 this->textBox6->Enabled = true;
				 this->button14->Enabled = true;
			 }
			 else 
			 {
				 this->textBox6->Enabled = false;
				 this->button14->Enabled = false;
			 }
		 }

private: System::Void pictureBox1_Click(System::Object^  sender, System::EventArgs^  e) {
			 System::Windows::Forms::MessageBox::Show("Choose a filename to log the results of the simulation run.  The simulator will save a new file with all of the vitals by time, as well as append a master summary file with summary statistics in the same directory.","File Logging Help",Windows::Forms::MessageBoxButtons::OK,Windows::Forms::MessageBoxIcon::Information);
		 }
private: System::Void button_chooseScript_Click(System::Object^  sender, System::EventArgs^  e) {
			 System::Windows::Forms::OpenFileDialog^ O = gcnew Windows::Forms::OpenFileDialog();
			 O->ShowDialog();
			 this->textBox_script->Text = O->FileName;
		 }
private: System::Void cb_DBrecord_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
		 }
private: System::Void pictureBox3_Click_1(System::Object^  sender, System::EventArgs^  e) {
			 System::Windows::Forms::MessageBox::Show("This prefix code will be used as the patient ID in the database.  It can be read by other programs to uniquely identify this patient and run.  If multiple runs are performed, the run number will be added to the ID code.","DB Recording Help",Windows::Forms::MessageBoxButtons::OK,Windows::Forms::MessageBoxIcon::Information);
		 }
private: System::Void timer_ControlWatcher_Tick(System::Object^  sender, System::EventArgs^  e) {
			 // We don't wait if the "listen to db" button isn't checked, we just start the next loop
			 if (!this->cb_DBlisten->Checked)
			 {
				 this->timer_ControlWatcher->Stop();
			  	 this->button1_Click(this,System::EventArgs::Empty);
				 return;
			 }
			 LIR::ControlResponse^ R = this->CheckControlTable("NewPatient");
			 if (R->CharValue->CompareTo("OK")==0)
			 {
				// The Database has responded OK, it's acknowledged the stop and is waiting for instructions
				// First, stop this timer
				this->timer_ControlWatcher->Stop();
				// So I guess we just push the damn power button again?
				this->button1_Click(this,System::EventArgs::Empty);
			 }
		 }
private: System::Void cb_MonitorOn_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 if (this->cb_MonitorOn->Checked==true)
			 {
				 WMon->Show();
				 //this->label_lidcoAlert->Visible = true;
			 }
			 else 
			 {
				 this->WMon->Hide();
				 //this->label_lidcoAlert->Visible = false;
			 }
		 }
private: System::Void button16_Click(System::Object^  sender, System::EventArgs^  e) {
			 this->Patient->AddMedication(DRUG_PRBC,50);
		 }
private: System::Void button15_Click(System::Object^  sender, System::EventArgs^  e) {
			 this->Patient->Bleed(50,false);
		 }
private: System::Void tabPage1_Click(System::Object^  sender, System::EventArgs^  e) {
		 }
private: System::Void textBox11_TextChanged(System::Object^  sender, System::EventArgs^  e) {
		 }
private: System::Void checkBoxGraph1_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 if (this->checkBoxGraph1->Checked)
			 {
				 this->comboBox1->SelectedIndex = 0;
				 this->comboBox1->Enabled = false;
				 this->comboBox3->SelectedIndex = 0;
				 this->comboBox3->Enabled = false;
			 }
			 else 
			 {
				this->comboBox1->Enabled = true;
				this->comboBox3->Enabled = true;
			 }
		 }
private: System::Void checkBoxGraph2_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
		 if (this->checkBoxGraph2->Checked)
			 {
				 this->comboBox2->SelectedIndex = 0;
				 this->comboBox2->Enabled = false;
				 this->comboBox4->SelectedIndex = 0;
				 this->comboBox4->Enabled = false;
			 }
			 else 
			 {
				this->comboBox2->Enabled = true;
				this->comboBox4->Enabled = true;
			 }
		 }
private: System::Void GetAvailableScripts(void)
		 {
			 System::IO::DirectoryInfo^ D = gcnew System::IO::DirectoryInfo(".");
			 array<System::IO::FileInfo^,1>^ SIMFILES = D->GetFiles("*_SimInfo.txt");
			
			 this->comboBox_SCRIPTS->Items->Clear();
			 this->comboBox_SCRIPTS->Items->Add("None");
			 
			 if (SIMFILES->Length > 0)
			 {
				 for(int i=0;i<SIMFILES->Length;i++)
				 {
					 this->comboBox_SCRIPTS->Items->Add(SIMFILES[i]->Name->Substring(0,SIMFILES[i]->Name->IndexOf("_SimInfo"))->Replace('_',' '));
				 }
			 }	

			 this->comboBox_SCRIPTS->SelectedIndex = 0;
				 
			 delete D;
		 }
private: System::Void Form1_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {

			 System::String^ S = "";
			 KEYS->Add(e->KeyChar.ToString());
			 while (KEYS->Count > 7) {KEYS->RemoveAt(0);}
			 for (int i=0; i<KEYS->Count; i++)
			 {
				 S += static_cast<System::String^>(KEYS[i]);
			 }
			 if (S->CompareTo("NIBBLER")==0)
			 {
				 System::Windows::Forms::MessageBox::Show(COMPILE_ID);
			 }
			 //this->label7->Text = S;
		 }
private: System::Void comboBox_SCRIPTS_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
			 if (this->comboBox_SCRIPTS->SelectedIndex == 0)
			 {
				 this->textBox_scenario->Text = "";
				 this->WMon->SetInteractiveMode(true);
			 }
			 else
			 {
				 System::String^ FILE = this->comboBox_SCRIPTS->SelectedItem->ToString();
				 FILE = FILE->Replace(' ','_');
				 try
				 {
					System::Collections::ArrayList^ BASE = gcnew System::Collections::ArrayList();
					System::IO::StreamReader^ R = gcnew System::IO::StreamReader(FILE+"_SimInfo.txt");
					this->textBox_scenario->Text = R->ReadLine();
					// cache the rest of the file until we confirm the script is good
					while (!R->EndOfStream)
					{
						BASE->Add(R->ReadLine());
					}
					R->Close();
					// We do this to confirm the script at least exists
					R = gcnew System::IO::StreamReader(FILE+"_SimScript.csv");
					R->Close();
					// Script at least exists, put it on deck
					this->textBox_script->Text = FILE+"_SimScript.csv";
					// And now load the rest of the defaults
					while (BASE->Count > 0)
					{
						System::String^ S = static_cast<System::String^>(BASE->default[0]);
						BASE->RemoveAt(0);
						if (S->IndexOf(':') > -1)
						{
							System::String^ V1;
							System::String^ V2;
							V1 = S->Substring(S->IndexOf(':')+1,S->IndexOf(',')-S->IndexOf(':')-1);
							V2 = S->Substring(S->IndexOf(',')+1,S->Length - S->IndexOf(',')-1);
							S = S->Substring(0,S->IndexOf(':'));
							if (S->CompareTo("HR")==0)
							{
								this->tb_baseHR->Text = V1;
								this->tb_maxHR->Text = V2;
							}
							if (S->CompareTo("SBP")==0)
							{
								this->tb_baseSBP->Text = V1;
								this->tb_maxSBP->Text = V2;
							}
							if (S->CompareTo("SepF")==0)
							{
								this->tb_baseSepF->Text = V1;
								this->tb_maxSepF->Text = V2;
							}
							if (S->CompareTo("CNT")==0)
							{
								this->tb_CNTmin->Text = V1;
								this->tb_CNTmax->Text = V2;
							}
							if (S->CompareTo("DBP")==0)
							{
								this->tb_baseDBP->Text = V1;
								this->tb_maxDBP->Text = V2;
							}
							if (S->CompareTo("CVP")==0)
							{
								this->tb_baseCVP->Text = V1;
								this->tb_maxCVP->Text = V2;
							}
							if (S->CompareTo("KG")==0)
							{
								this->tb_baseKG->Text = V1;
								this->tb_maxKg->Text = V2;
							}
							if (S->CompareTo("IN")==0)
							{
								this->tb_baseIn->Text = V1;
								this->tb_maxIn->Text = V2;
							}
							if (S->CompareTo("HGB")==0)
							{
								this->tb_lowHgb->Text = V1;
								this->tb_highHgb->Text = V2;
							}
							if (S->CompareTo("VOL")==0)
							{
								this->tb_BVmin->Text = V1;
								this->tb_BVmax->Text = V2;
							}
							if (S->CompareTo("CNT")==0)
							{
								this->tb_CNTmin->Text = V1;
								this->tb_CNTmax->Text = V2;
							}
						}
					}
					this->WMon->SetInteractiveMode(false);
				 }
				 catch (System::Exception^ e)
				 {
					//System::Windows::Forms::MessageBox::Show(e->ToString());
					this->textBox_scenario->Text = "This scenario is invalid";
					e;
				 }
			 }
		 }
private: System::Void checkBoxGraph2_CheckedChanged_1(System::Object^  sender, System::EventArgs^  e) {
			 if (this->checkBoxGraph2->Checked)
			 {
				 this->comboBox2->Enabled = false;
				 this->comboBox4->Enabled = false;
			 }
			 else
			 {
				 this->comboBox2->Enabled = true;
				 this->comboBox4->Enabled = true;
			 }
		 }
private: System::Void checkBox5_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			if (this->checkBox5->Checked)
			{
				WMon->ShowInteractivePanels();
			}
			else 
			{
				WMon->HideInteractivePanels();
			}
		 }

private: System::Void cb_BroadcastNet_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 if (this->cb_BroadcastNet->Checked)
			 {
				 this->tb_netadd->Enabled = true;
				 this->tb_netport->Enabled = true;
			 }
			 else 
			 {
				 this->tb_netadd->Enabled = false;
				 this->tb_netport->Enabled = false;
			 }
		 }
private: System::Void cb_listenNetwork_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 if (this->cb_listenNetwork->Checked)
			 {
				 this->tb_listenPort->Enabled = true;
			 }
			 else 
			 {
				 this->tb_listenPort->Enabled = true;
			 }
		 }
private: System::Void checkBoxBarGraph5_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
		  if (this->checkBoxBarGraph5->Checked)
			 {
				 WMon->Graph1->SetBarGraphModeOn();
			 }
			 else
			 {
				 WMon->Graph1->SetBarGraphModeOff();
			 }
		 }
private: System::Void checkBoxBarGraph6_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 if (this->checkBoxBarGraph6->Checked)
			 {
				 WMon->Graph2->SetBarGraphModeOn();
			 }
			 else
			 {
				 WMon->Graph2->SetBarGraphModeOff();
			 }
		 }
private: System::Void trackBar_ART_Scroll(System::Object^  sender, System::EventArgs^  e) {
			 WMon->SetArtScale(this->trackBar_ART->Value);
		 }
private: System::Void cb_Aline_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 if (this->cb_Aline->Checked == true) {WMon->SetARTon();}
			 else {WMon->SetARToff();}
		 }
private: System::Void cb_ETCO2_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 if (this->cb_ETCO2->Checked==true) {WMon->SetCO2on();}
			 else{WMon->SetCO2off();}
		 }
private: System::Void cb_PulseOx_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 if (this->cb_PulseOx->Checked==true) {WMon->SetPulseOxon();}
			 else{WMon->SetPulseOxoff();}
		 }
private: System::Void cb_EKG_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 if (this->cb_EKG->Checked==true) {WMon->SetEKGon();}
			 else{WMon->SetEKGoff();}
		 }
private: System::Void textBox6_TextChanged(System::Object^  sender, System::EventArgs^  e) {
}
};
}
