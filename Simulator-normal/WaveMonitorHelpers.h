/*
WaveMonitorHelpers.h
May 2011, JBR
required library for WaveMonitor.h
*/

#pragma once

namespace LIR {

	public ref class MonitorSlot
	{
	public: 
		String^ Name;
		bool active;
		Windows::Forms::GroupBox^ Box;
		Windows::Forms::Label^ Label;
		double value;

	public:
		MonitorSlot(void)
		{
			Name = "";
			active = 0;
		}

	};

	public ref class SlotOption
	{
	public:
		String^ Name;
		Drawing::Color^ Color;
		double Min;
		double Max;
		double Zero;
		bool BarGraph;

	public:
		SlotOption(String^ NN, Drawing::Color^ C, double NewMin, double NewMax, double NewZero, bool BarGraphTF)
		{
			Name=NN;
			Color=C;
			Min=NewMin;
			Max=NewMax;
			Zero=NewZero;
			BarGraph=BarGraphTF;

		}
	};

}
