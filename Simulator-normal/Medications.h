// Simulator.h
// Cardiodynamic simulator
// Copyright Sironis 2011, all rights reserved
// J. Rinehart

// 100-199: Crystalloids
// 200-299: Colloids

#pragma once

// Drips & Vasoactive Drugs 1-99
#define DRUG_EPHEDRINE 1		
#define	DRUG_NEOSYNEPHRINE 2	
#define DRUG_EPINEPHRINE 3		
#define DRUG_FENTANYL 4			
#define DRUG_VASOPRESSIN 5 
#define DRUG_NOREPINEPHRINE 6
#define DRUG_DOPAMINE 10
#define DRUG_DOBUTAMINE 7
#define DRUG_ESMOLOL 8
#define DRUG_NITROGLYCERINE 9

// All Crystalloid must be between 100-199 (LIR)
// Crystalloid dosages are always in ml
#define DRUG_LR 100
#define DRUG_NS 101
#define DRUG_PLASMALYTE 102

// All Colloids must be between 200-299 (LIR)
// Colloid dosages are always in ml
#define DRUG_ALBUMIN 200
#define DRUG_HESPAN 201
#define DRUG_VOLUVEN 202

// Blood products
#define DRUG_PRBC 300
#define DRUG_FFP 301
#define DRUG_PLATELETS 302

// Generic drugs
#define DRUG_ANTIBIOTIC 401


/*====================================================================================*/
// Drug infusion Limits for the Guardian CL Program
//
// NOTE: 'MAX_STEP' is the maximum allowed INCREASE for a change
//       The maximum permitted DECREASE is one-quarter this amount.
//
/*====================================================================================*/

// NANOGRAMS per min
#define DEFINED_NEO_SOFT	 400000
#define DEFINED_NEO_HARD	 400000
#define DEFINED_NEO_STEP	   2000
#define DEFINED_NEO_MAX_STEP  60000

// MICRO-UNITS per min
#define DEFINED_VASO_SOFT		100
#define DEFINED_VASO_HARD		200
#define DEFINED_VASO_STEP		  1
#define DEFINED_VASO_MAX_STEP    40

// NANOGRAMS per min
#define DEFINED_NOREPI_SOFT		15000
#define DEFINED_NOREPI_HARD		30000
#define DEFINED_NOREPI_STEP		  100
#define DEFINED_NOREPI_MAX_STEP  4000


/*====================================================================================*/
//
// Drug Structures for CD-SIM
//
/*====================================================================================*/

public ref struct DrugData {
	unsigned int DrugID;
	bool BolusIsInfusion;
	double BolusMin;
	double BolusMax;
	double BolusStep;
	double BolusSecondsPerUnitBolusDose;

	// mcg/Kg/min
	bool InfusionOK;
	double dripMin;
	double dripMax;
	double dripStep;

	System::String^ BolusUnits;
	System::String^ DripUnits;
	System::String^ DrugName;
};

public ref class Drugs {
	public:
		System::Collections::ArrayList^ DrugList;
	
	Drugs(void) {
		// I'm going to just do this manually
		// Enter the drugs alphabetically to avoid sorting later

		DrugList = gcnew System::Collections::ArrayList();
		
		DrugData^ D = gcnew DrugData;
		D->DrugID = DRUG_ALBUMIN;
		D->DrugName = "Albumin";
		/*D->BolusIsInfusion = true;
		D->BolusMax = 500;
		D->BolusMin = 250;
		D->BolusSecondsPerUnitBolusDose = 2.0;
		D->BolusStep = 250;*/
		D->BolusIsInfusion = true;
		D->BolusMax = 1000;
		D->BolusMin = 250;
		D->BolusSecondsPerUnitBolusDose = 0.1428;
		D->BolusStep = 250;
		D->dripMax = 0;
		D->dripMin = 0;
		D->dripStep = 0;
		D->InfusionOK = 0;
		D->BolusUnits = "ml";
		D->DripUnits = "";
		DrugList->Add(D);

		D = gcnew DrugData;
		D->DrugID = DRUG_ANTIBIOTIC;
		D->DrugName = "Antibiotic";
		D->BolusIsInfusion = false;
		D->BolusMax = 1;
		D->BolusMin = 1;
		D->BolusSecondsPerUnitBolusDose = 0;
		D->BolusStep = 1;
		D->dripMax = 0;
		D->dripMin = 0;
		D->dripStep = 0;
		D->InfusionOK = 0;
		D->BolusUnits = "g";
		D->DripUnits = "";
		//DrugList->Add(D);

		D = gcnew DrugData;
		D->DrugID = DRUG_DOPAMINE;
		D->DrugName = "Dopamine";
		D->BolusIsInfusion = false;
		D->BolusMax = 0;
		D->BolusMin = 0;
		D->BolusStep = 0;
		D->BolusSecondsPerUnitBolusDose = 0;
		D->InfusionOK = true;
		D->dripMin = 2;
		D->dripMax = 20;
		D->dripStep = 2;
		D->BolusUnits = "";
		D->DripUnits = "mcg/kg/min";
		//DrugList->Add(D);

		D = gcnew DrugData;
		D->DrugID = DRUG_DOBUTAMINE;
		D->DrugName = "Dobutamine";
		D->BolusIsInfusion = false;
		D->BolusMax = 0;
		D->BolusMin = 0;
		D->BolusStep = 0;
		D->BolusSecondsPerUnitBolusDose = 0;
		D->InfusionOK = true;
		D->dripMin = 2;
		D->dripMax = 20;
		D->dripStep = 2;
		D->BolusUnits = "";
		D->DripUnits = "mcg/kg/min";
		//DrugList->Add(D);

		D = gcnew DrugData;
		D->DrugID = DRUG_EPHEDRINE;
		D->DrugName = "Ephedrine";
		D->BolusIsInfusion = false;
		D->BolusMax = 10;
		D->BolusMin = 5;
		D->BolusStep = 5;
		D->BolusSecondsPerUnitBolusDose = 0;
		D->InfusionOK = false;
		D->dripMin = 0;
		D->dripMax = 0;
		D->dripStep = 0;
		D->BolusUnits = "mg";
		D->DripUnits = "";
		//DrugList->Add(D);

		D = gcnew DrugData;
		D->DrugID = DRUG_EPINEPHRINE;
		D->DrugName = "Epinephrine";
		D->BolusIsInfusion = false;
		D->BolusMax = 1000;
		D->BolusMin = 100;
		D->BolusStep = 100;
		D->BolusSecondsPerUnitBolusDose = 0;
		D->InfusionOK = true;
		D->dripMin = 1;
		D->dripMax = 20;
		D->dripStep = 1;
		D->BolusUnits = "mcg";
		D->DripUnits = "mcg/min";
		//DrugList->Add(D);

		D = gcnew DrugData;
		D->DrugID = DRUG_ESMOLOL;
		D->DrugName = "Esmolol";
		D->BolusIsInfusion = false;
		D->BolusMax = 50;
		D->BolusMin = 10;
		D->BolusStep = 10;
		D->BolusSecondsPerUnitBolusDose = 0;
		D->InfusionOK = true;
		D->dripMin = 5;
		D->dripMax = 30;
		D->dripStep = 5;
		D->BolusUnits = "mg";
		D->DripUnits = "mg/min";
		//DrugList->Add(D);

		D = gcnew DrugData;
		D->DrugID = DRUG_FENTANYL;
		D->DrugName = "Fentanyl";
		D->BolusIsInfusion = false;
		D->BolusMax = 250;
		D->BolusMin = 50;
		D->BolusStep = 50;
		D->BolusSecondsPerUnitBolusDose = 0;
		D->InfusionOK = true;
		D->dripMin = 0.5;
		D->dripMax = 5;
		D->dripStep = 0.5;
		D->BolusUnits = "mcg";
		D->DripUnits = "mcg/kg/hr";
		//DrugList->Add(D);

		D = gcnew DrugData;
		D->DrugID = DRUG_FFP;
		D->DrugName = "FFP";
		D->BolusIsInfusion = true;
		D->BolusMax = 250;
		D->BolusMin = 250;
		D->BolusStep = 250;
		D->BolusSecondsPerUnitBolusDose = 2.0;
		D->InfusionOK = true;
		D->dripMin = 0;
		D->dripMax = 0;
		D->dripStep = 0;
		D->BolusUnits = "ml";
		D->DripUnits = "";
		//DrugList->Add(D);

		D = gcnew DrugData;
		D->DrugID = DRUG_HESPAN;
		D->DrugName = "Hespan";
		D->BolusIsInfusion = true;
		D->BolusMax = 500;
		D->BolusMin = 250;
		D->BolusSecondsPerUnitBolusDose = 2.0;
		D->BolusStep = 250;
		D->dripMax = 0;
		D->dripMin = 0;
		D->dripStep = 0;
		D->InfusionOK = 0;
		D->BolusUnits = "ml";
		D->DripUnits = "";
		//DrugList->Add(D);
		
		D = gcnew DrugData;
		D->DrugName = "Lactated Ringers";
		D->DrugID = DRUG_LR;
		D->BolusIsInfusion = true;
		D->BolusMax = 1000;
		D->BolusMin = 250;
		D->BolusSecondsPerUnitBolusDose = 2.0;
		D->BolusStep = 250;
		D->dripMax = 1000;
		D->dripMin = 100;
		D->dripStep = 100;
		D->InfusionOK = 1;
		D->BolusUnits = "ml";
		D->DripUnits = "ml/hr";
		//DrugList->Add(D);

		D = gcnew DrugData;
		D->DrugID = DRUG_NITROGLYCERINE;
		D->DrugName = "Nitroglycerine";
		D->BolusIsInfusion = false;
		D->BolusMax = 20;
		D->BolusMin = 5;
		D->BolusStep = 5;
		D->BolusSecondsPerUnitBolusDose = 0;
		D->InfusionOK = true;
		D->dripMin = 1;
		D->dripMax = 20;
		D->dripStep = 1;
		D->BolusUnits = "mcg";
		D->DripUnits = "mcg/min";
		//DrugList->Add(D);
		
		D = gcnew DrugData;
		D->DrugName = "Norepinephrine";
		D->DrugID = DRUG_NOREPINEPHRINE;
		D->BolusIsInfusion = false;
		D->BolusMax = 20;
		D->BolusMin = 5;
		D->BolusSecondsPerUnitBolusDose = 0;
		D->BolusStep = 5;
		D->dripMax = 0.5;
		D->dripMin = 0.05;
		D->dripStep = 0.05;
		D->InfusionOK = 1;
		D->BolusUnits = "mcg";
		D->DripUnits = "mcg/kg/min";
		//DrugList->Add(D);

		D = gcnew DrugData;
		D->DrugName = "Normal Saline";
		D->DrugID = DRUG_NS;
		D->BolusIsInfusion = true;
		D->BolusMax = 1000;
		D->BolusMin = 250;
		D->BolusSecondsPerUnitBolusDose = 0.1428;
		D->BolusStep = 250;
		D->dripMax = 1000;
		D->dripMin = 100;
		D->dripStep = 100;
		D->InfusionOK = 1;
		D->BolusUnits = "ml";
		D->DripUnits = "ml/min";
		DrugList->Add(D);

		D = gcnew DrugData;
		D->DrugName = "Phenylephrine";
		D->DrugID = DRUG_NEOSYNEPHRINE;
		D->BolusIsInfusion = false;
		D->BolusMax = 200;
		D->BolusMin = 100;
		D->BolusSecondsPerUnitBolusDose = 0;
		D->BolusStep = 100;
		D->dripMax = 0.5;
		D->dripMin = 0.05;
		D->dripStep = 0.05;
		D->InfusionOK = 1;
		D->BolusUnits = "mcg";
		D->DripUnits = "mcg/kg/min";
		//DrugList->Add(D);

		D = gcnew DrugData;
		D->DrugName = "Plasmalyte";
		D->DrugID = DRUG_PLASMALYTE;
		D->BolusIsInfusion = true;
		D->BolusMax = 1000;
		D->BolusMin = 250;
		D->BolusSecondsPerUnitBolusDose = 2.0;
		D->BolusStep = 250;
		D->dripMax = 1000;
		D->dripMin = 100;
		D->dripStep = 100;
		D->InfusionOK = 1;
		D->BolusUnits = "ml";
		D->DripUnits = "ml/hr";
		//DrugList->Add(D);

		D = gcnew DrugData;
		D->DrugName = "Platelets";
		D->DrugID = DRUG_PLATELETS;
		D->BolusIsInfusion = true;
		D->BolusMax = 250;
		D->BolusMin = 250;
		D->BolusSecondsPerUnitBolusDose = 2.0;
		D->BolusStep = 250;
		D->dripMax = 0;
		D->dripMin = 0;
		D->dripStep = 0;
		D->InfusionOK = 0;
		D->BolusUnits = "ml";
		D->DripUnits = "";
		//DrugList->Add(D);

		D = gcnew DrugData;
		D->DrugID = DRUG_PRBC;
		D->DrugName = "Packed Red Cells";
		/*D->BolusIsInfusion = true;
		D->BolusMax = 400;
		D->BolusMin = 400;
		D->BolusSecondsPerUnitBolusDose = 2.0;
		D->BolusStep = 400;*/
		D->BolusIsInfusion = true;
		D->BolusMax = 1000;
		D->BolusMin = 250;
		D->BolusSecondsPerUnitBolusDose = 0.1428;
		D->BolusStep = 250;
		D->dripMax = 0;
		D->dripMin = 0;
		D->dripStep = 0;
		D->InfusionOK = 0;
		D->BolusUnits = "ml";
		D->DripUnits = "";
		DrugList->Add(D);

		D = gcnew DrugData;
		D->DrugID = DRUG_VASOPRESSIN;
		D->DrugName = "Vasopressin";
		D->BolusIsInfusion = false;
		D->BolusMax = 1;  // This is huge
		D->BolusMin = 0.1;
		D->BolusSecondsPerUnitBolusDose = 0;
		D->BolusStep = 0.1;
		D->dripMax = 0.04;
		D->dripMin = 0.01;
		D->dripStep = 0.01;
		D->InfusionOK = 1;
		D->BolusUnits = "units";
		D->DripUnits = "units/min";
		//DrugList->Add(D);

		D = gcnew DrugData;
		D->DrugID = DRUG_VOLUVEN;
		D->DrugName = "Voluven";
		D->BolusIsInfusion = true;
		D->BolusMax = 500;
		D->BolusMin = 250;
		D->BolusSecondsPerUnitBolusDose = 2.0;
		D->BolusStep = 250;
		D->dripMax = 0;
		D->dripMin = 0;
		D->dripStep = 0;
		D->InfusionOK = 0;
		D->BolusUnits = "ml";
		D->DripUnits = "";
		//DrugList->Add(D);
		
	}

DrugData^ GetDrugByName(System::String^ Name) {
		DrugData^ D;
		for (int i=0; i<DrugList->Count; i++)
		{
			DrugData^ D = static_cast<DrugData^>(DrugList[i]);
			if (D->DrugName->CompareTo(Name) == 0) {return D;}
		}
		D = gcnew DrugData();
		D->BolusIsInfusion = false;
		D->BolusMax=0;
		D->BolusMin=0;
		D->BolusSecondsPerUnitBolusDose = 0;
		D->BolusStep=0;
		D->BolusUnits = "NA";
		D->dripMax = 0;
		D->dripMin = 0;
		D->dripStep = 0;
		D->DripUnits = "NA";
		D->DrugName = "NA";
		D->DrugID = 0;
		D->InfusionOK = false;
		return D;
	}

DrugData^ GetDrugByID(int X) {
		DrugData^ D;
		for (int i=0; i<DrugList->Count; i++)
		{
			DrugData^ D = static_cast<DrugData^>(DrugList[i]);
			if (X == D->DrugID) {return D;}
		}
		D = gcnew DrugData();
		D->BolusIsInfusion = false;
		D->BolusMax=0;
		D->BolusMin=0;
		D->BolusSecondsPerUnitBolusDose = 0;
		D->BolusStep=0;
		D->BolusUnits = "NA";
		D->dripMax = 0;
		D->dripMin = 0;
		D->dripStep = 0;
		D->DripUnits = "NA";
		D->DrugName = "NA";
		D->DrugID = 0;
		D->InfusionOK = false;
		return D;
	}


};






