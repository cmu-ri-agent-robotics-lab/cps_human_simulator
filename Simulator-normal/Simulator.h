// Simulator.h
// Cardiodynamic simulator
// Copyright Sironis 2011, all rights reserved
// author J. Rinehart

#include "Medications.h"

#define NO_FLUID_SHIFTS

#define DEFINED_CPLv 0.45		//0.45
#define DEFINED_CPLratio 20		// the UNSTRESSED "dead" state
#define DEFINED_XA_FACTOR 3
#define DEFINED_XV_FACTOR 0.06	// 0.06
#define DEFINED_MaxCO 10
#define DEFINED_MAPset 85
#define DEFINED_HRgain 1.7
#define DEFINED_SVRgain 0.4
#define DEFINED_MaxSVR 50
#define DEFINED_MaxHR 200
#define DEFINED_nSV 1.1			//1.9
#define DEFINED_KC_FACTOR 0.000012

// seconds
#define DEFINED_ALPHA_STIM_HALFLIFE 120
#define DEFINED_BETA_STIM_HALFLIFE 300
#define DEFINED_DOPA_STIM_HALFLIFE 200
#define DEFINED_INJECTION_LAG_TIME 8

#pragma once

namespace Simulator {
double least(double X, double Y)
{
	if (X > Y)
	{
		return Y;
	}
	return X;
}
double greatest(double X, double Y)
{
	if (X > Y)
	{
		return X;
	}
	return Y;
}

int greatest(int X, int Y)
{
	if (X > Y) 
	{
		return X;
	}
	return Y;
}

public ref class ScriptEntry
{
public:
	int seconds;
	String^ command;
	String^ ScriptPhaseText;
	double CrystalloidPerSecond;
	double ColloidPerSecond;
	double PRBCPerSecond;
	double BloodLossPerSecond;
	double EventProbability;
	double SympatheticStim;
	double AlphaStim;
	double ParasympatheticStim;
	double Contractility;
	double SepsisFactor; // 0-8 with decimals just fine

public: ScriptEntry(void)
		{
			seconds=0;
			command="";
			ScriptPhaseText="";
			CrystalloidPerSecond=0;
			ColloidPerSecond=0;
			PRBCPerSecond=0;
			EventProbability=0;
			BloodLossPerSecond=0;
			SympatheticStim=0;
			AlphaStim=0;
			ParasympatheticStim=0;
			Contractility=0;
			SepsisFactor=0;
		}
public: ScriptEntry^ CreateCopy(void)
		{
			ScriptEntry^ S = gcnew ScriptEntry();
			S->seconds = seconds;
			S->command = command;
			S->ScriptPhaseText = ScriptPhaseText;
			S->CrystalloidPerSecond = CrystalloidPerSecond;
			S->ColloidPerSecond = ColloidPerSecond;
			S->PRBCPerSecond = PRBCPerSecond;
			S->BloodLossPerSecond = BloodLossPerSecond;
			S->SympatheticStim = SympatheticStim;
			S->AlphaStim = AlphaStim;
			S->ParasympatheticStim = ParasympatheticStim;
			S->Contractility = Contractility;
			S->SepsisFactor = SepsisFactor;
			return S;
		}
};

public ref class VitalsPack
{
public:
	double Cm;
	double Kg;
	double BSA;
	double CI;
	double CO;
	double COr;
	double MAP;
	double DBP;
	double SBP;
	double SDPD;	// Sistolic/diastolic pressure difference
	double HR; 
	double SV;
	double SVlim;
	double SV50;
	double SVR;
	double CVP;
	double PA_DP;
	double PA_SP;
	double PPV;
	double SVV;
	double PVI;
	double FTc;
	double SPO2;
	double RR;
	double PAP;
	double PaCO2;
	double HgB;
	double SpHbx;
	double SvO2;
	double SCvO2;
	double SympatheticOutflow;
	double ParasympatheticOutflow;
	double DeadspaceR;
	double CoreTemp;
	double DynamicOutflow;
	double Preload;
	//double LVEDV;
	//double LVESV;			// Total "Ideal" Blood Volume
	double BloodVolumeArt;		// Unstressed volume in arterial tree
	double BloodVolumeVen;		// unstressed volume in venous system
	double LastBloodVolume; // used only for logging file
	double Extravascular;
	double RedCellMass;
	double PlateletMass;
	double CoagMass;
	double MCP;					// The resting "dead" system pressure
	double PaS;					// Pressure in the stressed state
	double VaS;					// Arterial volume in stressed state (1/3)
	double PvS;					// Venous Pressure in the stressed state
	double VvS;					// Venous Volume in the stressed state (2/3)
	double Contractility;		// This is the BASE contractility, from which calculation begins
	double SepsisFactor;			// a real number range 0-8 (0=none, 8=maximal)
	double AlphaStim;			// A real number 0-100
	double BetaStim;			// A real number 0-100
	double DopaStim;			// A real number 0-100, increases HR prop. with square of value

public: VitalsPack()
		{
			Cm=0;
			Kg=0;
			BSA=0;
			CI=0;
			CO=0;
			COr=0;
			MAP=0;
			DBP=0;
			SBP=0;
			SDPD=0;
			HR=0;
			SV=0;
			SVlim=0;
			SV50=0;
			SVR=0;
			CVP=0;
			FTc=0;
			PA_DP=0;
			PA_SP=0;
			PPV=0;
			SVV=0;
			PVI=0;
			SPO2=0;
			RR=0;
			PAP=0;
			PaCO2=0;
			SpHbx=0;
			SvO2=0;
			SCvO2=0;
			HgB=0;
			DeadspaceR=0;
			CoreTemp=0;
			SympatheticOutflow=0;
			ParasympatheticOutflow=0;
			DynamicOutflow=0;
			Preload=0;
			//LVEDV=0;
			//LVESV=0;
			BloodVolumeArt=0;
			BloodVolumeVen=0;
			Extravascular=0;
			RedCellMass=0;
			CoagMass=0;
			PlateletMass=0;
			LastBloodVolume=0;
			MCP=0;
			PaS=0;
			VaS=0;
			PvS=0;
			VvS=0;
			Contractility=0;
			SepsisFactor=0;
			AlphaStim=0;
			BetaStim=0;
			DopaStim=0;
		}
public: double TotalBloodVolume(void)
		{
			return BloodVolumeVen + BloodVolumeArt;
		}
public: void SetBloodVolume(double newVolume, bool KeepHgbValueConstant )
		{
			if (KeepHgbValueConstant)
			{
				this->RedCellMass = newVolume/this->TotalBloodVolume()*this->RedCellMass;
			}

			this->BloodVolumeVen = newVolume - BloodVolumeArt;
			if (BloodVolumeVen < 0)
			{
				if (BloodVolumeVen + BloodVolumeArt < 0)
				{
					this->BloodVolumeVen = 0;
					this->BloodVolumeArt = 0;
				}
				else
				{
					BloodVolumeArt += BloodVolumeVen;
					BloodVolumeVen=0;
				}
			}
		}
public: VitalsPack^ Clone(void)
		{
			VitalsPack^ V = gcnew VitalsPack();

			V->CO = CO;
			V->COr = COr;
			V->CI = CI;
			V->MAP = MAP;
			V->DBP = DBP;
			V->SBP = SBP;
			V->SDPD = SDPD;
			V->HR = HR;
			V->SV = SV;
			V->SVlim = SVlim;
			V->SV50 = SV50;
			V->SVR = SVR;
			V->CVP = CVP;
			V->PA_DP = PA_DP;
			V->PA_SP = PA_SP;
			V->PPV = PPV;
			V->SVV = SVV;
			V->PVI = PVI;
			V->FTc = FTc;
			V->SPO2 = SPO2;
			V->RR = RR;
			V->PaCO2 = PaCO2;
			V->SpHbx = SpHbx;
			V->SvO2 = SvO2;
			V->SCvO2 = SCvO2;
			V->HgB = HgB;
			V->DeadspaceR=DeadspaceR;
			V->CoreTemp=CoreTemp;
			V->DynamicOutflow = DynamicOutflow;
			V->SympatheticOutflow = SympatheticOutflow;
			V->SDPD = SDPD;
			V->Cm = Cm;
			V->Kg = Kg;
			V->BSA = BSA;
			V->Preload = Preload;
			//V->LVEDV = LVEDV;
			//V->LVESV = LVESV;
			V->BloodVolumeArt = BloodVolumeArt;
			V->BloodVolumeVen = BloodVolumeVen;
			V->Extravascular = Extravascular;
			V->RedCellMass = RedCellMass;
			V->PlateletMass = PlateletMass;
			V->CoagMass = CoagMass;
			V->PAP = PAP;
			V->ParasympatheticOutflow = ParasympatheticOutflow;
			V->PaS = PaS;
			V->MCP = MCP;
			V->PvS = PvS;
			V->VaS = VaS;
			V->VvS = VvS;
			V->SepsisFactor = SepsisFactor;
			V->Contractility = Contractility;
			V->AlphaStim = AlphaStim;
			V->BetaStim = BetaStim;
			return V;
		}
public: static System::Void InitializeOutputFile(String^ FNAME, String^ ExtraFields)
	   {
		   IO::StreamWriter^ FILE = gcnew IO::StreamWriter(FNAME,0);
		   FILE->Write("Time\tCm\tKg\tBSA\tCO\tCOr\tCI\tMAP\tSBP\tDBP\tSDPD\tHR\tSV\tSVR\t");
		   FILE->Write("CVP\tPA_SP\tPA_DP\tPPV\tSVV\tPVI\tFTc\tSPO2\tRR\tPAP\tPaCO2\tHgB\t");
		   FILE->Write("SpHbx\tSvO2\tScvO2\tSymp\tPsymp\tDeadSp\tTemp\tDOutflow\tPreload\t");
		   FILE->Write("BloodVolumeChange\tBloodVolume\tBlood Optimal\tBlood Optimal R\tBlood Art\tBlood Ven\t");
		   FILE->Write("Extravascular\tRedCellMass\tPlateletMass\tCoagMass\t");
		   FILE->Write("MCP\tPaS\tPvS\tVaS\tVvS\tContrct\tSepsisF\tSVlim\tSV50\tAlphaStim\tBetaStim\tDopaStim");
		   FILE->Write(ExtraFields);
		   FILE->Write("\n");
		   FILE->Close();
	   }
public: System::Void WriteStateToOutputFile(String^ FNAME, String^ TimeString, double BloodOptimal, double BloodOptimalR, String^ ExtraFields)
	   {
			IO::StreamWriter^ FILE = gcnew IO::StreamWriter(FNAME,1);
			FILE->Write(TimeString + "\t");
			FILE->Write(Cm.ToString()+"\t");
			FILE->Write(Kg.ToString()+"\t");
			FILE->Write(BSA.ToString()+"\t");
			FILE->Write(CO.ToString()+"\t");
			FILE->Write(COr.ToString()+"\t");
			FILE->Write(CI.ToString()+"\t");
			FILE->Write(MAP.ToString()+"\t");
			FILE->Write(SBP.ToString()+"\t");
			FILE->Write(DBP.ToString()+"\t");
			FILE->Write(SDPD.ToString()+"\t");
			FILE->Write(HR.ToString()+"\t");
			FILE->Write(SV.ToString()+"\t");
			FILE->Write(SVR.ToString()+"\t");
			FILE->Write(CVP.ToString()+"\t");
			FILE->Write(PA_SP.ToString()+"\t");
			FILE->Write(PA_DP.ToString()+"\t");
			FILE->Write(PPV.ToString()+"\t");
			FILE->Write(SVV.ToString()+"\t");
			FILE->Write(PVI.ToString()+"\t");
			FILE->Write(FTc.ToString()+"\t");
			FILE->Write(SPO2.ToString()+"\t");
			FILE->Write(RR.ToString()+"\t");
			FILE->Write(PAP.ToString()+"\t");
			FILE->Write(PaCO2.ToString()+"\t");
			FILE->Write(HgB.ToString()+"\t");
			FILE->Write(SpHbx.ToString()+"\t");
			FILE->Write(SvO2.ToString()+"\t");
			FILE->Write(SCvO2.ToString()+"\t");
			FILE->Write(SympatheticOutflow.ToString()+"\t");
			FILE->Write(ParasympatheticOutflow.ToString()+"\t");
			FILE->Write(DeadspaceR.ToString()+"\t");
			FILE->Write(CoreTemp.ToString()+"\t");
			FILE->Write(DynamicOutflow.ToString()+"\t");
			FILE->Write(Preload.ToString()+"\t");
			//FILE->Write(LVEDV.ToString()+"\t");
			//FILE->Write(LVESV.ToString()+"\t");
			
			FILE->Write(((BloodVolumeArt+BloodVolumeVen)-LastBloodVolume).ToString()+"\t");
			LastBloodVolume = (BloodVolumeArt+BloodVolumeVen);

			FILE->Write((BloodVolumeArt+BloodVolumeVen).ToString()+"\t");
			
			// Blood R is now blood Optimal
			FILE->Write(BloodOptimal.ToString()+"\t");
			FILE->Write(BloodOptimalR.ToString()+"\t");

			FILE->Write(BloodVolumeArt.ToString()+"\t");
			FILE->Write(BloodVolumeVen.ToString()+"\t");
			FILE->Write(Extravascular.ToString()+"\t");
			FILE->Write(RedCellMass.ToString()+"\t");
			FILE->Write(PlateletMass.ToString()+"\t");
			FILE->Write(CoagMass.ToString()+"\t");
			FILE->Write(MCP.ToString()+"\t");
			FILE->Write(PaS.ToString()+"\t");
			FILE->Write(PvS.ToString()+"\t");
			FILE->Write(VaS.ToString()+"\t");
			FILE->Write(VvS.ToString()+"\t");
			FILE->Write(Contractility.ToString()+"\t");
			FILE->Write(SepsisFactor.ToString()+"\t");
			FILE->Write(SVlim.ToString()+"\t");
			FILE->Write(SV50.ToString()+"\t");
			FILE->Write(AlphaStim.ToString()+"\t");
			FILE->Write(BetaStim.ToString()+"\t");
			FILE->Write(DopaStim.ToString()+"\t");

			FILE->Write(ExtraFields);

			FILE->Write("\n");
			FILE->Close();
	   }

};

public ref struct Injection
{
	int DrugID;
	double Dose;
	int TimeSinceInjection;
};



public ref class CDSimPatient
{	
public: 
	VitalsPack^ Base;
	VitalsPack^ StartVitals;
	VitalsPack^ Vitals;
	int SimSeconds;
	double EPHEDRINE;
	double NEO;
	double NARC;
	double DOPA;
	double DOBS;
	double NITRO;
	double EPI;
	double VASO;
	double NOREPI;
	double ESMO;


private:

	double PTStability;
	double PTFlux;

	bool PPVSilence;
	bool PPVInvert;
	
	double PPVbias;			// Absolute %
	double PPVWander;		// Absolute %, max range +/-
	double PPVWanderRate;		// probability per second of change
	double PPVWanderActive;

	double SVErr;			// Absolute %, max range +/-
	double SVErrRate;		// probabilty per second of change
	double SVErrActive;

	double lastHRFlux;
	double lastSVFlux;
	double lastPPVwand;
	double lastBleed;

	double FluidOff;

	double SpHbModifier;				// Modifier to SpHb readings

	double LastDoseEpi;
	double LastDoseDobs;
	double LastDoseNorepi;
	double LastDoseVaso;
	double LastDoseNitro;
	double LastDoseEphed;
	double LastDoseNeo;
	double LastDoseCrystal;
	double LastDoseColl;
	double LastDoseFFP;
	double LastDoseEsmo;
	double LastDoseFent;
	double LastDoseDopa;
	double LastDoseBlood;
	double LastDosePlatelets;

	int RunMode;
	int ScriptOn;
	int ScriptSeconds;
	int ScriptPhase;
	unsigned char LastStimTicks;

	System::Random^ Rx;
	System::String^ ScriptPhaseDescription;

	Simulator::ScriptEntry^ ActiveScript;

	ArrayList^ MAPLOG;
	ArrayList^ SimScript;
	ArrayList^ ScriptPhases;
	ArrayList^ Injections;
	ArrayList^ Script;

public:
	// These are just some summary things for tracking
	ArrayList^ LOG_CO;
	ArrayList^ LOG_SV;
	ArrayList^ LOG_HR;
	ArrayList^ LOG_MAP;

public: 
	CDSimPatient(double SBPmin, double SBPmax, double DBPmin, double DBPmax, double HRmin, double HRmax,
				 double Kgmin, double Kgmax, double Inchesmin, double Inchesmax,
				 double CVPmin, double CVPmax, double HGBmin, double HGBmax, double BVmin, double BVmax, 
				 double CNTmin, double CNTmax, double SepFmin, double SepFmax) 
	{
		Rx = gcnew System::Random();
		//Rx->Next(Int32, Int32);
		//Rx->NextDouble();

		Base = gcnew VitalsPack();

		this->LOG_CO = gcnew ArrayList();
		this->LOG_MAP = gcnew ArrayList();
		this->LOG_HR = gcnew ArrayList();
		this->LOG_SV = gcnew ArrayList();
	
	    LastDoseEpi=0;
	    LastDoseDobs=0;
	    LastDoseNorepi=0;
	    LastDoseVaso=0;
	    LastDoseNitro=0;
	    LastDoseEphed=0;
	    LastDoseNeo=0;
	    LastDoseCrystal=0;
	    LastDoseColl=0;
	    LastDoseFFP=0;
	    LastDoseEsmo=0;
	    LastDoseFent=0;
	    LastDoseDopa=0;
	    LastDoseBlood=0;
	    LastDosePlatelets=0;

		this->ScriptPhase = -1;
		this->lastBleed = 0;
		this->LastStimTicks = 0;

		Base->SBP=double(Rx->NextDouble()*(SBPmax-SBPmin)+SBPmin);
		Base->DBP=double(Rx->NextDouble()*(DBPmax-DBPmin)+DBPmin);
		//Base->LVEDV=double(Rx->NextDouble()*(LVEDVmax-LVEDVmin)+LVEDVmin);
		//Base->LVESV=double(Rx->NextDouble()*(LVESVmax-LVESVmin)+LVESVmin);
		//Base->SV = Base->LVEDV - Base->LVESV;
		Base->CVP=double(Rx->NextDouble()*(CVPmax-CVPmin)+CVPmin);
		Base->HR=double(Rx->NextDouble()*(HRmax-HRmin)+HRmin);
		Base->Kg=double(Rx->NextDouble()*(Kgmax-Kgmin)+Kgmin);
		Base->Cm=double((Rx->NextDouble()*(Inchesmax-Inchesmin)+Inchesmin)*2.54);
		Base->BSA=double(Math::Sqrt((Base->Kg*Base->Cm)/3600));
		Base->DeadspaceR = double(0.95);
		Base->MAP=double((Base->SBP+Base->DBP+Base->DBP)/3);
			if (Base->DBP != 0) {Base->SDPD=(Base->SBP/Base->DBP);}
			else {Base->SDPD=1;}

		// ALL BLOOD VOLUMES ARE IN LITERS---------------------------------------
		// These are *****UNSTRESSED VOLUMES*****
		// The "optimal" blood volume from a cardiac output standpoint is (at CNT=3000)
		// OV = (60.18*Kg) + 633.5
		// If we start a sim, we will start the patient at exactly that volume,
		// and refer to this number as "100%"
		//Base->SetBloodVolume(((Base->Kg*60.18)+633.5)/1000);
		Base->SetBloodVolume((Base->Kg*70)/1000,true);
		Base->BloodVolumeArt = Base->TotalBloodVolume()/(DEFINED_CPLratio+1);
		Base->BloodVolumeVen = Base->TotalBloodVolume() - Base->BloodVolumeArt;
		
		// Set your reference points
		// Weight		SVlim	SV50	nSV		MaxSV
		//	45			4.15	3.05	55		95
		//	70			5.0		3.5		70		117
		//	120			6.1		4.1		100		140
		if ((Base->Kg > 29)&&(Base->Kg <= 150)) 
		{
			//Base->SVlim = 3.0;
			//Base->SV50 = 1.35;
			Base->SVlim = ((Base->Kg-50)*0.02)+2;
			Base->SV50 = ((Base->Kg-50)*0.013) + 0.7;
		}
		else 
		{
			System::Windows::Forms::MessageBox::Show("Weights outside 30-150 Kg are not tested");
		}
		// ELSE THEY STAY ZERO AND THE PATIENT HAS NO STROKE VOLUME AT ALL
		// BECAUSE THE SIMULATOR IS NOT CALIBRATED FOR THOSE RANGES


		Base->CO=6.0;
		Base->SVR=((Base->MAP-Base->CVP)/Base->CO);
		if (Base->BSA > 0) {Base->CI=Base->CO/Base->BSA;}
		else {Base->CI=0;}

		// ALL BLOOD VOLUMES ARE IN LITERS!!!!
		// ***Stressed State Calculations***
		Base->MCP = Base->BloodVolumeArt/(DEFINED_CPLv/DEFINED_CPLratio);
		Base->PaS = DEFINED_XA_FACTOR/(DEFINED_CPLv/DEFINED_CPLratio);
		Base->VaS = Base->TotalBloodVolume() * 1/2.5;
		Base->PvS = DEFINED_XV_FACTOR/(DEFINED_CPLv);
		Base->VvS = (Base->TotalBloodVolume() - Base->VaS) * 0.65;
		Base->Contractility = Rx->Next(int(CNTmin),int(CNTmax));

		Base->SepsisFactor = Rx->Next(int(SepFmin),int(SepFmax));

		// ENG FUDGE FACTOR

		this->ScriptPhaseDescription = "";

		Base->RedCellMass = double(Rx->NextDouble()*(HGBmax-HGBmin) + HGBmin) * Base->TotalBloodVolume();
		Base->PlateletMass = double(350 * Base->TotalBloodVolume());
		Base->CoagMass = 1;

		// Just init these here; we'll set them later
		// with "set" function calls.
		PTStability=0;
		PTFlux = 0;
		PPVbias=0;
		PPVWander=0;
		PPVWanderRate = 0;
		PPVSilence=false;
		PPVInvert=false;

		lastHRFlux = 0;
		lastSVFlux = 0;
		
		EPHEDRINE = 0.0;
		NARC = 0.0;
		NEO = 0;
		DOPA=0;
		DOBS=0;
		NITRO=0;
		EPI=0;
		VASO=0;
		NOREPI=0;
		ESMO=0;

		Vitals = Base->Clone();

		// Okay, I gotta move some fluid around
		// Move to 50% of the stressed volume to prime the pump
		Vitals->BloodVolumeArt = (Vitals->VaS+Vitals->BloodVolumeArt)/2;
		Vitals->BloodVolumeVen = Base->TotalBloodVolume() - Vitals->BloodVolumeArt;

		FluidOff=0;

		RunMode=0;
		ScriptOn=0;
		SimSeconds=0;
		ScriptSeconds=0;

		Injections = gcnew ArrayList();
		SimScript = gcnew ArrayList();
		MAPLOG = gcnew ArrayList();
		ScriptPhases = gcnew ArrayList();

		ActiveScript = gcnew Simulator::ScriptEntry();

		// Range of +/- one (STEADY) error to apply to SpHb
		SpHbModifier = double((Rx->NextDouble()*0.4) - 0.2);
		
		double VOLP = double(Rx->NextDouble()*(double(BVmax-BVmin)/100) + double(BVmin)/100);
		//double VOL = this->Vitals->TotalBloodVolume() - (double(Rx->NextDouble()*(double(BVmax-BVmin)/100) + double(BVmin)/100) * Base->TotalBloodVolume());
		//this->Bleed(VOL*1000,true);
		this->Vitals->SetBloodVolume(VOLP*(this->Base->Kg*61+634)/1000, true);
	};

	public: int GetScriptPhaseNumber(void)
	{
		return this->ScriptPhase;
	}
	public: System::String^ GetScriptPhaseText(int Phase)
	{
		if ((Phase<0)||(Phase > ScriptPhases->Count-1))
		{
			return "";
		}
		System::String^ S = static_cast<System::String^>(this->ScriptPhases[Phase]);
		return S;
	}
	public: System::Void WriteSummaryHeaderToFile(String^ FNAME)
	{
	    IO::StreamWriter^ F = gcnew IO::StreamWriter(FNAME,0);
		F->Write("Patient ID\tKg\tBase CNT\tBV start\tBV end\tBV Optimal\tPercent Optimal\tSV start\tSV end\tFluid Given\tOptimal Err\t");
		F->Write("PPVbias\tPPVInvert\tPPVSilence\tPPVWander\tPPVwRate\tSVErr\tSVeRate\n");
		F->Close();
	}

	public:System::Void WriteSummaryToFile(String^ FNAME, String^ PatientID)
    {
	    IO::StreamWriter^ F = gcnew IO::StreamWriter(FNAME,1);
		F->Write(PatientID+"\t");
		F->Write(Base->Kg.ToString()+"\t");
		F->Write(Base->Contractility.ToString()+"\t");
		F->Write((StartVitals->TotalBloodVolume()*1000).ToString()+"\t");
		F->Write((Vitals->TotalBloodVolume()*1000).ToString()+"\t");
		F->Write((GetOptimalBloodVolume(Base->Kg,Vitals->Contractility)).ToString()+"\t");
		double P = 100000*Vitals->TotalBloodVolume()/GetOptimalBloodVolume(Base->Kg,Vitals->Contractility);
		F->Write(P.ToString()+"\t");
		F->Write(StartVitals->SV+"\t");
		F->Write(Vitals->SV+"\t");
		F->Write((Vitals->TotalBloodVolume()*1000 - StartVitals->TotalBloodVolume()*1000).ToString()+"\t");
		F->Write((Vitals->TotalBloodVolume()*1000 - GetOptimalBloodVolume(Base->Kg,Vitals->Contractility)).ToString()+"\t");
		F->Write(this->PPVbias+"\t");
		F->Write(this->PPVInvert+"\t");
		F->Write(this->PPVSilence+"\t");
		F->Write(this->PPVWander+"\t");
		F->Write(this->PPVWanderRate+"\t");
		F->Write(this->SVErr+"\t");
		F->Write(this->SVErrRate+"\t");
		F->Write("\n");
		F->Close();
    }

	public: static double GetOptimalBloodVolume(double Kg, double CNT)
	{
		
		if (CNT==3000)
		{
			// This was a straightforward calculation for us.  Linear.
			return (Kg*60.75)+633.7;
		}
		if (Kg == 75)
		{
			return 0.00000000477*CNT*CNT*CNT - 0.00008*CNT*CNT + 0.5313*CNT + 4191;
		}
		
		// With CNT != 3000, we're stuck with our multi-polynomial planar 
		// function thingy.  Error level may be up to 200 ml at extremes of ranges but is accurate near middle range
		long double R = 0.0014958*Kg*Kg - 0.1366*Kg + 6.6014;
		R = R * 0.000000001;
		long double S = 0.0001242*Kg*Kg - 0.0057*Kg + 0.5286;
		S = S * 0.0001;
		long double T = 0.0000323*Kg*Kg + 0.0030*Kg + 0.1245;
		long double U = -0.043000*Kg*Kg + 52.493*Kg + 496.14;
	
		double OBV = R*CNT*CNT*CNT - S*CNT*CNT + T*CNT + U;		
		return OBV;
		
	}

	void StartLogging(String^ FNAME)
	{
		System::String^ ExtraFields = "\tEpi\tDobs\tNorepi\tVaso\tNitro\tEphed\tNeo\tCrystal\tColl\tFFP\tEsmo\tFent\tDopa\tBlood\tPlatelets";
		Vitals->InitializeOutputFile(FNAME,ExtraFields);
	}

	void LogNow(String ^FNAME)
	{
		System::String^ ExtraFields="";
		// NO \t ON THE FIRST FIELD
		ExtraFields += this->LastDoseEpi;
		ExtraFields += "\t"+this->LastDoseDobs;
		ExtraFields += "\t"+this->LastDoseNorepi;
		ExtraFields += "\t"+this->LastDoseVaso;
		ExtraFields += "\t"+this->LastDoseNitro;
		ExtraFields += "\t"+this->LastDoseEphed;
		ExtraFields += "\t"+this->LastDoseNeo;
		ExtraFields += "\t"+this->LastDoseCrystal;
		ExtraFields += "\t"+this->LastDoseColl;
		ExtraFields += "\t"+this->LastDoseFFP;
		ExtraFields += "\t"+this->LastDoseEsmo;
		ExtraFields += "\t"+this->LastDoseFent;
		ExtraFields += "\t"+this->LastDoseDopa;
		ExtraFields += "\t"+this->LastDoseBlood;
		ExtraFields += "\t"+this->LastDosePlatelets;

		Vitals->WriteStateToOutputFile(FNAME, this->SimSeconds.ToString(),this->GetOptimalBloodVolume(this->Vitals->Kg,this->Vitals->Contractility),this->Vitals->TotalBloodVolume()/this->GetOptimalBloodVolume(this->Vitals->Kg,this->Vitals->Contractility), ExtraFields);

	    LastDoseEpi=0;
	    LastDoseDobs=0;
	    LastDoseNorepi=0;
	    LastDoseVaso=0;
	    LastDoseNitro=0;
	    LastDoseEphed=0;
	    LastDoseNeo=0;
	    LastDoseCrystal=0;
	    LastDoseColl=0;
	    LastDoseFFP=0;
	    LastDoseEsmo=0;
	    LastDoseFent=0;
	    LastDoseDopa=0;
	    LastDoseBlood=0;
	    LastDosePlatelets=0;
	}

	void AddMedication(int drugID, double dose)
	{
		Injection^ I = gcnew Injection();
		I->Dose = dose;
		I->DrugID = drugID;
		I->TimeSinceInjection = 0;
		this->Injections->Add(I);
	}

	void SetPPVBehavior(double wander, double wanderRate, double bias, bool silence, bool invert)
	{
		this->PPVbias = bias;
		this->PPVWander = wander;
		this->PPVSilence = silence;
		this->PPVInvert = invert;
		this->PPVWanderRate = wanderRate;

		// Initialize
		this->PPVWanderActive = 1 + ((Rx->NextDouble()*2 - 1) * this->PPVWander)/100;
	}

	void SetSVBehavior(double error, double wanderRate)
	{
		this->SVErr = error;
		this->SVErrRate = wanderRate;
		
		// Initialize
		this->SVErrActive = 1 + ((Rx->NextDouble()*2 - 1) * this->SVErr)/100;
	}

	void SetPatientStability(double stability, double flux)
	{
		// Time to return to baseline; 60 seems to work
		this->PTStability = stability;
		// Random flux generator in the system; should be < 2 or it gets crazy
		this->PTFlux = flux;
	}

	bool LoadSimScript(String^ newFILE)
	{
		// The script loader expects a properly formatted script file;
		// if it errors it will abort the process and not load any of the script
		try 
		{	
			String^ L;
			IO::StreamReader^ FILE = gcnew IO::StreamReader(newFILE);
			// Strip the first line
			FILE->ReadLine();
			while (L=FILE->ReadLine())
			{
				Simulator::ScriptEntry^ En = gcnew Simulator::ScriptEntry();
				array<String^>^ FIELDS = L->Split(',');

				// Get the fields
				// The field is in minutes; so *60 to convert to seconds
				En->seconds = int::Parse(FIELDS[0]) * 60;
				En->command = FIELDS[1];
				En->ScriptPhaseText = FIELDS[2];
				ScriptPhases->Add(FIELDS[2]);
				if (FIELDS[3]->Length > 0) 
				{
					En->CrystalloidPerSecond = double::Parse(FIELDS[3])/60;
				}
				if (FIELDS[4]->Length > 0) 
				{
					En->ColloidPerSecond = double::Parse(FIELDS[4])/60;
				}
				if (FIELDS[5]->Length > 0) 
				{
					En->PRBCPerSecond = double::Parse(FIELDS[5])/60;
				}
				// the field is in ml/min, so we have to divide by 60 to get ml/sec (it's a double)
				if (FIELDS[6]->Length > 0) 
				{
					En->BloodLossPerSecond = double::Parse(FIELDS[6])/60;
				}
				if (FIELDS[7]->Length > 0) 
				{
					// This field affects only the fields that FOLLOW (fields 9+)
					En->EventProbability = double::Parse(FIELDS[7]);	
				}
				if (FIELDS[8]->Length > 0) 
				{
					En->SympatheticStim  = double::Parse(FIELDS[8]);
				}
				if (FIELDS[9]->Length > 0) 
				{
					En->AlphaStim = double::Parse(FIELDS[9]);
				}
				if (FIELDS[10]->Length > 0)
				{
					En->ParasympatheticStim = double::Parse(FIELDS[10]);
				}
				if (FIELDS[11]->Length > 0)
				{
					En->Contractility = double::Parse(FIELDS[11]);
				}
				if (FIELDS[12]->Length > 0)
				{
					En->SepsisFactor = double::Parse(FIELDS[12]);
				}
				this->SimScript->Add(En);
			}
			return true;
		}
		catch(Exception^ e)
		{
			this->SimScript->Clear();
			System::Windows::Forms::MessageBox::Show(e->ToString());
			e;
			return false;
		}
	}

private:
	int RunScripting(void)
	{
		// Do we need to change the script?
		//------------------------------------------------
		// Check the very first packed on the script array
		if (this->SimScript->Count > 0)
		{
			Simulator::ScriptEntry^ E = dynamic_cast<Simulator::ScriptEntry^>(SimScript->default[0]);
			if (this->SimSeconds >= E->seconds)
			{
				this->ActiveScript = E->CreateCopy();
				this->ScriptPhase++;
				// I think this will work;
				SimScript->RemoveAt(0);
			}
		}

		// Run any scripting
	
		this->Bleed(this->ActiveScript->BloodLossPerSecond,false);
		this->AddMedication(DRUG_LR,this->ActiveScript->CrystalloidPerSecond);
		this->AddMedication(DRUG_ALBUMIN,this->ActiveScript->ColloidPerSecond);
		this->AddMedication(DRUG_PRBC,this->ActiveScript->PRBCPerSecond);

		this->ScriptPhaseDescription = this->ActiveScript->ScriptPhaseText;

		if (this->Vitals->TotalBloodVolume() < 0)
		{
			this->Vitals->SetBloodVolume(0,true);
		}
		if (ActiveScript->command->CompareTo("END")==0)
		{
			// Return the die code. Die code is -8.
			// Why?  Because I said so. Don't ask annoying questions.
			return -8;
		}
		
		if (ActiveScript->EventProbability > 0)
		{
			// If there is a probability, roll for damage
			// if we miss the roll, zero out the rest of the line
			// AFTER THIS FIRST PASS, prob will be zero so the roll
			// result will play on until the next phase.
			if (Rx->NextDouble()*100 > ActiveScript->EventProbability)
			{
				ActiveScript->AlphaStim = 0;
				ActiveScript->Contractility = 0;
				ActiveScript->ParasympatheticStim = 0;
				ActiveScript->SympatheticStim = 0;
				ActiveScript->SepsisFactor = 0;
			}
		}

		ActiveScript->EventProbability = 0;
		
		double AltVal = 0.5;
		if (this->LastStimTicks < 30) {AltVal = AltVal/4;}
		if (this->LastStimTicks == 0) {AltVal = 0;}

		double A = ActiveScript->SympatheticStim/10 + ActiveScript->AlphaStim;
		if (this->Vitals->AlphaStim != A)
		{
			double M = (A - this->Vitals->AlphaStim)/DEFINED_ALPHA_STIM_HALFLIFE;
			if (M > 0)
			{
				this->Vitals->AlphaStim += greatest(M,AltVal);
			}
			else 
			{
				this->Vitals->AlphaStim += least(M,-AltVal);
			}
		}
		
		double B = ActiveScript->SympatheticStim/10;
		if (this->Vitals->BetaStim != B)
		{
			double M = (B - this->Vitals->BetaStim)/DEFINED_BETA_STIM_HALFLIFE;
			if (M > 0)
			{
				this->Vitals->BetaStim += greatest(M,AltVal);
			}
			else 
			{
				this->Vitals->BetaStim += M/8;
			}
		}

		double M = (-this->Vitals->DopaStim)/DEFINED_DOPA_STIM_HALFLIFE;
		if (M>0)
		{
			this->Vitals->DopaStim += M;
		}
		else 
		{
			this->Vitals->DopaStim += least(M,-AltVal/2);
		}
		
		this->NARC += ActiveScript->ParasympatheticStim / (60*30);
		this->EPHEDRINE += ActiveScript->SympatheticStim / (60*30);
	
		this->Vitals->Contractility += ActiveScript->Contractility;
		this->Vitals->SepsisFactor += ActiveScript->SepsisFactor;
		return 1;
	}

public:
	void Bleed(double MlBloodLoss, bool DoNotChangeHGB)
	{
		double v = MlBloodLoss/1000;

		if (v > 0) 
		{
			this->lastBleed = v;
		}
		// You need this - prevents blood volume < 0
		if (v > this->Vitals->TotalBloodVolume()) {this->Vitals->SetBloodVolume(v,true);}
		
		// Calc ratio of blood lost to total volume
		// for calculating cellular losses
		double R = v/this->Vitals->TotalBloodVolume();

		// Reduce the venous blood volume
		//this->Vitals->BloodVolume = Vitals->BloodVolume-volume;
		this->Vitals->BloodVolumeVen += -v;

		// Reduce cell masses by ratio lost
		if (DoNotChangeHGB)
		{
			Vitals->RedCellMass = double(Vitals->RedCellMass * (1-R));
		}
		else 
		{
			Vitals->RedCellMass = double(Vitals->RedCellMass * (1-R*1.25));	
		}
		Vitals->PlateletMass = double(Vitals->PlateletMass * (1-R));
		Vitals->CoagMass = double(Vitals->CoagMass * (1-R));
	}

private:
	double calculateStimLevel()
	{	
		// newstim contains the new working stim level
		// this->Stim is still referenced throughout as the active level

		int mapmean=0;
		double newstim=0;
		double falloff=0;

		// We need to know the average MAP over the last 20 cycles
		MAPLOG->Add(this->Vitals->MAP);
		while (MAPLOG->Count > 10) 
		{
			MAPLOG->RemoveAt(0);
		}
		for (int i=0; i<MAPLOG->Count; i++)
		{
			mapmean = int(mapmean + static_cast<double>(MAPLOG->default[i]));
		}
		mapmean = int(mapmean/MAPLOG->Count);

		
		// Now we decide what to do based on MAP
		if ((mapmean < (Base->MAP*4/5)) && (Vitals->CI < (Base->CI*4/5)))
		{
			int MAPLOW = int((Base->MAP*4/5) - mapmean);
			newstim=double(MAPLOW)/50;
			// Attenuate effect over 4
			if (newstim > 4)
			{
				newstim = ((newstim-4)/4)+4;
			}
		}
		else if ((mapmean > (Base->MAP*1.2)) && (this->Vitals->CI > (Base->CI*1.2)))
		{
			int MAPHIGH = int(mapmean - (Base->MAP*1.2));
			newstim = newstim - MAPHIGH/20;
		}

		// Add EPHEDRINE, ensure this is the min stim
		newstim = newstim + EPHEDRINE/500;
		if (newstim < EPHEDRINE) {newstim=EPHEDRINE/10;}

		// Calculate new stim level
		newstim = (this->Vitals->SympatheticOutflow*4 + newstim)/5;
		
		// Calculate falloff
		if (newstim <= 0) {newstim=0;}
		else 
		{
			newstim = newstim - double(greatest(newstim/250,0.0001));
		}

		// Hypotension loss of effect (cumulative per cycle)
		if (Vitals->MAP < 10){newstim = newstim*19/20;}
		if (Vitals->MAP < 4){newstim = 0;}

		return newstim;
	}

	void runElimination(void)
	{
		/*if (this->NEO > 0)
		{
			NEO = double(NEO - greatest((NEO/150),0.2));
			if (NEO<0) {NEO=0;}
		}
		if (this->EPHEDRINE > 0)
		{
			EPHEDRINE = double(EPHEDRINE - greatest((EPHEDRINE/250),0.01));
			if (EPHEDRINE <0) {EPHEDRINE=0;}
		}
		if (this->NARC > 0)
		{
			NARC = double(NARC - greatest((NARC/6000),0.002));
			if (NARC<0) {NARC=0;}
		}*/
	}

	void runThirdspacing(void)
	{
#ifndef NO_FLUID_SHIFTS
		if (this->FluidOff > 0) 
		{
			double loss = FluidOff/900;
			this->Vitals->BloodVolume = Vitals->BloodVolume - loss;
			this->Vitals->Extravascular += loss;
			this->FluidOff = this->FluidOff - loss;
			if (FluidOff < 0) {FluidOff=0;}
		}
#endif
	}

private: 
	double BalanceAlpha(double x)
	{
		if (x < 0) 
		{
			if (this->Vitals->AlphaStim < -34)
			{
				return x/4;
			}
			if (this->Vitals->AlphaStim <-44)
			{
				return x/8;
			}
			return x;
		}
		return x;
	}
public:
	void runInjections(void)
	{
		this->LastStimTicks++;
		for (int i=Injections->Count-1; i>=0; i=i-1)
		{
			Injection^ I = dynamic_cast<Injection^>(Injections->default[i]);
			I->TimeSinceInjection++;
			
			if ((I->DrugID >= 100) && (I->DrugID <=199))
				{
					Vitals->BloodVolumeVen += I->Dose/1000;
					Injections->RemoveAt(i);
					this->FluidOff += double(I->Dose*0.66/1000);
					this->LastDoseCrystal += I->Dose;
				}
			else if ((I->DrugID >= 200)&&(I->DrugID <209))
				{
					Vitals->BloodVolumeVen += I->Dose/1000;
					Injections->RemoveAt(i);
					this->FluidOff += (I->Dose/5000);
					this->LastDoseColl += I->Dose;
				}
			else if ((I->DrugID>=300)&&(I->DrugID <400))
				{
					Vitals->BloodVolumeVen += I->Dose/1000;
					// Assume the blood has a crit of 28
					if (I->DrugID == 300) {
						Vitals->RedCellMass += I->Dose/1000*28;
						this->LastDoseBlood += I->Dose;
					}
					if (I->DrugID == 301) {
						Vitals->PlateletMass += I->Dose/1000*28;
						this->LastDosePlatelets += I->Dose;
					}
					if (I->DrugID == 302) {
						Vitals->CoagMass += I->Dose/1000*28;
						this->LastDoseFFP += I->Dose;
					}
					Injections->RemoveAt(i);
				}
			else 
				{
				// Time to have an effect depends on CO but must be at least 8s
				if ((I->TimeSinceInjection > (50/this->Vitals->CO))&&(I->TimeSinceInjection > DEFINED_INJECTION_LAG_TIME))
				{
					if (I->DrugID == DRUG_EPINEPHRINE)
					{
						this->Vitals->DopaStim += I->Dose/50;
						//this->Vitals->AlphaStim += I->Dose/100;
						this->Vitals->BetaStim += I->Dose/10;
						this->LastStimTicks = 0;
						this->LastDoseEpi += I->Dose;
					}
					else if (I->DrugID == DRUG_NEOSYNEPHRINE)
					{
						this->Vitals->AlphaStim += I->Dose/2;
						this->LastStimTicks = 0;
						this->LastDoseNeo += I->Dose;
					}
					else if (I->DrugID == DRUG_EPHEDRINE)
					{
						this->Vitals->AlphaStim += I->Dose;
						this->Vitals->BetaStim += I->Dose*4;
						this->LastStimTicks = 0;
						this->LastDoseEphed = I->Dose;
					}
					else if (I->DrugID == DRUG_FENTANYL)
					{
						NARC += I->Dose;
						this->LastDoseFent += I->Dose;
					}
					else if (I->DrugID == DRUG_VASOPRESSIN)
					{
						this->Vitals->AlphaStim += I->Dose*30;
						this->LastStimTicks = 0;
						this->LastDoseVaso += I->Dose;
					}
					else if (I->DrugID == DRUG_NOREPINEPHRINE)
					{
						this->Vitals->AlphaStim += I->Dose;
						this->LastStimTicks = 0;
						this->LastDoseNorepi += I->Dose;
					}
					else if (I->DrugID == DRUG_DOPAMINE)
					{
						this->Vitals->DopaStim += I->Dose*2.5;
						this->Vitals->BetaStim += I->Dose*12.5;
						this->LastStimTicks = 0;
						this->LastDoseDopa += I->Dose;
					}
					else if (I->DrugID == DRUG_DOBUTAMINE)
					{
						//this->Vitals->AlphaStim += I->Dose*4;
						this->Vitals->BetaStim += I->Dose*12.5;
						this->LastStimTicks = 0;
						this->LastDoseDobs += I->Dose;
					}
					else if (I->DrugID == DRUG_ESMOLOL)
					{
						this->Vitals->DopaStim += -I->Dose;
						//this->Vitals->BetaStim += -I->Dose * MOD;
						this->LastStimTicks = 0;
						this->LastDoseEsmo += I->Dose;
					}
					else if (I->DrugID == DRUG_NITROGLYCERINE)
					{
						this->Vitals->AlphaStim += -I->Dose;
						//this->Vitals->DopaStim += -I->Dose*2;
						this->LastStimTicks = 0;
						this->LastDoseNitro += I->Dose;
					}
	
					Injections->RemoveAt(i);
				} // end else
			}
		}
	}

	void calculateEverything(void)
	{
		// Sepsis factor
		//double SepsisFactor = 8;  // Range 1-8; 8 is extreme; >8 is breaking the model
		double SepsisModifier =  1 - Vitals->SepsisFactor/10;
		double NARCModifier = NARC/5;
		if (NARCModifier < 1) {NARCModifier = 1;};

		double DAMOD = (Vitals->DopaStim/10*Vitals->DopaStim/10);
		if (Vitals->DopaStim < 0) {DAMOD = -DAMOD;}
		if (DAMOD < 0) {DAMOD = DAMOD * 4;}

		// Alpha Modifier affects HR
		double AlphaModifier = 1 + Vitals->AlphaStim/30;
		if (AlphaModifier < 0) {AlphaModifier = AlphaModifier * 4;}

		double slopeTemp = (Base->MCP - Vitals->PvS)/(Base->BloodVolumeVen - Vitals->VvS + Vitals->SepsisFactor/4);/// - Vitals->AlphaStim/10);
		double interTemp = Base->MCP - (slopeTemp * (Base->BloodVolumeVen + Vitals->SepsisFactor/4));// + Vitals->AlphaStim/10));
		Vitals->CVP = (slopeTemp*Vitals->BloodVolumeVen-Base->VvS)+interTemp + Vitals->AlphaStim/10;
		if (Vitals->CVP < 0) {Vitals->CVP = 0;}

		double slopeMAP = (1+Vitals->AlphaStim/1000-NARCModifier/100) * (Vitals->PaS - Base->MCP)/(Vitals->VaS - Base->BloodVolumeArt);
		double interMAP = Base->MCP - (slopeMAP*Base->BloodVolumeArt);
		Vitals->MAP = (slopeMAP*Vitals->BloodVolumeArt) + interMAP;
		if (Vitals->MAP - Base->MCP < 0) {Vitals->MAP = Base->MCP;}

		double MAPdelta = (Base->MAP - Vitals->MAP)/2;
		if (MAPdelta > 0)
		{
			if (this->Vitals->BetaStim > Math::Abs(MAPdelta))
			{
				MAPdelta = this->Vitals->BetaStim;
			}
		}
		else 
		{
			MAPdelta += this->Vitals->BetaStim;
		}

		// lastHRFlux tracks...well, HR Flux
		if (this->PTFlux > 0)
		{
			if (Rx->Next(60) < 5)
			{
				this->lastHRFlux = (10*this->PTFlux*(double(Rx->Next(0,100))/100)) - (5*this->PTFlux);
				this->lastSVFlux = 0;
			}
		}

		Vitals->HR = (((Vitals->HR*4) + Base->HR + (MAPdelta*DEFINED_HRgain/AlphaModifier) - NARCModifier + DAMOD)/5)*(100+lastHRFlux)/100;
		if (Vitals->HR < 0) {Vitals->HR = 0;}
		if (Vitals->HR > DEFINED_MaxHR) {Vitals->HR = DEFINED_MaxHR;}

		// STROKE VOLUME IS IN LITERS TOO!
		double SVterm1 = System::Math::Pow(Vitals->CVP,DEFINED_nSV);
		double SVterm2 = System::Math::Pow(Base->SV50,DEFINED_nSV);
		Vitals->SV = ((Base->SVlim*SVterm1)/(SVterm2+SVterm1))*DEFINED_KC_FACTOR*Vitals->Contractility/((1+Vitals->AlphaStim/500));
		if (Vitals->SV < 0) Vitals->SV=0;

		// CALCULATE SVR (also from a pressure set-point)
		//Vitals->SVR = Base->SVR + (MAPdelta*DEFINED_SVRgain);
		//Vitals->SVR = Vitals->SVR * SepsisModifier * AlphaModifier * AlphaModifier;
		double AlphaBonus = Vitals->AlphaStim/5;
		if (AlphaBonus > 10) {AlphaBonus = 10;}
		Vitals->SVR = Base->SVR + (MAPdelta*DEFINED_SVRgain) + (Vitals->AlphaStim/5) + AlphaBonus;
		Vitals->SVR = Vitals->SVR * SepsisModifier;
		//if (Vitals->SVR < 0) {Vitals->SVR = 0;}
		if (Vitals->SVR > DEFINED_MaxSVR) {Vitals->SVR = DEFINED_MaxSVR;}
		
		// MUST MODULATE CNT!!!11!
		Vitals->CO = Vitals->SV * Vitals->HR;
		Vitals->CI = Vitals->CO/Base->BSA;
		
		// Calculate the right cardiac output
		if (Vitals->SVR > 0)
		{
			if (Vitals->CVP < 0)
				{Vitals->COr = Vitals->MAP/Vitals->SVR;}
			else 
				{Vitals->COr = (Vitals->MAP - Vitals->CVP)/Vitals->SVR;}
		}
		else 
		{
			if (Vitals->CVP < 0)
				{Vitals->COr = Vitals->MAP;}
			else 
				{Vitals->COr = (Vitals->MAP - Vitals->CVP);}
		}

		// Differentials for volume changes
		double VvD = Vitals->COr - Vitals->CO;
		double VaD = Vitals->CO - Vitals->COr;

		// This process is clocked at 1/sec
		Vitals->BloodVolumeArt += VaD/60;
		Vitals->BloodVolumeVen += VvD/60;

		return;
	}
	double calcPPV(void)
	{
		if (this->PPVSilence) {return 0;}

		double slopeTemp = (Base->MCP - Vitals->PvS)/(Base->BloodVolumeVen - Vitals->VvS);
		double interTemp = Base->MCP - (slopeTemp * (Base->BloodVolumeVen));
		double CVP = (slopeTemp*(Vitals->BloodVolumeVen+0.5)-Base->VvS)+interTemp;

		if (CVP < 0) {CVP = 0;}

		double slopeMAP = (Vitals->PaS - Base->MCP)/(Vitals->VaS - Base->BloodVolumeArt);
		double interMAP = Base->MCP - (slopeMAP*Base->BloodVolumeArt);
		double MAP = (slopeMAP*Vitals->BloodVolumeArt) + interMAP;
		if (MAP - Base->MCP < 0) {MAP = Base->MCP;}

		double MAPdelta = DEFINED_MAPset - MAP;
		double HR = DEFINED_MAPset + (MAPdelta*DEFINED_HRgain);
		if (HR < 0) {HR = 0;}
		if (HR > DEFINED_MaxHR) {HR = DEFINED_MaxHR;}

		// STROKE VOLUME IS IN LITERS TOO!
		double SVterm1 = System::Math::Pow(CVP,DEFINED_nSV);
		double SVterm2 = System::Math::Pow(Base->SV50,DEFINED_nSV);
		double SV = ((Base->SVlim*SVterm1)/(SVterm2+SVterm1))*DEFINED_KC_FACTOR*Vitals->Contractility;
		if (SV < 0) SV=0;

		double dSV = 100*(SV-Vitals->SV)/((SV+Vitals->SV)/2);
		double PPV=dSV-2;
		if (PPV <0) {PPV=0;}
		if (PPV > 40){PPV=40;}

		if (PPV > 18) 
		{
			double d = PPV - 18;
			d = d/2;
			PPV = d+18;
		}
		if (PPV > 28) 
		{
			double d = PPV - 18;
			d = d/2;
			PPV = d+28;
		}
		if (PPV > 35) 
		{
			double d = PPV - 18;
			d = d/4;
			PPV = d+35;
		}

		if (this->PPVInvert) {return (40-PPV);}

		return PPV;
	}
	
public:
	int Cycle(void)
	{
		this->SimSeconds++;
		// Set the "Start vitals" reference to the 20 second vitals once the 
		// initialization dust has settled.
		if (this->SimSeconds < 20)
		{
			this->StartVitals = this->Vitals->Clone(); 
		}
		
		int x = this->RunScripting();
		if (x == -8) {return -8;}

		this->runElimination();
		this->runThirdspacing();
		this->runInjections();
		
		if (this->PPVWanderRate > 0)
		{
			if (Rx->NextDouble()*this->PPVWanderRate < 1)
			{
				this->PPVWanderActive = 1 + ((Rx->NextDouble()*2 - 1) * this->PPVWander)/100;
			}
		}
		if (this->SVErrRate > 0)
		{
			if (Rx->NextDouble()*this->SVErrRate < 1)
			{
				this->SVErrActive = 1 + ((Rx->NextDouble()*2 - 1) * this->SVErr)/100;
			}
		}

		// The model must at this point account for SV and CO
		this->calculateEverything();

		// Modulate for SV error
		Vitals->SV = Vitals->SV * this->SVErrActive;
		Vitals->CO = Vitals->CO * this->SVErrActive;

		Vitals->PPV = this->calcPPV() * this->PPVWanderActive + this->PPVbias;
		if (Vitals->PPV < 0) {Vitals->PPV = 0;}
		Vitals->SVV = Vitals->PPV;
		Vitals->PVI = Vitals->PPV + 3;
	
		Vitals->SPO2 = 100;
		Vitals->SCvO2 = 70;
		Vitals->SvO2 = 70;
		
		Vitals->PaCO2 = 40;
		Vitals->DeadspaceR = double(0.90);
		Vitals->DynamicOutflow = double(0.95);
		Vitals->RR = 12;
		Vitals->PAP = 25;

		Vitals->SympatheticOutflow = calculateStimLevel();
		Vitals->DBP = (3*Vitals->MAP/(Base->SDPD+2));
		Vitals->SBP =  (Base->SDPD*Vitals->DBP);

		Vitals->HgB = this->Vitals->RedCellMass/this->Vitals->TotalBloodVolume();
		if (Rx->NextDouble() < 0.1)
		{
			Vitals->SpHbx = double(this->SpHbModifier + ((Rx->NextDouble()*0.6)-0.3));
		}

		this->LOG_CO->Add(Vitals->CO);
		this->LOG_MAP->Add(Vitals->MAP);
		this->LOG_HR->Add(Vitals->HR);
		this->LOG_SV->Add(Vitals->SV);

		return 1;
	}


};

};