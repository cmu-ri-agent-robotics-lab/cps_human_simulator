// GEWaveformTools.h
// Reference structures and functions for GE Waveforms
// J. Rinehart 2011

//
// Namespace JANK:
//		fxn int					TCPDoubleToInt(int,int)
//		class					WavePacket
//		ArrayList^ WavePackets	ExtractPackets(ArrayList^ bytes)
//

#pragma once

namespace JANK {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

// TCPDoubleToInt
// Takes two bytes from TCP packet and converts to int value
int TCPDoubleToInt(int V1,int V2) 
	{
		if (V1 > 128) {
			V1 = V1 - 256;
			}
		return ((V1*256) + V2);
	};


// WavePacket: A structure to hold the data contained in a single waveform packet
public ref class WavePacket {
	// perl timestamp format (seconds since 1960?)

public:
	String^ timestamp;

	int timecode;

	int EKG1a;
	int EKG1b;
	int EKG1c;
	int EKG1d;
	
	int EKG2a;
	int EKG2b;
	int EKG2c;
	int EKG2d;

	int EKG3a;
	int EKG3b;
	int EKG3c;
	int EKG3d;

	int EKGVa;
	int EKGVb;
	int EKGVc;
	int EKGVd;
	
	int ARTa;
	int ARTb;
	int ARTmean;

	int PLETH;

public:
		WavePacket(void) 
		{
			timestamp="";

			timecode = 0;

			EKG1a=0;
			EKG1b=0;
			EKG1c=0;
			EKG1d=0;
			
			EKG2a=0;
			EKG2b=0;
			EKG2c=0;
			EKG2d=0;

			EKG3a=0;
			EKG3b=0;
			EKG3c=0;
			EKG3d=0;

			EKGVa=0;
			EKGVb=0;
			EKGVc=0;
			EKGVd=0;
			
			ARTa=0;
			ARTb=0;

			ARTmean=0;

			PLETH=0;
		}
	};


// OVERLOAD 1/2
// Function extracts all the packets in an ArrayList^ of bytes and returns an array of WavePackets
System::Collections::ArrayList^ ExtractPackets(ArrayList^ WSTREAM) {
		ArrayList^ Packets = gcnew ArrayList();
		WavePacket^ wpack = gcnew WavePacket();
		int i=-1;
		int packetPosition=0;
		String^ Timestamp = "";
		int TimeCode = 0;

		// Complex: Gonna create WavePackets on the fly
		// Must be max WSTREAM-50; smallest packet size is 50 bytes
		// so it's pointless to look for a header past that anyway.
		while (i < (WSTREAM->Count-50)) 
		{
			// Advance one position at a time looking for packet start
			i++;
			int S1 = *(dynamic_cast<int^>(WSTREAM->default[i]));
			int S2 = *(dynamic_cast<int^>(WSTREAM->default[i+1]));
			int S3 = *(dynamic_cast<int^>(WSTREAM->default[i+10]));

			if ((S1 == 10)&&(S2 == 10))
			{
				// This is probably timestamp code
				int Sx = *(dynamic_cast<int^>(WSTREAM->default[i+2]));
				int Sy = *(dynamic_cast<int^>(WSTREAM->default[i+3]));
				
				if ((Sx==10) && (Sy==10))
				{
					int j;
					bool stop=false;
					// Almost definitely timestamp
					for (j=0; (j<25) && ((j+i)<WSTREAM->Count) && (!stop); j++)
					{
						if (*(dynamic_cast<int^>(WSTREAM->default[i+j+4])) == 10) 
						{
							 if ((j >= 15) && (j <= 24))
							 {
								// Good enough, it's a timestamp.
								// add the chars to the string
								Timestamp = "";
								for (int k=0; k<j; k++)
								{
									Timestamp += Convert::ToChar((*dynamic_cast<int^>(WSTREAM->default[i+k+4])));
								}
								int Tc1 = *(dynamic_cast<int^>(WSTREAM->default[i+j+4+2]));
								int Tc2 = *(dynamic_cast<int^>(WSTREAM->default[i+j+4+3]));
								TimeCode = TCPDoubleToInt(Tc1,Tc2);
							 }
						 // exit the loop
						 stop=true;
						}
					}
				i=i+4+j;
				}
			}
			// If this is 070B or 073B and ten down the road is 08 we're on target 
			else if ((S1==7) && ((S2==11)||(S2==59)) && (S3==8))
			{
				// Get a WavePacket ready
				WavePacket^ PACK = gcnew WavePacket();

				// See if we have a timestamp to attach
				if (Timestamp->Length > 0) 
				{
					PACK->timestamp = Timestamp;
					Timestamp="";
				}
				PACK->timecode = TimeCode;				

				// We are assuming the EKG fields are always included
				// This could create problems if they were variable fields.

				// First four fields are EKG1
				PACK->EKG1a = TCPDoubleToInt(*(dynamic_cast<int ^>(WSTREAM->default[i+2])), *(dynamic_cast<int ^>(WSTREAM->default[i+3])));
				PACK->EKG1b = TCPDoubleToInt(*(dynamic_cast<int ^>(WSTREAM->default[i+4])), *(dynamic_cast<int ^>(WSTREAM->default[i+5])));
				PACK->EKG1c = TCPDoubleToInt(*(dynamic_cast<int ^>(WSTREAM->default[i+6])), *(dynamic_cast<int ^>(WSTREAM->default[i+7])));
				PACK->EKG1d = TCPDoubleToInt(*(dynamic_cast<int ^>(WSTREAM->default[i+8])), *(dynamic_cast<int ^>(WSTREAM->default[i+9])));
				// Advance the position
				i=i+10;

				// Next four are EKG2
				PACK->EKG2a = TCPDoubleToInt(*(dynamic_cast<int ^>(WSTREAM->default[i+2])), *(dynamic_cast<int ^>(WSTREAM->default[i+3])));
				PACK->EKG2b = TCPDoubleToInt(*(dynamic_cast<int ^>(WSTREAM->default[i+4])), *(dynamic_cast<int ^>(WSTREAM->default[i+5])));
				PACK->EKG2c = TCPDoubleToInt(*(dynamic_cast<int ^>(WSTREAM->default[i+6])), *(dynamic_cast<int ^>(WSTREAM->default[i+7])));
				PACK->EKG2d = TCPDoubleToInt(*(dynamic_cast<int ^>(WSTREAM->default[i+8])), *(dynamic_cast<int ^>(WSTREAM->default[i+9])));
				// Advance the position
				i=i+10;

				// Next four are EKG3
				PACK->EKG3a = TCPDoubleToInt(*(dynamic_cast<int ^>(WSTREAM->default[i+2])), *(dynamic_cast<int ^>(WSTREAM->default[i+3])));
				PACK->EKG3b = TCPDoubleToInt(*(dynamic_cast<int ^>(WSTREAM->default[i+4])), *(dynamic_cast<int ^>(WSTREAM->default[i+5])));
				PACK->EKG3c = TCPDoubleToInt(*(dynamic_cast<int ^>(WSTREAM->default[i+6])), *(dynamic_cast<int ^>(WSTREAM->default[i+7])));
				PACK->EKG3d = TCPDoubleToInt(*(dynamic_cast<int ^>(WSTREAM->default[i+8])), *(dynamic_cast<int ^>(WSTREAM->default[i+9])));
				// Advance the position
				i=i+10;

				// Next four are EKG4
				PACK->EKGVa = TCPDoubleToInt(*(dynamic_cast<int ^>(WSTREAM->default[i+2])), *(dynamic_cast<int ^>(WSTREAM->default[i+3])));
				PACK->EKGVb = TCPDoubleToInt(*(dynamic_cast<int ^>(WSTREAM->default[i+4])), *(dynamic_cast<int ^>(WSTREAM->default[i+5])));
				PACK->EKGVc = TCPDoubleToInt(*(dynamic_cast<int ^>(WSTREAM->default[i+6])), *(dynamic_cast<int ^>(WSTREAM->default[i+7])));
				PACK->EKGVd = TCPDoubleToInt(*(dynamic_cast<int ^>(WSTREAM->default[i+8])), *(dynamic_cast<int ^>(WSTREAM->default[i+9])));
				// Advance the position
				i=i+10;

				// Now we reach the variably present fields
				// We need a way to scan until the end.
				// We'll go until we find 56 0B (the BIS), OR we find a new timestamp start (0A 0A 0A 0A 
				
				int T1;
				int T2;
				int T3;
				int T4;

				do {
					// Get the next frame (two bytes), and then some
					T1 = *(dynamic_cast<int^>(WSTREAM->default[i]));
					T2 = *(dynamic_cast<int^>(WSTREAM->default[i+1]));
					T3 = *(dynamic_cast<int^>(WSTREAM->default[i+2]));
					T4 = *(dynamic_cast<int^>(WSTREAM->default[i+3]));

					// Now check for a match to one of the fields below

					// 1A field - PA?
					if ((T1 == 26)&&(T2 == 10)) 
					{
						i=i+6;
					}
					// 1B field - Art wave
					// Now includes the constant (/5)
					else if ((T1 == 27)&&(T2 == 10))
					{
						PACK->ARTa = TCPDoubleToInt(*(dynamic_cast<int ^>(WSTREAM->default[i+2])), *(dynamic_cast<int ^>(WSTREAM->default[i+3])))/5;
						PACK->ARTb = TCPDoubleToInt(*(dynamic_cast<int ^>(WSTREAM->default[i+4])), *(dynamic_cast<int ^>(WSTREAM->default[i+5])))/5;		
						i=i+6;
					}
					// 1C field - CVP
					else if ((T1 == 28)&&(T2 == 10))
					{
						i=i+6;
					}
					// 2709 - Pleth
					else if ((T1 == 39)&&(T2 == 9))
					{
						PACK->PLETH = TCPDoubleToInt(*(dynamic_cast<int ^>(WSTREAM->default[i+2])), *(dynamic_cast<int ^>(WSTREAM->default[i+3])));
						i=i+4;
					}
					// End sequence?
					//else if ((T1==86)&&(T2==11))
					//{
						// Do nothing - this is the BIS Sequence - I'm using
						// this as the terminator for packets
						
					//}
					// Safety Valve for stream end
					else if (i >= (WSTREAM->Count-8)) 
					{
						// we need to break here, we're out of Stream
						break;
					}
					else if ((T1==7)&&((T2==11)||(T2==59)))
					{
						// You ****MUST**** keep this block to prevent advancement!
						// (the else statement advances reader if this isn't here)
					}
					else {
						// Advance one BYTE.  We have to go one at a time b/c there's a frameshift
						// between network packages.
						i=i+1;
					}

				// Terminate on the next 070b (new sub-packet)
				// or on 0a0a0a0a new timecode
				} while ( (i < (WSTREAM->Count-50))
						  &&
						  (!((T1==10)&&(T2==10)&&(T3==10)&&(T4==10)))
						  &&
						 (!((T1==7)&&((T2==11)||(T2==59))))
						 );

				// We may have BIS, we may have run out of stream
				//if ((T1==86)&&(T2==11))
				if ((T1==7)&&((T2==11)||(T2==59)))
				{
					// BACKUP the pointer one.  It's going to be advanced one on the next pass and needs
					// to match on the 070b
					i=i-2;
				}
				if ((T1==10)&&(T2==10)&&(T3==10)&&(T4==10))
				{
					i=i-4;
					// back it up MOAR.
				}

				// Done collecting,
				// Add this WavePacket to the array
				Packets->Add(PACK);

				//return Packets;

			} // End of PACKET MATCHED

		}  // End while (i < WSTREAM...)

		return Packets;
		}

// OVERLOAD 2/2
// Function extracts all the packets in an ArrayList^ of bytes and returns an array of WavePackets
System::Collections::ArrayList^ ExtractPackets(array<Byte,1>^ WSTREAM,int Count) {
		ArrayList^ Packets = gcnew ArrayList();
		WavePacket^ wpack = gcnew WavePacket();
		int i=-1;
		int packetPosition=0;
		String^ Timestamp = "";
		int TimeCode = 0;

		// Complex: Gonna create WavePackets on the fly
		// Must be max WSTREAM-50; smallest packet size is 50 bytes
		// so it's pointless to look for a header past that anyway.
		while (i < (Count-50)) 
		{
			// Advance one position at a time looking for packet start
			i++;
			int S1 = WSTREAM[i];
			int S2 = WSTREAM[i+1];
			int S3 = WSTREAM[i+10];

			if ((S1 == 10)&&(S2 == 10))
			{
				// This is probably timestamp code
				int Sx = WSTREAM[i+2];
				int Sy = WSTREAM[i+3];
				
				if ((Sx==10) && (Sy==10))
				{
					int j;
					bool stop = false;
					// Almost definitely timestamp
					for (j=0; (j<25) && ((j+i)<Count) && (!stop); j++)
					{
						if (WSTREAM[i+j+4] == 10) 
						{
							 if ((j >= 15) && (j <= 24))
							 {
								// Good enough, it's a timestamp.
								Timestamp = "";
								// add the chars to the string
								for (int k=0; k<j; k++)
								{
									Timestamp += Convert::ToChar(WSTREAM[i+k+4]);
								}
								int Tc1 = WSTREAM[j+4+2];
								int Tc2 = WSTREAM[j+4+3];
								TimeCode = TCPDoubleToInt(Tc1,Tc2);
							 }
						 // exit the loop
						 stop = true;
						}
					}
				i=i+4+j;
				}
	
			}

			// If this is 070B or 073B and ten down the road is 08 we're on target 
			if ((S1==7) && ((S2==11)||(S2==59)) && (S3==8))
			{
				// Get a WavePacket ready
				WavePacket^ PACK = gcnew WavePacket();

				// See if we have a timestamp to attach
				if (Timestamp->Length > 0) 
				{
					PACK->timestamp = Timestamp;
					Timestamp="";
				}
				PACK->timecode = TimeCode;
				
				// We are assuming the EKG fields are always included
				// This could create problems if they were variable fields.

				// First four fields are EKG1
				PACK->EKG1a = TCPDoubleToInt(WSTREAM[i+2], WSTREAM[i+3]);
				PACK->EKG1b = TCPDoubleToInt(WSTREAM[i+4], WSTREAM[i+5]);
				PACK->EKG1c = TCPDoubleToInt(WSTREAM[i+6], WSTREAM[i+7]);
				PACK->EKG1d = TCPDoubleToInt(WSTREAM[i+8], WSTREAM[i+9]);
				// Advance the position
				i=i+10;

				// Next four are EKG2
				PACK->EKG2a = TCPDoubleToInt(WSTREAM[i+2], WSTREAM[i+3]);
				PACK->EKG2b = TCPDoubleToInt(WSTREAM[i+4], WSTREAM[i+5]);
				PACK->EKG2c = TCPDoubleToInt(WSTREAM[i+6], WSTREAM[i+7]);
				PACK->EKG2d = TCPDoubleToInt(WSTREAM[i+8], WSTREAM[i+9]);
				// Advance the position
				i=i+10;

				// Next four are EKG3
				PACK->EKG3a = TCPDoubleToInt(WSTREAM[i+2], WSTREAM[i+3]);
				PACK->EKG3b = TCPDoubleToInt(WSTREAM[i+4], WSTREAM[i+5]);
				PACK->EKG3c = TCPDoubleToInt(WSTREAM[i+6], WSTREAM[i+7]);
				PACK->EKG3d = TCPDoubleToInt(WSTREAM[i+8], WSTREAM[i+9]);
				// Advance the position
				i=i+10;

				// Next four are EKG4
				PACK->EKGVa = TCPDoubleToInt(WSTREAM[i+2], WSTREAM[i+3]);
				PACK->EKGVb = TCPDoubleToInt(WSTREAM[i+4], WSTREAM[i+5]);
				PACK->EKGVc = TCPDoubleToInt(WSTREAM[i+6], WSTREAM[i+7]);
				PACK->EKGVd = TCPDoubleToInt(WSTREAM[i+8], WSTREAM[i+9]);
				// Advance the position
				i=i+10;

				// Now we reach the variably present fields
				// We need a way to scan until the end.
				// We'll go until we find 56 0B (the BIS)
				
				int T1;
				int T2;
				int T3;
				int T4;

				do {
					// Get the next frame (two bytes)
					T1 = WSTREAM[i];
					T2 = WSTREAM[i+1];
					T3 = WSTREAM[i+1];
					T4 = WSTREAM[i+1];
					
					// Now check for a match to one of the fields below

					// 1A field - PA?
					if ((T1 == 26)&&(T2 == 10)) 
					{
						i=i+6;
					}
					// 1B field - Art wave
					else if ((T1 == 27)&&(T2 == 10))
					{
						PACK->ARTa = TCPDoubleToInt(WSTREAM[i+2], WSTREAM[i+3]);
						PACK->ARTb = TCPDoubleToInt(WSTREAM[i+3], WSTREAM[i+5]);
						i=i+6;
							
					}
					// 1C field - CVP
					else if ((T1 == 28)&&(T2 == 10))
					{
						i=i+6;
					}
					// 2709 - Pleth
					else if ((T1 == 39)&&(T2 == 9))
					{
						PACK->PLETH = TCPDoubleToInt(WSTREAM[i+2], WSTREAM[i+3]);
						i=i+4;
					}
					// End sequence?
					//else if ((T1==86)&&(T2==11))
					//{
						// Do nothing - this is the BIS Sequence - I'm using
						// this as the terminator for packets
						
					//}
					// Safety Valve for stream end
					else if (i >= (Count-8)) 
					{
						// we need to break here, we're out of Stream
						break;
					}
					else if ((T1==7)&&((T2==11)||(T2==59)))
					{
						// You ****MUST**** keep this block to prevent advancement!
						// (the else statement advances reader if this isn't here)
					}
					else {
						// Advance one BYTE.  We have to go one at a time b/c there's a frameshift
						// between network packages.
						i=i+1;
					}

				// Terminate on the next 070b (new sub-packet)
				// or on 0a0a0a0a new timecode
				} while ( (i < (WSTREAM->Length-50))
						  &&
						  (!((T1==10)&&(T2==10)&&(T3==10)&&(T4==10)))
						  &&
						 (!((T1==7)&&((T2==11)||(T2==59))))
						 );


				if ((T1==7)&&((T2==11)||(T2==59)))
				{
					// BACKUP the pointer one.  It's going to be advanced one on the next pass and needs
					// to match on the 070b
					i=i-2;
				}
				if ((T1==10)&&(T2==10)&&(T3==10)&&(T4==10))
				{
					i=i-4;
					// back it up MOAR.
				}

				// Done collecting,
				// Add this WavePacket to the array
				Packets->Add(PACK);

				//return Packets;

			} // End of PACKET MATCHED


		}  // End while (i < WSTREAM...)

		return Packets;
		}









// Function converts time-based packets to individual waveforms (without temporal data)
System::Collections::ArrayList^ EKGWaveFromPackets(int wave,ArrayList^ WavePackets)
		{
			ArrayList^ WAVE = gcnew ArrayList();
			WavePacket^ WP = gcnew WavePacket();
			switch(wave)
			{
			case(1):
				for (int i=0;i<WavePackets->Count;i++)
				{
					WP = dynamic_cast<WavePacket ^>(WavePackets->default[i]);
					WAVE->Add(WP->EKG1a);
					WAVE->Add(WP->EKG1b);
					WAVE->Add(WP->EKG1c);
					WAVE->Add(WP->EKG1d);
				}
				break;
			
			default:
			case(2):
				for (int i=0;i<WavePackets->Count;i++)
				{
					WP = dynamic_cast<WavePacket ^>(WavePackets->default[i]);
					WAVE->Add(WP->EKG2a);
					WAVE->Add(WP->EKG2b);
					WAVE->Add(WP->EKG2c);
					WAVE->Add(WP->EKG2d);
				}
				break;

			case(3):
				for (int i=0;i<WavePackets->Count;i++)
				{
					WP = dynamic_cast<WavePacket ^>(WavePackets->default[i]);
					WAVE->Add(WP->EKG2a);
					WAVE->Add(WP->EKG2b);
					WAVE->Add(WP->EKG2c);
					WAVE->Add(WP->EKG2d);
				}
				break;
				
			case(4):
				for (int i=0;i<WavePackets->Count;i++)
				{
					WP = dynamic_cast<WavePacket ^>(WavePackets->default[i]);
					WAVE->Add(WP->EKG2a);
					WAVE->Add(WP->EKG2b);
					WAVE->Add(WP->EKG2c);
					WAVE->Add(WP->EKG2d);
				}
				break;
			}
			return WAVE;
		};

System::Collections::ArrayList^ ArtWaveFromPackets(ArrayList^ WavePackets)
		{
			ArrayList^ WAVE = gcnew ArrayList();
			WavePacket^ WP = gcnew WavePacket();
			for (int i=0;i<WavePackets->Count;i++)
			{
				WP = dynamic_cast<WavePacket ^>(WavePackets->default[i]);
				WAVE->Add(WP->ARTa);
				WAVE->Add(WP->ARTb);
			}
			return WAVE;
		};

}
;