import socket
import sys
import time
import json
import random

def get_constants(prefix):
    """Create a dictionary mapping socket module constants to their names."""
    return dict( (getattr(socket, n), n)
                 for n in dir(socket)
                 if n.startswith(prefix)
                 )

def recv_timeout(the_socket,timeout=15):
    #make socket non blocking
    the_socket.setblocking(0)
     
    #total data partwise in an array
    total_data=[];
    data='';
     
    #beginning time
    begin=time.time()
    while 1:
        #if you got some data, then break after timeout
        if total_data and time.time()-begin > timeout:
            break
         
        #if you got no data at all, wait a little longer, twice the timeout
        elif time.time()-begin > timeout*2:
            break
         
        #recv something
        try:
            data = the_socket.recv(8192)
            if data:
                total_data.append(data)
                #change the beginning time for measurement
                begin=time.time()
            else:
                #sleep for sometime to indicate a gap
                time.sleep(0.01)
        except:
            pass
     
    #join all parts to make final string
    return ''.join(total_data)
 
#get reply and print
# print recv_timeout(sock_receive)


families = get_constants('AF_')
types = get_constants('SOCK_')
protocols = get_constants('IPPROTO_')

# Create a TCP/IP socket
# sock_receive = socket.create_connection(('128.237.225.131', 3150))

# after connecting you must settimeout(None) to set the socket into blocking mode
# sock_receive = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# sock_receive.settimeout(100)
# sock_receive.connect(('128.237.229.166', 3145))
# sock_receive.settimeout(None)



# message_to_send = make_dummy_json()
# Sending a new line character to conform to windows code
# sock_receive.sendall(message_to_send+'\n')

# Lets sleep for 5 seconds before trying to query data
# time.sleep(20)
# print >>sys.stderr, 'Message has been sent'
sock_new = socket.create_connection(('127.0.0.1', 3145))
print >>sys.stderr, 'Family  :', families[sock_new.family]
print >>sys.stderr, 'Type    :', types[sock_new.type]
print >>sys.stderr, 'Protocol:', protocols[sock_new.proto]
print >>sys.stderr

for i in range(500):
    print recv_timeout(sock_new,0.1)
# print recv_timeout(sock_receive)
#Close the socket


# print "Socket 3150 Closed"
# time.sleep(10)
# print "Socket 3145 Openend"

# sock_new = socket.create_connection(('128.237.225.131', 3145))
# print recv_timeout(sock_new,10)
sock_new.close()
# time.sleep(5)
# sock_receive.close()


##########################################

# try:
    
#     # Send data
#     message = 'This is the message.  It will be repeated.'
#     print >>sys.stderr, 'sending "%s"' % message
#     sock.sendall(message)

#     amount_received = 0
#     amount_expected = len(message)
#     amount_expected = 10000;

#     while amount_received < amount_expected:
#     data = sock_receive.recv()
#     amount_received += len(data)
#     print >>sys.stderr, 'received :', data.decode('utf-8-sig', errors = 'replace')
#     print >>sys.stderr, 'length: ', amount_received

# finally:
#     print >>sys.stderr, 'closing socket'
#     sock_receive.close()