# 0,0       X increases -->
# +---------------------------+
# |                           | Y increases
# |                           |     |
# |   1920 x 1080 screen      |     |
# |                           |     V
# |                           |
# |                           |
# +---------------------------+ 1919, 1079

import pyautogui
import time
import pywinauto
import argparse
import socket
import SocketServer
import json

# screenWidth, screenHeight = pyautogui.size()

# print "Screen Resolution"
# print screenWidth, screenHeight

class MyTCPHandler(SocketServer.BaseRequestHandler):
    """
    The request handler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """

    def handle(self):
    	activateSimWindow()
        # self.request is the TCP socket connected to the client
        self.data = self.request.recv(1024).strip()
        number = json.loads(self.data)["scenario"]
        print "Running - "+ self.data
        goToScenario(int(number))

def getSimState():
	im = pyautogui.screenshot()
	# Simulator is ON if the pixel is green color (R,G,B) and OFF otherwise
	state = pyautogui.pixelMatchesColor(ONx, ONy, (0,128,0))
	return state

def goToScenario(scenarioNum):
	if getSimState() == 1:
		simONOFF()
	else:
		pyautogui.moveTo(ONx, ONy, duration = 2)
	pyautogui.moveRel(dropX,dropY, duration = 2)
	pyautogui.click()
	pyautogui.moveRel(scenarioX, (scenarioNum+1)*scenarioY, duration = 1)
	pyautogui.click()
	simONOFF()

def simONOFF():
	pyautogui.moveTo(ONx, ONy, duration = 1)
	pyautogui.click()

def activateSimWindow():
	simApp = pywinauto.application.Application()
	simTitle = u'CDSIM'
	simHandle = pywinauto.findwindows.find_windows(title=simTitle)[0]
	simWindow = simApp.window_(handle = simHandle)
	simWindow.SetFocus()

# The only absilute position required here
# ONx, ONy = 155, 182
parser = argparse.ArgumentParser()
parser.add_argument('-l','--loc', help = 'x,y pixel location of ON/OFF button', metavar = 'x,y')
args = parser.parse_args()
(ONx, ONy) = tuple(map(int,args.loc.split(',')))

# Here on, all positions are relative to the previous mouse position
# Need some calibration here
dropX, dropY = 460, 10
scenarioX, scenarioY = 0, 13
# print pyautogui.position()
activateSimWindow()

tcpIP = '0.0.0.0'
tcpPort = 5600

# Create the server, binding to localhost on port 9999
server = SocketServer.TCPServer((tcpIP, tcpPort), MyTCPHandler)

# Activate the server; this will keep running until you
# interrupt the program with Ctrl-C
server.serve_forever()

# Activate simulator window


# goToScenario(1)
# time.sleep(6)
# goToScenario(2)
# time.sleep(6)
# goToScenario(3)
# time.sleep(6)
# goToScenario(4)
# time.sleep(6)
# goToScenario(5)
# time.sleep(6)
# goToScenario(6)
# time.sleep(6)
# simONOFF()